package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.user.ActivationActivity;
import com.tiketextra.tiketextra.activity.user.LoginActivity;
import com.tiketextra.tiketextra.activity.user.ProfileActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.model.SettingModel;

public class ContactActivity extends BaseActivity implements View.OnClickListener {

    private TextView textViewContactPhone, textViewContactMobile, textViewLiveChat, textViewContactWA, textViewContactBBM, textViewFacebook, textViewTwitter, textViewInstagram, textViewGooglePlus;
    private SettingModel settingModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarContact);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.contact_us);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : return true;
                    case R.id.menu_activation : {startActivity(new Intent(getApplicationContext(), ActivationActivity.class));break;}
                    case R.id.menu_profile :{if(isLoggedIn()){startActivity(new Intent(getApplicationContext(), ProfileActivity.class));}else {startActivity(new Intent(getApplicationContext(), LoginActivity.class));}break;}
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_contact);

        settingModel = new SettingModel(this);

        textViewContactPhone = findViewById(R.id.textViewContactPhone);
        textViewContactPhone.setText(settingModel.getValue("contact_phone"));
        textViewContactPhone.setOnClickListener(this);

        textViewContactMobile = findViewById(R.id.textViewContactMobile);
        textViewContactMobile.setText(settingModel.getValue("contact_mobile"));
        textViewContactMobile.setOnClickListener(this);
        textViewLiveChat = findViewById(R.id.textViewLiveChat);
        textViewLiveChat.setOnClickListener(this);

        textViewContactWA = findViewById(R.id.textViewContactWA);
        if(settingModel.getValue("contact_wa").length() > 0){
            textViewContactWA.setText("Whatsapp : "+settingModel.getValue("contact_wa"));
            textViewContactWA.setOnClickListener(this);
        }
        else{
            textViewContactWA.setVisibility(View.GONE);
        }
        textViewContactBBM = findViewById(R.id.textViewContactBBM);
        if(settingModel.getValue("contact_pin_bbm").length() > 0) {
            textViewContactBBM.setText("Pin BB : " + settingModel.getValue("contact_pin_bbm"));
            textViewContactBBM.setOnClickListener(this);
        }
        else{
            textViewContactBBM.setVisibility(View.GONE);
        }
        textViewFacebook = findViewById(R.id.textViewFacebook);
        if(settingModel.getValue("facebook_url").length() > 0 && settingModel.getValue("facebook_id").length() > 0 ) {
            textViewFacebook.setOnClickListener(this);
        }
        else{
            textViewFacebook.setVisibility(View.GONE);
        }
        textViewTwitter = findViewById(R.id.textViewTwitter);
        if(settingModel.getValue("twitter_id").length() > 0) {
            textViewTwitter.setOnClickListener(this);
        }
        else{
            textViewTwitter.setVisibility(View.GONE);
        }
        textViewInstagram = findViewById(R.id.textViewInstagram);
        if(settingModel.getValue("instagram_id").length() > 0) {
            textViewInstagram.setOnClickListener(this);
        }
        else {
            textViewInstagram.setVisibility(View.GONE);
        }
        textViewGooglePlus = findViewById(R.id.textViewGooglePlus);
        if(settingModel.getValue("googleplus_id").length() > 0) {
            textViewGooglePlus.setOnClickListener(this);
        }
        else{
            textViewGooglePlus.setVisibility(View.GONE);
        }
        ((TextView)findViewById(R.id.textViewAddress)).setText(settingModel.getValue("contact_address"));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewLiveChat:
                startActivity(new Intent(getApplicationContext(), LiveChatActivity.class));
                break;
            case R.id.textViewContactPhone:
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
                phoneIntent.setData(Uri.parse("tel:"+settingModel.getValue("contact_phone_strip")));
                startActivity(phoneIntent);
                break;
            case R.id.textViewContactMobile:
                Intent mobileIntent = new Intent(Intent.ACTION_DIAL);
                mobileIntent.setData(Uri.parse("tel:"+settingModel.getValue("contact_mobile_strip")));
                startActivity(mobileIntent);
                break;
            case R.id.textViewContactWA:
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Saya ingin bertanya ....");
//                sendIntent.setType("text/plain");
//                sendIntent.setPackage("com.whatsapp");
//                startActivity(sendIntent);

                PackageManager pm=getPackageManager();
                try {
                    PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
//                    Uri mUri = Uri.parse("smsto:"+getResources().getString(R.string.contact_wa_strip));
//                    Intent mIntent = new Intent(Intent.ACTION_SENDTO, mUri);
//                    mIntent.setType("text/plain");
//                    mIntent.setPackage("com.whatsapp");
//                    startActivity(mIntent);

                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
                    sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(settingModel.getValue("contact_wa_strip"))+"@s.whatsapp.net");//phone number without "+" prefix

                    startActivity(sendIntent);

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(this, R.string.contact_wa_not_install, Toast.LENGTH_SHORT).show();
                }
                catch (Exception e){
                    Toast.makeText(this, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.textViewContactBBM:
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(settingModel.getValue("contact_pin_bbm"), settingModel.getValue("contact_pin_bbm"));
                clipboard.setPrimaryClip(clip);
                Toast.makeText(this, R.string.contact_copy, Toast.LENGTH_SHORT).show();
                break;
            case R.id.textViewFacebook:
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String facebookUrl = getFacebookPageURL(this);
                facebookIntent.setData(Uri.parse(facebookUrl));
                startActivity(facebookIntent);
                break;
            case R.id.textViewTwitter:
                Intent intent = null;
                try {
                    // get the Twitter app if possible
                    getPackageManager().getPackageInfo("com.twitter.android", 0);
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id="+settingModel.getValue("twitter_id")));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                } catch (Exception e) {
                    // no Twitter app, revert to browser
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/"+settingModel.getValue("twitter_id")));
                }
                startActivity(intent);
                break;
            case R.id.textViewInstagram:
                Uri uri = Uri.parse("https://www.instagram.com/_u/"+settingModel.getValue("instagram_id"));
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.instagram.android");

                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/"+settingModel.getValue("instagram_id"))));
                }
                break;
            case R.id.textViewGooglePlus:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/"+settingModel.getValue("googleplus_id"))));
                break;
        }
    }

    private String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + settingModel.getValue("facebook_url");
            } else { //older versions of fb app
                return "fb://page/" +  settingModel.getValue("facebook_id");
            }
        } catch (PackageManager.NameNotFoundException e) {
            return  settingModel.getValue("facebook_url"); //normal web url
        }
    }
}
