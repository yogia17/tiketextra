package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.user.ActivationActivity;
import com.tiketextra.tiketextra.activity.user.LoginActivity;
import com.tiketextra.tiketextra.activity.user.ProfileActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;

public class LiveChatActivity extends BaseActivity {
    private WebView wv;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_chat);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : return true;
                    case R.id.menu_activation : {startActivity(new Intent(getApplicationContext(), ActivationActivity.class));break;}
                    case R.id.menu_profile :{if(isLoggedIn()){startActivity(new Intent(getApplicationContext(), ProfileActivity.class));}else {startActivity(new Intent(getApplicationContext(), LoginActivity.class));}break;}
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_contact);

        wv = (WebView) findViewById(R.id.wv);

        // setup wv
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
//    wv.setWebChromeClient(new WebChromeClient());
        wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        WebSettings settings = wv.getSettings();
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAppCacheEnabled(false);
        settings.setDomStorageEnabled(true);
        wv.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                findViewById(R.id.progressBarChat).setVisibility(View.GONE);
                wv.setVisibility(View.VISIBLE);
            }
        });
        handleLoadUrl();
    }

    private void handleLoadUrl() {
//        wv.loadUrl("https://arenatiket.com/home");

        wv.loadUrl("https://tawk.to/chat/58e1cba4f97dd14875f5b5cb/default/?$_tawk_popout=true");
    }
}