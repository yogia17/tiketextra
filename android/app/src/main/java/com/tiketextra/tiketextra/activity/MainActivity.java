package com.tiketextra.tiketextra.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.hitomi.cmlibrary.OnMenuStatusChangeListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
//import com.synnapps.carouselview.ImageListener;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.flight.FlightSearchActivity;
import com.tiketextra.tiketextra.activity.hotel.HotelSearchActivity;
import com.tiketextra.tiketextra.activity.train.TrainSearchActivity;
import com.tiketextra.tiketextra.activity.user.ActivationActivity;
import com.tiketextra.tiketextra.activity.user.ProfileActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.MainMenuAdapter;
import com.tiketextra.tiketextra.adapter.SlideshowAdapter;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.util.Tools;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Slideshow;
import com.tiketextra.tiketextra.widget.MainMenu;
import com.tiketextra.tiketextra.widget.SpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
//import com.synnapps.carouselview.CarouselView;

public class MainActivity extends BaseActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

//    private View parent_view;

    private RecyclerView recyclerView;
    private MainMenuAdapter mAdapter;

    private SettingModel settingModel;
    private SlideshowAdapter adapter;
    private ViewPager pager;
    private int currentItem;
    private TextView navigator;
    private Runnable runnable = null;
    private Handler handler = new Handler();
    private CircleMenu circleMenu;

    VideoView simpleVideoView, simpleVideoView2, simpleVideoView3;
    MediaController mediaControls;
//    CarouselView carouselView;
//    int[] sampleImages = {R.raw.video, R.raw.video};

    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        checkVersionCode();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }

        Toolbar toolbar = findViewById(R.id.toolbarMain);
        setSupportActionBar(toolbar);
        if(isLoggedIn()){
            ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(getSessionName());
            toolbar.setVisibility(View.VISIBLE);
        }
        else{
            toolbar.setVisibility(View.GONE);
        }

        settingModel = new SettingModel(this);

        initComponent();

        askPhonePermission();
        getDeviceImei();

        circleMenu = (CircleMenu) findViewById(R.id.circle_menu);

        circleMenu.setMainMenu(Color.parseColor("#CDCDCD"), R.mipmap.icon_menu, R.mipmap.icon_cancel);
        circleMenu.addSubMenu(Color.parseColor("#258CFF"), R.mipmap.whatsapp)
                .addSubMenu(Color.parseColor("#30A400"), R.mipmap.telegram)
                .addSubMenu(Color.parseColor("#FF4B32"), R.mipmap.facebook)
                .addSubMenu(Color.parseColor("#8A39FF"), R.mipmap.line)
                .addSubMenu(Color.parseColor("#FF6A00"), R.mipmap.phone)
                .addSubMenu(Color.parseColor("#FF6A00"), R.mipmap.sms)
                .addSubMenu(Color.parseColor("#FF6A00"), R.mipmap.icon_gps);

        circleMenu.setOnMenuSelectedListener(new OnMenuSelectedListener() {

                                                 @Override
                                                 public void onMenuSelected(int index) {
                                                     switch (index) {
                                                         case 0:
                                                             dataWA();
//                                                             Toast.makeText(MainActivity.this, "Home Button Clicked", Toast.LENGTH_SHORT).show();
                                                             break;
                                                         case 1:
                                                             startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_TELE)));
//                                                             telegram();
//                                                             Toast.makeText(MainActivity.this, "Search button Clicked", Toast.LENGTH_SHORT).show();
                                                             break;
                                                         case 2:
                                                             startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_FB)));
//                                                             Toast.makeText(MainActivity.this, "Notify button Clciked", Toast.LENGTH_SHORT).show();
                                                             break;
                                                         case 3:
                                                             startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_LINE)));
//                                                             Toast.makeText(MainActivity.this, "Settings button Clcked", Toast.LENGTH_SHORT).show();
                                                             break;
                                                         case 4:
                                                             phoneCall();
//                                                             startActivity(new Intent(Intent.ACTION_VI, Uri.parse(Configuration.TARGET_URL_LINE)));
//                                                             Toast.makeText(MainActivity.this, "GPS button Clicked", Toast.LENGTH_SHORT).show();
                                                             break;
                                                         case 5:
                                                             sendSMS();
//                                                             Toast.makeText(MainActivity.this, "GPS button Clicked", Toast.LENGTH_SHORT).show();
                                                             break;
//                                                         case 6:
//                                                             Toast.makeText(MainActivity.this, "youtube button Clicked", Toast.LENGTH_SHORT).show();
////                                                             getYoutube();
//                                                             break;
                                                         case 6:
                                                             startActivity(new Intent(MainActivity.this, MenuOnlineActivity.class));
                                                     }
                                                 }
                                             }

        );

        circleMenu.setOnMenuStatusChangeListener(new OnMenuStatusChangeListener() {

                                                     @Override
                                                     public void onMenuOpened() {
                                                         Toast.makeText(MainActivity.this, "Menu Opend", Toast.LENGTH_SHORT).show();
                                                     }

                                                     @Override
                                                     public void onMenuClosed() {
                                                         Toast.makeText(MainActivity.this, "Menu Closed", Toast.LENGTH_SHORT).show();
                                                     }
                                                 }
        );


//        if(inActivation()){
//            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
//            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISrecyclerViewIBLE);
//        }
//        else {
//            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
//            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
//        }

//        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
//        navigation.enableShiftingMode(false);
//        navigation.setTextSize(8);
//        navigation.enableAnimation(false);
//        navigation.enableItemShiftingMode(false);
//        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()){
//                    case R.id.menu_home : return true;
//                    case R.id.menu_contact : {startActivity(new Intent(MainActivity.this, ContactActivity.class));break;}
//                    case R.id.menu_activation : {startActivity(new Intent(MainActivity.this, ActivationActivity.class));break;}
//                    case R.id.menu_profile :{if(isLoggedIn()){startActivity(new Intent(getApplicationContext(), ProfileActivity.class));}else {startActivity(new Intent(getApplicationContext(), LoginActivity.class));}break;}
//                }
//                return false;
//            }
//        });
//        navigation.setSelectedItemId(R.id.menu_home);

//        MyIntentService.startBookingCheck(this);

        // Find your VideoView in your video_main.xml layout
        simpleVideoView = findViewById(R.id.simpleVideoView);

        if (mediaControls == null) {
            // create an object of media controller class
            mediaControls = new MediaController(MainActivity.this);
            mediaControls.setAnchorView(simpleVideoView);
        }
        // set the media controller for video view
        simpleVideoView.setMediaController(mediaControls);
        // set the uri for the video view
//        simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
        simpleVideoView.setVideoPath(
                "https://tiketextra.com/filebox/newslide/video/video.mp4");

        // start a video
        simpleVideoView.start();

        // implement on completion listener on video view
        simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
//                Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
            }
        });
        simpleVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                return false;
            }
        });

//        carouselView = findViewById(R.id.carouselView);
//        carouselView.setPageCount(sampleImages.length);
//        carouselView.setImageListener(imageListener);

        //video 2
//        simpleVideoView2 = findViewById(R.id.simpleVideoView);
//
//        if (mediaControls == null) {
//            // create an object of media controller class
//            mediaControls = new MediaController(MainActivity.this);
//            mediaControls.setAnchorView(simpleVideoView2);
//        }
//        // set the media controller for video view
//        simpleVideoView2.setMediaController(mediaControls);
//        // set the uri for the video view
//        simpleVideoView2.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
//        // start a video
//        simpleVideoView2.start();
//
//        // implement on completion listener on video view
//        simpleVideoView2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
//            }
//        });
//        simpleVideoView2.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//            @Override
//            public boolean onError(MediaPlayer mp, int what, int extra) {
//                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
//                return false;
//            }
//        });
//
//        ArrayList movies = new ArrayList();
//        movies.add(simpleVideoView);
//        movies.add(simpleVideoView2);
//
//        pager = findViewById(R.id.slideshow_pager);
//        adapter = new SlideshowAdapter(getSupportFragmentManager(), movies);
//        navigator = findViewById(R.id.activity_slideshow_possition);
//
//        currentItem = Integer.parseInt(settingModel.getValue("current_slide"));
//
//        pager.setAdapter(adapter);
//        pager.setCurrentItem(currentItem);
//        setNavigator();
//
//        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//                setNavigator();
//            }
//        });
//        startAutoSlider(adapter.getCount());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://tawk.to/chat/5cbeda34d6e05b735b43d643/default")));
            }
        });

//        ArrayList<Whatsapp> wa = dataWA().size();

//        ArrayList video = new ArrayList();
//        video.add(0, R.raw.video);
//        video.add(1, R.raw.video);
//
//        pager = findViewById(R.id.slideshow_pager);
//        adapter = new SlideshowAdapter(getSupportFragmentManager(), video);
//        navigator = findViewById(R.id.activity_slideshow_possition);
//
//        currentItem = Integer.parseInt(settingModel.getValue("current_video"));
//
//        pager.setAdapter(adapter);
//        pager.setCurrentItem(currentItem);
//        setNavigator();
//
//        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//                setNavigator();
//            }
//        });
//        startAutoSlider(adapter.getCount());

    }

//    ImageListener imageListener = new ImageListener() {
//        @Override
//        public void setImageForPosition(int position, ImageView imageView) {
//            imageView.setImageResource(sampleImages[position]);
//            simpleVideoView = findViewById(R.id.simpleVideoView);
//
//            if (mediaControls == null) {
//                // create an object of media controller class
//                mediaControls = new MediaController(MainActivity.this);
//                mediaControls.setAnchorView(simpleVideoView);
//            }
//            // set the media controller for video view
//            simpleVideoView.setMediaController(mediaControls);
//            // set the uri for the video view
//            simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
//            // start a video
//            simpleVideoView.start();
//
//            // implement on completion listener on video view
//            simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
//                }
//            });
//            simpleVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//                @Override
//                public boolean onError(MediaPlayer mp, int what, int extra) {
//                    Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
//                    return false;
//                }
//            });
//        }
//    };

    private void setNavigator() {
        String navigation = "";
        for (int i = 0; i < adapter.getCount(); i++) {
            if (i == pager.getCurrentItem()) {
                navigation += " "  + getString(R.string.material_icon_circle_full) + "  ";
            } else {
                navigation += " "  + getString(R.string.material_icon_circle_empty) + "  ";
            }
        }
        navigator.setText(navigation);
        settingModel.setValue("current_slide", String.valueOf(pager.getCurrentItem()));
    }

    private void initComponent() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        int countPerRow = width <= 750 ? 1 : 1;

//        recyclerView = findViewById(R.id.recyclerView);
//        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
//        recyclerView.addItemDecoration(new SpacingItemDecoration(countPerRow, Tools.dpToPx(this, 0), true));
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setNestedScrollingEnabled(false);




        List<MainMenu> items = new ArrayList<>();
        items.add(new MainMenu(20, getResources().getDrawable(R.drawable.button_tiket), null));
        items.add(new MainMenu(21, getResources().getDrawable(R.drawable.button_hotel), null));
//        items.add(new MainMenu(2, getResources().getDrawable(R.drawable.button_train), null));
//        items.add(new MainMenu(20, getResources().getDrawable(R.drawable.button_ship), null));

        items.add(new MainMenu(22, getResources().getDrawable(R.drawable.button_paket_tour), null));
//        items.add(new MainMenu(4, getResources().getDrawable(R.drawable.button_logistics), null));
//        items.add(new MainMenu(1, getResources().getDrawable(R.drawable.ic_menu_list), getString(R.string.menu_order)));
//        items.add(new MainMenu(9, getResources().getDrawable(R.drawable.button_ppob), null));
//        items.add(new MainMenu(10, getResources().getDrawable(R.drawable.ic_menu_contact), getString(R.string.menu_contact)));
        items.add(new MainMenu(0, getResources().getDrawable(R.drawable.button_sistem_online), null));
//        items.add(new MainMenu(2, getResources().getDrawable(R.drawable.button_train), null));
//        items.add(new MainMenu(3, getResources().getDrawable(R.drawable.button_hotel_ol), null));
//  if(!isLoggedIn()){
//            items.add(new MainMenu(5, getResources().getDrawable(R.drawable.ic_menu_profile), getString(R.string.login_register)));
//        }
//        if(inActivation()){
//            items.add(new MainMenu(6, getResources().getDrawable(R.drawable.ic_menu_activation), getString(R.string.activation)));
//        }


        if(isLoggedIn()){
//            items.add(new MainMenu(7, getResources().getDrawable(R.drawable.ic_menu_profile), getSessionName().length() > 15 ? getSessionName().substring(0, 15)+" ..." : getSessionName()));
            items.add(new MainMenu(8, getResources().getDrawable(R.drawable.ic_menu_exit), getString(R.string.logout)));
        }

        //set data and list adapter
//        mAdapter = new MainMenuAdapter(this, items);
//        recyclerView.setAdapter(mAdapter);

        // on item list clicked
//        mAdapter.setOnItemClickListener(new MainMenuAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, MainMenu obj, int position) {
//                Log.e(obj.title, obj.id+"");
//                if(obj.id==21) {
//                    dataWATour();
//                }
//                if(obj.id==20) {
//                    dataWA();
//                }
//                if(obj.id==0){
////                    dataWA();
//                    startActivity(new Intent(MainActivity.this, MenuOnlineActivity.class));
//
////                    if(settingModel.getValue("flight_feature").equals("1")) {
////                        startActivity(new Intent(MainActivity.this, FlightSearchActivity.class));
////                    }
////                    else {
////                        dialogFeatureDisabled();
////                    }
//                }
////                if(obj.id==1){
////                    startActivity(new Intent(MainActivity.this, TicketBookingActivity.class));
////                }
////                if(obj.id==2){
//////                    dataWA();
////
////                    if(settingModel.getValue("train_feature").equals("1")) {
////                        startActivity(new Intent(MainActivity.this, TrainSearchActivity.class));
////                    }
////                    else {
////                        dialogFeatureDisabled();
////                    }
////                }
////                if(obj.id==3){
//////                    dataWA();
//////                    try {
//////                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_WA+"Mau%20pesan%20hotel%20kak")));
//////                    } catch (android.content.ActivityNotFoundException ex) {
//////                        Toast.makeText( MainActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
//////                    }
////                    if(settingModel.getValue("hotel_feature").equals("1")) {
////                        startActivity(new Intent(MainActivity.this, HotelSearchActivity.class));
//////                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL+"hotel")));
////                    }
////                    else {
////                        dialogFeatureDisabled();
////                    }
////                }
////                if(obj.id==4){
////                    dataWA();
////                    try {
////                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_WA+"Mau%20kirim%20paket%20kak")));
////                    } catch (android.content.ActivityNotFoundException ex) {
////                        Toast.makeText( MainActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
////                    }
////                    startActivity(new Intent(MainActivity.this, PromoActivity.class));
////                }
//                if(obj.id==22){
//                    dataWATour();
////                    try {
////                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_WA+"Mau%20pesan%20paket%20wisata%20kak")));
////                    } catch (android.content.ActivityNotFoundException ex) {
////                        Toast.makeText( MainActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
////                    }
////                    if(settingModel.getValue("tour_feature").equals("1")) {
////                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL+"tour")));
////                    }
////                    else {
////                        dialogFeatureDisabled();
////                    }
////                    if(inActivation()){
////                        startActivity(new Intent(getApplicationContext(), ActivationActivity.class));
////                    }
////                    else {
////                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
////                    }
////                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                }
//                if(obj.id==6){
//                    startActivity(new Intent(getApplicationContext(), ActivationActivity.class));
//                }
//                if(obj.id==7){
//                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
//                }
//                if(obj.id==8){
//                    logout();
//                }
//                if(obj.id==9){
//                    dataWA();
////                    try {
////                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_WA+"Mau%20bayar%20ppob%20kak")));
////                    } catch (android.content.ActivityNotFoundException ex) {
////                        Toast.makeText( MainActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
////                    }
//
////
//                }
////                if(obj.id==10){
////                    startActivity(new Intent(MainActivity.this, ContactActivity.class));
////                }
//            }
//        });

        if(settingModel.valueExists("slideshow"+(isLoggedIn()?"_auth":""))) {

            String json = settingModel.getValue("slideshow" + (isLoggedIn() ? "_auth" : ""));

            if (json != null) {
                Gson gson = new Gson();
                ArrayList<Slideshow> slideshow = gson.fromJson(json, new TypeToken<ArrayList<Slideshow>>() {
                }.getType());


                pager = findViewById(R.id.slideshow_pager);
                adapter = new SlideshowAdapter(getSupportFragmentManager(), slideshow);
                navigator = findViewById(R.id.activity_slideshow_possition);

                currentItem = Integer.parseInt(settingModel.getValue("current_slide"));

                pager.setAdapter(adapter);
                pager.setCurrentItem(currentItem);
                setNavigator();

                pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                        setNavigator();
                    }
                });
                startAutoSlider(adapter.getCount());
            } else {
                findViewById(R.id.slideshow_pager).setVisibility(View.GONE);
                findViewById(R.id.activity_slideshow_possition).setVisibility(View.GONE);
                findViewById(R.id.headerLogoImage).setVisibility(View.GONE);
//                findViewById(R.id.imageSlideEmpty).setVisibility(View.VISIBLE);
            }
        }
        else {
            findViewById(R.id.slideshow_pager).setVisibility(View.GONE);
            findViewById(R.id.activity_slideshow_possition).setVisibility(View.GONE);
            findViewById(R.id.headerLogoImage).setVisibility(View.GONE);
//            findViewById(R.id.imageSlideEmpty).setVisibility(View.VISIBLE);
        }
    }

    private void startAutoSlider(final int count) {
        runnable = new Runnable() {
            @Override
            public void run() {
                int pos = pager.getCurrentItem();
                pos = pos + 1;
                if (pos >= count) pos = 0;
                pager.setCurrentItem(pos);
                handler.postDelayed(runnable, 3000);
            }
        };
        handler.postDelayed(runnable, 3000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



//        noinspection SimplifiableIfStatement
        if (id == R.id.menu_profile) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        }
        else if(id == R.id.menu_logout) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout(){
//        UserModel userModel = new UserModel(MainActivity.this);
//        userModel.deletePassenger();
        settingModel.setValue("promo_code", null);
        SessionManager session = new SessionManager(this);
        session.logout();
    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (circleMenu.isOpened())
            circleMenu.closeMenu();
        else
            finish();
    }

//    @Override
//    public void onBackPressed() {
//        if (exit) {
//            finishAffinity();
////            finish();
////            android.os.Process.killProcess(android.os.Process.myPid());
////            System.exit(1);
//        } else {
//            Toast.makeText(this, R.string.exit_prompt,
//                    Toast.LENGTH_SHORT).show();
//            exit = true;
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    exit = false;
//                }
//            }, 3 * 1000);
//
//        }
//    }


    private final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    //    private final int PERMISSIONS_REQUEST_LOCATION = 1000;
    private TelephonyManager mTelephonyManager;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    getDeviceImei();


                } else {

//                    askPhonePermission();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }


//            case PERMISSIONS_REQUEST_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//
//
//                } else {
//
////                    askPhonePermission();
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;
//            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String deviceid = mTelephonyManager.getDeviceId();
//        Log.e("imei", "DeviceImei " + deviceid);



        if(deviceid!=null && !deviceid.substring(0, 4).equals("0000")) {
            settingModel.setValue("device_id", deviceid);
        }

//        Toast.makeText(this, deviceid+" : "+settingModel.getValue("device_id"),
//                Toast.LENGTH_SHORT).show();
    }

    private void askPhonePermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
//            else if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
//            }

            else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);

                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }



    @Override
    public void onDestroy() {
        if (runnable != null) handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    private void dialogFeatureDisabled(){
        dialog(R.string.coming_soon, R.string.feature_disabled);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }

    public void checkVersionCode(){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-type", "application/json;charset=utf-8");

        String url = "https://api.tiketextra.com/setting/getVersionCode";

        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onStart(){
                super.onStart();
                setUseSynchronousMode(false);
            }

//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONArray responseBody) {
//                Log.e("Cem Error", "Error : " + responseBody);
//                if (responseBody.length() == 0){
//                    Toast.makeText(getApplicationContext(), "Maaf, semua customer service kami sedang sibuk, silakan tunggu beberapa saat lagi!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
//                }
//            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                try{
                    System.out.println("Horee success");
                    System.out.println("Cembeliq Test: " + responseBody);

                    if (responseBody.getString("success").equalsIgnoreCase("true")) {
                        try {
                            PackageInfo pInfo = getApplication().getPackageManager().getPackageInfo(getPackageName(), 0);
                            String version = pInfo.versionName;
                            int code = pInfo.versionCode;
                            Log.e("Cembeliq", "Cembeliq : "+code);
                            if (code <  responseBody.getInt("data")){
                                Log.e("Cembeliq", "cembeliq: "+responseBody.getInt("data"));
                                Intent intent = new Intent(Intent.ACTION_VIEW ,Uri.parse("market://details?id=com.tiketextra.tiketextra"));
                                startActivity(intent);
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Terjadi gangguan jaringan", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
                    }


                } catch (JSONException e) {
                    System.out.println("Cem Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                System.out.println("Cem Error: "+res);
            }
        });
    }

    public void phoneCall(){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-type", "application/json;charset=utf-8");

        String url = "https://api.tiketextra.com/hta";

        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onStart(){
                super.onStart();
                setUseSynchronousMode(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray responseBody) {
                Log.e("Cem Error", "Error : " + responseBody);
                if (responseBody.length() == 0){
                    Toast.makeText(getApplicationContext(), "Maaf, semua customer service kami sedang sibuk, silakan tunggu beberapa saat lagi!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                try{
                    System.out.println("Horee success");
                    System.out.println("Cembeliq Test: " + responseBody);

                    try {
                        updateStatusWA(responseBody.getString("hta_phone"));
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:+"+responseBody.getString("hta_phone"))));

                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MainActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Cem Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                System.out.println("Cem Error: "+res);
            }
        });
    }

    public void sendSMS(){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-type", "application/json;charset=utf-8");

        String url = "https://api.tiketextra.com/hta";

        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onStart(){
                super.onStart();
                setUseSynchronousMode(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray responseBody) {
                Log.e("Cem Error", "Error : " + responseBody);
                if (responseBody.length() == 0){
                    Toast.makeText(getApplicationContext(), "Maaf, semua customer service kami sedang sibuk, silakan tunggu beberapa saat lagi!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                System.out.println("Horee success");
                System.out.println("Cembeliq Test: " + responseBody);

                try {
//                        updateStatusWA(responseBody.getString("hta_phone"));
//                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+responseBody.getString("hta_phone"))));
                    Intent sInt = new Intent(Intent.ACTION_VIEW);
                    sInt.putExtra("address", new String[]{"+"+responseBody.getString("hta_phone")});
                    sInt.putExtra("sms_body","Saya mau pesan tiket");
                    sInt.setType("vnd.android-dir/mms-sms");
                    Toast.makeText(MainActivity.this, "SMS Sent Successfully", Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "SMS Failed to Send, Please try again", Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                System.out.println("Cem Error: "+res);
            }
        });
    }

    public void dataWATour(){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-type", "application/json;charset=utf-8");

        String url = "https://api.tiketextra.com/htaTour";

        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onStart(){
                super.onStart();
                setUseSynchronousMode(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray responseBody) {
                Log.e("Cem Error", "Error : " + responseBody);
                if (responseBody.length() == 0){
                    Toast.makeText(getApplicationContext(), "Maaf, semua customer service kami sedang sibuk, silakan tunggu beberapa saat lagi!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                try{
                    System.out.println("Horee success");
                    System.out.println("Cembeliq Test: " + responseBody);

                    try {
                        updateStatusWA(responseBody.getString("hta_phone"));
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_WA + responseBody.getString("hta_phone") + "?text=" + responseBody.getString("hta_desc"))));

                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MainActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Cem Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                System.out.println("Cem Error: "+res);
            }
        });
    }

    public void dataWA(){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-type", "application/json;charset=utf-8");

        String url = "https://api.tiketextra.com/hta";

        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onStart(){
                super.onStart();
                setUseSynchronousMode(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray responseBody) {
                Log.e("Cem Error", "Error : " + responseBody);
                if (responseBody.length() == 0){
                    Toast.makeText(getApplicationContext(), "Maaf, semua customer service kami sedang sibuk, silakan tunggu beberapa saat lagi!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                try{
                    System.out.println("Horee success");
                    System.out.println("Cembeliq Test: " + responseBody);

                    try {
                        updateStatusWA(responseBody.getString("hta_phone"));
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL_WA + responseBody.getString("hta_phone") + "?text=" + responseBody.getString("hta_desc"))));

                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MainActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Cem Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                System.out.println("Cem Error: "+res);
            }
        });
    }

    public void updateStatusWA(final String hp){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-type", "application/json;charset=utf-8");
        RequestParams params = new RequestParams();
        params.put("hp", hp);
        params.put("status", 1);

        String url = "https://api.tiketextra.com/hta/" + hp + "/0";

        client.get(url, null, new JsonHttpResponseHandler() {
            @Override
            public void onStart(){
                super.onStart();
//                setUseSynchronousMode(false);
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                System.out.println("Cem berhasil update : " + hp);
                System.out.println("Cembeliq Test: " + responseBody);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                System.out.println("Cem Error: "+res);
            }
        });


    }
}




