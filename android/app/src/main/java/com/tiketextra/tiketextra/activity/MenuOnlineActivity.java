package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.flight.FlightSearchActivity;
import com.tiketextra.tiketextra.activity.hotel.HotelSearchActivity;
import com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity;
import com.tiketextra.tiketextra.activity.train.TrainSearchActivity;
import com.tiketextra.tiketextra.activity.user.ActivationActivity;
import com.tiketextra.tiketextra.activity.user.LoginActivity;
import com.tiketextra.tiketextra.activity.user.ProfileActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.MenuOnlineAdapter;
import com.tiketextra.tiketextra.adapter.SlideshowAdapter;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Slideshow;
import com.tiketextra.tiketextra.util.Tools;
import com.tiketextra.tiketextra.widget.MainMenu;
import com.tiketextra.tiketextra.widget.SpacingItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MenuOnlineActivity extends BaseActivity {
    private RecyclerView recyclerView;
    private MenuOnlineAdapter mAdapter;

    private SettingModel settingModel;
    private SlideshowAdapter adapter;
    private ViewPager pager;
    private int currentItem;
    private TextView navigator;
    private Runnable runnable = null;
    private Handler handler = new Handler();
    private ImageView ivMenuOnline;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_menu_online);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMenuOnline);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText("Menu Sistem Online");

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }

//        toolbar = (Toolbar) findViewById(R.id.toolbarMain);
//        setSupportActionBar(toolbar);
//        if(isLoggedIn()){
//            ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(getSessionName());
//            toolbar.setVisibility(View.VISIBLE);
//        }
//        else{
//            toolbar.setVisibility(View.GONE);
//        }

        settingModel = new SettingModel(this);

        initComponent();

        askPhonePermission();
        getDeviceImei();

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : return true;
                    case R.id.menu_contact : {startActivity(new Intent(MenuOnlineActivity.this, ContactActivity.class));break;}
                    case R.id.menu_activation : {startActivity(new Intent(MenuOnlineActivity.this, ActivationActivity.class));break;}
                    case R.id.menu_profile :{if(isLoggedIn()){startActivity(new Intent(getApplicationContext(), ProfileActivity.class));}else {startActivity(new Intent(getApplicationContext(), LoginActivity.class));}break;}
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_home);

//        MyIntentService.startBookingCheck(this);
    }


    private void setNavigator() {
        String navigation = "";
        for (int i = 0; i < adapter.getCount(); i++) {
            if (i == pager.getCurrentItem()) {
                navigation += " "  + getString(R.string.material_icon_circle_full) + "  ";
            } else {
                navigation += " "  + getString(R.string.material_icon_circle_empty) + "  ";
            }
        }
        navigator.setText(navigation);
        settingModel.setValue("current_slide", String.valueOf(pager.getCurrentItem()));
    }

    private void initComponent() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        int countPerRow = width <= 750 ? 2 : 3;

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, countPerRow));
        recyclerView.addItemDecoration(new SpacingItemDecoration(countPerRow, Tools.dpToPx(this, 8), true));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        List<MainMenu> items = new ArrayList<>();
        items.add(new MainMenu(0, getResources().getDrawable(R.drawable.ic_menu_flight), getString(R.string.menu_flight)));
        items.add(new MainMenu(2, getResources().getDrawable(R.drawable.ic_menu_train), getString(R.string.menu_train)));
        items.add(new MainMenu(3, getResources().getDrawable(R.drawable.ic_menu_hotel), getString(R.string.menu_hotel)));
        items.add(new MainMenu(5, getResources().getDrawable(R.drawable.ic_menu_tour), getString(R.string.menu_tour)));
        items.add(new MainMenu(4, getResources().getDrawable(R.drawable.ic_menu_promo), getString(R.string.promo)));
        items.add(new MainMenu(1, getResources().getDrawable(R.drawable.ic_menu_list), getString(R.string.menu_order)));
//        items.add(new MainMenu(10, getResources().getDrawable(R.drawable.ic_menu_contact), getString(R.string.menu_contact)));

//        if(!isLoggedIn()){
//            items.add(new MainMenu(5, getResources().getDrawable(R.drawable.ic_menu_profile), getString(R.string.login_register)));
//        }
        if(inActivation()){
            items.add(new MainMenu(6, getResources().getDrawable(R.drawable.ic_menu_activation), getString(R.string.activation)));
        }


        if(isLoggedIn()){
//            items.add(new MainMenu(7, getResources().getDrawable(R.drawable.ic_menu_profile), getSessionName().length() > 15 ? getSessionName().substring(0, 15)+" ..." : getSessionName()));
            items.add(new MainMenu(8, getResources().getDrawable(R.drawable.ic_menu_exit), getString(R.string.logout)));
        }

        //set data and list adapter
        mAdapter = new MenuOnlineAdapter(this, items);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new MenuOnlineAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, MainMenu obj, int position) {
                Log.e(obj.title, obj.id+"");
                if(obj.id==0){
                    if(settingModel.getValue("flight_feature").equals("1")) {
                        startActivity(new Intent(MenuOnlineActivity.this, FlightSearchActivity.class));
                    }
                    else {
                        dialogFeatureDisabled();
                    }
                }
                if(obj.id==1){
                    startActivity(new Intent(MenuOnlineActivity.this, TicketBookingActivity.class));
                }
                if(obj.id==2){
                    if(settingModel.getValue("train_feature").equals("1")) {
                        startActivity(new Intent(MenuOnlineActivity.this, TrainSearchActivity.class));
                    }
                    else {
                        dialogFeatureDisabled();
                    }
                }
                if(obj.id==3){
                    if(settingModel.getValue("hotel_feature").equals("1")) {
                        startActivity(new Intent(MenuOnlineActivity.this, HotelSearchActivity.class));
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL+"hotel")));
                    }
                    else {
                        dialogFeatureDisabled();
                    }
                }
                if(obj.id==4){
                    startActivity(new Intent(MenuOnlineActivity.this, PromoActivity.class));
                }
                if(obj.id==5){
                    if(settingModel.getValue("tour_feature").equals("1")) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.TARGET_URL+"tour")));
                    }
                    else {
                        dialogFeatureDisabled();
                    }
//                    if(inActivation()){
//                        startActivity(new Intent(getApplicationContext(), ActivationActivity.class));
//                    }
//                    else {
//                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                    }
//                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
                if(obj.id==6){
                    startActivity(new Intent(getApplicationContext(), ActivationActivity.class));
                }
                if(obj.id==7){
                    startActivity(new Intent(MenuOnlineActivity.this, ProfileActivity.class));
                }
                if(obj.id==8){
                    logout();
                }
//                if(obj.id==10){
//                    startActivity(new Intent(MainActivity.this, ContactActivity.class));
//                }
            }
        });
        getImageMenuOnline();
//        findViewById(R.id.imageSlideEmpty).setVisibility(View.VISIBLE);

//        if(settingModel.valueExists("slideshow"+(isLoggedIn()?"_auth":""))) {
//
//            String json = settingModel.getValue("slideshow" + (isLoggedIn() ? "_auth" : ""));
//
//            if (json != null) {
//                Gson gson = new Gson();
//                ArrayList<Slideshow> slideshow = gson.fromJson(json, new TypeToken<ArrayList<Slideshow>>() {
//                }.getType());
//
//
//                pager = (ViewPager) findViewById(R.id.slideshow_pager);
//                adapter = new SlideshowAdapter(getSupportFragmentManager(), slideshow);
//                navigator = (TextView) findViewById(R.id.activity_slideshow_possition);
//
//                currentItem = Integer.parseInt(settingModel.getValue("current_slide"));
//
//                pager.setAdapter(adapter);
//                pager.setCurrentItem(currentItem);
//                setNavigator();
//
//                pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                    @Override
//                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                    }
//
//                    @Override
//                    public void onPageSelected(int position) {
//
//                    }
//
//                    @Override
//                    public void onPageScrollStateChanged(int state) {
//                        setNavigator();
//                    }
//                });
//                startAutoSlider(adapter.getCount());
//            } else {
//                findViewById(R.id.slideshow_pager).setVisibility(View.GONE);
//                findViewById(R.id.activity_slideshow_possition).setVisibility(View.GONE);
//                findViewById(R.id.headerLogoImage).setVisibility(View.GONE);
//                findViewById(R.id.imageSlideEmpty).setVisibility(View.VISIBLE);
//            }
//        }
//        else {
//            findViewById(R.id.slideshow_pager).setVisibility(View.GONE);
//            findViewById(R.id.activity_slideshow_possition).setVisibility(View.GONE);
//            findViewById(R.id.headerLogoImage).setVisibility(View.GONE);
//            findViewById(R.id.imageSlideEmpty).setVisibility(View.VISIBLE);
//        }
    }
    public void getImageMenuOnline(){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-type", "application/json;charset=utf-8");

        String url = "https://api.tiketextra.com/setting/getImage";

        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onStart(){
                super.onStart();
                setUseSynchronousMode(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray responseBody) {
                Log.e("Cem Error", "Error : " + responseBody);
                if (responseBody.length() == 0){
                    Toast.makeText(getApplicationContext(), "Maaf, semua customer service kami sedang sibuk, silakan tunggu beberapa saat lagi!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                try{
                    System.out.println("Horee success");
                    System.out.println("Cembeliq Test: " + responseBody);
                    Log.e("Cembeliq", responseBody.getString("success"));
                    ivMenuOnline = findViewById(R.id.imageSlideEmpty);
                    if (responseBody.getString("success").equalsIgnoreCase("true")){
                        Log.e("Cembeliq", responseBody.getString("data"));
                        Picasso.with(getBaseContext())
                                .load(responseBody.getString("data"))
                                .fit()
                                .placeholder(R.drawable.logo)
                                .into(ivMenuOnline);
//                        findViewById(R.id.imageSlideEmpty).setBackground(Drawable.createFromPath(responseBody.getString("data")));
                    }else {
                        ivMenuOnline.setImageResource(R.drawable.logo);
                    }

                } catch (JSONException e) {
                    System.out.println("Cem Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                System.out.println("Cem Error: "+res);
            }
        });
    }
//    private void startAutoSlider(final int count) {
//        runnable = new Runnable() {
//            @Override
//            public void run() {
//                int pos = pager.getCurrentItem();
//                pos = pos + 1;
//                if (pos >= count) pos = 0;
//                pager.setCurrentItem(pos);
//                handler.postDelayed(runnable, 3000);
//            }
//        };
//        handler.postDelayed(runnable, 3000);
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.profile_menu, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



//        noinspection SimplifiableIfStatement
        if (id == R.id.menu_profile) {
            startActivity(new Intent(MenuOnlineActivity.this, ProfileActivity.class));
        }
        else if(id == R.id.menu_logout) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout(){
//        UserModel userModel = new UserModel(MainActivity.this);
//        userModel.deletePassenger();
        settingModel.setValue("promo_code", null);
        SessionManager session = new SessionManager(this);
        session.logout();
    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        //finish();
        startActivity(new Intent(this, MainActivity.class));
//        if (exit) {
//            finishAffinity();
////            finish();
////            android.os.Process.killProcess(android.os.Process.myPid());
////            System.exit(1);
//        } else {
//            Toast.makeText(this, R.string.exit_prompt,
//                    Toast.LENGTH_SHORT).show();
//            exit = true;
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    exit = false;
//                }
//            }, 3 * 1000);
//
//        }
    }


    private final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    //    private final int PERMISSIONS_REQUEST_LOCATION = 1000;
    private TelephonyManager mTelephonyManager;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    getDeviceImei();


                } else {

//                    askPhonePermission();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }


//            case PERMISSIONS_REQUEST_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//
//
//                } else {
//
////                    askPhonePermission();
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;
//            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String deviceid = mTelephonyManager.getDeviceId();
//        Log.e("imei", "DeviceImei " + deviceid);



        if(deviceid!=null && !deviceid.substring(0, 4).equals("0000")) {
            settingModel.setValue("device_id", deviceid);
        }

//        Toast.makeText(this, deviceid+" : "+settingModel.getValue("device_id"),
//                Toast.LENGTH_SHORT).show();
    }

    private void askPhonePermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
//            else if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
//            }

            else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);

                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }



    @Override
    public void onDestroy() {
        if (runnable != null) handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    private void dialogFeatureDisabled(){
        dialog(R.string.coming_soon, R.string.feature_disabled);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }
}
