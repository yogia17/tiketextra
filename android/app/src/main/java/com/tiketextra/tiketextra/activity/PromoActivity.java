package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.PromoListAdapter;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Promo;

import java.util.ArrayList;

public class PromoActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPromo);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.promo);

        SettingModel settingModel = new SettingModel(this);

        if(settingModel.valueExists("promo"+(isLoggedIn()?"_auth":""))) {

            String json = settingModel.getValue("promo"+(isLoggedIn()?"_auth":""));
            if (json != null) {
                final Gson gson = new Gson();
                final ArrayList<Promo> promos = gson.fromJson(json, new TypeToken<ArrayList<Promo>>() {
                }.getType());

                PromoListAdapter adapter = new PromoListAdapter(this, promos, this.getCurLocale());
                ListView promoList = (ListView) findViewById(R.id.promoList);
                promoList.setAdapter(adapter);
                promoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(PromoActivity.this, PromoInfoActivity.class);
                        intent.putExtra("promo", gson.toJson(promos.get(i)));
                        startActivity(intent);
                    }
                });
            } else {
                findViewById(R.id.promoList).setVisibility(View.GONE);
                findViewById(R.id.layoutPromoEmpty).setVisibility(View.VISIBLE);
            }
        }
        else{
            findViewById(R.id.promoList).setVisibility(View.GONE);
            findViewById(R.id.layoutPromoEmpty).setVisibility(View.VISIBLE);
        }
    }
}
