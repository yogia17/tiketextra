package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Promo;
import com.google.gson.Gson;

import org.sufficientlysecure.htmltextview.HtmlTextView;

public class PromoInfoActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPromoInfo);
        setSupportActionBar(toolbar);

        Gson gson = new Gson();
        final Promo promo = gson.fromJson(getIntent().getStringExtra("promo"), Promo.class);

        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(promo.getName());


        ImageUtil.displayImage((ImageView) findViewById(R.id.imageViewPromo), Configuration.SLIDESHOW_TARGET_URL+promo.getImage(), null);

        ((TextView) findViewById(R.id.textViewPromoTitle)).setText(promo.getTitle(getCurLocale()));
        ((TextView) findViewById(R.id.textViewPromoCode)).setText(promo.getName());
        ((HtmlTextView) findViewById(R.id.textViewPromoDescription)).setHtml(promo.getDescription(getCurLocale()));

        findViewById(R.id.promoButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(PromoInfoActivity.this.findViewById(android.R.id.content), getString(R.string.promo_code_copied, promo.getName()), Snackbar.LENGTH_LONG).show();

//                Toast.makeText(PromoInfoActivity.this, getString(R.string.promo_code_copied), Toast.LENGTH_SHORT).show();
                new SettingModel(PromoInfoActivity.this).setValue("promo_code", promo.getName());
            }
        });
    }
}
