package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.object.Slideshow;
import com.google.gson.Gson;

import org.sufficientlysecure.htmltextview.HtmlTextView;

public class SlideInfoActivity extends BaseActivity {
    private Toolbar toolbar;
    private ImageView image;
    private TextView title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_info);

        Gson gson = new Gson();
        Slideshow slide = gson.fromJson(getIntent().getStringExtra("slide"), Slideshow.class);

        toolbar = (Toolbar) findViewById(R.id.toolbarSlideInfo);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        title = (TextView) findViewById(R.id.textViewToolbarTitle);
        title.setText(slide.getTitle(getCurLocale()));

        image = (ImageView) findViewById(R.id.imageViewSlide);
        ImageUtil.displayImage(image, Configuration.SLIDESHOW_TARGET_URL+slide.getImage(), null);


        ((HtmlTextView) findViewById(R.id.textViewSlide)).setHtml(slide.getDescription(getCurLocale()));

    }
}
