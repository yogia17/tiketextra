package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
//import android.util.Log;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.model.SettingModel;

import org.json.JSONObject;

public class SplashScreenActivity extends BaseActivity {

    private SettingModel settingModel;
    private boolean openFirst;
    private int inc = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        settingModel = new SettingModel(this);
        settingModel.setValue("current_slide", "0");
        openFirst = settingModel.getValue("open_first").equals("0");
//        updatePrefetch = settingModel.updatePrefetch() || !openFirst;

//        if(settingModel.updatePrefetch()){
//            bindLogo();
//            prefetch();
//        }
//        else {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */
                @Override
                public void run() {
                    if (settingModel.updatePrefetch()) {
                        bindLogo();
                        prefetch();
                    }
                        finish();
                        if (openFirst) {
                            Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(SplashScreenActivity.this, WizardActivity.class);
                            startActivity(intent);
                        }
                }
            }, 3000);
//        }

    }


    private void prefetch(){
        JsonObjectRequest jsonReq = CustomRequest.get(Configuration.WEB_SERVICE_CONTROLLER + "prefetch", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("update", "perfett");
                settingModel.updatePrefetchData(response);
//                finish();
//                if (openFirst) {
//                    Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
//                    startActivity(intent);
//                } else {
////                    Intent intent = new Intent(SplashScreenActivity.this, WizardActivity.class);
////                    startActivity(intent);
//                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                    Log.e("Error: ", error.toString());
                VolleyLog.d("Error: ", error.toString());
                    Toast.makeText(getApplicationContext(), R.string.error_data_communication, Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }



    private void bindLogo() {
        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.imageViewSplashLogo);
        final AlphaAnimation animation1 = new AlphaAnimation(0.6f, 1.0f);
        animation1.setDuration(700);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.6f);
        animation2.setDuration(700);
        //animation1 AnimationListener
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                splash.startAnimation(animation2);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                splash.startAnimation(animation1);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        splash.startAnimation(animation2);
    }

}
