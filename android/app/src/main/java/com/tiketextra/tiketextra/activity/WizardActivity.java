package com.tiketextra.tiketextra.activity;
//import com.tiketextra.tiketextra.MainActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.model.SettingModel;

public class WizardActivity extends AppCompatActivity {

    private static final int MAX_STEP = 4;

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private String about_title_array[] = {
            "Pesawat",
            "Kereta Api",
            "Hotel"
    };
    private String about_description_array[] = {
            "Dapatkan kemudahan reservasi semua tiket pesawat hanya dalam satu genggaman",
            "Reservasikan tiket kereta api Anda kepada kami, mudah, singkat tanpa antri",
            "Temukan harga terbaik untuk penginapan Anda",
    };
    private int about_images_array[] = {
            R.drawable.ic_wizard_flight,
            R.drawable.ic_wizard_train,
            R.drawable.ic_wizard_hotel
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stepper_wizard_light);
        initComponent();

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initComponent() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        // adding bottom dots
        bottomProgressDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        findViewById(R.id.btnSkip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingModel(getApplicationContext()).setValue("open_first", "0");
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
        findViewById(R.id.btnStart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingModel(getApplicationContext()).setValue("open_first", "0");
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
        findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingModel(getApplicationContext()).setValue("open_first", "0");
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

    }

    private void bottomProgressDots(int current_index) {
        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[MAX_STEP];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.grey_800), PorterDuff.Mode.SRC_IN);
        }
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(final int position) {
            bottomProgressDots(position);

            if(position==0){
                findViewById(R.id.layoutButtonSkip).setVisibility(View.VISIBLE);
                findViewById(R.id.layoutButtonStart).setVisibility(View.GONE);
            }
            else if (position==MAX_STEP-1){
                findViewById(R.id.layoutButtonSkip).setVisibility(View.GONE);
                findViewById(R.id.layoutButtonStart).setVisibility(View.VISIBLE);
            }
            else{
                findViewById(R.id.layoutButtonSkip).setVisibility(View.GONE);
                findViewById(R.id.layoutButtonStart).setVisibility(View.GONE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(position > 0 && position < MAX_STEP) {
                View view = layoutInflater.inflate(R.layout.item_stepper_wizard, container, false);
                ((TextView) view.findViewById(R.id.title)).setText(about_title_array[position-1]);
                ((TextView) view.findViewById(R.id.description)).setText(about_description_array[position-1]);
                ((ImageView) view.findViewById(R.id.image)).setImageResource(about_images_array[position-1]);
                container.addView(view);
                return view;
            }
            else if(position==0){
                View view = layoutInflater.inflate(R.layout.item_stepper_start, container, false);
                container.addView(view);
                return view;
            }
            else{
                View view = layoutInflater.inflate(R.layout.item_stepper_finish, container, false);
                container.addView(view);
                return null;
            }
        }

        @Override
        public int getCount() {
            return MAX_STEP;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}