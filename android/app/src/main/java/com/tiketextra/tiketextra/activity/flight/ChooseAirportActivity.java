package com.tiketextra.tiketextra.activity.flight;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.AirportListViewAdapter;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.object.Airport;

import java.util.ArrayList;
import java.util.Locale;

public class ChooseAirportActivity extends BaseActivity {
    private EditText inputAirportName;
    private ArrayList<Airport> allAirport = new ArrayList<>();


    private AirportListViewAdapter adapter;
    private ListView listViewAirport;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_airport);
        toolbar = findViewById(R.id.toolbarChooseAirport);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().getStringExtra("airportType").equals("fromPort")){
            ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.choose_from);
        }
        else if(getIntent().getStringExtra("airportType").equals("toPort")){
            ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.choose_to);
        }

        AirportModel airportModel = new AirportModel(this);
        allAirport = airportModel.getAllAirports(airportModel.isZeroHit()?"region":"hit");
        adapter = new AirportListViewAdapter(this, allAirport);
        listViewAirport = findViewById(R.id.listViewAirport);
        listViewAirport.setAdapter(adapter);


        inputAirportName = findViewById(R.id.inputAirportName);
        inputAirportName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = inputAirportName.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });
    }


}
