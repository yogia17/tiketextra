package com.tiketextra.tiketextra.activity.flight;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Contact;
import com.tiketextra.tiketextra.object.Fare;
import com.tiketextra.tiketextra.object.FareComponent;
import com.tiketextra.tiketextra.object.FareRoute;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FlightBookingActivity extends BaseActivity {

    private String from, to, isReturn, adultCount, childCount, infantCount, goDate, backDate="", feedBooking = "empty", departBooking="empty", returnBooking="empty";
    private Map<String, Integer> percentProgress = new HashMap<>();


    private int adultDepartDiscount = 0, adultDepartSurcharge = 0, adultReturnDiscount = 0, adultReturnSurcharge = 0;
    private int childDepartDiscount = 0, childDepartSurcharge = 0, childReturnDiscount = 0, childReturnSurcharge = 0;
    private int infantDepartDiscount = 0, infantDepartSurcharge = 0, infantReturnDiscount = 0, infantReturnSurcharge = 0;

    private boolean adultDepartDiscountFromProfit = false, adultDepartSurchargeFromProfit = false, adultReturnDiscountFromProfit = false, adultReturnSurchargeFromProfit = false;
    private boolean childDepartDiscountFromProfit = false, childDepartSurchargeFromProfit = false, childReturnDiscountFromProfit = false, childReturnSurchargeFromProfit = false;
    private boolean infantDepartDiscountFromProfit = false, infantDepartSurchargeFromProfit = false, infantReturnDiscountFromProfit = false, infantReturnSurchargeFromProfit = false;


    private ProgressBar progressBar;
    private FareRoute fareDepart, fareReturn;

    private String[][] adultPassenger, childPassenger, infantPassenger;
    private Contact contact;
    private boolean errorBooking = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_booking);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setMax(100);

        Bundle parameter = getIntent().getBundleExtra("parameter");
        Gson gson = new Gson();
        fareDepart = gson.fromJson(parameter.getString("depart_choice"), FareRoute.class);

        from = fareDepart.getDepartPort();
        to = fareDepart.getArrivePort();
        isReturn = parameter.getString("type").equals("oneway")?"0":"1";
        adultCount = Integer.toString(parameter.getInt("adult"));
        childCount = Integer.toString(parameter.getInt("child"));
        infantCount = Integer.toString(parameter.getInt("infant"));
        goDate = fareDepart.getDepartDate();
        percentProgress.put(fareDepart.getAirlineCode()+"booking", 0);

        if(!parameter.getString("type").equals("oneway")){
            fareReturn = gson.fromJson(parameter.getString("return_choice"), FareRoute.class);
            backDate = fareReturn.getDepartDate();
            if(!fareDepart.getAirlineCode().equals(fareReturn.getAirlineCode())){
                percentProgress.put(fareReturn.getAirlineCode()+"booking", 0);
            }
        }

        adultPassenger = ItineraryHelper.passengerToArray((Map<Integer, Passenger>) gson.fromJson(getIntent().getStringExtra("adult_passenger"), new TypeToken<Map<Integer, Passenger>>() {}.getType()));
        childPassenger = ItineraryHelper.passengerToArray((Map<Integer, Passenger>) gson.fromJson(getIntent().getStringExtra("child_passenger"), new TypeToken<Map<Integer, Passenger>>() {}.getType()));
        infantPassenger = ItineraryHelper.passengerToArray((Map<Integer, Passenger>) gson.fromJson(getIntent().getStringExtra("infant_passenger"), new TypeToken<Map<Integer, Passenger>>() {}.getType()));
        contact = gson.fromJson(getIntent().getStringExtra("contact"), Contact.class);

        ((TextView) findViewById(R.id.percentProgress)).setText(getString(R.string.flight_booking_now,"..."));

        bindLogo();
        composeDiscountAndSurcharge("depart", fareDepart.getFare());
        if(isReturn.equals("1")){
            composeDiscountAndSurcharge("return", fareReturn.getFare());
        }
        booking();
    }

    private void bindLogo(){
        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.imageViewSplashLogo);
        final AlphaAnimation animation1 = new AlphaAnimation(0.6f, 1.0f);
        animation1.setDuration(700);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.6f);
        animation2.setDuration(700);
        //animation1 AnimationListener
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                splash.startAnimation(animation2);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                splash.startAnimation(animation1);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        splash.startAnimation(animation2);
    }

    private void composeDiscountAndSurcharge(String direction, Map<String, FareComponent> fare) {

        if(direction.equals("depart")){
            if(fare.containsKey("adult")){
                adultDepartDiscount = fare.get("adult").getDiscount();
                adultDepartSurcharge = fare.get("adult").getSurcharge();
                adultDepartDiscountFromProfit = fare.get("adult").isDiscountRuleFromProfit();
                adultDepartSurchargeFromProfit = fare.get("adult").isSurchargeRuleFromProfit();
            }
            if(fare.containsKey("child")){
                childDepartDiscount = fare.get("child").getDiscount();
                childDepartSurcharge = fare.get("child").getSurcharge();
                childDepartDiscountFromProfit = fare.get("child").isDiscountRuleFromProfit();
                childDepartSurchargeFromProfit = fare.get("child").isSurchargeRuleFromProfit();
            }
            if(fare.containsKey("infant")){
                infantDepartDiscount = fare.get("infant").getDiscount();
                infantDepartSurcharge = fare.get("infant").getSurcharge();
                infantDepartDiscountFromProfit = fare.get("infant").isDiscountRuleFromProfit();
                infantDepartSurchargeFromProfit = fare.get("infant").isSurchargeRuleFromProfit();
            }
        }
        else{
            if(fare.containsKey("adult")){
                adultReturnDiscount = fare.get("adult").getDiscount();
                adultReturnSurcharge = fare.get("adult").getSurcharge();
                adultReturnDiscountFromProfit = fare.get("adult").isDiscountRuleFromProfit();
                adultReturnSurchargeFromProfit = fare.get("adult").isSurchargeRuleFromProfit();
            }
            if(fare.containsKey("child")){
                childReturnDiscount = fare.get("child").getDiscount();
                childReturnSurcharge = fare.get("child").getSurcharge();
                childReturnDiscountFromProfit = fare.get("child").isDiscountRuleFromProfit();
                childReturnSurchargeFromProfit = fare.get("child").isSurchargeRuleFromProfit();
            }
            if(fare.containsKey("infant")){
                infantReturnDiscount = fare.get("infant").getDiscount();
                infantReturnSurcharge = fare.get("infant").getSurcharge();
                infantReturnDiscountFromProfit = fare.get("infant").isDiscountRuleFromProfit();
                infantReturnSurchargeFromProfit = fare.get("infant").isSurchargeRuleFromProfit();
            }
        }

    }

    @Override
    public void onBackPressed() {
        if(errorBooking){
            startActivity(new Intent(FlightBookingActivity.this, FlightSearchActivity.class));
        }

    }

    private void booking(){
        final Gson gson = new Gson();
        final Map<String, String> params = new HashMap<>();
        params.put("flight_from", from);
        params.put("flight_to", to);
        params.put("flight_trip", isReturn.equals("0")?"oneway":"roundtrip");
        params.put("flight_depart", goDate);
        params.put("flight_return", backDate);

        params.put("flight_adult", adultCount);
        params.put("flight_child", childCount);
        params.put("flight_infant", infantCount);

        params.put("adult", gson.toJson(adultPassenger));
        params.put("child", gson.toJson(childPassenger));
        params.put("infant", gson.toJson(infantPassenger));

        params.put("depart_carrier", fareDepart.getFlight().get(0).getAirlineCode());
        params.put("depart_class", fareDepart.getFareClass());
        params.put("depart_choice", fareDepart.getFareID());
        params.put("depart_flight", gson.toJson(fareDepart.getFlight()));

        params.put("depart_adult_discount", Integer.toString(adultDepartDiscount));
        params.put("depart_child_discount", Integer.toString(childDepartDiscount));
        params.put("depart_infant_discount", Integer.toString(infantDepartDiscount));

        params.put("depart_adult_surcharge", Integer.toString(adultDepartSurcharge));
        params.put("depart_child_surcharge", Integer.toString(childDepartSurcharge));
        params.put("depart_infant_surcharge", Integer.toString(infantDepartSurcharge));

        params.put("depart_adult_discount_from_profit", adultDepartDiscountFromProfit ? "1" : "0");
        params.put("depart_child_discount_from_profit", childDepartDiscountFromProfit ? "1" : "0");
        params.put("depart_infant_discount_from_profit", infantDepartDiscountFromProfit ? "1" : "0");

        params.put("depart_adult_surcharge_from_profit", adultDepartSurchargeFromProfit ? "1" : "0");
        params.put("depart_child_surcharge_from_profit", childDepartSurchargeFromProfit ? "1" : "0");
        params.put("depart_infant_surcharge_from_profit", infantDepartSurchargeFromProfit ? "1" : "0");

        params.put("depart_hidden_transit", Integer.toString(fareDepart.getHiddenTransitCount()));

        if(isReturn.equals("1")){
            params.put("return_carrier", fareReturn.getFlight().get(0).getAirlineCode());
            params.put("return_class", fareReturn.getFareClass());
            params.put("return_choice", fareReturn.getFareID());
            params.put("return_flight", gson.toJson(fareReturn.getFlight()));

            params.put("return_adult_discount", Integer.toString(adultReturnDiscount));
            params.put("return_child_discount", Integer.toString(childReturnDiscount));
            params.put("return_infant_discount", Integer.toString(infantReturnDiscount));

            params.put("return_adult_surcharge", Integer.toString(adultReturnSurcharge));
            params.put("return_child_surcharge", Integer.toString(childReturnSurcharge));
            params.put("return_infant_surcharge", Integer.toString(infantReturnSurcharge));

            params.put("return_adult_discount_from_profit", adultReturnDiscountFromProfit ? "1" : "0");
            params.put("return_child_discount_from_profit", childReturnDiscountFromProfit ? "1" : "0");
            params.put("return_infant_discount_from_profit", infantReturnDiscountFromProfit ? "1" : "0");

            params.put("return_adult_surcharge_from_profit", adultReturnSurchargeFromProfit ? "1" : "0");
            params.put("return_child_surcharge_from_profit", childReturnSurchargeFromProfit ? "1" : "0");
            params.put("return_infant_surcharge_from_profit", infantReturnSurchargeFromProfit ? "1" : "0");

            params.put("return_hidden_transit", Integer.toString(fareReturn.getHiddenTransitCount()));
        }

        params.put("contact_phone", contact.getPhone());
        params.put("contact_email", contact.getEmail());
        params.put("contact_name", contact.getFullName());
        params.put("contact_title", contact.getTitle());
        String json = gson.toJson(adultPassenger);
        System.out.println("cembeliq : " +json);

        params.put("auth_mode", isLoggedIn()?"_auth":"");
        params.put("feed", feedBooking);

        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "booking", params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
//                Log.e("JSON", response.toString());
                try {
                    String status = response.getString("status");
                    if(status.equals("1")) {
                        JSONArray progress = response.getJSONArray("progress");
                        feedBooking = "";
                        for(int i=0; i<progress.length(); i++) {
                            JSONObject tprogress = (JSONObject) progress.get(i);
                            percentProgress.put(tprogress.getString("airline_code")+"booking", tprogress.getInt("rate"));
                            if (tprogress.getString("state").equals(Configuration.API_STATE_RUN)) {
                                feedBooking += tprogress.toString() + ",";
                            } else if (tprogress.getString("state").equals(Configuration.API_STATE_FINISH)) {
                                if (tprogress.getString("airline_code").equals(fareDepart.getFlight().get(0).getAirlineCode())) {
                                    departBooking = tprogress.getString("data");
                                } else {
                                    returnBooking = tprogress.getString("data");
                                }
//                                counterBooking++;
                            }
                            else if(tprogress.getString("state").equals(Configuration.API_STATE_ERROR)){
                                dialogBookingError(tprogress.getString("message"));
                                errorBooking = true;

                            }
                        }
                        setProgress();
                        if(feedBooking.length() > 10){
                            feedBooking = "{\"progress\":[" + feedBooking.substring(0, feedBooking.length()-1) + "],\"id\":\"" + response.getString("id") + "\",\"status\":" + status + "}";
                            booking();
                        }
                        else{
                            if(errorBooking==false) {
                                params.put("depart_booking", departBooking);
                                params.put("return_booking", returnBooking);
                                JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "booked", params, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
//                                    //Log.e("responsnseee", response.toString());

                                        try {
                                            ReservationModel reservationModel = new ReservationModel(FlightBookingActivity.this);
                                            Reservation reservation = gson.fromJson(response.getJSONObject("reservation").toString(), Reservation.class);
                                            reservationModel.insertReservationObject(reservation);

                                            JSONArray passengers = response.getJSONArray("passenger");
                                            for (int i = 0; i < passengers.length(); i++) {
                                                Passenger passenger = gson.fromJson(passengers.get(i).toString(), Passenger.class);
                                                reservationModel.insertPassengerObject(passenger);
                                            }
                                            JSONArray fares = response.getJSONArray("fare");
                                            for (int i = 0; i < fares.length(); i++) {
                                                Fare fare = gson.fromJson(fares.get(i).toString(), Fare.class);
                                                reservationModel.insertFareObject(fare);
                                            }

                                            ArrayList<Bank> banks = new ArrayList<>();
                                            JSONArray bank = response.getJSONArray("bank");
                                            for (int i = 0; i < bank.length(); i++) {
                                                banks.add(gson.fromJson(bank.get(i).toString(), Bank.class));
                                            }

                                            Intent intent = new Intent(FlightBookingActivity.this, FlightPaymentActivity.class);
                                            intent.putExtra("reservation_id", reservation.getReservationID());
                                            intent.putExtra("bank", gson.toJson(banks));
                                            startActivity(intent);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            dialogInternalError();
                                        }

                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError arg0) {
                                        VolleyLog.d("Error: ", arg0.getMessage());
                                        dialogInternalError();
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(jsonReq);
                            }
                        }
                    }
                    else{
                        dialogBookingError(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogErrorConnection();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                dialogErrorConnection();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);

    }




    private void setProgress() {
        int count;
        int total = count = 0;
        for (Map.Entry<String, Integer> entry : this.percentProgress.entrySet()) {
            total += entry.getValue();
            count++;
        }
        int proc = (total / count);
        progressBar.setProgress(proc);
        ((TextView) findViewById(R.id.percentProgress)).setText(getString(R.string.flight_booking_now,"("+proc+"%)"));
//        if (proc == 50 && counter == 0) {
//            dialogTicketNotFound();
//        }
    }

    private void dialogTicketNotFound(){
        dialog(R.string.error_occurred, R.string.error_ticket_not_found);
    }

    private void dialogInternalError(){
        dialog(R.string.error_occurred, R.string.error_internal);
    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialog(int title, int message){
        Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FlightBookingActivity.this, FlightSearchActivity.class));
            }
        });

        mDialog.show();

    }

    private void dialogBookingError(String message){
        Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(R.string.error_occurred);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FlightBookingActivity.this, FlightSearchActivity.class));
            }
        });

        mDialog.show();
    }

    private void dialogBookingErrorByCode(int errorCode){
        Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(R.string.booking_failure);

        String[] arrError = getResources().getStringArray(R.array.error_code);

        mDialogText.setText(arrError[errorCode]);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FlightBookingActivity.this, FlightSearchActivity.class));
            }
        });

        mDialog.show();
    }
}
