package com.tiketextra.tiketextra.activity.flight;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.custom_view.ItemPassengerLayout;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.model.AirlineModel;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Airport;
import com.tiketextra.tiketextra.object.Flight;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

public class FlightBookingResultActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_summary);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarResultSummary);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.my_booking);

        ReservationModel reservationModel = new ReservationModel(this);
        final Reservation reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));


        findViewById(R.id.contactLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.passengerLayout).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.contactNameTextView)).setText(ItineraryHelper.decodeAdultTitle(reservation.getContactTitle(), getApplicationContext()) + " " + reservation.getContactName());
        ((TextView) findViewById(R.id.contactEmailTextView)).setText(reservation.getContactEmail());
        ((TextView) findViewById(R.id.contactPhoneTextView)).setText(reservation.getContactPhone());

        LinearLayout passengerLayout = (LinearLayout) findViewById(R.id.passengerLayout);
        int number = 0;
        for (Passenger entry : reservation.getPassengers()) {
            ItemPassengerLayout item = new ItemPassengerLayout(this);
            item.setText(++number+".", (entry.getType().equals("adult")? ItineraryHelper.decodeAdultTitle(entry.getTitle(), getApplicationContext()) : (entry.getType().equals("child") ? ItineraryHelper.decodeChildTitle(entry.getTitle(), getApplicationContext()) : ItineraryHelper.decodeInfantTitle(entry.getTitle(), getApplicationContext()) )) + " " + entry.getName(), entry.getBirthDate() != null ? Utils.mediumDate(entry.getBirthDate(), this) : getString(getResources().getIdentifier(entry.getType(), "string", getPackageName())));
            passengerLayout.addView(item);
        }

        String passenger = reservation.getAdultCount()+" "+getResources().getString(R.string.adult).toLowerCase() + (reservation.getChildCount() > 0 ? ", "+reservation.getChildCount()+" "+getResources().getString(R.string.child).toLowerCase():"") + (reservation.getInfantCount() > 0 ? ", "+reservation.getInfantCount()+" "+getResources().getString(R.string.infant).toLowerCase():"");
        ((TextView) findViewById(R.id.textViewPassengerCount)).setText(passenger);

        AirportModel airportModel = new AirportModel(this);

        ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(reservation.getDepartDate(), this));
        ((TextView) findViewById(R.id.departHintTextView)).setText(airportModel.getAirport(reservation.getFromPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ airportModel.getAirport(reservation.getToPort()).getCity());


        AirlineModel airlineModel = new AirlineModel(this);

        for (int i = 0; i < reservation.getDepartFlightArray().length || i <= 5; i++) {
            if (i == 0) {
                Flight fl = reservation.getDepartFlightArray()[i];
                ((ImageView) findViewById(R.id.depart_imageViewAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                ((TextView) findViewById(R.id.depart_textViewAirline)).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                ((TextView) findViewById(R.id.depart_textViewFlightNumber)).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                ((TextView) findViewById(R.id.depart_departTime)).setText(fl.getDepartTime());
                ((TextView) findViewById(R.id.depart_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(R.id.depart_arriveTime)).setText(fl.getArriveTime());
                ((TextView) findViewById(R.id.depart_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Airport airport = airportModel.getAirport(fl.getDepartPort());
                ((TextView) findViewById(R.id.depart_departCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_departAirport)).setText(airport.getAirportName());
                airport = airportModel.getAirport(fl.getArrivePort());
                ((TextView) findViewById(R.id.depart_arriveCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_arriveAirport)).setText(airport.getAirportName());

            } else if (i > 0 && i <= 5 && i < reservation.getDepartFlightArray().length) {
                Flight fl = reservation.getDepartFlightArray()[i];

                ((ImageView) findViewById(getResources().getIdentifier("depart" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewAirline", "id", getPackageName()))).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Airport airport = airportModel.getAirport(fl.getDepartPort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departAirport", "id", getPackageName()))).setText(airport.getAirportName());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getDepartFlightArray()[i-1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getDepartFlightArray()[i-1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + airport.getCity());

                airport = airportModel.getAirport(fl.getArrivePort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveAirport", "id", getPackageName()))).setText(airport.getAirportName());


                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
            } else if (i > 0 && i <= 5 && i >= reservation.getDepartFlightArray().length) {
                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
            }
        }
        if(reservation.getDepartHiddenTransit() > 0){
            findViewById(R.id.departHiddenTransitLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.departHiddenTransitTextView)).setText(getString(R.string.transit_not_mentioned, ""+reservation.getDepartHiddenTransit()));
        }

        ((TextView) findViewById(R.id.departSummaryTextView)).setText(reservation.getFromPort() + " - " + reservation.getToPort() + (reservation.isReturn() && reservation.getCarrierID1().equals(reservation.getCarrierID2())?" - "+reservation.getFromPort():"") + " x " + (reservation.getAdultCount() + reservation.getChildCount() + reservation.getInfantCount()) + " " + getString(R.string.person).toLowerCase());
        ((TextView) findViewById(R.id.departFareTextView)).setText(Utils.numberFormatCurrency(reservation.getTotalDepartPrice() - reservation.getTotalDepartDiscount()));

        int total = reservation.getTotalDepartPrice();
        int discount = reservation.getTotalDepartDiscount();

        //------------------------------------------------------------------------
        if(!reservation.isReturn()){
            findViewById(R.id.returnDetailInfoLayout).setVisibility(View.GONE);
            findViewById(R.id.returnFareLayout).setVisibility(View.GONE);
        }
        else {
            ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns) + " - " + Utils.mediumDate(reservation.getReturnDate(), this));
            ((TextView) findViewById(R.id.returnHintTextView)).setText(airportModel.getAirport(reservation.getToPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + airportModel.getAirport(reservation.getFromPort()).getCity());

            for (int i = 0; i < reservation.getReturnFlightArray().length || i <= 5; i++) {
                if (i == 0) {
                    Flight fl = reservation.getReturnFlightArray()[i];
                    ((ImageView) findViewById(R.id.return_imageViewAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                    ((TextView) findViewById(R.id.return_textViewAirline)).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                    ((TextView) findViewById(R.id.return_textViewFlightNumber)).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                    ((TextView) findViewById(R.id.return_departTime)).setText(fl.getDepartTime());
                    ((TextView) findViewById(R.id.return_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(R.id.return_arriveTime)).setText(fl.getArriveTime());
                    ((TextView) findViewById(R.id.return_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Airport airport = airportModel.getAirport(fl.getDepartPort());
                    ((TextView) findViewById(R.id.return_departCity)).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(R.id.return_departAirport)).setText(airport.getAirportName());
                    airport = airportModel.getAirport(fl.getArrivePort());
                    ((TextView) findViewById(R.id.return_arriveCity)).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(R.id.return_arriveAirport)).setText(airport.getAirportName());

                } else if (i > 0 && i <= 5 && i < reservation.getReturnFlightArray().length) {
                    Flight fl = reservation.getReturnFlightArray()[i];

                    ((ImageView) findViewById(getResources().getIdentifier("return" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewAirline", "id", getPackageName()))).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Airport airport = airportModel.getAirport(fl.getDepartPort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departCity", "id", getPackageName()))).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departAirport", "id", getPackageName()))).setText(airport.getAirportName());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getReturnFlightArray()[i - 1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getReturnFlightArray()[i - 1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) + " " + airport.getCity());

                    airport = airportModel.getAirport(fl.getArrivePort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveCity", "id", getPackageName()))).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveAirport", "id", getPackageName()))).setText(airport.getAirportName());


                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
                } else if (i > 0 && i <= 5 && i >= reservation.getReturnFlightArray().length) {
                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
                }
            }
            if(reservation.getReturnHiddenTransit() > 0){
                findViewById(R.id.returnHiddenTransitLayout).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.returnHiddenTransitTextView)).setText(getString(R.string.transit_not_mentioned, ""+reservation.getReturnHiddenTransit()));
            }

            if (!reservation.getCarrierID1().equals(reservation.getCarrierID2())) {
                ((TextView) findViewById(R.id.returnSummaryTextView)).setText(reservation.getToPort() + " - " + reservation.getFromPort() + " x " + (reservation.getAdultCount() + reservation.getChildCount() + reservation.getInfantCount()) + " " + getString(R.string.person).toLowerCase());
                ((TextView) findViewById(R.id.returnFareTextView)).setText(Utils.numberFormatCurrency(reservation.getTotalReturnPrice() - reservation.getTotalReturnDiscount()));

                total += reservation.getTotalReturnPrice();
                discount += reservation.getTotalReturnDiscount();
            }
            else{
                findViewById(R.id.returnFareLayout).setVisibility(View.GONE);
            }
        }

        ((TextView) findViewById(R.id.fareHintTextView)).setText(!reservation.isReturn()?R.string.oneway:R.string.roundtrip);

        if(discount < 0){
            findViewById(R.id.discountFareLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.discountFareTextView)).setText(Utils.numberFormatCurrency(discount));
        }


        if(reservation.getPromoAmount() != 0){
            findViewById(R.id.promoFareLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.promoSummaryTextView)).setText(getString(R.string.promo)+" : "+reservation.getPromoCode());
            ((TextView) findViewById(R.id.promoFareTextView)).setText(Utils.numberFormatCurrency(reservation.getPromoAmount()));
//            if(!reservation.getIsConfirmed().equals("Y")) {
                total += reservation.getPromoAmount();
//            }
        }
        int totalFare = total;
        if(reservation.getAmount() > 0){
            total = reservation.getAmount();
        }

        if(totalFare-total != 0){
            findViewById(R.id.paymentChannelFareLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.paymentChannelSummaryTextView)).setText(getString(total - totalFare < 0 ? R.string.bank_discount : R.string.transaction_fee));
            ((TextView) findViewById(R.id.paymentChannelFareTextView)).setText(Utils.numberFormatCurrency(total - totalFare));
        }

        ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / 1000)));
        ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - 3));


        if(reservation.getBank()==null){
            ((TextView) findViewById(R.id.buttonBookingText)).setText(R.string.finish_payment);
        }
        else{
            ((TextView) findViewById(R.id.buttonBookingText)).setText(R.string.back);
        }
        findViewById(R.id.buttonBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reservation.getBank()==null){
                    finish();
                }
                else{
                    startActivity(new Intent(FlightBookingResultActivity.this, TicketBookingActivity.class));
                }
            }
        });

    }
}
