package com.tiketextra.tiketextra.activity.flight;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.custom_view.ItemPassengerForm;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.model.ContactModel;
import com.tiketextra.tiketextra.object.Contact;
import com.tiketextra.tiketextra.object.FareRoute;
import com.tiketextra.tiketextra.object.Passenger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class FlightPassengerActivity extends BaseActivity {

    private Map<Integer, Passenger> adultPassenger, childPassenger, infantPassenger;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_passenger);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPassenger);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.passenger_form);

        adultPassenger = new HashMap<>();
        childPassenger = new HashMap<>();
        infantPassenger = new HashMap<>();

        FareRoute departChoice, returnChoice;
        int[][] passengerData = new int[3][];

        Gson gson = new Gson();
        departChoice = gson.fromJson(getIntent().getBundleExtra("parameter").getString("depart_choice"), FareRoute.class);
        if (getIntent().getBundleExtra("parameter").getString("type").equals("roundtrip")) {
            returnChoice = gson.fromJson(getIntent().getBundleExtra("parameter").getString("return_choice"), FareRoute.class);
            if (getIntent().getBundleExtra("parameter").getBoolean("is_international")) {
                passengerData[0] = ItineraryHelper.getInternationalRoundtripPassengerData(departChoice.getAirlineCode(), returnChoice.getAirlineCode(), "adult");
                passengerData[1] = ItineraryHelper.getInternationalRoundtripPassengerData(departChoice.getAirlineCode(), returnChoice.getAirlineCode(), "child");
                passengerData[2] = ItineraryHelper.getInternationalRoundtripPassengerData(departChoice.getAirlineCode(), returnChoice.getAirlineCode(), "infant");
            } else {
                passengerData[0] = ItineraryHelper.getDomesticRoundtripPassengerData(departChoice.getAirlineCode(), returnChoice.getAirlineCode(), "adult");
                passengerData[1] = ItineraryHelper.getDomesticRoundtripPassengerData(departChoice.getAirlineCode(), returnChoice.getAirlineCode(), "child");
                passengerData[2] = ItineraryHelper.getDomesticRoundtripPassengerData(departChoice.getAirlineCode(), returnChoice.getAirlineCode(), "infant");
            }
        } else {
            if (getIntent().getBundleExtra("parameter").getBoolean("is_international")) {
                passengerData[0] = ItineraryHelper.getInternationalOnewayPassengerData(departChoice.getAirlineCode(), "adult");
                passengerData[1] = ItineraryHelper.getInternationalOnewayPassengerData(departChoice.getAirlineCode(), "child");
                passengerData[2] = ItineraryHelper.getInternationalOnewayPassengerData(departChoice.getAirlineCode(), "infant");
            } else {
                passengerData[0] = ItineraryHelper.getDomesticOnewayPassengerData(departChoice.getAirlineCode(), "adult");
                passengerData[1] = ItineraryHelper.getDomesticOnewayPassengerData(departChoice.getAirlineCode(), "child");
                passengerData[2] = ItineraryHelper.getDomesticOnewayPassengerData(departChoice.getAirlineCode(), "infant");
            }
        }


        LinearLayout layoutPassengerForm = (LinearLayout) findViewById(R.id.layoutPassengerForm);
        LinearLayout.LayoutParams layoutParamsMarginBottom = new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        layoutParamsMarginBottom.setMargins(0, 0, 0, getResources().getDimensionPixelOffset(R.dimen.activity_vertical_margin));


        final int adult = getIntent().getBundleExtra("parameter").getInt("adult");
        final int child = getIntent().getBundleExtra("parameter").getInt("child");
        final int infant = getIntent().getBundleExtra("parameter").getInt("infant");

        final ItemPassengerForm[][] passengerForms = new ItemPassengerForm[3][];

        if (adult > 0) {
            passengerForms[0] = new ItemPassengerForm[adult];
            for (int i = 0; i < adult; i++) {
                ItemPassengerForm itemPassengerForm = new ItemPassengerForm(this, passengerData[0], "adult", i+1, adult, getIntent().getBundleExtra("parameter"), new Passenger(i, "adult"));
                layoutPassengerForm.addView(itemPassengerForm, layoutParamsMarginBottom);
                passengerForms[0][i] = itemPassengerForm;
            }
        }
        if (child > 0) {
            passengerForms[1] = new ItemPassengerForm[child];
            for (int i = 0; i < child; i++) {
                ItemPassengerForm itemPassengerForm = new ItemPassengerForm(this, passengerData[1], "child", i+1, adult, getIntent().getBundleExtra("parameter"), new Passenger(i, "child"));
                layoutPassengerForm.addView(itemPassengerForm, layoutParamsMarginBottom);
                passengerForms[1][i] = itemPassengerForm;
            }
        }
        if (infant > 0) {
            passengerForms[2] = new ItemPassengerForm[infant];
            for (int i = 0; i < infant; i++) {
                ItemPassengerForm itemPassengerForm = new ItemPassengerForm(this, passengerData[2], "infant", i+1, adult, getIntent().getBundleExtra("parameter"), new Passenger(i, "infant"));
                layoutPassengerForm.addView(itemPassengerForm, layoutParamsMarginBottom);
                passengerForms[2][i] = itemPassengerForm;
            }
        }

        ArrayList<String> contactNames = new ArrayList<>();
        final ContactModel contactModel = new ContactModel(this);
        final ArrayList<Contact> contacts = contactModel.getAllContacts();
        for (Contact contact: contacts) {
            contactNames.add(contact.getFullName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.my_autocomplete_item, contactNames);

        ArrayAdapter<String> spinnerTitle = new ArrayAdapter<>(this, R.layout.my_spinner_item, ItineraryHelper.getAdultTitle(getApplicationContext()));
        final Spinner contactTitle = findViewById(R.id.spinnerContactTitle);
        contactTitle.setAdapter(spinnerTitle);
        final AutoCompleteTextView nameEditText = (AutoCompleteTextView)findViewById(R.id.nameContactEditText);
        final EditText mobile = (EditText) findViewById(R.id.phoneEditText);
        final EditText email = (EditText) findViewById(R.id.emailEditText);
        nameEditText.setAdapter(adapter);
        nameEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Contact cont = contactModel.getContactByName(Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")));
                contactTitle.setSelection(ItineraryHelper.gatAdultTitleIdx(cont.getTitle(), getApplicationContext()));
                mobile.setText(cont.getPhone());
                email.setText(cont.getEmail());
            }
        });

        if(isLoggedIn()){
            findViewById(R.id.contactFormLayout).setVisibility(View.GONE);
            contactTitle.setSelection(ItineraryHelper.gatAdultTitleIdx(getSessionTitle(), getApplicationContext()));
            nameEditText.setText(getSessionName());
            mobile.setText(getSessionMobile());
            email.setText(getSessionEmail());
        }

        findViewById(R.id.buttonSubmitPassenger).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean[] adultCheck = new boolean[adult];
                boolean valid = true, adultAssocPass = true;

                if(email.getText().toString().isEmpty()){
                    email.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = false;
                    email.requestFocus();
                }
                else if (!email.getText().toString().matches("^([a-z0-9\\+_\\-]+)(\\.[a-z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$")) {
                    email.setError(getResources().getString(R.string.passenger_email_validation));
                    valid = false;
                    email.requestFocus();
                }

                if(mobile.getText().toString().isEmpty()){
                    mobile.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = false;
                    mobile.requestFocus();
                }
                else if (!mobile.getText().toString().matches("^[0]{1}[1-9]{1}[0-9]{7,15}$")) {
                    mobile.setError(getResources().getString(R.string.passenger_mobile_validation));
                    valid = false;
                    mobile.requestFocus();
                }

                if(nameEditText.getText().toString().isEmpty()){
                    nameEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = false;
                    nameEditText.requestFocus();
                }
                else if(!nameEditText.getText().toString().matches("[a-z A-Z]+?")) {
                    nameEditText.setError(getResources().getString(R.string.passenger_name_validation));
                    valid = false;
                    nameEditText.requestFocus();
                }

                if(contactTitle.getSelectedItemPosition()==0){
                    valid = false;
                    ((TextView) contactTitle.getSelectedView()).setError(getResources().getString(R.string.passenger_empty_validation));
                    contactTitle.requestFocus();
                }

                if (infant > 0) {
                    for (int i = infant-1; i >=0; i--) {
                        boolean validation = passengerForms[2][i].validate();
                        if(validation){
                            infantPassenger.put(i, passengerForms[2][i].getPassenger());
                            if(adultCheck[passengerForms[2][i].getPassenger().getAdultAssocNumber()-1] == false){
                                adultCheck[passengerForms[2][i].getPassenger().getAdultAssocNumber()-1] = true;
                            }
                            else{
                                valid = false;
                                adultAssocPass = false;
                            }
                        }
                        else {
                            valid = false;
                        }
                    }
                }

                if (child > 0) {
                    for (int i = child-1; i >=0; i--) {
                        boolean validation = passengerForms[1][i].validate();
                        if(validation){
                            childPassenger.put(i, passengerForms[1][i].getPassenger());
                        }
                        else {
                            valid = false;
                        }
                    }
                }

                if (adult > 0) {
                    for (int i = adult-1; i >=0; i--) {
                        adultCheck[i] = false;
                        boolean validation = passengerForms[0][i].validate();
                        if(validation){
                            adultPassenger.put(i, passengerForms[0][i].getPassenger());
                        }
                        else {
                            valid = false;
                        }
                    }
                }

                if(!adultAssocPass){
                    Toast.makeText(getApplicationContext(), R.string.passenger_adult_assoc_validation, Toast.LENGTH_LONG).show();
                }

                if(valid){
                    Gson gson = new Gson();
                    Intent intent = new Intent(FlightPassengerActivity.this, FlightPassengerSummaryActivity.class);
                    Contact cont = contactModel.getContactByName(Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")));

                    if(cont == null){
                        contactModel.insert(ItineraryHelper.encodeAdultTitle(contactTitle.getSelectedItem().toString(), getApplicationContext()), Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobile.getText().toString(), email.getText().toString());
                    }
                    else{
                        contactModel.update(cont.getContactID(), ItineraryHelper.encodeAdultTitle(contactTitle.getSelectedItem().toString(), getApplicationContext()), Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobile.getText().toString(), email.getText().toString());

                    }
                    Contact contact = new Contact(ItineraryHelper.encodeAdultTitle(contactTitle.getSelectedItem().toString(), getApplicationContext()), Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobile.getText().toString(), email.getText().toString());
                    intent.putExtra("contact", gson.toJson(contact));
                    intent.putExtra("parameter", getIntent().getBundleExtra("parameter"));
                    Map<Integer, Passenger> adult = new TreeMap<>(adultPassenger);
                    Map<Integer, Passenger> child = new TreeMap<>(childPassenger);
                    Map<Integer, Passenger> infant = new TreeMap<>(infantPassenger);
                    intent.putExtra("adult_passenger", gson.toJson(adult));
                    intent.putExtra("child_passenger", gson.toJson(child));
                    intent.putExtra("infant_passenger", gson.toJson(infant));
                    startActivity(intent);


                }
            }
        });


    }
}
