package com.tiketextra.tiketextra.activity.flight;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
//import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.custom_view.ItemPassengerLayout;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.model.AirlineModel;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.object.Airport;
import com.tiketextra.tiketextra.object.Contact;
import com.tiketextra.tiketextra.object.FareRoute;
import com.tiketextra.tiketextra.object.Flight;
import com.tiketextra.tiketextra.object.Passenger;

import java.util.Map;

public class FlightPassengerSummaryActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_summary);

        Toolbar toolbar = findViewById(R.id.toolbarResultSummary);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.ticket_detail);

        final Gson gson = new Gson();
        boolean discountFromProfit = false;

        findViewById(R.id.contactLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.passengerLayout).setVisibility(View.VISIBLE);

        Contact contact = gson.fromJson(getIntent().getStringExtra("contact"), Contact.class);

        ((TextView) findViewById(R.id.contactNameTextView)).setText(ItineraryHelper.decodeAdultTitle(contact.getTitle(), getApplicationContext()) + " " + contact.getFullName());
        ((TextView) findViewById(R.id.contactEmailTextView)).setText(contact.getEmail());
        ((TextView) findViewById(R.id.contactPhoneTextView)).setText(contact.getPhone());

        LinearLayout passengerLayout = (LinearLayout) findViewById(R.id.passengerLayout);
        String[] type = {"adult", "child", "infant"};
        int number = 0;
        for(int i=0; i<type.length; i++) {
            Map<Integer, Passenger> passenger = gson.fromJson(getIntent().getStringExtra(type[i]+"_passenger"), new TypeToken<Map<Integer, Passenger>>() {}.getType());
            for (Map.Entry<Integer, Passenger> entry : passenger.entrySet()) {
//                Log.e(entry.getValue().getTitle() + " " + entry.getValue().getName(), entry.getValue().getGender());
                ItemPassengerLayout item = new ItemPassengerLayout(this);
                item.setText(++number+".", (entry.getValue().getType().equals("adult")? ItineraryHelper.decodeAdultTitle(entry.getValue().getTitle(), getApplicationContext()) : (entry.getValue().getType().equals("child") ? ItineraryHelper.decodeChildTitle(entry.getValue().getTitle(), getApplicationContext()) : ItineraryHelper.decodeInfantTitle(entry.getValue().getTitle(), getApplicationContext()) )) + " " + entry.getValue().getName(), entry.getValue().getBirthDate()!=null && entry.getValue().isPrintBirthDate()?Utils.mediumDate(entry.getValue().getBirthDate(), this):getString(getResources().getIdentifier(entry.getValue().getType(), "string", getPackageName())));
                passengerLayout.addView(item);
            }
        }
        final Bundle parameter = getIntent().getBundleExtra("parameter");
        String passenger = parameter.getInt("adult")+" "+getResources().getString(R.string.adult).toLowerCase() + (parameter.getInt("child") > 0 ? ", "+parameter.getInt("child")+" "+getResources().getString(R.string.child).toLowerCase():"") + (parameter.getInt("infant") > 0 ? ", "+parameter.getInt("infant")+" "+getResources().getString(R.string.infant).toLowerCase():"");
        ((TextView) findViewById(R.id.textViewPassengerCount)).setText(passenger);


        FareRoute fareDepart = gson.fromJson(parameter.getString("depart_choice"), FareRoute.class);

        AirportModel airportModel = new AirportModel(this);

        ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(fareDepart.getDepartDatetime(), this));
        ((TextView) findViewById(R.id.departHintTextView)).setText(airportModel.getAirport(fareDepart.getFlight().get(0).getDepartPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ airportModel.getAirport(fareDepart.getFlight().get(fareDepart.getFlight().size() - 1).getArrivePort()).getCity());



        AirlineModel airlineModel = new AirlineModel(this);

        for (int i = 0; i < fareDepart.getFlight().size() || i <= 5; i++) {
            if (i == 0) {
                Flight fl = fareDepart.getFlight().get(i);
                ((ImageView) findViewById(R.id.depart_imageViewAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                ((TextView) findViewById(R.id.depart_textViewAirline)).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                ((TextView) findViewById(R.id.depart_textViewFlightNumber)).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                ((TextView) findViewById(R.id.depart_departTime)).setText(fl.getDepartTime());
                ((TextView) findViewById(R.id.depart_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(R.id.depart_arriveTime)).setText(fl.getArriveTime());
                ((TextView) findViewById(R.id.depart_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Airport airport = airportModel.getAirport(fl.getDepartPort());
                ((TextView) findViewById(R.id.depart_departCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_departAirport)).setText(airport.getAirportName());
                airport = airportModel.getAirport(fl.getArrivePort());
                ((TextView) findViewById(R.id.depart_arriveCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_arriveAirport)).setText(airport.getAirportName());

            } else if (i > 0 && i <= 5 && i < fareDepart.getFlight().size()) {
                Flight fl = fareDepart.getFlight().get(i);

                ((ImageView) findViewById(getResources().getIdentifier("depart" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewAirline", "id", getPackageName()))).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Airport airport = airportModel.getAirport(fl.getDepartPort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departAirport", "id", getPackageName()))).setText(airport.getAirportName());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(fareDepart.getFlight().get(i-1).getArriveDatetime(), fl.getDepartDatetime(), fareDepart.getFlight().get(i-1).getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + airport.getCity());

                airport = airportModel.getAirport(fl.getArrivePort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveAirport", "id", getPackageName()))).setText(airport.getAirportName());


                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
            } else if (i > 0 && i <= 5 && i >= fareDepart.getFlight().size()) {
                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
            }
        }
        if(fareDepart.getHiddenTransitCount() > 0){
            findViewById(R.id.departHiddenTransitLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.departHiddenTransitTextView)).setText(getString(R.string.transit_not_mentioned, ""+fareDepart.getHiddenTransitCount()));
        }
        ((TextView) findViewById(R.id.departSummaryTextView)).setText(fareDepart.getDepartPort() + " - " + fareDepart.getArrivePort() + " x " + (parameter.getInt("adult") + parameter.getInt("child") + parameter.getInt("infant")) + " " + getString(R.string.person).toLowerCase());
        ((TextView) findViewById(R.id.departFareTextView)).setText(Utils.numberFormatCurrency(fareDepart.getTotalPriceBeforeDiscount(parameter.getInt("adult"), parameter.getInt("child"), parameter.getInt("infant"))));

        int total = fareDepart.getTotalPrice(parameter.getInt("adult"), parameter.getInt("child"), parameter.getInt("infant"));
        int discount = fareDepart.getTotalDiscount(parameter.getInt("adult"), parameter.getInt("child"), parameter.getInt("infant"));

        discountFromProfit = discountFromProfit || fareDepart.getFare().get("adult").isDiscountRuleFromProfit();

        //------------------------------------------------------------------------
        if(parameter.getString("type").equals("oneway")){
            findViewById(R.id.returnDetailInfoLayout).setVisibility(View.GONE);
            findViewById(R.id.returnFareLayout).setVisibility(View.GONE);
        }
        else {
            FareRoute fareReturn = gson.fromJson(parameter.getString("return_choice"), FareRoute.class);

            ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns)+" - "+ Utils.mediumDate(fareReturn.getDepartDatetime(), this));
            ((TextView) findViewById(R.id.returnHintTextView)).setText(airportModel.getAirport(fareReturn.getFlight().get(0).getDepartPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ airportModel.getAirport(fareReturn.getFlight().get(fareReturn.getFlight().size() - 1).getArrivePort()).getCity());



            for (int i = 0; i < fareReturn.getFlight().size() || i <= 5; i++) {
                if (i == 0) {
                    Flight fl = fareReturn.getFlight().get(i);
                    ((ImageView) findViewById(R.id.return_imageViewAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                    ((TextView) findViewById(R.id.return_textViewAirline)).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                    ((TextView) findViewById(R.id.return_textViewFlightNumber)).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                    ((TextView) findViewById(R.id.return_departTime)).setText(fl.getDepartTime());
                    ((TextView) findViewById(R.id.return_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(R.id.return_arriveTime)).setText(fl.getArriveTime());
                    ((TextView) findViewById(R.id.return_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Airport airport = airportModel.getAirport(fl.getDepartPort());
                    ((TextView) findViewById(R.id.return_departCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                    ((TextView) findViewById(R.id.return_departAirport)).setText(airport.getAirportName());
                    airport = airportModel.getAirport(fl.getArrivePort());
                    ((TextView) findViewById(R.id.return_arriveCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                    ((TextView) findViewById(R.id.return_arriveAirport)).setText(airport.getAirportName());

                } else if (i > 0 && i <= 5 && i < fareReturn.getFlight().size()) {
                    Flight fl = fareReturn.getFlight().get(i);

                    ((ImageView) findViewById(getResources().getIdentifier("return" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewAirline", "id", getPackageName()))).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Airport airport = airportModel.getAirport(fl.getDepartPort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departAirport", "id", getPackageName()))).setText(airport.getAirportName());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(fareReturn.getFlight().get(i-1).getArriveDatetime(), fl.getDepartDatetime(), fareReturn.getFlight().get(i-1).getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + airport.getCity());

                    airport = airportModel.getAirport(fl.getArrivePort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveAirport", "id", getPackageName()))).setText(airport.getAirportName());


                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
                } else if (i > 0 && i <= 5 && i >= fareReturn.getFlight().size()) {
                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
                }
            }
            if(fareReturn.getHiddenTransitCount() > 0){
                findViewById(R.id.returnHiddenTransitLayout).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.returnHiddenTransitTextView)).setText(getString(R.string.transit_not_mentioned, ""+fareReturn.getHiddenTransitCount()));
            }
            ((TextView) findViewById(R.id.returnSummaryTextView)).setText(fareReturn.getDepartPort() + " - " + fareReturn.getArrivePort() + " x " + (parameter.getInt("adult") + parameter.getInt("child") + parameter.getInt("infant")) + " " + getString(R.string.person).toLowerCase());
            ((TextView) findViewById(R.id.returnFareTextView)).setText(Utils.numberFormatCurrency(fareReturn.getTotalPriceBeforeDiscount(parameter.getInt("adult"), parameter.getInt("child"), parameter.getInt("infant"))));

            total += fareReturn.getTotalPrice(parameter.getInt("adult"), parameter.getInt("child"), parameter.getInt("infant"));
            discount += fareReturn.getTotalDiscount(parameter.getInt("adult"), parameter.getInt("child"), parameter.getInt("infant"));

            discountFromProfit = discountFromProfit || fareReturn.getFare().get("adult").isDiscountRuleFromProfit();
        }

        ((TextView) findViewById(R.id.fareHintTextView)).setText(parameter.getString("type").equals("oneway")?R.string.oneway:R.string.roundtrip);

        if(discount < 0 && discountFromProfit==false){
            findViewById(R.id.discountFareLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.discountFareTextView)).setText(Utils.numberFormatCurrency(discount));
            ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / 1000)));
            ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - 3));
        }
        else if(discountFromProfit){
//            findViewById(R.id.discountFareLayout).setVisibility(View.VISIBLE);
//            ((TextView) findViewById(R.id.discountFareTextView)).setText(R.string.price_before_discount);
            total = total - discount;
            ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / 1000)));
            ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - 3));
        }
        else{
            ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / 1000)));
            ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - 3));
        }


        ((TextView) findViewById(R.id.buttonBookingText)).setText(R.string.booking);

        findViewById(R.id.buttonBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog mDialog = new Dialog(FlightPassengerSummaryActivity.this, R.style.CustomDialogTheme);

                mDialog.setContentView(R.layout.dialog_yes_no_question);
                mDialog.setCancelable(true);
                TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                mDialogHeader.setText(R.string.continue_to_booking);
                mDialogText.setText(R.string.msg_continue_to_booking);

                ((TextView) mDialog.findViewById(R.id.dialog_info_ok)).setText(R.string.okay);
                ((TextView) mDialog.findViewById(R.id.dialog_info_no)).setText(R.string.cancel);

                mDialog.show();

                mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                        Intent intent = new Intent(FlightPassengerSummaryActivity.this, FlightBookingActivity.class);
                        intent.putExtra("parameter", getIntent().getBundleExtra("parameter"));
                        intent.putExtra("contact", getIntent().getStringExtra("contact"));
                        intent.putExtra("adult_passenger", getIntent().getStringExtra("adult_passenger"));
                        intent.putExtra("child_passenger", getIntent().getStringExtra("child_passenger"));
                        intent.putExtra("infant_passenger", getIntent().getStringExtra("infant_passenger"));
                        startActivity(intent);
                    }
                });

                mDialog.findViewById(R.id.dialog_info_no).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });
            }
        });

    }
}
