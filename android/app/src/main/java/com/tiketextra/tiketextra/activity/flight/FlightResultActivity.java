package com.tiketextra.tiketextra.activity.flight;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.user.LoginActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.FlightResultListAdapter;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.listener.OnViewClickListener;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Airport;
import com.tiketextra.tiketextra.object.FareComponent;
import com.tiketextra.tiketextra.object.FareRoute;
import com.tiketextra.tiketextra.object.Flight;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlightResultActivity extends BaseActivity implements OnViewClickListener {

    private Bundle parameter;
    private String feed = "empty";
    private Map<String, Integer> percentProgress = new HashMap<>();
    private int counter = 0;

    private ProgressBar progressSearching, progressSplash;

    private List<FareRoute> fareRouteListDepart = new ArrayList<>();
    private List<FareRoute> fareRouteListReturn = new ArrayList<>();
    private ListView listViewDepart, listViewReturn;
    private FlightResultListAdapter adapterDepart, adapterReturn;
    private int versionCode;
    private boolean showCarrierResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_result);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFlightResult);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppHelper.mode = Configuration.MODE_DEPART;
        AppHelper.departSort = 0;
        AppHelper.returnSort = 0;

        AppHelper.departFilterAirline = new ArrayList<>();
        AppHelper.returnFilterAirline = new ArrayList<>();

        AppHelper.departFilterClass = new ArrayList<>();
        AppHelper.returnFilterClass = new ArrayList<>();
        AppHelper.departSelectedClass = new ArrayList<>();
        AppHelper.returnSelectedClass = new ArrayList<>();

        AppHelper.departFilterTransitNum = new ArrayList<>();
        AppHelper.returnFilterTransitNum = new ArrayList<>();
        AppHelper.departSelectedTransitNum = new ArrayList<>();
        AppHelper.returnSelectedTransitNum = new ArrayList<>();

        AppHelper.departPriceMin = AppHelper.departPriceMax = AppHelper.returnPriceMin = AppHelper.returnPriceMax
                = AppHelper.departSelectedPriceMin = AppHelper.departSelectedPriceMax = AppHelper.returnSelectedPriceMin = AppHelper.returnSelectedPriceMax
                = 0;

        parameter = getIntent().getBundleExtra("parameter");


        AirportModel airportModel = new AirportModel(this);
        final String from = airportModel.getAirport(parameter.getString("from")).getCity();
        final String to = airportModel.getAirport(parameter.getString("to")).getCity();
        final String passenger = parameter.getInt("adult")+" "+getResources().getString(R.string.adult).toLowerCase() + (parameter.getInt("child") > 0 ? ", "+parameter.getInt("child")+" "+getResources().getString(R.string.child).toLowerCase():"") + (parameter.getInt("infant") > 0 ? ", "+parameter.getInt("infant")+" "+getResources().getString(R.string.infant).toLowerCase():"");



        ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(getString(R.string.depart) + " " + Utils.mediumDate(parameter.getString("depart"), this));

        ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(from+" "+getResources().getString(R.string.to).toLowerCase()+" "+to+" - "+passenger);
        findViewById(R.id.toolbarSubTitleTextView).setSelected(true);

        findViewById(R.id.toolbarTitleTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FlightResultActivity.this, FlightSearchActivity.class));
            }
        });

        findViewById(R.id.toolbarSubTitleTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FlightResultActivity.this, FlightSearchActivity.class));
            }
        });
        bindLogo();
        progressSearching = findViewById(R.id.searchProgressBar);
        progressSplash = findViewById(R.id.progressBar);

        this.listViewDepart = findViewById(R.id.resultListDepart);
        this.listViewDepart.setVisibility(View.GONE);
        this.adapterDepart = new FlightResultListAdapter(getLayoutInflater(), this.fareRouteListDepart, Configuration.MODE_DEPART, this);
        this.listViewDepart.setAdapter(this.adapterDepart);

        if(parameter.getString("type").equals("roundtrip")){
            this.listViewReturn = findViewById(R.id.resultListReturn);
            this.adapterReturn = new FlightResultListAdapter(getLayoutInflater(), this.fareRouteListReturn, Configuration.MODE_RETURN, this);
            this.listViewReturn.setAdapter(this.adapterReturn);
        }

        findViewById(R.id.changeDepartTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.mode = Configuration.MODE_DEPART;

                findViewById(R.id.departureInfoLayout).setVisibility(View.GONE);
                findViewById(R.id.commonInfoLayout).setVisibility(View.GONE);
                findViewById(R.id.resultListReturn).setVisibility(View.GONE);
                findViewById(R.id.resultListDepart).setVisibility(View.VISIBLE);

                ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(getString(R.string.depart) + " " + Utils.mediumDate(parameter.getString("depart"), FlightResultActivity.this));
                ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(from + " " + getResources().getString(R.string.to).toLowerCase() + " " + to + " - " + passenger);
                findViewById(R.id.toolbarSubTitleTextView).setSelected(true);
            }
        });

        findViewById(R.id.buttonFlightSort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FlightResultActivity.this, FlightSortActivity.class);
                i.putExtra("mode", AppHelper.mode);
                startActivityForResult(i, 1);

            }
        });

        findViewById(R.id.buttonFlightFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FlightResultActivity.this, FlightFilterActivity.class);
                i.putExtra("mode", AppHelper.mode);
                startActivityForResult(i, 1);
            }
        });

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        SettingModel settingModel = new SettingModel(this);
        int campaign_count = Integer.parseInt(settingModel.getValue("member_campaign_count"));
        /*if(campaign_count < 2 && !isLoggedIn()){
            settingModel.setValue("member_campaign_count", Integer.toString(campaign_count+1));
            final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

            mDialog.setContentView(R.layout.dialog_yes_no_question);
            mDialog.setCancelable(true);
            TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
            TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

            mDialogHeader.setText(R.string.title_member_campaign);
            mDialogText.setText(R.string.msg_member_campaign);

            ((TextView) mDialog.findViewById(R.id.dialog_info_ok)).setText(R.string.okay_agree);
            ((TextView) mDialog.findViewById(R.id.dialog_info_no)).setText(R.string.later);

            mDialog.show();

            mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                    startActivity(new Intent(FlightResultActivity.this, LoginActivity.class));
                }
            });

            mDialog.findViewById(R.id.dialog_info_no).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                }
            });
        }*/

        findViewById(R.id.needUpdateLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
            }
        });

        findViewById(R.id.emptyResultLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FlightResultActivity.this, FlightSearchActivity.class));
            }
        });

        discover();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            updateLayout();
        }
    }

    private void discover(){
        Map<String, String> params = new HashMap<>();
        final SettingModel settingModel = new SettingModel(this);
        params.put("from_port", parameter.getString("from"));
        params.put("to_port", parameter.getString("to"));
        params.put("is_return", parameter.getString("type").equals("oneway")?"0":"1");
        params.put("adult_count", Integer.toString(parameter.getInt("adult")));
        params.put("child_count", Integer.toString(parameter.getInt("child")));
        params.put("infant_count", Integer.toString(parameter.getInt("infant")));
        params.put("go_date", parameter.getString("depart"));
        params.put("back_date", parameter.getString("return"));
        params.put("airlines", "LIO");

        SessionManager sessionManager = new SessionManager(FlightResultActivity.this);
        params.put("auth_mode", sessionManager.isLoggedIn()?"_auth":"");
        params.put("feed", feed);
        params.put("version_code", Integer.toString(versionCode));
        Log.e("CEMBELIQ", params.toString());
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "find", params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
				Log.e("JSON", response.toString());
                try {
                    String status = response.getString("status");
                    if(status.equals("1")){
                        JSONArray progress = response.getJSONArray("progress");
                        feed = "";
                        for(int i=0; i<progress.length(); i++){
                            JSONObject tprogress = (JSONObject) progress.get(i);
                            percentProgress.put(tprogress.getString("airline_code"), tprogress.getInt("rate"));
                            if(tprogress.getString("state").equals(Configuration.API_STATE_RUN)){
                                feed += tprogress.toString() + ",";
                            }
                            else if(tprogress.getString("state").equals(Configuration.API_STATE_FINISH)){
                                updateTable(tprogress);
                                if((counter==0 && AppHelper.departSelectedAirlineAtFlightForm.size()==0) || (showCarrierResult && AppHelper.departSelectedAirlineAtFlightForm.size() > 0)){
                                    findViewById(R.id.progressSplashLogo).setVisibility(View.GONE);
                                    progressSearching.setVisibility(View.VISIBLE);
                                    listViewDepart.setVisibility(View.VISIBLE);
                                }
                                counter++;
                            }
                        }
                        setProgressBar();
                        if(feed.length() > 10){
                            feed = "{\"progress\":[" + feed.substring(0, feed.length()-1) + "],\"id\":\"" + response.getString("id") + "\",\"status\":" + status + "}";
                            discover();
                        }
                        else{
                            progressSearching.setVisibility(View.GONE);
                        }
                    }
                    else{
                        progressSearching.setVisibility(View.GONE);

                        if(status.equals("need_update")){
                            findViewById(R.id.needUpdateLayout).setVisibility(View.VISIBLE);
                        }
                        else{
                            findViewById(R.id.emptyResultLayout).setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    Toast.makeText(context, R.string.error_data_communication, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                Toast.makeText(FlightResultActivity.this, R.string.error_data_communication, Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private void bindLogo(){
        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.imageViewSplashLogo);
        final AlphaAnimation animation1 = new AlphaAnimation(0.6f, 1.0f);
        animation1.setDuration(700);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.6f);
        animation2.setDuration(700);
        //animation1 AnimationListener
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                splash.startAnimation(animation2);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                splash.startAnimation(animation1);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        splash.startAnimation(animation2);
    }

    private void setProgressBar() {
        int count;
        int total = count = 0;
        for (Map.Entry<String, Integer> entry : this.percentProgress.entrySet()) {
            total += entry.getValue();
            count++;
        }
        int proc = (total / count);
        progressSearching.setProgress(proc);
        progressSplash.setProgress(proc);
        if (proc == 100 && counter == 0) {
//        if (counter == 0) {
            progressSearching.setVisibility(View.GONE);
            findViewById(R.id.emptyResultLayout).setVisibility(View.VISIBLE);
        }
    }

    private void updateTable(JSONObject tprogress) throws JSONException{
        AirportModel airportModel = new AirportModel(this);
        String[] c_class = {"pro", "eco", "bus"};
        String[] ages = {"adult", "child", "infant"};
        JSONObject data = tprogress.getJSONObject("data");



        JSONArray dataGo = data.getJSONArray("go");
        for(int i=0; i<dataGo.length(); i++){
            showCarrierResult = false;
            JSONObject dataRow = (JSONObject) dataGo.get(i);
            JSONArray flight = dataRow.getJSONArray("flights");
            ArrayList<Flight> flights = new ArrayList<Flight>();
            int stopCount = 0;
            for(int f=0; f<flight.length(); f++){
                JSONObject objFlight = (JSONObject) flight.get(f);
                Airport departPort = airportModel.getAirport(objFlight.getString("depart_port"));
                Airport arrivePort = airportModel.getAirport(objFlight.getString("arrive_port"));
                flights.add(new Flight(tprogress.getString("airline_code"), objFlight.getString("flight_num"), objFlight.getString("depart_datetime"),
                        departPort.getTimezone(), departPort.getCity(), departPort.getIataCode(),
                        objFlight.getString("arrive_datetime"), arrivePort.getTimezone(),
                        arrivePort.getCity(), arrivePort.getIataCode()));
                stopCount += objFlight.has("stop_count") ? objFlight.getInt("stop_count") : 0;
                showCarrierResult = showCarrierResult || (AppHelper.departSelectedAirlineAtFlightForm.size() > 0 && AppHelper.departSelectedAirline.contains(flights.get(f).getFlightCode())) || AppHelper.departSelectedAirlineAtFlightForm.size()==0;
            }

            JSONObject fare = dataRow.getJSONObject("fares");
            for(int j=0; j<c_class.length; j++){
                if(!fare.isNull(c_class[j])){
//					//Log.e(c_class[j], fare.getJSONObject(c_class[j]).toString());
                    JSONObject byAges = fare.getJSONObject(c_class[j]).getJSONObject("by_ages");
                    Map<String, FareComponent> fc = new HashMap<>();
                    for(int a=0; a<ages.length; a++){
                        if(!byAges.isNull(ages[a])){
                            fc.put(ages[a], new FareComponent(
                                            byAges.getJSONObject(ages[a]).isNull("basic")?0:byAges.getJSONObject(ages[a]).getInt("basic"),
                                            byAges.getJSONObject(ages[a]).isNull("tax")?0:byAges.getJSONObject(ages[a]).getInt("tax"),
                                            byAges.getJSONObject(ages[a]).isNull("fuel")?0:byAges.getJSONObject(ages[a]).getInt("fuel"),
                                            byAges.getJSONObject(ages[a]).isNull("adm")?0:byAges.getJSONObject(ages[a]).getInt("adm"),
                                            byAges.getJSONObject(ages[a]).isNull("iwjr")?0:byAges.getJSONObject(ages[a]).getInt("iwjr"),
                                            byAges.getJSONObject(ages[a]).isNull("surcharge")?0:byAges.getJSONObject(ages[a]).getInt("surcharge"),
                                            byAges.getJSONObject(ages[a]).isNull("discount")?0:byAges.getJSONObject(ages[a]).getInt("discount"),
                                            byAges.getJSONObject(ages[a]).isNull("total")?0:byAges.getJSONObject(ages[a]).getInt("total"),
                                            byAges.getJSONObject(ages[a]).getString("discount_rule_from"),byAges.getJSONObject(ages[a]).getString("surcharge_rule_from")
                                    )
                            );
                        }
                    }
                    FareRoute objFareRoute = new FareRoute(c_class[j], fare.getJSONObject(c_class[j]).getString("id"), fc, flights, stopCount);
                    objFareRoute.setFilterAirline(!showCarrierResult);

                    for (Flight fl : objFareRoute.getFlight()) {
                        if (!AppHelper.departFilterAirline.contains(fl.getFlightCode())) {
                            AppHelper.departFilterAirline.add(fl.getFlightCode());
                            if(AppHelper.departSelectedAirlineAtFlightForm.size() == 0) {
                                AppHelper.departSelectedAirline.add(fl.getFlightCode());
                            }
                        }
                    }
                        if (AppHelper.departPriceMin == 0 || objFareRoute.getFare().get("adult").getTotal() < AppHelper.departPriceMin) {
                            AppHelper.departPriceMin = objFareRoute.getFare().get("adult").getTotal();

                            AppHelper.departPriceMin -= (AppHelper.departPriceMin % 50000);
                            AppHelper.departSelectedPriceMin = AppHelper.departPriceMin;
                        }
                        if (AppHelper.departPriceMax == 0 || objFareRoute.getFare().get("adult").getTotal() > AppHelper.departPriceMax) {
                            AppHelper.departPriceMax = objFareRoute.getFare().get("adult").getTotal();

                            AppHelper.departPriceMax += (50000 - (AppHelper.departPriceMax % 50000));
                            AppHelper.departSelectedPriceMax = AppHelper.departPriceMax;
                        }

                        if (!AppHelper.departFilterClass.contains(objFareRoute.getFareClass())) {
                            AppHelper.departFilterClass.add(objFareRoute.getFareClass());
                            AppHelper.departSelectedClass.add(objFareRoute.getFareClass());
                        }

                        if (!AppHelper.departFilterTransitNum.contains(Integer.toString(objFareRoute.getFlight().size() - 1 + objFareRoute.getHiddenTransitCount()))) {
                            AppHelper.departFilterTransitNum.add(Integer.toString(objFareRoute.getFlight().size() - 1 + objFareRoute.getHiddenTransitCount()));
                            AppHelper.departSelectedTransitNum.add(Integer.toString(objFareRoute.getFlight().size() - 1 + objFareRoute.getHiddenTransitCount()));
                        }


                    this.fareRouteListDepart.add(objFareRoute);
                }
            }
        }
        this.adapterDepart.updateDataSetChanged();

        if(parameter.getString("type").equals("roundtrip")){
            JSONArray dataBack = data.getJSONArray("back");
            for(int i=0; i<dataBack.length(); i++){
                showCarrierResult = false;
                JSONObject dataRow = (JSONObject) dataBack.get(i);
                JSONArray flight = dataRow.getJSONArray("flights");
                ArrayList<Flight> flights = new ArrayList<Flight>();
                int stopCount = 0;
                for(int f=0; f<flight.length(); f++){
                    JSONObject objFlight = (JSONObject) flight.get(f);
                    Airport departPort = airportModel.getAirport(objFlight.getString("depart_port"));
                    Airport arrivePort = airportModel.getAirport(objFlight.getString("arrive_port"));
                    flights.add(new Flight(tprogress.getString("airline_code"), objFlight.getString("flight_num"), objFlight.getString("depart_datetime"),
                            departPort.getTimezone(), departPort.getCity(), departPort.getIataCode(),
                            objFlight.getString("arrive_datetime"), arrivePort.getTimezone(),
                            arrivePort.getCity(), arrivePort.getIataCode()));
                    stopCount += objFlight.has("stop_count") ? objFlight.getInt("stop_count") : 0;
                    showCarrierResult = showCarrierResult || (AppHelper.returnSelectedAirlineAtFlightForm.size() > 0 && AppHelper.returnSelectedAirline.contains(flights.get(f).getFlightCode())) || AppHelper.returnSelectedAirlineAtFlightForm.size()==0;
                }

                JSONObject fare = dataRow.getJSONObject("fares");
                for(int j=0; j<c_class.length; j++){
                    if(!fare.isNull(c_class[j])){
//					//Log.e(c_class[j], fare.getJSONObject(c_class[j]).toString());
                        JSONObject byAges = fare.getJSONObject(c_class[j]).getJSONObject("by_ages");
                        Map<String, FareComponent> fc = new HashMap<>();
                        for(int a=0; a<ages.length; a++){
                            if(!byAges.isNull(ages[a])){
                                fc.put(ages[a], new FareComponent(
                                                byAges.getJSONObject(ages[a]).isNull("basic")?0:byAges.getJSONObject(ages[a]).getInt("basic"),
                                                byAges.getJSONObject(ages[a]).isNull("tax")?0:byAges.getJSONObject(ages[a]).getInt("tax"),
                                                byAges.getJSONObject(ages[a]).isNull("fuel")?0:byAges.getJSONObject(ages[a]).getInt("fuel"),
                                                byAges.getJSONObject(ages[a]).isNull("adm")?0:byAges.getJSONObject(ages[a]).getInt("adm"),
                                                byAges.getJSONObject(ages[a]).isNull("iwjr")?0:byAges.getJSONObject(ages[a]).getInt("iwjr"),
                                                byAges.getJSONObject(ages[a]).isNull("surcharge")?0:byAges.getJSONObject(ages[a]).getInt("surcharge"),
                                                byAges.getJSONObject(ages[a]).isNull("discount")?0:byAges.getJSONObject(ages[a]).getInt("discount"),
                                                byAges.getJSONObject(ages[a]).isNull("total")?0:byAges.getJSONObject(ages[a]).getInt("total"),
                                                byAges.getJSONObject(ages[a]).getString("discount_rule_from"),byAges.getJSONObject(ages[a]).getString("surcharge_rule_from")
                                        )
                                );
                            }
                        }
                        FareRoute objFareRoute = new FareRoute(c_class[j], fare.getJSONObject(c_class[j]).getString("id"), fc, flights, stopCount);
                        objFareRoute.setFilterAirline(!showCarrierResult);



                        for (Flight fl : objFareRoute.getFlight()) {

                            if (!AppHelper.returnFilterAirline.contains(fl.getFlightCode())) {
                                AppHelper.returnFilterAirline.add(fl.getFlightCode());
                                if(AppHelper.returnSelectedAirlineAtFlightForm.size() == 0) {
                                    AppHelper.returnSelectedAirline.add(fl.getFlightCode());
                                }
                            }
                        }


                        if (AppHelper.returnPriceMin == 0 || objFareRoute.getFare().get("adult").getTotal() < AppHelper.returnPriceMin) {
                            AppHelper.returnPriceMin = objFareRoute.getFare().get("adult").getTotal();

                            AppHelper.returnPriceMin -= (AppHelper.returnPriceMin % 50000);
                            AppHelper.returnSelectedPriceMin = AppHelper.returnPriceMin;
                        }
                        if (AppHelper.returnPriceMax == 0 || objFareRoute.getFare().get("adult").getTotal() > AppHelper.returnPriceMax) {
                            AppHelper.returnPriceMax = objFareRoute.getFare().get("adult").getTotal();

                            AppHelper.returnPriceMax += (50000 - (AppHelper.returnPriceMax % 50000));
                            AppHelper.returnSelectedPriceMax = AppHelper.returnPriceMax;
                        }

                        if (!AppHelper.returnFilterClass.contains(objFareRoute.getFareClass())) {
                            AppHelper.returnFilterClass.add(objFareRoute.getFareClass());
                            AppHelper.returnSelectedClass.add(objFareRoute.getFareClass());
                        }

                        if (!AppHelper.returnFilterTransitNum.contains(Integer.toString(objFareRoute.getFlight().size() - 1 + objFareRoute.getHiddenTransitCount()))) {
                            AppHelper.returnFilterTransitNum.add(Integer.toString(objFareRoute.getFlight().size() - 1 + objFareRoute.getHiddenTransitCount()));
                            AppHelper.returnSelectedTransitNum.add(Integer.toString(objFareRoute.getFlight().size() - 1 + objFareRoute.getHiddenTransitCount()));
                        }

                        this.fareRouteListReturn.add(objFareRoute);
                    }
                }
            }
            this.adapterReturn.updateDataSetChanged();
        }
    }


    private void updateLayout(){
        for (int i=0; i<AppHelper.departSelectedAirline.size(); i++){
            Log.e("airline", AppHelper.departSelectedAirline.get(i));
        }
        if(AppHelper.mode.equals(Configuration.MODE_DEPART)) {
            for (FareRoute fare : fareRouteListDepart) {

                for(int i=0; i<fare.getFlight().size(); i++){
                    if(!AppHelper.departSelectedAirline.contains(fare.getFlight().get(i).getFlightCode())){
                        fare.setFilterAirline(true);
                        break;
                    }
                    else {
                        fare.setFilterAirline(false);
                    }
                }

                if(!AppHelper.departSelectedClass.contains(fare.getFareClass())){
                    fare.setFilterClass(true);
                }
                else {
                    fare.setFilterClass(false);
                }
                if(!AppHelper.departSelectedTransitNum.contains(Integer.toString(fare.getFlight().size()-1+fare.getHiddenTransitCount()))){
                    fare.setFilterTransitNum(true);
                }
                else {
                    fare.setFilterTransitNum(false);
                }
                if(fare.getFare().get("adult").getTotal() < AppHelper.departSelectedPriceMin || fare.getFare().get("adult").getTotal() > AppHelper.departSelectedPriceMax){
                    fare.setFilterPrice(true);
                }
                else {
                    fare.setFilterPrice(false);
                }
            }
            this.adapterDepart.updateDataSetChanged();
        }
        else{

            for (FareRoute fare : fareRouteListReturn) {

                for(int i=0; i<fare.getFlight().size(); i++){
                    if(!AppHelper.returnSelectedAirline.contains(fare.getFlight().get(i).getFlightCode())){
                        fare.setFilterAirline(true);
                        break;
                    }
                    else {
                        fare.setFilterAirline(false);
                    }
                }

                if(!AppHelper.returnSelectedClass.contains(fare.getFareClass())){
                    fare.setFilterClass(true);
                }
                else {
                    fare.setFilterClass(false);
                }
                if(!AppHelper.returnSelectedTransitNum.contains(Integer.toString(fare.getFlight().size()-1+fare.getHiddenTransitCount()))){
                    fare.setFilterTransitNum(true);
                }
                else {
                    fare.setFilterTransitNum(false);
                }
                if(fare.getFare().get("adult").getTotal() < AppHelper.returnSelectedPriceMin || fare.getFare().get("adult").getTotal() > AppHelper.returnSelectedPriceMax){
                    fare.setFilterPrice(true);
                }
                else {
                    fare.setFilterPrice(false);
                }
            }
            this.adapterReturn.updateDataSetChanged();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        updateLayout();
    }

    @Override
    public void setOnViewClickListener(View view, int position) {

    }

    @Override
    public void setOnViewClickListener(View view, int position, String mode) {
            if (mode.equals(Configuration.MODE_DEPART)) {
                if (view.getId() == listViewDepart.findViewById(R.id.commonInfoLayout).getId()) {
                    for (FareRoute fare : fareRouteListDepart) {
                        fare.setSelected(false);
                    }
                    ((FareRoute) listViewDepart.getAdapter().getItem(position)).setSelected(true);
                    adapterDepart.updateDataSetChanged();
                }
                else if (view.getId() == listViewDepart.findViewById(R.id.chooseButton).getId()) {
                    Gson gson = new Gson();
                    if(parameter.getString("type").equals("oneway")) {
                        Intent intent = new Intent(FlightResultActivity.this, FlightResultSummaryActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("depart_choice", gson.toJson(listViewDepart.getAdapter().getItem(position)));
                        bundle.putString("type", parameter.getString("type"));
                        bundle.putBoolean("is_international", parameter.getBoolean("is_international"));
                        bundle.putInt("adult", parameter.getInt("adult"));
                        bundle.putInt("child", parameter.getInt("child"));
                        bundle.putInt("infant", parameter.getInt("infant"));
                        intent.putExtra("parameter", bundle);
                        startActivity(intent);
                    }
                    else{
                        AppHelper.mode = Configuration.MODE_RETURN;

                        AirportModel airportModel = new AirportModel(this);
                        String from = airportModel.getAirport(parameter.getString("to")).getCity();
                        String to = airportModel.getAirport(parameter.getString("from")).getCity();
                        String passenger = parameter.getInt("adult")+" "+getResources().getString(R.string.adult).toLowerCase();
                        passenger += parameter.getInt("child") > 0 ? ", "+parameter.getInt("child")+getResources().getString(R.string.child).toLowerCase():"";
                        passenger += parameter.getInt("infant") > 0 ? ", "+parameter.getInt("infant")+getResources().getString(R.string.infant).toLowerCase():"";


                        ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(getString(R.string.goback) + " " + Utils.mediumDate(parameter.getString("return"), this));
                        ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(from+" "+getResources().getString(R.string.to).toLowerCase()+" "+to+" - "+passenger);
                        findViewById(R.id.toolbarSubTitleTextView).setSelected(true);

                        findViewById(R.id.departureInfoLayout).setVisibility(View.VISIBLE);
                        findViewById(R.id.commonInfoLayout).setVisibility(View.VISIBLE);
                        findViewById(R.id.resultListReturn).setVisibility(View.VISIBLE);
                        findViewById(R.id.resultListDepart).setVisibility(View.GONE);

                        FareRoute fare = (FareRoute) listViewDepart.getAdapter().getItem(position);
                        ((TextView) findViewById(R.id.departureTextView)).setText(getResources().getString(R.string.departure)+" - "+Utils.mediumDate(fare.getDepartDatetime(), this));

                        ImageView imageViewAirline = findViewById(R.id.imageViewAirline);
                        TextView tvDepartTime = findViewById(R.id.textViewDepartTime);
                        TextView tvDepartPort = findViewById(R.id.textViewDepartPort);
                        TextView tvArriveTime = findViewById(R.id.textViewArriveTime);
                        TextView tvArrivePort = findViewById(R.id.textViewArrivePort);
                        TextView tvPriceBefore = findViewById(R.id.textViewPriceBefore);
                        TextView tvPrice1 = findViewById(R.id.textViewPrice1);
                        TextView tvPrice2 = findViewById(R.id.textViewPrice2);
                        TextView textViewItinerary = findViewById(R.id.textViewItinerary);
                        TextView textViewFareClass = findViewById(R.id.textViewCabinClass);
                        
                        imageViewAirline.setImageResource(getResources().getIdentifier(("ic_air_"+(fare.getAirlineCode().equals("AIR")?"AIR":fare.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                        tvDepartTime.setText(fare.getDepartTime());
                        tvDepartPort.setText(fare.getDepartPort());
                        int diff;
                        tvArriveTime.setText((diff= Utils.dateDiff(fare.getDepartDatetime(), fare.getArriveDatetime())) > 0 ? Html.fromHtml(fare.getArriveTime() + "<sup><small>(+" + diff + ")</small></sup>") : fare.getArriveTime());
                        tvArrivePort.setText(fare.getArrivePort());

                        textViewItinerary.setText(Utils.strTimeDiff(fare.getFlight().get(0).getDepartDatetime(), fare
                                .getFlight().get(fare.getFlight().size() - 1)
                                .getArriveDatetime(), fare.getFlight().get(0).getDepartTimezone() - fare.getFlight().get(fare.getFlight().size() - 1).getArriveTimezone()) + "  ( " + ((fare.getFlight().size() + fare.getHiddenTransitCount())==1 ? getResources().getString(R.string.direct) : (fare.getFlight().size() - 1 + fare.getHiddenTransitCount()) + " " + getResources().getString(R.string.transit)) + " )");


                        if (fare.getFareClass().equals("pro")) {
                            textViewFareClass.setText(getResources().getString(R.string.class_pro));
                            textViewFareClass.setTextColor(getResources().getColor(R.color.green_500));
                        } else if (fare.getFareClass().equals("eco")) {
                            textViewFareClass.setText(getResources().getString(R.string.class_eco));
                            textViewFareClass.setTextColor(getResources().getColor(R.color.blue_500));
                        } else if (fare.getFareClass().equals("bus")) {
                            textViewFareClass.setText(getResources().getString(R.string.class_bus));
                            textViewFareClass.setTextColor(getResources().getColor(R.color.orange_500));
                        }
                        
                        tvPrice1.setText(Utils.numberFormat((int)Math.floor(fare.getFare().get("adult").getTotal()/1000)));
                        tvPrice2.setText(Integer.toString(fare.getFare().get("adult").getTotal()).substring(Integer.toString(fare.getFare().get("adult").getTotal()).length() - 3));

                        if(fare.getFare().get("adult").getTotal() - fare.getFare().get("adult").getDiscount() > fare.getFare().get("adult").getTotal()) {
                            tvPriceBefore.setText(Utils.numberFormatCurrency(fare.getFare().get("adult").getTotal() - fare.getFare().get("adult").getDiscount()));
                            tvPriceBefore.setPaintFlags(tvPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            tvPriceBefore.setVisibility(View.VISIBLE);
                        }
                        else{
                            tvPriceBefore.setVisibility(View.GONE);
                        }
                        
                        
                        
                        AppHelper.departChoice = gson.toJson(listViewDepart.getAdapter().getItem(position));
                    }
                }
            }
            else {
                if (view.getId() == listViewReturn.findViewById(R.id.commonInfoLayout).getId()) {
                    for (FareRoute fare : fareRouteListReturn) {
                        fare.setSelected(false);
                    }
                    ((FareRoute) listViewReturn.getAdapter().getItem(position)).setSelected(true);
                    adapterReturn.updateDataSetChanged();
                }
                else if (view.getId() == listViewReturn.findViewById(R.id.chooseButton).getId()) {
                    Gson gson = new Gson();
                    Intent intent = new Intent(this, FlightResultSummaryActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("depart_choice", AppHelper.departChoice);
                    bundle.putString("return_choice", gson.toJson(listViewReturn.getAdapter().getItem(position)));
                    bundle.putString("type", parameter.getString("type"));
                    bundle.putBoolean("is_international", parameter.getBoolean("is_international"));
                    bundle.putInt("adult", parameter.getInt("adult"));
                    bundle.putInt("child", parameter.getInt("child"));
                    bundle.putInt("infant", parameter.getInt("infant"));
                    intent.putExtra("parameter", bundle);
                    startActivity(intent);
                }
            }
    }
}
