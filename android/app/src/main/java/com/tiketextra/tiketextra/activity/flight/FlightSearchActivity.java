package com.tiketextra.tiketextra.activity.flight;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.format.ArrayWeekDayFormatter;
import com.prolificinteractive.materialcalendarview.format.MonthArrayTitleFormatter;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity.MenuOnlineActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.helper.HighlightWeekendsDecorator;
import com.tiketextra.tiketextra.helper.SelectionDecorator;
import com.tiketextra.tiketextra.model.AirlineModel;
import com.tiketextra.tiketextra.object.Airline;
import com.tiketextra.tiketextra.object.Carrier;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Airport;
import com.tiketextra.tiketextra.service.GPSTracker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
public class FlightSearchActivity extends BaseActivity {

    private String from, to;
    private int adult = 1, child = 0, infant = 0;
    private String calendarMode = "go";
    private MaterialCalendarView calendarWidget;
    private Date goDate;
    private Date backDate;
    private boolean isReturn = false;


    private ImageView adultIncreaseButton;
    private ImageView adultDecreaseButton;
    private ImageView childIncreaseButton;
    private ImageView childDecreaseButton;
    private ImageView infantIncreaseButton;
    private ImageView infantDecreaseButton;
    private TextView adultCountTextView;
    private TextView childCountTextView;
    private TextView infantCountTextView;
    private String[] sCarriers;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFlightSearch);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.flight_search);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }
        adultIncreaseButton = findViewById(R.id.adultIncreaseButton);
        adultDecreaseButton = findViewById(R.id.adultDecreaseButton);
        childIncreaseButton = findViewById(R.id.childIncreaseButton);
        childDecreaseButton = findViewById(R.id.childDecreaseButton);
        infantIncreaseButton = findViewById(R.id.infantIncreaseButton);
        infantDecreaseButton = findViewById(R.id.infantDecreaseButton);
        adultCountTextView = findViewById(R.id.adultCountTextView);
        childCountTextView = findViewById(R.id.childCountTextView);
        infantCountTextView = findViewById(R.id.infantCountTextView);



        calendarWidget = findViewById(R.id.calendarView);


        SettingModel settingModel = new SettingModel(this);
        this.from=settingModel.getValue("search_from_port");
        this.to=settingModel.getValue("search_to_port");
        initFromToPort();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Dialog mDialog = new Dialog(FlightSearchActivity.this, R.style.CustomDialogTheme);

                mDialog.setContentView(R.layout.dialog_info);
                mDialog.setCancelable(true);
                TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
                TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

                mDialogHeader.setText(R.string.get_location);
                mDialogText.setText(R.string.get_nearest_airport);

                mDialogOKButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });

                mDialog.show();
                getAirportRoute(mDialog);
                initFromToPort();
//                finish();
            }
        }, 0);




        findViewById(R.id.buttonFromPort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FlightSearchActivity.this, ChooseAirportActivity.class);
                i.putExtra("airportType", "fromPort");
                startActivityForResult(i, 1);
            }
        });

        findViewById(R.id.buttonToPort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FlightSearchActivity.this, ChooseAirportActivity.class);
                i.putExtra("airportType", "toPort");
                startActivityForResult(i, 2);
            }
        });

        findViewById(R.id.buttonRevert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String temp = from;
                from = to;
                to = temp;
                initFromToPort();
            }
        });



        findViewById(R.id.buttonOneway).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOneway();

            }
        });

        findViewById(R.id.buttonRoundtrip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickRoundtrip();
            }
        });

        initDate(Configuration.DEPART_EXTRA, Configuration.RETURN_EXTRA_FROM_DEPART);

        findViewById(R.id.buttonDepartDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarMode = "go";
                showCalendar();
            }
        });

        findViewById(R.id.buttonReturnDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarMode = "back";
                showCalendar();
            }
        });

        calendarWidget.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                calendarWidget.removeDecorators();
                Log.e("date", date.toString());
                Log.e("date", date.getDate().toString());
                if (calendarMode.equals("go")) {

                    ArrayList<CalendarDay> goSelectedDay = new ArrayList<>();
                    goSelectedDay.add(date);

                    SelectionDecorator goSelectedDecorator = new SelectionDecorator(FlightSearchActivity.this, SelectionDecorator.LEFT_DECORATOR, goSelectedDay);
                    calendarWidget.addDecorator(goSelectedDecorator);

                    goDate = date.getDate();
                    if (backDate.compareTo(date.getDate()) < 0) {
                        backDate = goDate;
                    }
                    if (isReturn) {
                        ArrayList<CalendarDay> rangeDays = Utils.getDates(goDate, backDate);

                        ArrayList<CalendarDay> backSelectedDay = new ArrayList<>();
                        backSelectedDay.add(CalendarDay.from(backDate));
                        SelectionDecorator rangeDecorator = new SelectionDecorator(FlightSearchActivity.this, SelectionDecorator.RANGE_DECORATOR, rangeDays);
                        SelectionDecorator backSelectedDecorator = new SelectionDecorator(FlightSearchActivity.this, SelectionDecorator.RIGHT_DECORATOR, backSelectedDay);
                        calendarWidget.addDecorator(rangeDecorator);
                        calendarWidget.addDecorator(backSelectedDecorator);
                    }
                    else{
                        clickOneway();
                    }
                }
                else{
                    calendarWidget.setDateSelected(backDate, false);
                    backDate = date.getDate();
                    if (goDate.compareTo(date.getDate()) > 0) {
                        goDate = backDate;
                    }

                    ArrayList<CalendarDay> rangeDays = Utils.getDates(goDate, backDate);
                    ArrayList<CalendarDay> goSelectedDay = new ArrayList<>();
                    goSelectedDay.add(CalendarDay.from(goDate));

                    ArrayList<CalendarDay> backSelectedDay = new ArrayList<>();
                    backSelectedDay.add(date);
                    SelectionDecorator rangeDecorator = new SelectionDecorator(FlightSearchActivity.this, SelectionDecorator.RANGE_DECORATOR,
                            rangeDays);
                    SelectionDecorator goSelectedDecorator = new SelectionDecorator(FlightSearchActivity.this, SelectionDecorator.LEFT_DECORATOR,
                            goSelectedDay);
                    SelectionDecorator backSelectedDecorator = new SelectionDecorator(FlightSearchActivity.this, SelectionDecorator.RIGHT_DECORATOR,
                            backSelectedDay);
                    calendarWidget.addDecorator(rangeDecorator);
                    calendarWidget.addDecorator(goSelectedDecorator);
                    calendarWidget.addDecorator(backSelectedDecorator);
                    clickRoundtrip();
                }
                addHolidayDecorator();
                initDate(Utils.getDateDifference(new Date(), goDate), Utils.getDateDifference(goDate, backDate));
                findViewById(R.id.calendarLayout).setVisibility(View.GONE);
            }
        });


        adultIncreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adult = Integer.parseInt(adultCountTextView.getText().toString());
                if(adult < Configuration.MAX_PASSENGER - child){
                    adult++;
                }
                if(adult==Configuration.MAX_PASSENGER - child){
//                    adultIncreaseButton.setVisibility(View.INVISIBLE);
//                    childIncreaseButton.setVisibility(View.INVISIBLE);
                }
                if(adult == 2){
//                    adultDecreaseButton.setVisibility(View.VISIBLE);
                }
                adultCountTextView.setText(Integer.toString(adult));

            }
        });

        adultDecreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adult = Integer.parseInt(adultCountTextView.getText().toString());
                if(adult==Configuration.MAX_PASSENGER - child){
//                    adultIncreaseButton.setVisibility(View.VISIBLE);
//                    childIncreaseButton.setVisibility(View.VISIBLE);
                }
                if(adult > 1){
                    if(adult==infant){
                        infant--;
                        infantCountTextView.setText(Integer.toString(infant));
                    }
                    adult--;

                }
                if(adult == 1){
//                    adultDecreaseButton.setVisibility(View.INVISIBLE);
                }
                adultCountTextView.setText(Integer.toString(adult));
            }
        });

        childIncreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                child = Integer.parseInt(childCountTextView.getText().toString());
                if(child < Configuration.MAX_PASSENGER - adult){
                    child++;
                }
                if(child==Configuration.MAX_PASSENGER - adult){
//                    adultIncreaseButton.setVisibility(View.INVISIBLE);
//                    childIncreaseButton.setVisibility(View.INVISIBLE);
                }
                if(child == 1){
//                    childDecreaseButton.setVisibility(View.VISIBLE);
                }
                childCountTextView.setText(Integer.toString(child));
            }
        });

        childDecreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                child = Integer.parseInt(childCountTextView.getText().toString());
                if(child==Configuration.MAX_PASSENGER - adult){
//                    infantIncreaseButton.setVisibility(View.VISIBLE);
//                    childIncreaseButton.setVisibility(View.VISIBLE);
                }
                if(child > 0){
                    child--;
                }
                if(child == 0){
//                    childDecreaseButton.setVisibility(View.INVISIBLE);
                }
                childCountTextView.setText(Integer.toString(child));
            }
        });

        infantIncreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infant = Integer.parseInt(infantCountTextView.getText().toString());
                if(infant < adult){
                    infant++;
                }
                if(infant==adult){
//                    infantIncreaseButton.setVisibility(View.INVISIBLE);
                }
                if(infant == 0){
//                    infantDecreaseButton.setVisibility(View.VISIBLE);
                }
                infantCountTextView.setText(Integer.toString(infant));
            }
        });

        infantDecreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infant = Integer.parseInt(infantCountTextView.getText().toString());
                if(infant==adult){
//                    infantIncreaseButton.setVisibility(View.VISIBLE);
                }
                if(infant > 0){
                    infant--;
                }
                if(infant == 0){
//                    infantDecreaseButton.setVisibility(View.INVISIBLE);
                }
                infantCountTextView.setText(Integer.toString(infant));
            }
        });

        final AirlineModel airlineModel = new AirlineModel(this);
        ArrayList<Carrier> carriers = airlineModel.getAllAirlineCarriers();

        sCarriers = new String[carriers.size() + 1];
        sCarriers[0] = getString(R.string.all_airline);
        for (int i=0; i<carriers.size(); i++){
            sCarriers[i+1] = carriers.get(i).getName();
        }

        ArrayAdapter<String> spinnerCarrierArrayAdapter = new ArrayAdapter<>(this, R.layout.my_spinner_item, sCarriers);
        spinnerCarrierArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        ((Spinner) findViewById(R.id.spinnerCarrier)).setAdapter(spinnerCarrierArrayAdapter);

        findViewById(R.id.buttonFlightSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AirportModel airportModel = new AirportModel(getApplicationContext());
                if(!airportModel.existAirport(from)){
                    dialogValidation(R.string.wrong_form_filling, R.string.from_airport_empty);
                }
                else{
                    if(!airportModel.existAirport(to)){
                        dialogValidation(R.string.wrong_form_filling, R.string.to_airport_empty);
                    }
                    else {
                        if (from.equals(to)) {
                            dialogValidation(R.string.wrong_form_filling, R.string.from_to_airport_match);
                        }
                        else{


                            AppHelper.departSelectedAirline = new ArrayList<>();
                            AppHelper.returnSelectedAirline = new ArrayList<>();

                            AppHelper.departSelectedAirlineAtFlightForm = new ArrayList<>();
                            AppHelper.returnSelectedAirlineAtFlightForm = new ArrayList<>();

                            if(((Spinner) findViewById(R.id.spinnerCarrier)).getSelectedItemPosition() > 0){
                                String carrierName = sCarriers[((Spinner) findViewById(R.id.spinnerCarrier)).getSelectedItemPosition()];
                                ArrayList<Airline> airlines = airlineModel.getAirlineExcludeCarrierID(airlineModel.getCarrierByName(carrierName).getCarrierID());
//                                ArrayList<Airline> currAirlines = airlineModel.get
                                for(int i=0; i<airlines.size(); i++){
                                    AppHelper.departSelectedAirline.add(airlines.get(i).getFlightCode());
                                    AppHelper.departSelectedAirlineAtFlightForm.add(airlines.get(i).getFlightCode());
                                    if(isReturn){
                                        AppHelper.returnSelectedAirline.add(airlines.get(i).getFlightCode());
                                        AppHelper.returnSelectedAirlineAtFlightForm.add(airlines.get(i).getFlightCode());
                                    }
                                }
                            }

                            SettingModel settingModel = new SettingModel(getApplicationContext());
                            settingModel.setValue("flight_search_timestamp", Long.toString(System.currentTimeMillis()));


                            settingModel.setValue("search_from_port", from);
                            settingModel.setValue("search_to_port", to);



                            Intent i = new Intent(FlightSearchActivity.this, FlightResultActivity.class);
                            Bundle bundle = new Bundle();
                            Airport fromAirport = airportModel.getAirport(from);
                            Airport toAirport = airportModel.getAirport(to);

                            airportModel.updateAirportHit(from, fromAirport.getHit()+1);
                            airportModel.updateAirportHit(to, toAirport.getHit()+1);

                            if(fromAirport.getCountryCode().equals(Configuration.COUNTRY_BASE)){
                                bundle.putBoolean("is_international", !(toAirport.getCountryCode().equals(Configuration.COUNTRY_BASE)));
                            }
                            else{
                                bundle.putBoolean("is_international", true);
                            }

                            bundle.putString("from", from);
                            bundle.putString("to", to);
                            bundle.putString("depart", new SimpleDateFormat("yyyy-MM-dd").format(goDate.getTime()));
                            bundle.putString("return", isReturn?new SimpleDateFormat("yyyy-MM-dd").format(backDate.getTime()):"");
                            bundle.putString("type", isReturn?"roundtrip":"oneway");
                            bundle.putInt("adult", adult);
                            bundle.putInt("child", child);
                            bundle.putInt("infant", infant);
                            i.putExtra("parameter", bundle);
                            startActivity(i);



                        }
                    }
                }
            }
        });
    }

    private void showCalendar(){
        findViewById(R.id.calendarLayout).setVisibility(View.VISIBLE);
    }

    private void addHolidayDecorator() {
        calendarWidget.addDecorator(new HighlightWeekendsDecorator());
    }

    private void initDate(int departExtra, int returnExtra) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        calendarWidget.state().edit().setMinimumDate(cal).commit();
        calendarWidget.setTitleFormatter(new MonthArrayTitleFormatter(getResources().getTextArray(R.array.custom_months)));
        calendarWidget.setWeekDayFormatter(new ArrayWeekDayFormatter(getResources().getTextArray(R.array.custom_weekdays)));

        addHolidayDecorator();

        cal.add(Calendar.DAY_OF_MONTH, departExtra);
        goDate = cal.getTime();
        calendarWidget.setDateSelected(goDate, true);

        ((TextView) findViewById(R.id.textDayDepart)).setText(Utils.getDayName(cal.get(Calendar.DAY_OF_WEEK), getApplicationContext()));
        ((TextView) findViewById(R.id.textDateMonthDepart)).setText(cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getUcMonthShortName(cal.get(Calendar.MONTH), getApplicationContext()));
        ((TextView) findViewById(R.id.textYearDepart)).setText(cal.get(Calendar.YEAR)+"");

        cal.add(Calendar.DAY_OF_MONTH, returnExtra);
        backDate = cal.getTime();
        ((TextView) findViewById(R.id.textDayReturn)).setText(Utils.getDayName(cal.get(Calendar.DAY_OF_WEEK), getApplicationContext()));
        ((TextView) findViewById(R.id.textDateMonthReturn)).setText(cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getUcMonthShortName(cal.get(Calendar.MONTH), getApplicationContext()));
        ((TextView) findViewById(R.id.textYearReturn)).setText(cal.get(Calendar.YEAR)+"");

    }

    private void clickOneway(){

        isReturn = false;
//        ((TextView) findViewById(R.id.textViewOneway)).setTypeface(((TextView) findViewById(R.id.textViewOneway)).getTypeface(), Typeface.BOLD);
        ((TextView) findViewById(R.id.textViewOneway)).setTextColor(getResources().getColor(R.color.grey_800));
        findViewById(R.id.buttonOneway).setBackground(getResources().getDrawable(R.drawable.capsule_active));
        findViewById(R.id.buttonOneway).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));

//        ((TextView) findViewById(R.id.textViewRoundtrip)).setTypeface(((TextView) findViewById(R.id.textViewRoundtrip)).getTypeface(), Typeface.NORMAL);
        ((TextView) findViewById(R.id.textViewRoundtrip)).setTextColor(getResources().getColor(R.color.grey_500));
        findViewById(R.id.buttonRoundtrip).setBackground(getResources().getDrawable(R.drawable.capsule_inactive));
        findViewById(R.id.buttonRoundtrip).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));


        ((TextView) findViewById(R.id.textViewReturn)).setTextColor(getResources().getColor(R.color.grey_500));
        ((TextView) findViewById(R.id.textDayReturn)).setTextColor(getResources().getColor(R.color.grey_500));
        ((TextView) findViewById(R.id.textDateMonthReturn)).setTextColor(getResources().getColor(R.color.grey_500));
        ((TextView) findViewById(R.id.textYearReturn)).setTextColor(getResources().getColor(R.color.grey_500));

        ((ImageView)findViewById(R.id.imageViewReturn)).setImageDrawable(getResources().getDrawable(R.drawable.ic_calendar_inactive));
    }

    private void clickRoundtrip(){

        isReturn = true;
//        ((TextView) findViewById(R.id.textViewOneway)).setTypeface(((TextView) findViewById(R.id.textViewOneway)).getTypeface(), Typeface.NORMAL);
        ((TextView) findViewById(R.id.textViewOneway)).setTextColor(getResources().getColor(R.color.grey_500));
        findViewById(R.id.buttonOneway).setBackground(getResources().getDrawable(R.drawable.capsule_inactive));
        findViewById(R.id.buttonOneway).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));

//        ((TextView) findViewById(R.id.textViewRoundtrip)).setTypeface(((TextView) findViewById(R.id.textViewRoundtrip)).getTypeface(), Typeface.BOLD);
        ((TextView) findViewById(R.id.textViewRoundtrip)).setTextColor(getResources().getColor(R.color.grey_800));
        findViewById(R.id.buttonRoundtrip).setBackground(getResources().getDrawable(R.drawable.capsule_active));
        findViewById(R.id.buttonRoundtrip).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));


        ((TextView) findViewById(R.id.textViewReturn)).setTextColor(getResources().getColor(R.color.grey_700));
        ((TextView) findViewById(R.id.textDayReturn)).setTextColor(getResources().getColor(R.color.grey_700));
        ((TextView) findViewById(R.id.textDateMonthReturn)).setTextColor(getResources().getColor(R.color.grey_800));
        ((TextView) findViewById(R.id.textYearReturn)).setTextColor(getResources().getColor(R.color.grey_700));

        ((ImageView)findViewById(R.id.imageViewReturn)).setImageDrawable(getResources().getDrawable(R.drawable.ic_calendar_active));

    }

    private void initFromToPort(){
        AirportModel airportModel = new AirportModel(this);
        Airport fromAirport = airportModel.getAirport(from);
        Airport toAirport = airportModel.getAirport(to);
        ((TextView) findViewById(R.id.textFromPort)).setText(fromAirport.getIataCode());
        ((TextView) findViewById(R.id.textToPort)).setText(toAirport.getIataCode());
        ((TextView) findViewById(R.id.textFromCity)).setText((Configuration.COUNTRY_BASE.equals(fromAirport.getCountryCode())?fromAirport.getCity():fromAirport.getCity()+" - "+fromAirport.getCountry()));
        ((TextView) findViewById(R.id.textToCity)).setText((Configuration.COUNTRY_BASE.equals(toAirport.getCountryCode())?toAirport.getCity():toAirport.getCity()+" - "+toAirport.getCountry()));

    }

    private void dialogValidation(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                if (data.getStringExtra("iataCode").equals(to)) {
                    dialogValidation(R.string.wrong_form_filling, R.string.from_to_airport_match);
                }
                else {
                    this.from = data.getStringExtra("iataCode");
                    initFromToPort();
                }
            }
        }
        else if(requestCode==2){
            if(resultCode==RESULT_OK){
                if (from.equals(data.getStringExtra("iataCode"))) {
                    dialogValidation(R.string.wrong_form_filling, R.string.from_to_airport_match);
                }
                else {
                    this.to = data.getStringExtra("iataCode");
                    initFromToPort();
                }
            }
        }
    }

    private void getAirportRoute(Dialog mDialog){
        SettingModel settingModel = new SettingModel(this);
        AirportModel airportModel = new AirportModel(this);
        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()){
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            if(latitude==0 && longitude==0){
                this.from=settingModel.getValue("search_from_port");
                this.to=settingModel.getValue("search_to_port");
            }
            else{
                this.from = Utils.getNearestAirport(latitude, longitude, airportModel.getAllAirports("hit"));
                if(this.from.equals(settingModel.getValue("search_to_port"))){
                    this.to = settingModel.getValue("search_from_port");
                }
                else{
                    this.to = settingModel.getValue("search_to_port");
                }

            }
//            Log.e("latitude", latitude+"");
//            Log.e("longitude", longitude+"");
        }
        else{
            this.from=settingModel.getValue("search_from_port");
            this.to=settingModel.getValue("search_to_port");
        }
        mDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.calendarLayout).getVisibility() == View.VISIBLE) {
            findViewById(R.id.calendarLayout).setVisibility(View.GONE);
        }  else {
            Intent i = new Intent(this, com.tiketextra.tiketextra.MainActivity.class);
            // i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
            //startActivity(new Intent(this, MenuOnlineActivity.class));
        }
    }
}
