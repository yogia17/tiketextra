package com.tiketextra.tiketextra.activity.flight;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;

public class FlightSortActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_sort);
        Toolbar toolbar = findViewById(R.id.toolbarSort);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(getString(R.string.sort)+" "+getString(getIntent().getStringExtra("mode").equals(Configuration.MODE_DEPART)?R.string.depart_flight:R.string.return_flight));


        RadioGroup pointsRadioGroup = findViewById(R.id.points_radio_group);

        switch (getIntent().getStringExtra("mode")){
            case Configuration.MODE_DEPART:{
                switch (AppHelper.departSort){
                    case Configuration.SORT_LOWEST_PRICE:{
                        pointsRadioGroup.check(R.id.lowestPriceRadioButton);
                        break;
                    }
                    case Configuration.SORT_DEPART_TIME:{
                        pointsRadioGroup.check(R.id.departTimeRadioButton);
                        break;
                    }
                    case Configuration.SORT_ARRIVE_TIME:{
                        pointsRadioGroup.check(R.id.arriveTimeRadioButton);
                        break;
                    }
                    case Configuration.SORT_DURATION:{
                        pointsRadioGroup.check(R.id.durationRadioButton);
                        break;
                    }
                    case Configuration.SORT_CABIN_CLASS:{
                        pointsRadioGroup.check(R.id.cabinClassRadioButton);
                        break;
                    }
                }
                break;
            }
            case Configuration.MODE_RETURN:{
                switch (AppHelper.returnSort){
                    case Configuration.SORT_LOWEST_PRICE:{
                        pointsRadioGroup.check(R.id.lowestPriceRadioButton);
                        break;
                    }
                    case Configuration.SORT_DEPART_TIME:{
                        pointsRadioGroup.check(R.id.departTimeRadioButton);
                        break;
                    }
                    case Configuration.SORT_ARRIVE_TIME:{
                        pointsRadioGroup.check(R.id.arriveTimeRadioButton);
                        break;
                    }
                    case Configuration.SORT_DURATION:{
                        pointsRadioGroup.check(R.id.durationRadioButton);
                        break;
                    }
                    case Configuration.SORT_CABIN_CLASS:{
                        pointsRadioGroup.check(R.id.cabinClassRadioButton);
                        break;
                    }
                }
                break;
            }
        }


        pointsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                View radioButton = radioGroup.findViewById(i);
                int idx = radioGroup.indexOfChild(radioButton);
                switch (getIntent().getStringExtra("mode")) {
                    case Configuration.MODE_DEPART: {
                        AppHelper.departSort = idx;
                        break;
                    }
                    case Configuration.MODE_RETURN: {
                        AppHelper.returnSort = idx;
                        break;
                    }
                }


            }
        });

        findViewById(R.id.buttonApplySort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FlightResultActivity.class);
                FlightSortActivity.this.setResult(Activity.RESULT_OK, intent);
                FlightSortActivity.this.finish();
            }
        });
    }
}
