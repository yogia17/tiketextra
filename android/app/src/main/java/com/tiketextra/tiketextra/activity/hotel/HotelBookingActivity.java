package com.tiketextra.tiketextra.activity.hotel;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Contact;
import com.tiketextra.tiketextra.object.Fare;
import com.tiketextra.tiketextra.object.Hotel;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.object.Room;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HotelBookingActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_booking);

        bindLogo();

        final Gson gson = new Gson();
        Contact contact = gson.fromJson(getIntent().getStringExtra("contact"), Contact.class);
        Hotel hotel = gson.fromJson(getIntent().getStringExtra("hotel_detail"), Hotel.class);
        Room room = gson.fromJson(getIntent().getStringExtra("room_detail"), Room.class);

        final Map<String, String> params = new HashMap<>();
        params.put("auth_mode", isLoggedIn() ? "_auth" : "");
        params.put("contact_phone", contact.getPhone());
        params.put("contact_email", contact.getEmail());
        params.put("contact_name", contact.getFullName());
        params.put("contact_title", contact.getTitle());
        params.put("contact_city", contact.getCity());
        params.put("biller_id", hotel.getBillerID());
        params.put("hotel_id", hotel.getFpID());
        params.put("mid", getIntent().getStringExtra("mid"));
        params.put("room", getIntent().getStringExtra("room"));
        params.put("checkin_date", getIntent().getStringExtra("checkin_date"));
        params.put("checkout_date", getIntent().getStringExtra("checkout_date"));
        params.put("internal_code", room.getInternalCode());
        params.put("category_id", room.getCategoryID());
        params.put("type_name", room.getName());
        params.put("price", room.getTotalPrice()+"");
        params.put("hotel_name", hotel.getName());
        params.put("hotel_rating", hotel.getRating()+"");
        params.put("hotel_address", hotel.getAddress());
        params.put("hotel_image", hotel.getImage());
        params.put("hotel_region", hotel.getRegion());
        params.put("breakfast", room.isBreakfast() ? "1" : "0");
        params.put("room_policy", room.getPolicy());


//        Log.e("auth_mode", isLoggedIn() ? "_auth" : "");
//        Log.e("contact_phone", contact.getPhone());
//        Log.e("contact_email", contact.getEmail());
//        Log.e("contact_name", contact.getFullName());
//        Log.e("contact_title", contact.getTitle());
//        Log.e("contact_city", contact.getCity());
//        Log.e("biller_id", hotel.getBillerID());
//        Log.e("hotel_id", hotel.getFpID());
//        Log.e("mid", getIntent().getStringExtra("mid"));
//        Log.e("room", getIntent().getStringExtra("room"));
//        Log.e("checkin_date", getIntent().getStringExtra("checkin_date"));
//        Log.e("checkout_date", getIntent().getStringExtra("checkout_date"));
//        Log.e("internal_code", room.getInternalCode());
//        Log.e("category_id", room.getCategoryID());
//        Log.e("type_name", room.getName());
//        Log.e("price", room.getTotalPrice()+"");
//        Log.e("hotel_name", hotel.getName());
//        Log.e("hotel_rating", hotel.getRating()+"");
//        Log.e("hotel_address", hotel.getAddress());
//        Log.e("hotel_image", hotel.getImage());
//        Log.e("hotel_region", hotel.getRegion());
//        Log.e("breakfast", room.isBreakfast() ? "1" : "0");
//        Log.e("room_policy", room.getPolicy());



        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "hotel/booking", params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
//                Log.e("response", response.toString());
                try {
                    int status = response.getInt("status");
                    if(status == 0) {
                        dialogErrorAPI(response.getString("message"));
                    }
                    else {
                        ReservationModel reservationModel = new ReservationModel(HotelBookingActivity.this);
                        Reservation reservation = gson.fromJson(response.getJSONObject("reservation").toString(), Reservation.class);
                        reservationModel.insertReservationObject(reservation);

                        JSONArray passengers = response.getJSONArray("passenger");
                        for (int i = 0; i < passengers.length(); i++) {
                            Passenger passenger = gson.fromJson(passengers.get(i).toString(), Passenger.class);
                            reservationModel.insertPassengerObject(passenger);
                        }
                        JSONArray fares = response.getJSONArray("fare");
                        for (int i = 0; i < fares.length(); i++) {
                            Fare fare = gson.fromJson(fares.get(i).toString(), Fare.class);
                            reservationModel.insertFareObject(fare);
                        }

                        ArrayList<Bank> banks = new ArrayList<>();
                        JSONArray bank = response.getJSONArray("bank");
                        for (int i = 0; i < bank.length(); i++) {
                            banks.add(gson.fromJson(bank.get(i).toString(), Bank.class));
                        }

                        Intent intent = new Intent(HotelBookingActivity.this, HotelPaymentActivity.class);
                        intent.putExtra("reservation_id", reservation.getReservationID());
//                        Log.e("reservation_id", ""+reservation.getReservationID());
//                        Log.e("reservation", reservation.toString());
                        intent.putExtra("bank", gson.toJson(banks));
                        startActivity(intent);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogErrorConnection();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                dialogErrorConnection();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private void bindLogo(){
        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.imageViewSplashLogo);
        final AlphaAnimation animation1 = new AlphaAnimation(0.6f, 1.0f);
        animation1.setDuration(700);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.6f);
        animation2.setDuration(700);
        //animation1 AnimationListener
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                splash.startAnimation(animation2);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                splash.startAnimation(animation1);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        splash.startAnimation(animation2);
    }

    private void dialogErrorAPI(String message){
        Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(R.string.error_occurred);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HotelBookingActivity.this, HotelSearchActivity.class));
            }
        });

        mDialog.show();
    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialog(int title, int message){
        Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HotelBookingActivity.this, HotelSearchActivity.class));
            }
        });

        mDialog.show();

    }
}
