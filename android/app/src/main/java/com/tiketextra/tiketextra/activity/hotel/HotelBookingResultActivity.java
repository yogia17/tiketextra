package com.tiketextra.tiketextra.activity.hotel;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONException;

public class HotelBookingResultActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_summary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarResultSummary);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.my_booking);

        ReservationModel reservationModel = new ReservationModel(this);
        final Reservation reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));
        ((TextView) findViewById(R.id.textViewName)).setText(reservation.getContactName());
        ((TextView) findViewById(R.id.textViewEmail)).setText(reservation.getContactEmail());
        ((TextView) findViewById(R.id.textViewPhone)).setText(reservation.getContactPhone());

        try {
            ((TextView) findViewById(R.id.textViewHotelName)).setText(reservation.getHotel().getName());
            RatingBar ratingBar = findViewById(R.id.ratingBar);
            if(reservation.getHotel().getRating() > 0) {
                ratingBar.setRating(reservation.getHotel().getRating());
                LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
                stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.grey_100), PorterDuff.Mode.SRC_ATOP);
            }
            else {
                ratingBar.setVisibility(View.GONE);
            }
            ((TextView) findViewById(R.id.textViewAddress)).setText(reservation.getHotel().getAddress());
            ((TextView) findViewById(R.id.textViewBookingCode)).setText(reservation.getBookingCode1());

            ((TextView) findViewById(R.id.textViewCheckinDate)).setText(Utils.mediumDate(reservation.getCheckinDate(), getApplicationContext()));
            ((TextView) findViewById(R.id.textViewCheckoutDate)).setText(Utils.mediumDate(reservation.getCheckoutDate(), getApplicationContext()));
            int dateDiff = Utils.dateDiff(reservation.getCheckinDate(), reservation.getCheckoutDate());
            ((TextView) findViewById(R.id.textViewDuration)).setText(dateDiff + " " + getString(dateDiff > 1 ? R.string.nights : R.string.night));
            ((TextView) findViewById(R.id.textViewNumberRooms)).setText(reservation.getNumberRooms() + " " + getString(reservation.getNumberRooms() > 1 ? R.string.rooms : R.string.room));

            ((TextView) findViewById(R.id.textViewRoomName)).setText(reservation.getRoom().getName());
            if(reservation.getRoom().isBreakfast()){
                findViewById(R.id.layoutBreakfast).setVisibility(View.VISIBLE);
                findViewById(R.id.layoutNotBreakfast).setVisibility(View.GONE);
            }
            else {
                findViewById(R.id.layoutBreakfast).setVisibility(View.GONE);
                findViewById(R.id.layoutNotBreakfast).setVisibility(View.VISIBLE);
            }
            if(reservation.getRoom().getPolicy().equals("")){
                findViewById(R.id.textViewPolicy).setVisibility(View.GONE);
            }
            else {
                findViewById(R.id.textViewPolicy).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.textViewPolicy)).setText(reservation.getRoom().getPolicy());
            }

            ((TextView) findViewById(R.id.textViewRoom)).setText(reservation.getRoom().getName());
            ((TextView) findViewById(R.id.textViewHotelItinerary)).setText(reservation.getHotel().getName());
            ((TextView) findViewById(R.id.textViewItinerary)).setText(Utils.mediumDate(reservation.getCheckinDate(), this) + ", " + dateDiff + " " + getString(dateDiff > 1 ? R.string.nights : R.string.night) + ", " + reservation.getNumberRooms() + " " + (getString(reservation.getNumberRooms() > 1 ? R.string.rooms : R.string.room)));

            if(reservation.getTotalDiscount() < 0){
                findViewById(R.id.discountFareLayout).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.discountFareTextView)).setText(Utils.numberFormatCurrency(reservation.getTotalDiscount()));
            }

            int total = reservation.getTotalFare();
            ((TextView) findViewById(R.id.fareTextView)).setText(Utils.numberFormatCurrency(total));
            
            if(reservation.getPromoAmount() != 0){
                findViewById(R.id.promoFareLayout).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.promoSummaryTextView)).setText(getString(R.string.promo)+" : "+reservation.getPromoCode());
                ((TextView) findViewById(R.id.promoFareTextView)).setText(Utils.numberFormatCurrency(reservation.getPromoAmount()));
//            if(!reservation.getIsConfirmed().equals("Y")) {
                total += reservation.getPromoAmount();
//            }
            }
            int totalFare = total;
            if(reservation.getAmount() > 0){
                total = reservation.getAmount();
            }

            if(totalFare-total != 0){
                findViewById(R.id.paymentChannelFareLayout).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.paymentChannelSummaryTextView)).setText(getString(total - totalFare < 0 ? R.string.bank_discount : R.string.transaction_fee));
                ((TextView) findViewById(R.id.paymentChannelFareTextView)).setText(Utils.numberFormatCurrency(total - totalFare));
            }

            ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / 1000)));
            ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - 3));


            if(reservation.getBank()==null){
                ((TextView) findViewById(R.id.buttonBookingText)).setText(R.string.finish_payment);
            }
            else{
                ((TextView) findViewById(R.id.buttonBookingText)).setText(R.string.back);
            }
            findViewById(R.id.buttonBooking).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(reservation.getBank()==null){
                        finish();
                    }
                    else{
                        startActivity(new Intent(HotelBookingResultActivity.this, TicketBookingActivity.class));
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
