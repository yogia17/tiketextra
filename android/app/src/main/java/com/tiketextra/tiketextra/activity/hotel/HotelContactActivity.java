package com.tiketextra.tiketextra.activity.hotel;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.model.ContactModel;
import com.tiketextra.tiketextra.object.Contact;
import com.tiketextra.tiketextra.object.Hotel;
import com.tiketextra.tiketextra.object.Room;
import com.tiketextra.tiketextra.util.Utils;

import java.util.ArrayList;

public class HotelContactActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarGuest);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.hotel_guest_form);

        Gson gson = new Gson();
        final Hotel hotel = gson.fromJson(getIntent().getStringExtra("hotel_detail"), Hotel.class);
        ((TextView) findViewById(R.id.textViewHotelName)).setText(hotel.getName());
        ((TextView) findViewById(R.id.textViewAddress)).setText(hotel.getAddress());

        ((TextView) findViewById(R.id.textViewCheckinDate)).setText(Utils.mediumDate(getIntent().getStringExtra("checkin_date"), getApplicationContext()));
        ((TextView) findViewById(R.id.textViewCheckoutDate)).setText(Utils.mediumDate(getIntent().getStringExtra("checkout_date"), getApplicationContext()));
        int dateDiff = Utils.dateDiff(getIntent().getStringExtra("checkin_date"), getIntent().getStringExtra("checkout_date"));
        ((TextView) findViewById(R.id.textViewDuration)).setText(dateDiff + " " + getString(dateDiff > 1 ? R.string.nights : R.string.night));
        int numberRooms = Integer.parseInt(getIntent().getStringExtra("room"));
        ((TextView) findViewById(R.id.textViewNumberRooms)).setText(numberRooms + " " + getString(numberRooms > 1 ? R.string.rooms : R.string.room));

        final Room room = gson.fromJson(getIntent().getStringExtra("room_detail"), Room.class);

        TextView tvName = findViewById(R.id.textViewRoomName);
        tvName.setText(room.getName());

        ImageView imageView = findViewById(R.id.imageRoom);
        if (room.getImage().toLowerCase().contains("default")) {
            imageView.setBackground(getResources().getDrawable(R.drawable.ic_menu_hotel));
        } else {
            ImageUtil.displayImage(imageView, room.getImage(), null);
        }
        TextView tvOccupancy = findViewById(R.id.textViewOccupancy);
        tvOccupancy.setText(room.getOccupancy() + " " + getResources().getString(room.getOccupancy() > 1 ? R.string.guests_per_room : R.string.guest_per_room));
        if (room.isBreakfast()) {
            findViewById(R.id.layoutBreakfast).setVisibility(View.VISIBLE);
            findViewById(R.id.layoutNotBreakfast).setVisibility(View.GONE);
        } else {
            findViewById(R.id.layoutBreakfast).setVisibility(View.GONE);
            findViewById(R.id.layoutNotBreakfast).setVisibility(View.VISIBLE);
        }

        TextView tvRoomLeft = findViewById(R.id.textViewRoomLeft);
        if (room.getAllotment() > 10) {
            tvRoomLeft.setVisibility(View.GONE);
        } else {
            tvRoomLeft.setVisibility(View.VISIBLE);
            tvRoomLeft.setText(room.getAllotment() + " " + getResources().getString(room.getAllotment() > 1 ? R.string.rooms_left : R.string.room_left));
        }
        TextView tvPrice = findViewById(R.id.textViewPrice);
        tvPrice.setText(Utils.numberFormatCurrency(room.getPrice() * dateDiff * numberRooms));

        if(room.getPolicy().equals("")){
            findViewById(R.id.layoutPolicy).setVisibility(View.GONE);
        }
        else {
            ((TextView) findViewById(R.id.textViewPolicy)).setText(room.getPolicy());
        }
        findViewById(R.id.buttonChangeRoom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ArrayList<String> contactNames = new ArrayList<>();
        final ContactModel contactModel = new ContactModel(this);
        final ArrayList<Contact> contacts = contactModel.getAllContacts();
        for (Contact contact: contacts) {
            contactNames.add(contact.getFullName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.my_autocomplete_item, contactNames);

        ArrayAdapter<String> spinnerTitle = new ArrayAdapter<>(this, R.layout.my_spinner_item, ItineraryHelper.getAdultTitle(getApplicationContext()));
        final Spinner contactTitle = findViewById(R.id.spinnerContactTitle);
        contactTitle.setAdapter(spinnerTitle);
        final AutoCompleteTextView nameEditText = (AutoCompleteTextView)findViewById(R.id.nameContactEditText);
        final EditText mobile = (EditText) findViewById(R.id.phoneEditText);
        final EditText email = (EditText) findViewById(R.id.emailEditText);
        final EditText city = (EditText) findViewById(R.id.cityEditText);
        if(isLoggedIn()){
            email.setText(getSessionEmail());
            email.setEnabled(false);
        }
        nameEditText.setAdapter(adapter);
        nameEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Contact cont = contactModel.getContactByName(Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")));
                contactTitle.setSelection(ItineraryHelper.gatAdultTitleIdx(cont.getTitle(), getApplicationContext()));
                mobile.setText(cont.getPhone());
                if(!isLoggedIn()){
                    email.setText(cont.getEmail());
                }
            }
        });

        findViewById(R.id.buttonSubmitPassenger).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean valid = true;



                if(city.getText().toString().isEmpty()){
                    city.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = false;
                    city.requestFocus();
                }
                else if(!city.getText().toString().matches("[a-z A-Z]+?")) {
                    city.setError(getResources().getString(R.string.passenger_name_validation));
                    valid = false;
                    city.requestFocus();
                }

                if(email.getText().toString().isEmpty()){
                    email.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = false;
                    email.requestFocus();
                }
                else if (!email.getText().toString().matches("^([a-z0-9\\+_\\-]+)(\\.[a-z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$")) {
                    email.setError(getResources().getString(R.string.passenger_email_validation));
                    valid = false;
                    email.requestFocus();
                }

                if(mobile.getText().toString().isEmpty()){
                    mobile.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = false;
                    mobile.requestFocus();
                }
                else if (!mobile.getText().toString().matches("^[0]{1}[1-9]{1}[0-9]{7,15}$")) {
                    mobile.setError(getResources().getString(R.string.passenger_mobile_validation));
                    valid = false;
                    mobile.requestFocus();
                }

                if(nameEditText.getText().toString().isEmpty()){
                    nameEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = false;
                    nameEditText.requestFocus();
                }
                else if(!nameEditText.getText().toString().matches("[a-z A-Z]+?")) {
                    nameEditText.setError(getResources().getString(R.string.passenger_name_validation));
                    valid = false;
                    nameEditText.requestFocus();
                }

                if(contactTitle.getSelectedItemPosition()==0){
                    valid = false;
                    ((TextView) contactTitle.getSelectedView()).setError(getResources().getString(R.string.passenger_empty_validation));
                    contactTitle.requestFocus();
                }

                if(valid){
                    final Dialog mDialog = new Dialog(HotelContactActivity.this, R.style.CustomDialogTheme);

                    mDialog.setContentView(R.layout.dialog_yes_no_question);
                    mDialog.setCancelable(true);
                    TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                    TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                    mDialogHeader.setText(R.string.continue_to_booking);
                    mDialogText.setText(R.string.msg_continue_to_booking);

                    ((TextView) mDialog.findViewById(R.id.dialog_info_ok)).setText(R.string.okay);
                    ((TextView) mDialog.findViewById(R.id.dialog_info_no)).setText(R.string.cancel);

                    mDialog.show();

                    mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialog.dismiss();
                            Gson gson = new Gson();
                            Intent intent = new Intent(HotelContactActivity.this, HotelBookingActivity.class);
                            Contact cont = contactModel.getContactByName(Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")));

                            if(cont == null){
                                contactModel.insert(ItineraryHelper.encodeAdultTitle(contactTitle.getSelectedItem().toString(), getApplicationContext()), Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobile.getText().toString(), email.getText().toString());
                            }
                            else{
                                contactModel.update(cont.getContactID(), ItineraryHelper.encodeAdultTitle(contactTitle.getSelectedItem().toString(), getApplicationContext()), Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobile.getText().toString(), email.getText().toString());

                            }
                            Contact contact = new Contact(ItineraryHelper.encodeAdultTitle(contactTitle.getSelectedItem().toString(), getApplicationContext()), Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobile.getText().toString(), email.getText().toString(), city.getText().toString());
                            intent.putExtra("contact", gson.toJson(contact));
                            intent.putExtra("room_detail", gson.toJson(room));
                            intent.putExtra("hotel_detail", gson.toJson(hotel));
                            intent.putExtra("room", getIntent().getStringExtra("room"));
                            intent.putExtra("mid", getIntent().getStringExtra("mid"));
                            intent.putExtra("checkin_date", getIntent().getStringExtra("checkin_date"));
                            intent.putExtra("checkout_date", getIntent().getStringExtra("checkout_date"));
                            startActivity(intent);
                        }
                    });

                    mDialog.findViewById(R.id.dialog_info_no).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialog.dismiss();
                        }
                    });

                }
            }
        });


    }
}
