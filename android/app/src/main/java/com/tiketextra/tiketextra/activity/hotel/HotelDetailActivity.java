package com.tiketextra.tiketextra.activity.hotel;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.RoomRecyclerViewAdapter;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.listener.RoomItemListener;
import com.tiketextra.tiketextra.object.Hotel;
import com.tiketextra.tiketextra.object.Room;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class HotelDetailActivity extends BaseActivity implements RoomItemListener {

    private Hotel hotel;
    private String mid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);

        Toolbar toolbar = findViewById(R.id.toolbarHotelResult);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final JSONObject jsonData;

        String data = getIntent().getStringExtra("data");
        try {
            jsonData = new JSONObject(data);
            Gson gson = new Gson();

            mid = jsonData.getString("mid");

//            this.hotel = new Hotel()

            this.hotel = gson.fromJson(getIntent().getStringExtra("hotel_detail"), Hotel.class);
            if(hotel.getFpID().equals("")){
                hotel.setFpID(jsonData.getString("hotel_id"));
            }
            hotel.setHotelID(jsonData.getString("hotel_id"));
            hotel.setName(jsonData.getString("name"));
            hotel.setRating(jsonData.getInt("rating"));
            hotel.setAddress(jsonData.getString("address"));

            ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(jsonData.getString("name"));
            int dateDiff = Utils.dateDiff(getIntent().getStringExtra("checkin_date"), getIntent().getStringExtra("checkout_date"));
            ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(Utils.mediumDate(getIntent().getStringExtra("checkin_date"), this) + ", " + dateDiff + " " + getString(dateDiff > 1 ? R.string.nights : R.string.night) + ", " + getIntent().getStringExtra("room") + " " + (getString(Integer.parseInt(getIntent().getStringExtra("room")) > 1 ? R.string.rooms : R.string.room)));
            findViewById(R.id.toolbarSubTitleTextView).setSelected(true);

            final JSONArray images = jsonData.getJSONArray("images");
            if(images.length() > 0){
                ImageUtil.displayImage((ImageView) findViewById(R.id.mainImageView), ((JSONObject) images.get(0)).getString("path"), null);
                for (int idx=0; idx<5; idx++){
                    final int i = idx;
                    if(i >= images.length()){
                        findViewById(getResources().getIdentifier("image"+(i+1)+"ImageView", "id", getPackageName())).setVisibility(View.GONE);
                        if(i==4){
                            findViewById(getResources().getIdentifier("image"+(i+1)+"CoverImageView", "id", getPackageName())).setVisibility(View.GONE);
                            findViewById(getResources().getIdentifier("imageViewAll", "id", getPackageName())).setVisibility(View.GONE);
                        }
                    }
                    else {
                        ImageUtil.displayImage((ImageView) findViewById(getResources().getIdentifier("image"+(i+1)+"ImageView", "id", getPackageName())), ((JSONObject) images.get(i)).getString("path"), null);
                        findViewById(getResources().getIdentifier("image"+(i+1)+"ImageView", "id", getPackageName())).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if(i < 4) {
                                    try {
                                        ImageUtil.displayImage((ImageView) findViewById(R.id.mainImageView), ((JSONObject) images.get(i)).getString("path"), null);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else {
                                    Intent intent = new Intent(HotelDetailActivity.this, HotelGalleryActivity.class);
                                    try {
                                        intent.putExtra("name", jsonData.getString("name"));
                                        intent.putExtra("images", images.toString());
                                        startActivity(intent);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }
                        });
                    }
                }
            }
            else {
                findViewById(R.id.pagerLayout).setVisibility(View.GONE);
            }

            ((TextView) findViewById(R.id.textViewName)).setText(jsonData.getString("name"));
            ((TextView) findViewById(R.id.textViewAddress)).setText(jsonData.getString("address"));
            RatingBar ratingBar = findViewById(R.id.ratingBar);
            if(jsonData.getInt("rating") > 0) {
                ratingBar.setRating(jsonData.getInt("rating"));
                LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
                stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            }
            else {
                ratingBar.setVisibility(View.GONE);
            }
            JSONArray facility = jsonData.getJSONArray("facility");
            String facilities = "";
            if(facility.length() > 0) {
                for (int i = 0; i < facility.length(); i++) {
                    facilities += ((JSONObject) facility.get(i)).getString("name") + ", ";
                }
                facilities = facilities.substring(0, facilities.length() - 2);
            }
            ((TextView) findViewById(R.id.textViewFacility)).setText(facilities);

            JSONArray rooms = jsonData.getJSONArray("rooms");
            ArrayList<Room> arrayListRoom = new ArrayList<>();
            for (int i=0; i<rooms.length(); i++){
                String policies = "";
                JSONArray policy = ((JSONObject) rooms.get(i)).getJSONArray("policy");
                if(policy.length() > 0) {
                    for (int j = 0; j < policy.length(); j++) {
                        policies += policy.getString(j) + "; ";
                    }
                    policies = policies.substring(0, policies.length() - 2);
                }
                arrayListRoom.add(new Room(((JSONObject) rooms.get(i)).getString("room_id"), ((JSONObject) rooms.get(i)).getString("category_id"), ((JSONObject) rooms.get(i)).getString("type_name"), ((JSONObject) rooms.get(i)).getString("image"),
                        ((JSONObject) rooms.get(i)).getString("biller_id"), ((JSONObject) rooms.get(i)).getString("internal_code"), policies, ((JSONObject) rooms.get(i)).getInt("occupancy"),
                        ((JSONObject) rooms.get(i)).getInt("allotment"), ((JSONObject) rooms.get(i)).getInt("price_before"), ((JSONObject) rooms.get(i)).getInt("price"), ((JSONObject) rooms.get(i)).getInt("total_price"),
                        ((JSONObject) rooms.get(i)).getInt("breakfast") == 1,((JSONObject) rooms.get(i)).getInt("bookable") == 1
                        ));
            }
            Collections.sort(arrayListRoom);
            RecyclerView recyclerView = findViewById(R.id.recyclerViewRoom);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            RoomRecyclerViewAdapter adapter = new RoomRecyclerViewAdapter(arrayListRoom, this, this);
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
//        startActivity(new Intent(HotelDetailActivity.this, HotelSearchActivity.class));
    }

    @Override
    public void onItemClick(Room item) {
        if(item.isBookable()) {
            Gson gson = new Gson();
            Intent intent = new Intent(HotelDetailActivity.this, HotelContactActivity.class);
            intent.putExtra("room_detail", gson.toJson(item));
            intent.putExtra("hotel_detail", gson.toJson(hotel));
            intent.putExtra("room", getIntent().getStringExtra("room"));
            intent.putExtra("checkin_date", getIntent().getStringExtra("checkin_date"));
            intent.putExtra("checkout_date", getIntent().getStringExtra("checkout_date"));
            intent.putExtra("mid", mid);
            startActivity(intent);
        }
    }
}
