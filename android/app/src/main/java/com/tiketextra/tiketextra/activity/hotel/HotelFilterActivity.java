package com.tiketextra.tiketextra.activity.hotel;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.util.Utils;

import org.florescu.android.rangeseekbar.RangeSeekBar;

public class HotelFilterActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_filter);
        Toolbar toolbar = findViewById(R.id.toolbarFilter);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(getString(R.string.filter_hotel));
        final TextView tvPriceMin = findViewById(R.id.minPriceTextView);
        final TextView tvPriceMax = findViewById(R.id.maxPriceTextView);
        RangeSeekBar rangeSeekBar = findViewById(R.id.rangeSeekbarPrice);

        final EditText editTextHotelName = findViewById(R.id.editTextHotelName);
        editTextHotelName.setText(AppHelper.hotelFilterName);

        editTextHotelName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                AppHelper.hotelFilterName = editTextHotelName.getText().toString();
            }
        });

        for (int r=0; r <= 5; r++){

            final int i = r;
            if(AppHelper.hotelFilterRating.contains(Integer.toString(i))) {

                CheckBox cbRating = findViewById(getResources().getIdentifier("checkBoxRating" + i, "id", getPackageName()));

                if (AppHelper.hotelSelectedRating.contains(Integer.toString(i))) {
                    cbRating.setChecked(true);
                } else {
                    cbRating.setChecked(false);
                }
                cbRating.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            AppHelper.hotelSelectedRating.add(Integer.toString(i));
                        } else {
                            AppHelper.hotelSelectedRating.remove(Integer.toString(i));
                        }
                    }
                });
            }
            else {
                LinearLayout layout = findViewById(getResources().getIdentifier("layoutRating" + i, "id", getPackageName()));
                layout.setVisibility(View.GONE);
            }
        }

        tvPriceMin.setText(Utils.numberFormat(AppHelper.hotelSelectedPriceMin));
        tvPriceMax.setText(Utils.numberFormat(AppHelper.hotelSelectedPriceMax));

        rangeSeekBar.setRangeValues(AppHelper.hotelPriceMin, AppHelper.hotelPriceMax);
        rangeSeekBar.setSelectedMinValue(AppHelper.hotelSelectedPriceMin);
        rangeSeekBar.setSelectedMaxValue(AppHelper.hotelSelectedPriceMax);

        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<Integer> bar, Integer minValue, Integer maxValue) {
                AppHelper.hotelSelectedPriceMin = minValue;
                AppHelper.hotelSelectedPriceMax = maxValue;

                tvPriceMin.setText(Utils.numberFormat(AppHelper.hotelSelectedPriceMin));
                tvPriceMax.setText(Utils.numberFormat(AppHelper.hotelSelectedPriceMax));
            }
        });

        findViewById(R.id.buttonApplyFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HotelResultActivity.class);
                HotelFilterActivity.this.setResult(Activity.RESULT_OK, intent);
                HotelFilterActivity.this.finish();
            }
        });

    }
}
