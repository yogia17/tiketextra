package com.tiketextra.tiketextra.activity.hotel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.ImageRecyclerViewAdapter;
import com.tiketextra.tiketextra.listener.ImageItemListener;
import com.tiketextra.tiketextra.object.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class HotelGalleryActivity extends BaseActivity implements ImageItemListener {

    private String hotelName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_gallery);

        Toolbar toolbar = findViewById(R.id.toolbarHotelGallery);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(R.string.photo_gallery);
        ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(this.hotelName = getIntent().getStringExtra("name"));
        findViewById(R.id.toolbarSubTitleTextView).setSelected(true);

        try {
            JSONArray images = new JSONArray(getIntent().getStringExtra("images"));

            ArrayList<Image> imagesList = new ArrayList<>();

            for (int i=0; i<images.length(); i++){
                imagesList.add(new Image(((JSONObject)images.get(i)).getString("value"), ((JSONObject)images.get(i)).getString("path")));
            }

            Collections.sort(imagesList);
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            ImageRecyclerViewAdapter adapter = new ImageRecyclerViewAdapter(imagesList, this, this);
            recyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemClick(Image item) {
        Intent intent = new Intent(HotelGalleryActivity.this, HotelImageActivity.class);
        intent.putExtra("name", item.getName());
        intent.putExtra("path", item.getPath());
        intent.putExtra("hotel", hotelName);
        startActivity(intent);
    }
}
