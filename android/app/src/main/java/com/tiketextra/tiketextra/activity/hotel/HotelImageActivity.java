package com.tiketextra.tiketextra.activity.hotel;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.helper.ImageUtil;

public class HotelImageActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_image);

        Toolbar toolbar = findViewById(R.id.toolbarHotelImage);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(getIntent().getStringExtra("name"));
        ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(getIntent().getStringExtra("hotel"));
        findViewById(R.id.toolbarSubTitleTextView).setSelected(true);

        ImageUtil.displayImage((ImageView) findViewById(R.id.imageView), getIntent().getStringExtra("path"), null);
    }
}
