package com.tiketextra.tiketextra.activity.hotel;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.user.LoginActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.HotelResultListAdapter;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.listener.OnViewClickListener;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Hotel;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HotelResultActivity extends BaseActivity implements OnViewClickListener {

    private Bundle parameter;
//    private String feed = "empty";
//    private Map<String, Integer> percentProgress = new HashMap<>();
//    private int counter = 0;
//
//    private ProgressBar progressSearching, progressSplash;
//
    private List<Hotel> hotelList = new ArrayList<>();
    private ListView listViewHotel;
    private HotelResultListAdapter adapter;
    private int versionCode;
//    private boolean showCarrierResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_result);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHotelResult);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppHelper.hotelFilterName = "";

        AppHelper.hotelFilterRating = new ArrayList<>();
        AppHelper.hotelSelectedRating = new ArrayList<>();
        AppHelper.hotelSort = AppHelper.hotelPriceMin = AppHelper.hotelPriceMax = AppHelper.hotelSelectedPriceMin = AppHelper.hotelSelectedPriceMax = 0;

        parameter = getIntent().getBundleExtra("parameter");

        ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(parameter.getString("value"));
        int dateDiff = Utils.dateDiff(parameter.getString("checkin_date"), parameter.getString("checkout_date"));
        ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(Utils.mediumDate(parameter.getString("checkin_date"), this) + ", " + dateDiff + " " + getString(dateDiff > 1 ? R.string.nights : R.string.night) + ", " + parameter.getString("room") + " " + (getString(Integer.parseInt(parameter.getString("room")) > 1 ? R.string.rooms : R.string.room)));
        findViewById(R.id.toolbarSubTitleTextView).setSelected(true);

        findViewById(R.id.toolbarTitleTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HotelResultActivity.this, HotelSearchActivity.class));
            }
        });

        findViewById(R.id.toolbarSubTitleTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HotelResultActivity.this, HotelSearchActivity.class));
            }
        });
        bindLogo();

        this.listViewHotel = findViewById(R.id.resultListHotel);
        this.listViewHotel.setVisibility(View.GONE);
        this.adapter = new HotelResultListAdapter(getLayoutInflater(), this.hotelList, this);
        this.listViewHotel.setAdapter(this.adapter);




        findViewById(R.id.buttonHotelSort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HotelResultActivity.this, HotelSortActivity.class);
                i.putExtra("mode", AppHelper.mode);
                startActivityForResult(i, 1);

            }
        });

        findViewById(R.id.buttonHotelFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HotelResultActivity.this, HotelFilterActivity.class);
                i.putExtra("mode", AppHelper.mode);
                startActivityForResult(i, 1);
            }
        });
        
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        SettingModel settingModel = new SettingModel(this);
        int campaign_count = Integer.parseInt(settingModel.getValue("member_campaign_count"));
        /*if(campaign_count < 2 && !isLoggedIn()){
            settingModel.setValue("member_campaign_count", Integer.toString(campaign_count+1));
            final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

            mDialog.setContentView(R.layout.dialog_yes_no_question);
            mDialog.setCancelable(true);
            TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
            TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

            mDialogHeader.setText(R.string.title_member_campaign);
            mDialogText.setText(R.string.msg_member_campaign);

            ((TextView) mDialog.findViewById(R.id.dialog_info_ok)).setText(R.string.okay_agree);
            ((TextView) mDialog.findViewById(R.id.dialog_info_no)).setText(R.string.later);

            mDialog.show();

            mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                    startActivity(new Intent(HotelResultActivity.this, LoginActivity.class));
                }
            });

            mDialog.findViewById(R.id.dialog_info_no).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                }
            });
        }*/

        findViewById(R.id.needUpdateLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
            }
        });


        findViewById(R.id.emptyResultLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HotelResultActivity.this, HotelSearchActivity.class));
            }
        });

        discover();
    }


    private void bindLogo(){
        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.imageViewSplashLogo);
        final AlphaAnimation animation1 = new AlphaAnimation(0.6f, 1.0f);
        animation1.setDuration(700);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.6f);
        animation2.setDuration(700);
        //animation1 AnimationListener
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                splash.startAnimation(animation2);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                splash.startAnimation(animation1);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation arg0) {}
        });

        splash.startAnimation(animation2);
    }

    private void discover(){
        Map<String, String> params = new HashMap<>();
        params.put("city_id", parameter.getString("key"));
        params.put("room", parameter.getString("room"));
        params.put("checkin_date", parameter.getString("checkin_date"));
        params.put("checkout_date", parameter.getString("checkout_date"));

        SessionManager sessionManager = new SessionManager(HotelResultActivity.this);
        params.put("auth_mode", sessionManager.isLoggedIn()?"_auth":"");
        params.put("version_code", Integer.toString(versionCode));

        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "hotel/find", params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                Log.e("JSON", response.toString());
                try {
                    String status = response.getString("status");
                    if(status.equals("1")){
                        JSONArray list_Hotel = response.getJSONArray("data");
                        findViewById(R.id.progressSplashLogo).setVisibility(View.GONE);
                        if(list_Hotel.length() > 0){
                            listViewHotel.setVisibility(View.VISIBLE);
                            updateTable(list_Hotel);
                        }
                        else {
                            findViewById(R.id.emptyResultLayout).setVisibility(View.VISIBLE);
                        }
                    }
                    else{
                        findViewById(R.id.progressSplashLogo).setVisibility(View.GONE);
                        if(status.equals("need_update")){
                            findViewById(R.id.needUpdateLayout).setVisibility(View.VISIBLE);
                        }
                        else{
                            findViewById(R.id.emptyResultLayout).setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    Toast.makeText(context, R.string.error_data_communication, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                Toast.makeText(HotelResultActivity.this, R.string.error_data_communication, Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private void updateTable(JSONArray list) throws JSONException{
        for (int i=0; i < list.length(); i++) {
            JSONObject hotel = (JSONObject) list.get(i);
            Hotel objHotel = new Hotel(hotel.getString("id"), hotel.getString("name"), hotel.getString("address"), parameter.getString("value"), hotel.getString("image"), hotel.getString("biller_id"), hotel.getString("fp_id"), hotel.getInt("rating"), hotel.getInt("price_before"), hotel.getInt("price"));

            if(!AppHelper.hotelSelectedRating.contains(Integer.toString(objHotel.getRating()))) {
                AppHelper.hotelSelectedRating.add(Integer.toString(objHotel.getRating()));
                AppHelper.hotelFilterRating.add(Integer.toString(objHotel.getRating()));
            }

            if(AppHelper.hotelPriceMin == 0 || objHotel.getPrice() < AppHelper.hotelPriceMin){
                AppHelper.hotelPriceMin = objHotel.getPrice();
                AppHelper.hotelPriceMin -= (AppHelper.hotelPriceMin % 10000);
                AppHelper.hotelSelectedPriceMin = AppHelper.hotelPriceMin;
            }

            if(AppHelper.hotelPriceMax == 0 || objHotel.getPrice() > AppHelper.hotelPriceMax){
                AppHelper.hotelPriceMax = objHotel.getPrice();
                AppHelper.hotelPriceMax += (AppHelper.hotelPriceMax % 10000);
                AppHelper.hotelSelectedPriceMax = AppHelper.hotelPriceMax;
            }

            hotelList.add(objHotel);
        }
        adapter.updateDataSetChanged();
    }

    @Override
    public void setOnViewClickListener(View view, int position) {
        final Hotel hotel = hotelList.get(position);
        listViewHotel.setVisibility(View.GONE);
        findViewById(R.id.progressSplashLogo).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.textViewLoadingHotel)).setText(R.string.please_wait);

        Map<String, String> params = new HashMap<>();
        params.put("hotel_id", hotel.getFpID());
        params.put("biller_id", hotel.getBillerID());
        params.put("room", parameter.getString("room"));
        params.put("checkin_date", parameter.getString("checkin_date"));
        params.put("checkout_date", parameter.getString("checkout_date"));

        SessionManager sessionManager = new SessionManager(HotelResultActivity.this);
        params.put("auth_mode", sessionManager.isLoggedIn()?"_auth":"");
        params.put("version_code", Integer.toString(versionCode));

        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "hotel/detail", params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                Log.e("JSON", response.toString());
                try {
                    String status = response.getString("status");
                    if(status.equals("1")){
                        Gson gson = new Gson();
                        Intent intent = new Intent(HotelResultActivity.this, HotelDetailActivity.class);
                        intent.putExtra("data", response.toString());
                        intent.putExtra("hotel_detail", gson.toJson(hotel));
                        intent.putExtra("room", parameter.getString("room"));
                        intent.putExtra("checkin_date", parameter.getString("checkin_date"));
                        intent.putExtra("checkout_date", parameter.getString("checkout_date"));
                        startActivity(intent);
                    }
                    else{
                        findViewById(R.id.progressSplashLogo).setVisibility(View.GONE);
                        if(status.equals("need_update")){
                            findViewById(R.id.needUpdateLayout).setVisibility(View.VISIBLE);
                        }
                        else{
                            findViewById(R.id.emptyResultLayout).setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    Toast.makeText(context, R.string.error_data_communication, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                Toast.makeText(HotelResultActivity.this, R.string.error_data_communication, Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    public void setOnViewClickListener(View view, int position, String mode) {

    }



    private void updateLayout(){
            for (Hotel fare : hotelList) {
                if(!AppHelper.hotelSelectedRating.contains(Integer.toString(fare.getRating()))){
                    fare.setFilterRating(true);
                }
                else {
                    fare.setFilterRating(false);
                }

                if(fare.getPrice() < AppHelper.hotelSelectedPriceMin || fare.getPrice() > AppHelper.hotelSelectedPriceMax){
                    fare.setFilterPrice(true);
                }
                else {
                    fare.setFilterPrice(false);
                }

                if(fare.getName().toLowerCase().indexOf(AppHelper.hotelFilterName.toLowerCase())==-1){
                    fare.setFilterName(true);
                }
                else {
                    fare.setFilterName(false);
                }
            }
            this.adapter.updateDataSetChanged();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            updateLayout();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if(hotelList.size() > 0) {
            findViewById(R.id.progressSplashLogo).setVisibility(View.GONE);
            listViewHotel.setVisibility(View.VISIBLE);
        }
        updateLayout();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
