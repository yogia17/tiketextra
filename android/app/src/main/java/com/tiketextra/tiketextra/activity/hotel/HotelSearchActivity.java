package com.tiketextra.tiketextra.activity.hotel;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.format.ArrayWeekDayFormatter;
import com.prolificinteractive.materialcalendarview.format.MonthArrayTitleFormatter;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity.MenuOnlineActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.HighlightWeekendsDecorator;
import com.tiketextra.tiketextra.helper.SelectionDecorator;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.object.Hotel;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HotelSearchActivity extends BaseActivity {

    private String calendarMode = "in";
    private MaterialCalendarView calendarWidget;
    private Date checkinDate;
    private Date checkoutDate;
    private TextView roomCountTextView;
    private TextView nightCountTextView;
    private String key, type, location, billerID;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHotelSearch);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.hotel_voucher);


        findViewById(R.id.buttonDestinationHotel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HotelSearchActivity.this, ChooseHotelActivity.class);
                startActivityForResult(i, 1);
            }
        });

        final ImageView roomIncreaseButton = findViewById(R.id.roomIncreaseButton);
        final ImageView roomDecreaseButton = findViewById(R.id.roomDecreaseButton);

        final ImageView nightIncreaseButton = findViewById(R.id.nightIncreaseButton);
        final ImageView nightDecreaseButton = findViewById(R.id.nightDecreaseButton);

        roomCountTextView = findViewById(R.id.roomCountTextView);
        nightCountTextView = findViewById(R.id.nightCountTextView);

        calendarWidget = findViewById(R.id.calendarView);
        initDate(Configuration.DEPART_EXTRA, Configuration.RETURN_EXTRA_FROM_DEPART);
        addCalendarDecorator();

        findViewById(R.id.buttonDepartDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarMode = "in";
                showCalendar();
            }
        });

        findViewById(R.id.buttonReturnDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarMode = "out";
                showCalendar();
            }
        });


        calendarWidget.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
//                Log.e("date", date.toString());
//                Log.e("date", date.getDate().toString());
                if (calendarMode.equals("in")) {

//                    ArrayList<CalendarDay> goSelectedDay = new ArrayList<>();
//                    goSelectedDay.add(date);
//
//                    SelectionDecorator goSelectedDecorator = new SelectionDecorator(HotelSearchActivity.this, SelectionDecorator.LEFT_DECORATOR, goSelectedDay);
//                    calendarWidget.addDecorator(goSelectedDecorator);

                    checkinDate = date.getDate();
                    if (checkoutDate.compareTo(date.getDate()) < 1) {
                        checkoutDate = Utils.addDays(checkinDate, 1);
                    }
//                        ArrayList<CalendarDay> rangeDays = Utils.getDates(checkinDate, checkoutDate);
//
//                        ArrayList<CalendarDay> backSelectedDay = new ArrayList<>();
//                        backSelectedDay.add(CalendarDay.from(checkoutDate));
//                        SelectionDecorator rangeDecorator = new SelectionDecorator(HotelSearchActivity.this, SelectionDecorator.RANGE_DECORATOR, rangeDays);
//                        SelectionDecorator backSelectedDecorator = new SelectionDecorator(HotelSearchActivity.this, SelectionDecorator.RIGHT_DECORATOR, backSelectedDay);
//                        calendarWidget.addDecorator(rangeDecorator);
//                        calendarWidget.addDecorator(backSelectedDecorator);

                }
                else{

                    calendarWidget.setDateSelected(checkoutDate, false);
                    checkoutDate = date.getDate();

                    if (checkinDate.compareTo(date.getDate()) > 0) {
                        Date now = new Date();
//                        Log.e("kondisi", "now "+checkoutDate.compareTo(now));
                        if(checkoutDate.compareTo(now) < 0){
                            checkoutDate = Utils.addDays(checkinDate, 1);
                            calendarWidget.setDateSelected(date, false);
                        }
                        //kalau di hari ini pakai model di bawah ini
//                        checkoutDate = Utils.addDays(checkinDate, 1);
                        else {
                            //kalau di atas hari ini pakai model di bawah ini
                            checkinDate = Utils.subtractDays(checkoutDate, 1);



                        }
                    }
//                    else {
//                    }
                }
                addCalendarDecorator();
                initDate(Utils.getDateDifference(new Date(), checkinDate), Utils.getDateDifference(checkinDate, checkoutDate));
                findViewById(R.id.calendarLayout).setVisibility(View.GONE);
                nightCountTextView.setText(Utils.getDateDifference(checkinDate, checkoutDate) + "");

            }
        });

        roomIncreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roomCountTextView.setText(""+((Integer.parseInt(roomCountTextView.getText().toString()))+1));
            }
        });

        roomDecreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(roomCountTextView.getText().toString()) > 1) {
                    roomCountTextView.setText("" + ((Integer.parseInt(roomCountTextView.getText().toString())) - 1));
                }
            }
        });

        nightIncreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarWidget.setDateSelected(checkoutDate, false);
                checkoutDate = Utils.addDays(checkoutDate, 1);
                nightCountTextView.setText(Utils.getDateDifference(checkinDate, checkoutDate) + "");
                initDate(Utils.getDateDifference(new Date(), checkinDate), Utils.getDateDifference(checkinDate, checkoutDate));
                addCalendarDecorator();
            }
        });

        nightDecreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkinDate.compareTo(Utils.subtractDays(checkoutDate, 1)) < 0) {
                    calendarWidget.setDateSelected(checkoutDate, false);
                    checkoutDate = Utils.subtractDays(checkoutDate, 1);
                    nightCountTextView.setText(Utils.getDateDifference(checkinDate, checkoutDate) + "");
                    initDate(Utils.getDateDifference(new Date(), checkinDate), Utils.getDateDifference(checkinDate, checkoutDate));
                    addCalendarDecorator();
                }
            }
        });

        findViewById(R.id.buttonHotelSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(key==null){
                    dialogValidation(R.string.wrong_form_filling, R.string.hotel_destination_empty_validation);
                }
                else {
                    if(type.equals("business")){
                        findViewById(R.id.layoutFormSearch).setVisibility(View.GONE);
                        findViewById(R.id.layoutLoading).setVisibility(View.VISIBLE);
                        int versionCode = 0;
                        try {
                            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                            versionCode = packageInfo.versionCode;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }

                        Map<String, String> params = new HashMap<>();
                        params.put("hotel_id", key);
                        params.put("biller_id", billerID);
                        params.put("room", roomCountTextView.getText().toString());
                        params.put("checkin_date", new SimpleDateFormat("yyyy-MM-dd").format(checkinDate.getTime()));
                        params.put("checkout_date", new SimpleDateFormat("yyyy-MM-dd").format(checkoutDate.getTime()));

                        SessionManager sessionManager = new SessionManager(HotelSearchActivity.this);
                        params.put("auth_mode", sessionManager.isLoggedIn()?"_auth":"");
                        params.put("version_code", Integer.toString(versionCode));

                        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "hotel/detail", params, new Response.Listener<JSONObject>() {


                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("JSON", response.toString());
                                try {
                                    String status = response.getString("status");
                                    if(status.equals("1")){
                                        Hotel hotel = new Hotel("", "", "", location, "", billerID, "", 0, 0, 0);
                                        Gson gson = new Gson();
                                        Intent intent = new Intent(HotelSearchActivity.this, HotelDetailActivity.class);
                                        intent.putExtra("data", response.toString());
                                        intent.putExtra("hotel_detail", gson.toJson(hotel));
                                        intent.putExtra("room", roomCountTextView.getText().toString());
                                        intent.putExtra("checkin_date", new SimpleDateFormat("yyyy-MM-dd").format(checkinDate.getTime()));
                                        intent.putExtra("checkout_date", new SimpleDateFormat("yyyy-MM-dd").format(checkoutDate.getTime()));
                                        startActivity(intent);
                                    }
                                    else{

//                                        findViewById(R.id.layoutFormSearch).setVisibility(View.VISIBLE);
                                        findViewById(R.id.progressSplashLogo).setVisibility(View.GONE);

                                        if(status.equals("need_update")){
                                            findViewById(R.id.needUpdateLayout).setVisibility(View.VISIBLE);
                                        }
                                        else{
                                            findViewById(R.id.emptyResultLayout).setVisibility(View.VISIBLE);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
//                    Toast.makeText(context, R.string.error_data_communication, Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError arg0) {
                                VolleyLog.d("Error: ", arg0.getMessage());
                                Toast.makeText(HotelSearchActivity.this, R.string.error_data_communication, Toast.LENGTH_LONG).show();
                            }
                        });
                        AppController.getInstance().addToRequestQueue(jsonReq);

                    }
                    else {
                        Intent i = new Intent(HotelSearchActivity.this, HotelResultActivity.class);
                        Bundle bundle = new Bundle();

                        bundle.putString("key", key);
                        bundle.putString("room", roomCountTextView.getText().toString());
                        bundle.putString("checkin_date", new SimpleDateFormat("yyyy-MM-dd").format(checkinDate.getTime()));
                        bundle.putString("checkout_date", new SimpleDateFormat("yyyy-MM-dd").format(checkoutDate.getTime()));
                        bundle.putString("value", ((TextView) findViewById(R.id.textDestinationHotel)).getText().toString());
                        i.putExtra("parameter", bundle);
                        startActivity(i);
                    }

                }
            }
        });

        findViewById(R.id.needUpdateLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
            }
        });


        findViewById(R.id.emptyResultLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.emptyResultLayout).setVisibility(View.GONE);
                findViewById(R.id.layoutFormSearch).setVisibility(View.VISIBLE);
            }
        });
    }

    private void dialogValidation(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }

    private void addCalendarDecorator(){
        calendarWidget.removeDecorators();
        ArrayList<CalendarDay> rangeDays = Utils.getDates(checkinDate, checkoutDate);
        ArrayList<CalendarDay> goSelectedDay = new ArrayList<>();
        goSelectedDay.add(CalendarDay.from(checkinDate));

        ArrayList<CalendarDay> backSelectedDay = new ArrayList<>();
        backSelectedDay.add(CalendarDay.from(checkoutDate));
        SelectionDecorator rangeDecorator = new SelectionDecorator(HotelSearchActivity.this, SelectionDecorator.RANGE_DECORATOR,
                rangeDays);
        SelectionDecorator goSelectedDecorator = new SelectionDecorator(HotelSearchActivity.this, SelectionDecorator.LEFT_DECORATOR,
                goSelectedDay);
        SelectionDecorator backSelectedDecorator = new SelectionDecorator(HotelSearchActivity.this, SelectionDecorator.RIGHT_DECORATOR,
                backSelectedDay);
        calendarWidget.addDecorator(rangeDecorator);
        calendarWidget.addDecorator(goSelectedDecorator);
        calendarWidget.addDecorator(backSelectedDecorator);

        addHolidayDecorator();
    }

    private void showCalendar(){
        findViewById(R.id.calendarLayout).setVisibility(View.VISIBLE);
    }

    private void addHolidayDecorator() {
        calendarWidget.addDecorator(new HighlightWeekendsDecorator());
    }

    private void initDate(int departExtra, int returnExtra) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        calendarWidget.state().edit().setMinimumDate(cal).commit();
        calendarWidget.setTitleFormatter(new MonthArrayTitleFormatter(getResources().getTextArray(R.array.custom_months)));
        calendarWidget.setWeekDayFormatter(new ArrayWeekDayFormatter(getResources().getTextArray(R.array.custom_weekdays)));

        addHolidayDecorator();

        cal.add(Calendar.DAY_OF_MONTH, departExtra);
        checkinDate = cal.getTime();
        calendarWidget.setDateSelected(checkinDate, true);

        ((TextView) findViewById(R.id.textDayDepart)).setText(Utils.getDayName(cal.get(Calendar.DAY_OF_WEEK), getApplicationContext()));
        ((TextView) findViewById(R.id.textDateMonthDepart)).setText(cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getUcMonthShortName(cal.get(Calendar.MONTH), getApplicationContext()));
        ((TextView) findViewById(R.id.textYearDepart)).setText(cal.get(Calendar.YEAR)+"");

        cal.add(Calendar.DAY_OF_MONTH, returnExtra);
        checkoutDate = cal.getTime();
        ((TextView) findViewById(R.id.textDayReturn)).setText(Utils.getDayName(cal.get(Calendar.DAY_OF_WEEK), getApplicationContext()));
        ((TextView) findViewById(R.id.textDateMonthReturn)).setText(cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getUcMonthShortName(cal.get(Calendar.MONTH), getApplicationContext()));
        ((TextView) findViewById(R.id.textYearReturn)).setText(cal.get(Calendar.YEAR)+"");

        nightCountTextView.setText(Utils.getDateDifference(checkinDate, checkoutDate) + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            this.key = data.getStringExtra("key");
            this.type = data.getStringExtra("type");
            this.location = data.getStringExtra("location");
            this.billerID = data.getStringExtra("biller_id");
            ((TextView) findViewById(R.id.textDestinationHotel)).setText(data.getStringExtra("value"));
        }
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.calendarLayout).getVisibility() == View.VISIBLE) {
            findViewById(R.id.calendarLayout).setVisibility(View.GONE);
        }  else {
            finish();
            //startActivity(new Intent(this, MenuOnlineActivity.class));
        }
    }
}
