package com.tiketextra.tiketextra.activity.hotel;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;

public class HotelSortActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_sort);
        Toolbar toolbar = findViewById(R.id.toolbarSort);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(getString(R.string.sort_hotel));

        RadioGroup pointsRadioGroup = findViewById(R.id.points_radio_group);
        switch (AppHelper.hotelSort){
            case Configuration.SORT_LOWEST_PRICE: {
                pointsRadioGroup.check(R.id.lowestPriceRadioButton);
                break;
            }
            case Configuration.SORT_HIGHEST_PRICE: {
                pointsRadioGroup.check(R.id.highestPriceRadioButton);
                break;
            }
            case Configuration.SORT_RATING_5_TO_1: {
                pointsRadioGroup.check(R.id.highToLowRatingRadioButton);
                break;
            }
            case Configuration.SORT_RATING_1_TO_5: {
                pointsRadioGroup.check(R.id.lowToHighRadioButton);
                break;
            }
            case Configuration.SORT_HIGHEST_DISCOUNT: {
                pointsRadioGroup.check(R.id.discountRadioButton);
                break;
            }
        }

        pointsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                View radioButton = radioGroup.findViewById(i);
                int idx = radioGroup.indexOfChild(radioButton);
                AppHelper.hotelSort = idx;
            }
        });

        findViewById(R.id.buttonApplySort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HotelResultActivity.class);
                HotelSortActivity.this.setResult(Activity.RESULT_OK, intent);
                HotelSortActivity.this.finish();
            }
        });

    }
}
