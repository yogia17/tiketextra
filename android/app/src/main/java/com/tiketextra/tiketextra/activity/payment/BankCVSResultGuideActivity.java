package com.tiketextra.tiketextra.activity.payment;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.flight.FlightPaymentActivity;
import com.tiketextra.tiketextra.activity.hotel.HotelPaymentActivity;
import com.tiketextra.tiketextra.activity.train.TrainPaymentActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BankCVSResultGuideActivity extends BaseActivity {

    private ReservationModel reservationModel;
    private Reservation reservation;


    private boolean flagDialog = false;

    private boolean flag = false, flagRepeat = false;
    private Handler handler;
    private Runnable statusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                if(flagRepeat) {
                    ItineraryHelper.checkReservation(getApplicationContext(), reservation.getReservationID());

                    if ((reservation.getStatus1().equals("issued") || (reservation.getStatus2()!=null && reservation.getStatus2().equals("issued"))) && flagDialog == false) {
                        flagDialog = true;
                        final Dialog mDialog = new Dialog(BankCVSResultGuideActivity.this, R.style.CustomDialogTheme);

                        mDialog.setContentView(R.layout.dialog_info);
                        mDialog.setCancelable(true);
                        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                        mDialogHeader.setText(R.string.issued_success);
                        mDialogText.setText(R.string.issued_success_text);

                        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mDialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(), reservation.getCarrierType().equals("flight")?com.tiketextra.tiketextra.activity.reservation.flight.TicketIssuedActivity.class:(reservation.getCarrierType().equals("train")?com.tiketextra.tiketextra.activity.reservation.train.TicketIssuedActivity.class:com.tiketextra.tiketextra.activity.reservation.hotel.TicketIssuedActivity.class));
                                intent.putExtra("active", true);
                                startActivity(intent);
                            }
                        });

                        mDialog.show();
                    }
                }
                flagRepeat = true;

            } finally {
                handler.postDelayed(statusChecker, Configuration.RESERVATION_CHECK_DELAY);
            }
        }
    };

    void startRepeatingTask() {
        statusChecker.run();
    }

    void stopRepeatingTask() {
        handler.removeCallbacks(statusChecker);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(flag){
            startRepeatingTask();
        }
        flag = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_cvs_result_guide);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarBankCVSResultGuide);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.payment);

        reservationModel = new ReservationModel(this);
        final Bank bank = new Gson().fromJson(getIntent().getStringExtra("bank"), Bank.class);

        reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));

        handler = new Handler();
        startRepeatingTask();


        ((TextView) findViewById(R.id.reservationIdTextView)).setText(reservation.getBookingCode1()+(reservation.isReturn() && !(reservation.getCarrierID1().equals(reservation.getCarrierID2()) && (reservation.getBookingCode2()==null || reservation.getBookingCode2().equals("")))?" - "+reservation.getBookingCode2():""));
//        ((TextView) findViewById(R.id.immediatelyTransferTextView)).setText(Html.fromHtml(getString(R.string.immediately_transfer_payment)), TextView.BufferType.SPANNABLE);

        ((TextView) findViewById(R.id.immediatelyTransferTextView)).setText(Html.fromHtml(getString(R.string.immediately_transfer_payment)), TextView.BufferType.SPANNABLE);

        if(bank.getGateway().equals("1")){
            ((TextView) findViewById(R.id.bankNameTextView)).setText(bank.getName()+" - "+bank.getOwner());
            ((TextView) findViewById(R.id.accountNumberTextView)).setText(bank.getAccount());
            findViewById(R.id.paymentInstructionTextView).setVisibility(View.GONE);
            findViewById(R.id.directTransferInstructionLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.finishLayout).setVisibility(View.GONE);
        }
        else {
            ((TextView) findViewById(R.id.bankNameTextView)).setText(bank.getName());
            ((TextView) findViewById(R.id.accountNumberTextView)).setText(reservation.getPaidFrom());
            findViewById(R.id.paymentInstructionTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.directTransferInstructionLayout).setVisibility(View.GONE);
            findViewById(R.id.finishLayout).setVisibility(View.VISIBLE);
        }
        ((TextView) findViewById(R.id.transferAmountTextView)).setText(Utils.numberFormatCurrency(reservation.getAmount()));
        ((TextView) findViewById(R.id.datetimePaybyTextView)).setText(Utils.longDateTime(reservation.getTimeLimit(), this) + (getCurLang().equals("en")?" GMT+7":" WIB"));
        ((TextView) findViewById(R.id.timerTextView)).setText(Utils.strTimeDiff(Utils.getDateTimeNowInUTCPlus7(), reservation.getTimeLimit()));


        if(bank.getGateway().equals("1") || bank.getGateway().equals("4")) {
            findViewById(R.id.layoutAccountNumber).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Virtual Account Number", reservation.getPaidFrom());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(BankCVSResultGuideActivity.this, bank.getGateway().equals("5") ? R.string.payment_code_copied : R.string.account_number_copied, Toast.LENGTH_LONG).show();
                }
            });
        }
        else{
            findViewById(R.id.buttonCopyAccountNumber).setVisibility(View.GONE);
        }
        findViewById(R.id.layoutBookingCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BankCVSResultGuideActivity.this, reservation.getCarrierType().equals("flight")?com.tiketextra.tiketextra.activity.flight.FlightBookingResultActivity.class:(reservation.getCarrierType().equals("train")?com.tiketextra.tiketextra.activity.train.TrainBookingResultActivity.class:com.tiketextra.tiketextra.activity.hotel.HotelBookingResultActivity.class));
                intent.putExtra("reservation_id", reservation.getReservationID());
                startActivity(intent);
            }
        });

        // ATM
        if(showGuide(getIntent().getBundleExtra("guidance"), 0)){
            findViewById(R.id.atmButton).setVisibility(View.VISIBLE);
            findViewById(R.id.atmLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.atmDivider).setVisibility(View.VISIBLE);

            ((HtmlTextView) findViewById(R.id.textViewATM)).setHtml(getIntent().getBundleExtra("guidance").getString("atm"));
        }
        // internet
        if(showGuide(getIntent().getBundleExtra("guidance"), 1)){
            findViewById(R.id.internetBankingButton).setVisibility(View.VISIBLE);
            findViewById(R.id.internetBankingLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.internetBankingDivider).setVisibility(View.VISIBLE);

            ((HtmlTextView) findViewById(R.id.textViewInternetBanking)).setHtml(getIntent().getBundleExtra("guidance").getString("internet_banking"));
        }
        // mobile
        if(showGuide(getIntent().getBundleExtra("guidance"), 2)){
            findViewById(R.id.mobileBankingButton).setVisibility(View.VISIBLE);
            findViewById(R.id.mobileBankingLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.mobileBankingDivider).setVisibility(View.VISIBLE);

            ((HtmlTextView) findViewById(R.id.textViewMobileBanking)).setHtml(getIntent().getBundleExtra("guidance").getString("mobile_banking"));
        }
        // sms
        if(showGuide(getIntent().getBundleExtra("guidance"), 3)){
            findViewById(R.id.smsBankingButton).setVisibility(View.VISIBLE);
            findViewById(R.id.smsBankingLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.smsBankingDivider).setVisibility(View.VISIBLE);

            ((HtmlTextView) findViewById(R.id.textViewSmsBanking)).setHtml(getIntent().getBundleExtra("guidance").getString("sms_banking"));
        }

        if(showGuide(getIntent().getBundleExtra("guidance"), 4)){
            findViewById(R.id.convenienceStoreButton).setVisibility(View.VISIBLE);
            findViewById(R.id.convenienceStoreLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.convenienceStoreDivider).setVisibility(View.VISIBLE);

            ((HtmlTextView) findViewById(R.id.textViewconvenienceStore)).setHtml(getIntent().getBundleExtra("guidance").getString("convenience_store"));
        }

        if(bank.getGateway().equals("4")) {
            findViewById(R.id.atmLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.convenienceStoreLayout).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.textViewFinish)).setText(R.string.finish);
            ((TextView) findViewById(R.id.accountNumberTypeTextView)).setText(R.string.virtual_account_number);
        }
        else if(bank.getGateway().equals("5")) {
            findViewById(R.id.atmLayout).setVisibility(View.GONE);
            findViewById(R.id.convenienceStoreLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.textViewFinish)).setText(R.string.finish);
            ((TextView) findViewById(R.id.accountNumberTypeTextView)).setText(R.string.payment_code);
        }
        else if(bank.getGateway().equals("1")){
            ((TextView) findViewById(R.id.textViewFinish)).setText(R.string.payment_confirmation);
            ((TextView) findViewById(R.id.accountNumberTypeTextView)).setText(R.string.destination_account);
        }
        findViewById(R.id.mobileBankingLayout).setVisibility(View.GONE);
        findViewById(R.id.internetBankingLayout).setVisibility(View.GONE);
        findViewById(R.id.smsBankingLayout).setVisibility(View.GONE);
        findViewById(R.id.atmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hiddenLayout();
                findViewById(R.id.atmLayout).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.mobileBankingButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hiddenLayout();
                findViewById(R.id.mobileBankingLayout).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.internetBankingButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hiddenLayout();
                findViewById(R.id.internetBankingLayout).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.smsBankingButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hiddenLayout();
                findViewById(R.id.smsBankingLayout).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.convenienceStoreButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hiddenLayout();
                findViewById(R.id.convenienceStoreLayout).setVisibility(View.VISIBLE);
            }
        });

        findViewById(R.id.finishButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bank.getGateway().equals("1")) {
                    Intent intent = new Intent(BankCVSResultGuideActivity.this, PaymentConfirmationActivity.class);
                    intent.putExtra("reservation_id", reservation.getReservationID());
                    intent.putExtra("bank", getIntent().getStringExtra("bank"));
                    startActivity(intent);
                }
                else {
                    startActivity(new Intent(BankCVSResultGuideActivity.this, reservation.getCarrierType().equals("flight") ? com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity.class : (reservation.getCarrierType().equals("train") ? com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity.class : com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class)));
                }
            }
        });

        findViewById(R.id.confirmDirectTransferButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BankCVSResultGuideActivity.this, PaymentConfirmationActivity.class);
                intent.putExtra("reservation_id", reservation.getReservationID());
                intent.putExtra("bank", getIntent().getStringExtra("bank"));
                startActivity(intent);
            }
        });

        findViewById(R.id.changePaymentMethodButton1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePaymentMethod();
            }
        });

        findViewById(R.id.changePaymentMethodButton2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePaymentMethod();
            }
        });
    }

    private void changePaymentMethod() {
        final Map<String, String> params = new HashMap<>();
        params.put("reservation_id", Integer.toString(reservation.getReservationID()));
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "rollback_payment", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") == 1) {
                            JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "reservation", params, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

//                Log.e("response", response.toString());

                                    try {
                                        Gson gson = new Gson();
                                        reservation = gson.fromJson(response.getJSONObject("reservation").toString(), Reservation.class);
                                        reservationModel.updateReservationObject(reservation);
                                        reservationModel.setReservationBankID(reservation.getReservationID(), reservation.getBankID());
                                        ArrayList<Bank> banks = new ArrayList<>();
                                        Bank reservationBank = null;
                                        JSONArray bank = response.getJSONArray("bank");
                                        for (int i = 0; i < bank.length(); i++) {
                                            Bank curr = gson.fromJson(bank.get(i).toString(), Bank.class);
                                            banks.add(curr);
                                            if (reservation.getBankID() == curr.getBankID()) {
                                                reservationBank = curr;
                                            }
                                        }

                                        if (reservation.getBank() == null) {
                                            Intent intent = new Intent(BankCVSResultGuideActivity.this, reservation.getCarrierType().equals("flight") ? FlightPaymentActivity.class: (reservation.getCarrierType().equals("train") ? TrainPaymentActivity.class : HotelPaymentActivity.class));
                                            intent.putExtra("reservation_id", reservation.getReservationID());
                                            intent.putExtra("bank", gson.toJson(banks));
                                            startActivity(intent);
                                        }
                                        else if(reservation.getIsConfirmed().equals("Y")){
                                            Intent intent = new Intent(BankCVSResultGuideActivity.this, PaymentAfterConfirmActivity.class);
                                            intent.putExtra("reservation_id", reservation.getReservationID());
                                            startActivity(intent);
                                        }
                                        else {
                                            if (reservationBank.getGateway().equals("1") || reservationBank.getGateway().equals("4") || reservationBank.getGateway().equals("5")) {
                                                final Bank finalReservationBank = reservationBank;
                                                Map<String, String> params = new HashMap<>();
                                                params.put("reservation_id", Integer.toString(reservation.getReservationID()));
                                                params.put("bank_id", Integer.toString(finalReservationBank.getBankID()));

                                                JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + (finalReservationBank.getGateway().equals("1") || finalReservationBank.getGateway().equals("4")?"get_bank_user_guide":"get_cvs_user_guide"), params, new Response.Listener<JSONObject>() {
                                                    @Override
                                                    public void onResponse(JSONObject response) {
                                                        try {

                                                            if(response.getInt("status")==1){
                                                                Gson gson = new Gson();
                                                                Intent intent = new Intent(BankCVSResultGuideActivity.this, BankCVSResultGuideActivity.class);
                                                                intent.putExtra("reservation_id", reservation.getReservationID());
                                                                intent.putExtra("bank", gson.toJson(finalReservationBank));
//                                                intent.putExtra("type", reservation.getCarrierType());

                                                                Bundle bundle = new Bundle();
                                                                bundle.putString("atm", response.getString("atm"));
                                                                bundle.putString("mobile_banking", response.getString("mobile_banking"));
                                                                bundle.putString("internet_banking", response.getString("internet_banking"));
                                                                bundle.putString("sms_banking", response.getString("sms_banking"));
                                                                bundle.putString("convenience_store", response.getString("convenience_store"));
                                                                intent.putExtra("guidance", bundle);
                                                                startActivity(intent);
                                                            }
                                                            else{
                                                                dialogErrorConnection();
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                            dialogErrorConnection();
                                                        }



                                                    }
                                                }, new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError arg0) {
                                                        VolleyLog.d("Error: ", arg0.getMessage());
                                                        dialogErrorConnection();
                                                    }
                                                });
                                                AppController.getInstance().addToRequestQueue(jsonReq);
                                            }

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialogErrorConnection();
                                    }


                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError arg0) {
                                    VolleyLog.d("Error: ", arg0.getMessage());
                                    dialogErrorConnection();
                                }
                            });
                            AppController.getInstance().addToRequestQueue(jsonReq);

                    } else {
                        dialogErrorTransaction();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogErrorConnection();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                dialogErrorConnection();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private boolean showGuide(Bundle bundle, int channel){
        if(channel==0){
            return !bundle.getString("atm").equals("");
        }
        else if(channel==1){
            return !bundle.getString("internet_banking").equals("");
        }
        else if(channel==2){
            return !bundle.getString("mobile_banking").equals("");
        }
        else if(channel==3){
            return !bundle.getString("sms_banking").equals("");
        }
        else if(channel==4){
            return !bundle.getString("convenience_store").equals("");
        }
        else return false;
    }
//
//    private int accountIndex(String account){
//        if(account.toLowerCase().equals("cena")){
//            return 0;
//        }
//        if(account.toLowerCase().equals("bmri")){
//            return 1;
//        }
//        if(account.toLowerCase().equals("bnin")){
//            return 2;
//        }
//        if(account.toLowerCase().equals("hnbn")){
//            return 3;
//        }
//        if(account.toLowerCase().equals("bbba")){
//            return 4;
//        }
//        if(account.toLowerCase().equals("ibbk")){
//            return 5;
//        }
//        if(account.toLowerCase().equals("bnia")){
//            return 6;
//        }
//        if(account.toLowerCase().equals("brin")){
//            return 7;
//        }
//        else{
//            return 8;
//        }
//    }

    private void dialogErrorConnection() {
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogErrorTransaction() {
        dialog(R.string.error_occurred, R.string.error_transaction_handshaking);
    }

    private void dialog(int title, int message) {
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        findViewById(R.id.dialog_info_text).setVisibility(View.VISIBLE);
        findViewById(R.id.dividerBankButton).setVisibility(View.VISIBLE);
        findViewById(R.id.linearLayoutBankAction).setVisibility(View.VISIBLE);

        findViewById(R.id.progressBarBank).setVisibility(View.GONE);

        mDialog.show();
    }

    private void hiddenLayout(){
        findViewById(R.id.atmLayout).setVisibility(View.GONE);
        findViewById(R.id.mobileBankingLayout).setVisibility(View.GONE);
        findViewById(R.id.internetBankingLayout).setVisibility(View.GONE);
        findViewById(R.id.smsBankingLayout).setVisibility(View.GONE);
        findViewById(R.id.convenienceStoreLayout).setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(BankCVSResultGuideActivity.this, reservation.getCarrierType().equals("flight")? com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity.class : (reservation.getCarrierType().equals("train") ? com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity.class : com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class)));
    }
}
