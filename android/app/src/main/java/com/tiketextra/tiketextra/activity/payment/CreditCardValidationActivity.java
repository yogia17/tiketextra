package com.tiketextra.tiketextra.activity.payment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreditCardValidationActivity extends BaseActivity {

    private WebView wv;
    private Bank bank;

    private ReservationModel reservationModel;
    private Reservation reservation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_validation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarCreditCardValidation);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bank = new Gson().fromJson(getIntent().getStringExtra("bank"), Bank.class);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(bank.getName());


        reservationModel = new ReservationModel(this);
        reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));

        wv = findViewById(R.id.wv);

        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setLoadWithOverviewMode(true);

        // setup wv
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        WebSettings settings = wv.getSettings();
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAppCacheEnabled(false);
        settings.setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        wv.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                findViewById(R.id.progressBarCreditCardValidation).setVisibility(View.GONE);
                wv.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                final String [] path = url.split("/");

                if(path[3].equals("webservice") && path[4].equals("cc_ok")){

                    reservationModel.setReservationBank(reservation.getReservationID(), bank.getBankID(), bank.getName(), Integer.parseInt(path[5]), Integer.parseInt(path[6]), Integer.parseInt(path[7]), Integer.parseInt(path[8]), "");
                    reservationModel.confirmPayment(reservation.getReservationID(), bank.getBankID(), Integer.parseInt(path[5]), bank.getName(), path[9], "N");
                    Intent intent = new Intent(CreditCardValidationActivity.this, PaymentAfterConfirmActivity.class);
                    intent.putExtra("reservation_id", reservation.getReservationID());
                    startActivity(intent);
                }
                else {
                    final Dialog mDialog = new Dialog(CreditCardValidationActivity.this, R.style.CustomDialogTheme);

                    mDialog.setContentView(R.layout.dialog_info);
                    mDialog.setCancelable(true);
                    TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                    TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                    mDialogHeader.setText(R.string.error_occurred);
                    mDialogText.setText(path[5]);

                    mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialog.dismiss();
                            backToPayment();
                        }
                    });

                }

                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        handleLoadUrl();
    }

    private void handleLoadUrl() {

        try {
            String post = "timeStamp="+ URLEncoder.encode(getIntent().getStringExtra("timeStamp"), "UTF-8");
            post += "&tXid="+URLEncoder.encode(getIntent().getStringExtra("tXid"), "UTF-8");
            post += "&cardNo="+URLEncoder.encode(getIntent().getStringExtra("cardNo"), "UTF-8");
            post += "&cardExpYymm="+URLEncoder.encode(getIntent().getStringExtra("cardExpYymm"), "UTF-8");
            post += "&cardCvv="+URLEncoder.encode(getIntent().getStringExtra("cardCvv"), "UTF-8");
            post += "&cardHolderNm="+URLEncoder.encode(getIntent().getStringExtra("cardHolderNm"), "UTF-8");
            post += "&merchantToken="+URLEncoder.encode(getIntent().getStringExtra("merchantToken"), "UTF-8");
            post += "&callBackUrl="+URLEncoder.encode(getIntent().getStringExtra("callBackUrl"), "UTF-8");

            wv.postUrl(getIntent().getStringExtra("url"), post.getBytes());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    private void backToPayment() {
        Map<String, String> params = new HashMap<>();
        params.put("reservation_id", Integer.toString(reservation.getReservationID()));
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "reservation", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    Gson gson = new Gson();
                    ArrayList<Bank> banks = new ArrayList<>();
                    JSONArray bank = response.getJSONArray("bank");
                    for (int i = 0; i < bank.length(); i++) {
                        Bank curr = gson.fromJson(bank.get(i).toString(), Bank.class);
                        banks.add(curr);
                    }
                    if (reservation.getBank() == null) {
                        Intent intent = new Intent(CreditCardValidationActivity.this, reservation.getCarrierType().equals("flight") ? com.tiketextra.tiketextra.activity.flight.FlightPaymentActivity.class : (reservation.getCarrierType().equals("train") ? com.tiketextra.tiketextra.activity.train.TrainPaymentActivity.class : com.tiketextra.tiketextra.activity.hotel.HotelPaymentActivity.class));
                        intent.putExtra("reservation_id", reservation.getReservationID());
                        intent.putExtra("bank", gson.toJson(banks));
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(CreditCardValidationActivity.this, reservation.getCarrierType().equals("flight")? com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity.class : (reservation.getCarrierType().equals("train") ? com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity.class : com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class));
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogErrorConnection();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                dialogErrorConnection();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        findViewById(R.id.containerPassenger).setVisibility(View.VISIBLE);
        findViewById(R.id.tabsPassenger).setVisibility(View.VISIBLE);
        findViewById(R.id.progressBarConfirmSeat).setVisibility(View.GONE);

        mDialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CreditCardValidationActivity.this, reservation.getCarrierType().equals("flight")? com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity.class : (reservation.getCarrierType().equals("train") ? com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity.class : com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class));
        startActivity(intent);
    }
}
