package com.tiketextra.tiketextra.activity.payment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Reservation;

import org.sufficientlysecure.htmltextview.HtmlTextView;

public class PaymentAfterConfirmActivity extends BaseActivity {

    private ReservationModel reservationModel;
    private Reservation reservation;


    private boolean flagDialog = false;

    private boolean flag = false, flagRepeat = false;
    private Handler handler;
    private Runnable statusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                if(flagRepeat) {
                    ItineraryHelper.checkReservation(getApplicationContext(), reservation.getReservationID());

                    if ((reservation.getStatus1().equals("issued") || (reservation.getStatus2()!=null && reservation.getStatus2().equals("issued"))) && flagDialog == false) {
                        flagDialog = true;
                        final Dialog mDialog = new Dialog(PaymentAfterConfirmActivity.this, R.style.CustomDialogTheme);

                        mDialog.setContentView(R.layout.dialog_info);
                        mDialog.setCancelable(true);
                        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                        mDialogHeader.setText(R.string.issued_success);
                        mDialogText.setText(R.string.issued_success_text);

                        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mDialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(), reservation.getCarrierType().equals("flight")?com.tiketextra.tiketextra.activity.reservation.flight.TicketIssuedActivity.class:(reservation.getCarrierType().equals("train")?com.tiketextra.tiketextra.activity.reservation.train.TicketIssuedActivity.class:com.tiketextra.tiketextra.activity.reservation.hotel.TicketIssuedActivity.class));
                                intent.putExtra("active", true);
                                startActivity(intent);
                            }
                        });

                        mDialog.show();
                    }
                }
                flagRepeat = true;

            } finally {
                handler.postDelayed(statusChecker, Configuration.RESERVATION_CHECK_DELAY);
            }
        }
    };

    void startRepeatingTask() {
        statusChecker.run();
    }

    void stopRepeatingTask() {
        handler.removeCallbacks(statusChecker);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(flag){
            startRepeatingTask();
        }
        flag = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_after_confirm);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPaymentAfterConfirm);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        reservationModel = new ReservationModel(this);
        reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));

        handler = new Handler();
        startRepeatingTask();

        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(reservation.getBank());

        ((TextView) findViewById(R.id.textViewPaymentResult)).setText(R.string.thank_you);

        ((HtmlTextView) findViewById(R.id.textViewPaymentMessage)).setHtml(getResources().getString(R.string.payment_confirmation_success, reservation.getContactEmail()));

        findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopRepeatingTask();
                startActivity(new Intent(PaymentAfterConfirmActivity.this, MainActivity.class));
//                startActivity(new Intent(PaymentAfterConfirmActivity.this, reservation.getCarrierType().equals("flight")? com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity.class : (reservation.getCarrierType().equals("train") ? com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity.class : com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class)));
            }
        });

    }

    @Override
    public void onBackPressed() {
        stopRepeatingTask();
        startActivity(new Intent(PaymentAfterConfirmActivity.this, MainActivity.class));
//        startActivity(new Intent(PaymentAfterConfirmActivity.this, reservation.getCarrierType().equals("flight")? com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity.class : (reservation.getCarrierType().equals("train") ? com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity.class : com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class)));
    }
}
