package com.tiketextra.tiketextra.activity.payment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Flight;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.object.Train;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PaymentConfirmationActivity extends BaseActivity {

    private Reservation reservation;
    private TextView textViewTimeLimit;
    private Bank bank;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirmation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPaymentConfirmation);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.payment_confirmation);

        final ReservationModel reservationModel = new ReservationModel(this);
        bank = new Gson().fromJson(getIntent().getStringExtra("bank"), Bank.class);
        reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));

        textViewTimeLimit = findViewById(R.id.textViewTimeLimit);
        textViewTimeLimit.setText(Utils.longDateTime(reservation.getTimeLimit(), this) + (getCurLang().equals("en")?" GMT+7":" WIB"));
        String routeStr = "";
        int count = 0;
        Calendar basic = Calendar.getInstance();
        Calendar compareDepart = Calendar.getInstance();
        Calendar compareArrive = Calendar.getInstance();
        if(reservation.getCarrierType().equals("flight") || reservation.getCarrierType().equals("train")) {
            AirportModel airportModel = new AirportModel(this);
            StationModel stationModel = new StationModel(this);
            if (reservation.getCarrierType().equals("flight")) {
                ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure) + " - " + Utils.mediumDate(reservation.getDepartDate(), this));
                ((TextView) findViewById(R.id.departHintTextView)).setText(airportModel.getAirport(reservation.getFromPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + airportModel.getAirport(reservation.getToPort()).getCity());

                for (Flight fl : reservation.getDepartFlightArray()) {
                    String supDepart = "";
                    String supArrive = "";
                    if (count == 0) {
                        basic.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
                        ((ImageView) findViewById(R.id.imageViewDepartAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                        count++;
                    }
                    compareDepart.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
                    compareArrive.set(fl.getArriveYear(), fl.getArriveMonth(), fl.getArriveDay());
                    long ex;
                    if ((ex = (compareDepart.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                        supDepart = "<sup><em><small>+" + ex + "</small></em></sup>";
                    }
                    if ((ex = (compareArrive.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                        supArrive = "<sup><em><small>+" + ex + "</small></em></sup>";
                    }
                    routeStr += fl.getDepartTime() + supDepart + " " + fl.getDepartPort()
                            + " - " + fl.getArriveTime() + supArrive + " "
                            + fl.getArrivePort() + "<br/>";

                }
                routeStr = routeStr.length() > 0 ? routeStr.substring(0,
                        routeStr.length() - 5) : routeStr;
                ((TextView) findViewById(R.id.textViewDepartRoute)).setText(Html.fromHtml(routeStr));
                ((TextView) findViewById(R.id.textViewDepartItinerary)).setText(Utils.strTimeDiff(reservation.getDepartFlightArray()[0].getDepartDatetime(), reservation
                        .getDepartFlightArray()[reservation.getDepartFlightArray().length - 1]
                        .getArriveDatetime(), reservation.getDepartFlightArray()[0].getDepartTimezone() - reservation.getDepartFlightArray()[reservation.getDepartFlightArray().length - 1].getArriveTimezone()) + "  ( " + ((reservation.getDepartFlightArray().length + reservation.getDepartHiddenTransit()) == 1 ? getString(R.string.direct) : (reservation.getDepartFlightArray().length - 1 + reservation.getDepartHiddenTransit()) + " " + getString(R.string.transit)) + " )");
            } else if (reservation.getCarrierType().equals("train")) {
                ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure) + " - " + Utils.mediumDate(reservation.getDepartDate(), this));
                ((TextView) findViewById(R.id.departHintTextView)).setText(stationModel.getStation(reservation.getFromPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + stationModel.getStation(reservation.getToPort()).getCity());
                ((ImageView) findViewById(R.id.imageViewDepartAirline)).setImageResource(R.drawable.ic_kai);
                ((ImageView) findViewById(R.id.imageViewDepartCarrierType)).setImageResource(R.drawable.ic_train_go);

                for (Train fl : reservation.getDepartTrainArray()) {
                    String supDepart = "";
                    String supArrive = "";
                    compareDepart.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
                    compareArrive.set(fl.getArriveYear(), fl.getArriveMonth(), fl.getArriveDay());
                    long ex;
                    if ((ex = (compareDepart.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                        supDepart = "<sup><em><small>+" + ex + "</small></em></sup>";
                    }
                    if ((ex = (compareArrive.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                        supArrive = "<sup><em><small>+" + ex + "</small></em></sup>";
                    }
                    routeStr += fl.getDepartTime() + supDepart + " " + fl.getDepartPort()
                            + " - " + fl.getArriveTime() + supArrive + " "
                            + fl.getArrivePort() + "<br/>";

                }
                routeStr = routeStr.length() > 0 ? routeStr.substring(0,
                        routeStr.length() - 5) : routeStr;
                ((TextView) findViewById(R.id.textViewDepartRoute)).setText(Html.fromHtml(routeStr));
                ((TextView) findViewById(R.id.textViewDepartItinerary)).setText(Utils.strTimeDiff(reservation.getDepartTrainArray()[0].getDepartDatetime(), reservation
                        .getDepartTrainArray()[reservation.getDepartTrainArray().length - 1]
                        .getArriveDatetime(), reservation.getDepartTrainArray()[0].getDepartTimezone() - reservation.getDepartTrainArray()[reservation.getDepartTrainArray().length - 1].getArriveTimezone()));
            }

            ((TextView) findViewById(R.id.textViewDepartBookingCode)).setText(reservation.getBookingCode1());

            if (reservation.getClass1().equals("pro")) {
                ((TextView) findViewById(R.id.textViewDepartClass)).setText(R.string.class_pro);
                ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.green_500));
            } else if (reservation.getClass1().equals("eco")) {
                ((TextView) findViewById(R.id.textViewDepartClass)).setText(R.string.class_eco);
                ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.blue_500));
            } else if (reservation.getClass1().equals("bus")) {
                ((TextView) findViewById(R.id.textViewDepartClass)).setText(R.string.class_bus);
                ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.orange_500));
            } else if (reservation.getClass1().equals("exe")) {
                ((TextView) findViewById(R.id.textViewDepartClass)).setText(R.string.class_exe);
                ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.red_500));
            }

            if (reservation.isReturn()) {
                findViewById(R.id.returnFlight).setVisibility(View.VISIBLE);
                if (reservation.getCarrierType().equals("flight")) {
                    ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns) + " - " + Utils.mediumDate(reservation.getReturnDate(), this));
                    ((TextView) findViewById(R.id.returnHintTextView)).setText(airportModel.getAirport(reservation.getToPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + airportModel.getAirport(reservation.getFromPort()).getCity());

                    routeStr = "";
                    count = 0;
                    for (Flight fl : reservation.getReturnFlightArray()) {
                        String supDepart = "";
                        String supArrive = "";
                        if (count == 0) {
                            basic.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
                            ((ImageView) findViewById(R.id.imageViewReturnAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                            count++;
                        }
                        compareDepart.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
                        compareArrive.set(fl.getArriveYear(), fl.getArriveMonth(), fl.getArriveDay());
                        long ex;
                        if ((ex = (compareDepart.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                            supDepart = "<sup><em><small>+" + ex + "</small></em></sup>";
                        }
                        if ((ex = (compareArrive.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                            supArrive = "<sup><em><small>+" + ex + "</small></em></sup>";
                        }
                        routeStr += fl.getDepartTime() + supDepart + " " + fl.getDepartPort()
                                + " - " + fl.getArriveTime() + supArrive + " "
                                + fl.getArrivePort() + "<br/>";

                    }
                    routeStr = routeStr.length() > 0 ? routeStr.substring(0,
                            routeStr.length() - 5) : routeStr;
                    ((TextView) findViewById(R.id.textViewReturnRoute)).setText(Html.fromHtml(routeStr));
                    ((TextView) findViewById(R.id.textViewReturnItinerary)).setText(Utils.strTimeDiff(reservation.getReturnFlightArray()[0].getDepartDatetime(), reservation
                            .getReturnFlightArray()[reservation.getReturnFlightArray().length - 1]
                            .getArriveDatetime(), reservation.getReturnFlightArray()[0].getDepartTimezone() - reservation.getReturnFlightArray()[reservation.getReturnFlightArray().length - 1].getArriveTimezone()) + "  ( " + ((reservation.getReturnFlightArray().length + reservation.getReturnHiddenTransit()) == 1 ? getString(R.string.direct) : (reservation.getReturnFlightArray().length - 1 + reservation.getReturnHiddenTransit()) + " " + getString(R.string.transit)) + " )");

                } else if (reservation.getCarrierType().equals("train")) {
                    ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns) + " - " + Utils.mediumDate(reservation.getReturnDate(), this));
                    ((TextView) findViewById(R.id.returnHintTextView)).setText(stationModel.getStation(reservation.getToPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + stationModel.getStation(reservation.getFromPort()).getCity());
                    ((ImageView) findViewById(R.id.imageViewReturnAirline)).setImageResource(R.drawable.ic_kai);
                    ((ImageView) findViewById(R.id.imageViewReturnCarrierType)).setImageResource(R.drawable.ic_train_go);
                    findViewById(R.id.imageViewReturnCarrierType).setScaleX(-1);
                    routeStr = "";
                    for (Train fl : reservation.getReturnTrainArray()) {
                        String supDepart = "";
                        String supArrive = "";
                        compareDepart.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
                        compareArrive.set(fl.getArriveYear(), fl.getArriveMonth(), fl.getArriveDay());
                        long ex;
                        if ((ex = (compareDepart.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                            supDepart = "<sup><em><small>+" + ex + "</small></em></sup>";
                        }
                        if ((ex = (compareArrive.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0) {
                            supArrive = "<sup><em><small>+" + ex + "</small></em></sup>";
                        }
                        routeStr += fl.getDepartTime() + supDepart + " " + fl.getDepartPort()
                                + " - " + fl.getArriveTime() + supArrive + " "
                                + fl.getArrivePort() + "<br/>";

                    }
                    routeStr = routeStr.length() > 0 ? routeStr.substring(0,
                            routeStr.length() - 5) : routeStr;
                    ((TextView) findViewById(R.id.textViewReturnRoute)).setText(Html.fromHtml(routeStr));
                    ((TextView) findViewById(R.id.textViewReturnItinerary)).setText(Utils.strTimeDiff(reservation.getReturnTrainArray()[0].getDepartDatetime(), reservation
                            .getReturnTrainArray()[reservation.getReturnTrainArray().length - 1]
                            .getArriveDatetime(), reservation.getReturnTrainArray()[0].getDepartTimezone() - reservation.getReturnTrainArray()[reservation.getReturnTrainArray().length - 1].getArriveTimezone()));
                }
                ((TextView) findViewById(R.id.textViewReturnBookingCode)).setText(reservation.getCarrierID1().equals(reservation.getCarrierID2()) && (reservation.getBookingCode2() == null || reservation.getBookingCode2().equals("")) ? reservation.getBookingCode1() : reservation.getBookingCode2());

                if (reservation.getClass2().equals("pro")) {
                    ((TextView) findViewById(R.id.textViewReturnClass)).setText(R.string.class_pro);
                    ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.green_500));
                } else if (reservation.getClass2().equals("eco")) {
                    ((TextView) findViewById(R.id.textViewReturnClass)).setText(R.string.class_eco);
                    ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.blue_500));
                } else if (reservation.getClass2().equals("bus")) {
                    ((TextView) findViewById(R.id.textViewReturnClass)).setText(R.string.class_bus);
                    ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.orange_500));
                } else if (reservation.getClass2().equals("exe")) {
                    ((TextView) findViewById(R.id.textViewReturnClass)).setText(R.string.class_exe);
                    ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.red_500));
                }
            } else {
                findViewById(R.id.returnFlight).setVisibility(View.GONE);
            }

            ((TextView) findViewById(R.id.departPortCodeTextView)).setText(reservation.getFromPort());
            ((TextView) findViewById(R.id.arrivePortCodeTextView)).setText(reservation.getToPort());
            if (reservation.isReturn()) {
                ((ImageView) findViewById(R.id.directionArrowImageView)).setImageResource(R.drawable.ic_swap);
                ((TextView) findViewById(R.id.itineraryTripTextView)).setText(R.string.roundtrip);
            }
            ((TextView) findViewById(R.id.passengerCountTextView)).setText(reservation.getAdultCount() + " " + getResources().getString(R.string.adult).toLowerCase() + (reservation.getChildCount() > 0 ? ", " + reservation.getChildCount() + " " + getResources().getString(R.string.child).toLowerCase() : "") + (reservation.getInfantCount() > 0 ? ", " + reservation.getInfantCount() + " " + getResources().getString(R.string.infant).toLowerCase() : ""));

            ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(reservation.getAmount() / (Integer.toString(reservation.getAmount()).length() >= 3 ? 1000 : 1))));
            ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(reservation.getAmount()).substring(Integer.toString(reservation.getAmount()).length() - (Integer.toString(reservation.getAmount()).length() >= 3 ? 3 : 0)));
        }
        ((TextView) findViewById(R.id.textViewDestinationBank)).setText(getString(R.string.destination_bank)+" : "+bank.getName()+" - "+bank.getOwner());
        ((TextView) findViewById(R.id.textViewDestinationAccount)).setText(getString(R.string.destination_account)+" : "+bank.getAccount());

        ((EditText) findViewById(R.id.editTextAmount)).setText(Integer.toString(reservation.getAmount()));

        ArrayList<String> dateList = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(reservation.getTimeBooking1().substring(0, 4)), Integer.parseInt(reservation.getTimeBooking1().substring(5, 7))-1, Integer.parseInt(reservation.getTimeBooking1().substring(8, 10))); //Year, month and day of month
        Date toDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        while (!cal.getTime().after(toDate)) {
            dateList.add(Utils.mediumDate(format.format(cal.getTime()), getApplicationContext()));
            cal.add(Calendar.DATE, 1);
        }

        final Spinner spinnerPaidDate = (Spinner) findViewById(R.id.spinnerPaidDate);
        ArrayAdapter<String> dateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, dateList);
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaidDate.setAdapter(dateAdapter);

        final Spinner spinnerPaidHour = (Spinner) findViewById(R.id.spinnerPaidHour);
        ArrayList<String> hourList = new ArrayList<String>();
        for(int i=0; i<24; i++){
            hourList.add(i<=9?"0"+i:i+"");
        }
        ArrayAdapter<String> hourAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, hourList);
        hourAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaidHour.setAdapter(hourAdapter);

        final Spinner spinnerPaidMinute = (Spinner) findViewById(R.id.spinnerPaidMinute);
        ArrayList<String> minuteList = new ArrayList<String>();
        for(int i=0; i<60; i++){
            minuteList.add(i<=9?"0"+i:i+"");
        }
        ArrayAdapter<String> minuteAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, minuteList);
        minuteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaidMinute.setAdapter(minuteAdapter);

        findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean valid = true;
                final EditText paidFrom = findViewById(R.id.editTextPaidFrom);
                final EditText amount = findViewById(R.id.editTextAmount);

                if (amount.getText().toString().isEmpty()) {
                    amount.setError(getResources().getString(R.string.amount_empty));
                    valid = false;
                    amount.requestFocus();
                }
                else if (!amount.getText().toString().matches("[0-9]+?")) {
                    amount.setError(getResources().getString(R.string.amount_format_error));
                    valid = false;
                    amount.requestFocus();
                }

                if (paidFrom.getText().toString().isEmpty()) {
                    paidFrom.setError(getResources().getString(R.string.paid_from_empty));
                    valid = false;
                    paidFrom.requestFocus();
                }

                if(valid){
                    findViewById(R.id.linearLayoutPaymentConfirmationForm).setVisibility(View.GONE);
                    findViewById(R.id.confirmButton).setVisibility(View.GONE);
                    findViewById(R.id.progressPaymentConfirmation).setVisibility(View.VISIBLE);

                    final Map<String, String> params = new HashMap<>();
                    params.put("reservation_id", Integer.toString(reservation.getReservationID()));
                    params.put("bank_id", Integer.toString(bank.getBankID()));
                    params.put("amount", amount.getText().toString());
                    params.put("paid_from", paidFrom.getText().toString());
                    params.put("paid_date", Integer.toString(spinnerPaidDate.getSelectedItemPosition()));
                    params.put("paid_hour", Integer.toString(spinnerPaidHour.getSelectedItemPosition()));
                    params.put("paid_minute", Integer.toString(spinnerPaidMinute.getSelectedItemPosition()));

                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "confirm_payment", params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                if(response.getInt("status")==1){
                                    reservationModel.confirmPayment(reservation.getReservationID(), bank.getBankID(), Integer.parseInt(amount.getText().toString()), paidFrom.getText().toString(), response.getString("paid_time_stamp"), "N");
                                    Intent intent = new Intent(PaymentConfirmationActivity.this, PaymentAfterConfirmActivity.class);
                                    intent.putExtra("reservation_id", reservation.getReservationID());
                                    startActivity(intent);
                                }
                                else{
                                    dialogErrorConnection();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialogErrorConnection();
                            }



                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);
                }
            }
        });



    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        findViewById(R.id.linearLayoutPaymentConfirmationForm).setVisibility(View.VISIBLE);
        findViewById(R.id.confirmButton).setVisibility(View.VISIBLE);
        findViewById(R.id.progressPaymentConfirmation).setVisibility(View.GONE);

        mDialog.show();
    }
}
