package com.tiketextra.tiketextra.activity.reservation.flight;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
//import android.util.Log;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
//import com.tiketextra.tiketextra.MainActivity;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.MenuOnlineActivity;
import com.tiketextra.tiketextra.activity.flight.FlightPaymentActivity;
import com.tiketextra.tiketextra.activity.flight.FlightSearchActivity;
import com.tiketextra.tiketextra.activity.hotel.HotelSearchActivity;
import com.tiketextra.tiketextra.activity.payment.BankCVSResultGuideActivity;
import com.tiketextra.tiketextra.activity.payment.PaymentAfterConfirmActivity;
import com.tiketextra.tiketextra.activity.train.TrainSearchActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.FlightReservationRecyclerViewAdapter;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.helper.ViewAnimation;
import com.tiketextra.tiketextra.listener.ReservationItemListener;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Fare;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TicketBookingActivity extends BaseActivity implements ReservationItemListener {

    private boolean rotate = false;
    private View layoutFlightSearch, layoutTrainSearch, layoutHotelSearch;
    private View layoutBackDrop;
    private FlightReservationRecyclerViewAdapter adapter;
    private ArrayList<Reservation> reservations;

    private ReservationModel reservationModel;
    private SettingModel settingModel;

    private int exponent = 0, base = 3;
    private boolean flag = false, flagRepeat = false, needUpdate;
    private Handler handler;
    private SessionManager sessionManager;
    private Runnable statusChecker = new Runnable() {

        private void threadMsg(String msg) {

            if (!msg.equals(null) && !msg.equals("")) {
                Message msgObj = handler.obtainMessage();
                Bundle b = new Bundle();
                b.putString("message", msg);
                msgObj.setData(b);
                handler.sendMessage(msgObj);
            }
        }

        @Override
        public void run() {
            needUpdate = settingModel.updateBookingSessionTime();
            try {
                if((flagRepeat && needUpdate) || (flagRepeat)) {


                    if(isLoggedIn()){
                        threadMsg("show");
                        Log.e("login", "check res");
                        Map<String, String> params = new HashMap<>();
                        params.put("email", getSessionEmail());
                        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "get_user_reservation", params, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
//                                Log.e("response", response.toString());
                                try {
                                    if (response.getInt("status") == 1) {
                                        sessionManager.updateSession(response.getString("title"), Utils.upperCaseAllFirst(response.getString("name").replaceAll("\\s+", " ")), response.getString("mobile"), getSessionEmail());

                                        Gson gson = new Gson();
                                        Reservation reservation;
                                        JSONArray arrReservation = response.getJSONArray("reservation");
                                        for (int i = 0; i < arrReservation.length(); i++) {
                                            reservation = gson.fromJson(arrReservation.get(i).toString(), Reservation.class);
//                                        Log.e("res", reservation.getReservationID() + ", "+reservation.getBankID());
                                            if (reservationModel.getReservationExists(reservation.getReservationID())) {
                                                reservationModel.updateReservationObject(reservation);
                                            } else {
                                                reservationModel.insertReservationObject(reservation);
                                            }
                                            if (reservation.getBankID() != 0) {
                                                reservationModel.setReservationBankID(reservation.getReservationID(), reservation.getBankID());
                                            }

                                        }
                                        JSONArray passengers = response.getJSONArray("passenger");
                                        for (int i = 0; i < passengers.length(); i++) {
                                            Passenger passenger = gson.fromJson(passengers.get(i).toString(), Passenger.class);
                                            if(reservationModel.getPassengerExists(passenger.getPassengerID())) {
                                                reservationModel.updatePassengerObject(passenger);
                                            }
                                            else {
                                                reservationModel.insertPassengerObject(passenger);
                                            }
                                        }
                                        JSONArray fares = response.getJSONArray("fare");
                                        for (int i = 0; i < fares.length(); i++) {
                                            Fare fare = gson.fromJson(fares.get(i).toString(), Fare.class);
                                            if(reservationModel.getFareExists(fare.getFareID())) {
                                                reservationModel.updateFareObject(fare);
                                            }
                                            else {
                                                reservationModel.insertFareObject(fare);
                                            }
                                        }

//                                        UserModel userModel = new UserModel(TicketBookingActivity.this);
//
//                                        JSONArray passengerArr = response.getJSONArray("user_passenger");
//                                        for (int i = 0; i < passengerArr.length(); i++) {
//                                            JSONObject passenger = (JSONObject) passengerArr.get(i);
//                                            userModel.updatePassenger(passenger.getInt("passenger_id"), passenger.getString("title_id"), passenger.getString("name"), passenger.getString("date_of_birth"), passenger.getString("nationality_id"), passenger.getString("passport_number"), passenger.getString("passport_issuing_country"), passenger.getString("passport_expiry_date"));
//                                        }

                                        reservations.clear();
                                        reservations = reservationModel.getFlightReservationBooking();

                                        initReservationList();

                                    }
                                }catch (JSONException e) {
                                    e.printStackTrace();
                                    threadMsg("close");
                                    dialogErrorSync();
                                }
                                threadMsg("close");
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError arg0) {
                                VolleyLog.d("Error: ", arg0.getMessage());
                                threadMsg("close");
                                dialogErrorSync();
                            }
                        });
                        AppController.getInstance().addToRequestQueue(jsonReq);

                    }
                    else {
                        ArrayList<Reservation> ress = reservationModel.getFlightReservationBooking();
                        if (ress.size() > 0) {
                            threadMsg("show");
                            String reservation_id = "";
                            for (int i = 0; i < ress.size(); i++) {
                                reservation_id += "," + ress.get(i).getReservationID();
                            }

                            reservation_id = reservation_id.substring(1);

//                            Log.e("background srv", "booking check " + reservation_id);


                            Map<String, String> params = new HashMap<>();
                            params.put("reservation_id", reservation_id);
                            JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "get_reservation", params, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

//                                    Log.e("res", response.toString());

                                    try {
                                        Reservation reservation;
                                        JSONArray arrReservation = response.getJSONArray("reservation");
                                        for (int i = 0; i < arrReservation.length(); i++) {


                                            reservation = new Gson().fromJson(arrReservation.get(i).toString(), Reservation.class);
                                            reservationModel.updateReservationObject(reservation);


                                            if (reservation.getBankID() != 0) {
                                                reservationModel.setReservationBankID(reservation.getReservationID(), reservation.getBankID());
                                            }
                                        }

                                        reservations.clear();
                                        reservations = reservationModel.getFlightReservationBooking();

                                        initReservationList();


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        threadMsg("close");
                                        dialogErrorSync();
                                    }
                                    threadMsg("close");
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError arg0) {
                                    VolleyLog.d("Error: ", arg0.getMessage());
                                    threadMsg("close");
                                    dialogErrorSync();
                                }
                            });
                            AppController.getInstance().addToRequestQueue(jsonReq);
                        }
                    }

                }

            } finally {
                handler.postDelayed(statusChecker, (long) (!flagRepeat && needUpdate ? 3000 : Configuration.RESERVATION_CHECK_DELAY * Math.pow(base, exponent <= 3 ? exponent++ : exponent)));
                flagRepeat = true;
            }
        }
    };

    void startRepeatingTask() {
        statusChecker.run();
    }

    void stopRepeatingTask() {
//        Log.e("stop", "repeat");
        handler.removeCallbacks(statusChecker);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(flag){
//            Log.e("onResume", "repeat");
            startRepeatingTask();
        }
        flag = true;
    }

    @Override
    protected void onPause() {
//        Log.e("onPause", "repeat");
        super.onPause();
        stopRepeatingTask();
    }


    @Override
    public void onDestroy() {
//        Log.e("onDestroy", "repeat");
        super.onDestroy();
        stopRepeatingTask();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_booking);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarBookingList);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.my_bookings_hint);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
//                    case R.id.menu_home : {
//                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
//                        break;
//                    }
                    case R.id.menu_hotel : {
//                        Intent intent = new Intent(getApplicationContext(), com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class);
//                        startActivity(intent);
                        startActivity(new Intent(getApplicationContext(), com.tiketextra.tiketextra.activity.reservation.hotel.TicketBookingActivity.class));
                        break;
                    }
                    case R.id.menu_flight : return true;
                    case R.id.menu_train : {
                        startActivity(new Intent(getApplicationContext(), com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity.class));
                        break;
                    }
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_flight);

        findViewById(R.id.fabBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TicketBookingActivity.class));
            }
        });
        findViewById(R.id.fabIssuedActive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TicketIssuedActivity.class);
                intent.putExtra("active", true);
                startActivity(intent);
            }
        });
        findViewById(R.id.fabIssuedHistory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TicketIssuedActivity.class);
                intent.putExtra("active", false);
                startActivity(intent);
            }
        });

        settingModel = new SettingModel(this);
        sessionManager = new SessionManager(this);
        reservationModel = new ReservationModel(this);
        handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                String aResponse = msg.getData().getString("message");
                if(aResponse.equals("show")){
                    findViewById(R.id.syncLayout).setVisibility(View.VISIBLE);
                    findViewById(R.id.reservationLayout).setVisibility(View.GONE);
                }
                else{
                    findViewById(R.id.syncLayout).setVisibility(View.GONE);
                    findViewById(R.id.reservationLayout).setVisibility(View.VISIBLE);
                }
            }
        };

        reservations = reservationModel.getFlightReservationBooking();

        initReservationList();

        startRepeatingTask();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.flight_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_flight_search) {
            if(settingModel.getValue("flight_feature").equals("1")) {
                startActivity(new Intent(TicketBookingActivity.this, FlightSearchActivity.class));
            }
            else {
                dialogFeatureDisabled();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initReservationList(){
        layoutBackDrop = findViewById(R.id.backDrop);
        layoutBackDrop.setVisibility(View.GONE);

        if (reservations.size() == 0) {
            findViewById(R.id.bookingEmpty).setVisibility(View.VISIBLE);
            findViewById(R.id.recyclerViewBooking).setVisibility(View.GONE);

            initEmptyReservation();

        } else {
            findViewById(R.id.bookingEmpty).setVisibility(View.GONE);
            findViewById(R.id.recyclerViewBooking).setVisibility(View.VISIBLE);

            RecyclerView recyclerView = findViewById(R.id.recyclerViewBooking);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);

            adapter = new FlightReservationRecyclerViewAdapter(reservations, true, this, this);
            recyclerView.setAdapter(adapter);
        }
    }

    private void initEmptyReservation() {
        FloatingActionButton fabFlight = findViewById(R.id.fabFlightSearch);
        FloatingActionButton fabTrain = findViewById(R.id.fabTrainSearch);
        FloatingActionButton fabHotel = findViewById(R.id.fabHotelSearch);
        final FloatingActionButton fabSearch = findViewById(R.id.fabSearch);
        layoutFlightSearch = findViewById(R.id.layoutFlightSearch);
        layoutTrainSearch = findViewById(R.id.layoutTrainSearch);
        layoutHotelSearch = findViewById(R.id.layoutHotelSearch);
        ViewAnimation.initShowOut(layoutFlightSearch);
        ViewAnimation.initShowOut(layoutTrainSearch);
        ViewAnimation.initShowOut(layoutHotelSearch);

        fabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFabMode(v);
            }
        });
        layoutBackDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFabMode(fabSearch);
            }
        });

        fabFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(settingModel.getValue("flight_feature").equals("1")) {
                    startActivity(new Intent(TicketBookingActivity.this, FlightSearchActivity.class));
                }
                else {
                    dialogFeatureDisabled();
                }
            }
        });

        fabTrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(settingModel.getValue("train_feature").equals("1")) {
                    startActivity(new Intent(TicketBookingActivity.this, TrainSearchActivity.class));
                }
                else {
                    dialogFeatureDisabled();
                }
            }
        });

        fabHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(settingModel.getValue("hotel_feature").equals("1")) {
                    startActivity(new Intent(TicketBookingActivity.this, HotelSearchActivity.class));
                }
                else {
                    dialogFeatureDisabled();
                }
            }
        });
    }

    private void toggleFabMode(View v) {
        rotate = ViewAnimation.rotateFab(v, !rotate);
        if (rotate) {
            ViewAnimation.showIn(layoutFlightSearch);
            ViewAnimation.showIn(layoutTrainSearch);
            ViewAnimation.showIn(layoutHotelSearch);
            layoutBackDrop.setVisibility(View.VISIBLE);
        } else {
            ViewAnimation.showOut(layoutFlightSearch);
            ViewAnimation.showOut(layoutTrainSearch);
            ViewAnimation.showOut(layoutHotelSearch);
            layoutBackDrop.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(TicketBookingActivity.this, com.tiketextra.tiketextra.MainActivity.class);
        // i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void dialogErrorSync(){
        dialog(R.string.error_occurred, R.string.error_data_sync);
    }

    private void dialogErrorConnection() {
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogFeatureDisabled(){
        dialog(R.string.coming_soon, R.string.feature_disabled);
    }

    private void dialog(int title, int message) {
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }

    @Override
    public void onItemClick(Reservation item) {
        final Reservation reservation = item;
        Map<String, String> params = new HashMap<>();
        params.put("reservation_id", Integer.toString(reservation.getReservationID()));
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "reservation", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

//                Log.e("response", response.toString());

                try {
//                    Log.e("bankid", reservation.getBank() + reservation.getBankID());
                    Gson gson = new Gson();
                    ArrayList<Bank> banks = new ArrayList<>();
                    Bank reservationBank = null;
                    JSONArray bank = response.getJSONArray("bank");
                    for (int i = 0; i < bank.length(); i++) {
                        Bank curr = gson.fromJson(bank.get(i).toString(), Bank.class);
                        banks.add(curr);
                        if (reservation.getBankID() == curr.getBankID()) {
                            reservationBank = curr;
                        }
                    }

                    if (reservation.getBank() == null) {
                        Intent intent = new Intent(TicketBookingActivity.this, FlightPaymentActivity.class);
                        intent.putExtra("reservation_id", reservation.getReservationID());
                        intent.putExtra("bank", gson.toJson(banks));
                        startActivity(intent);
                    }
                    else if(reservation.getIsConfirmed().equals("Y")){
                        Intent intent = new Intent(TicketBookingActivity.this, PaymentAfterConfirmActivity.class);
                        intent.putExtra("reservation_id", reservation.getReservationID());
                        startActivity(intent);
                    }
                    else {
                        if (reservationBank.getGateway().equals("1") || reservationBank.getGateway().equals("4") || reservationBank.getGateway().equals("5")) {
                                final Bank finalReservationBank = reservationBank;
                                Map<String, String> params = new HashMap<>();
                                params.put("reservation_id", Integer.toString(reservation.getReservationID()));
                                params.put("bank_id", Integer.toString(finalReservationBank.getBankID()));

                                JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + (finalReservationBank.getGateway().equals("1") || finalReservationBank.getGateway().equals("4")?"get_bank_user_guide":"get_cvs_user_guide"), params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {

                                            if(response.getInt("status")==1){
                                                Gson gson = new Gson();
                                                Intent intent = new Intent(TicketBookingActivity.this, BankCVSResultGuideActivity.class);
                                                intent.putExtra("reservation_id", reservation.getReservationID());
                                                intent.putExtra("bank", gson.toJson(finalReservationBank));
//                                                intent.putExtra("type", reservation.getCarrierType());

                                                Bundle bundle = new Bundle();
                                                bundle.putString("atm", response.getString("atm"));
                                                bundle.putString("mobile_banking", response.getString("mobile_banking"));
                                                bundle.putString("internet_banking", response.getString("internet_banking"));
                                                bundle.putString("sms_banking", response.getString("sms_banking"));
                                                bundle.putString("convenience_store", response.getString("convenience_store"));
                                                intent.putExtra("guidance", bundle);
                                                startActivity(intent);
                                            }
                                            else{
                                                dialogErrorConnection();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            dialogErrorConnection();
                                        }



                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError arg0) {
                                        VolleyLog.d("Error: ", arg0.getMessage());
                                        dialogErrorConnection();
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(jsonReq);
                            }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogErrorConnection();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                dialogErrorConnection();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);

    }
}
