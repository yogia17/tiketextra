package com.tiketextra.tiketextra.activity.reservation.flight;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.custom_view.ItemPassengerLayout;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.helper.ViewAnimation;
import com.tiketextra.tiketextra.model.AirlineModel;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Airport;
import com.tiketextra.tiketextra.object.Flight;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

import org.sufficientlysecure.htmltextview.HtmlTextView;

public class TicketIssuedDetailActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ticket_issued_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTicketIssued);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.ticket_detail);

        ReservationModel reservationModel = new ReservationModel(this);
        final Reservation reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));


        ((TextView) findViewById(R.id.departBookingCode)).setText(reservation.getBookingCode1());

        LinearLayout passengerLayout = (LinearLayout) findViewById(R.id.passengerLayout);
        int number = 0;
        for (Passenger entry : reservation.getPassengers()) {
            ItemPassengerLayout item = new ItemPassengerLayout(this);
            item.setText(++number+".", (entry.getType().equals("adult")? ItineraryHelper.decodeAdultTitle(entry.getTitle(), getApplicationContext()) : (entry.getType().equals("child") ? ItineraryHelper.decodeChildTitle(entry.getTitle(), getApplicationContext()) : ItineraryHelper.decodeInfantTitle(entry.getTitle(), getApplicationContext()) )) + " " + entry.getName(), entry.getBirthDate() != null ? Utils.mediumDate(entry.getBirthDate(), this) : getString(getResources().getIdentifier(entry.getType(), "string", getPackageName())));
            passengerLayout.addView(item);
        }

        String passenger = reservation.getAdultCount()+" "+getResources().getString(R.string.adult).toLowerCase() + (reservation.getChildCount() > 0 ? ", "+reservation.getChildCount()+" "+getResources().getString(R.string.child).toLowerCase():"") + (reservation.getInfantCount() > 0 ? ", "+reservation.getInfantCount()+" "+getResources().getString(R.string.infant).toLowerCase():"");
        ((TextView) findViewById(R.id.textViewPassengerCount)).setText(passenger);

        AirportModel airportModel = new AirportModel(this);

        ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(reservation.getDepartDate(), this));
        ((TextView) findViewById(R.id.departHintTextView)).setText(airportModel.getAirport(reservation.getFromPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ airportModel.getAirport(reservation.getToPort()).getCity());


        AirlineModel airlineModel = new AirlineModel(this);

        for (int i = 0; i < reservation.getDepartFlightArray().length || i <= 5; i++) {
            if (i == 0) {
                Flight fl = reservation.getDepartFlightArray()[i];
                ((ImageView) findViewById(R.id.depart_imageViewAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                ((TextView) findViewById(R.id.depart_textViewAirline)).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                ((TextView) findViewById(R.id.depart_textViewFlightNumber)).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                ((TextView) findViewById(R.id.depart_departTime)).setText(fl.getDepartTime());
                ((TextView) findViewById(R.id.depart_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(R.id.depart_arriveTime)).setText(fl.getArriveTime());
                ((TextView) findViewById(R.id.depart_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Airport airport = airportModel.getAirport(fl.getDepartPort());
                ((TextView) findViewById(R.id.depart_departCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_departAirport)).setText(airport.getAirportName());
                airport = airportModel.getAirport(fl.getArrivePort());
                ((TextView) findViewById(R.id.depart_arriveCity)).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_arriveAirport)).setText(airport.getAirportName());

            } else if (i > 0 && i <= 5 && i < reservation.getDepartFlightArray().length) {
                Flight fl = reservation.getDepartFlightArray()[i];
                ((ImageView) findViewById(getResources().getIdentifier("depart" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewAirline", "id", getPackageName()))).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Airport airport = airportModel.getAirport(fl.getDepartPort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departAirport", "id", getPackageName()))).setText(airport.getAirportName());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getDepartFlightArray()[i-1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getDepartFlightArray()[i-1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + airport.getCity());

                airport = airportModel.getAirport(fl.getArrivePort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveCity", "id", getPackageName()))).setText(airport.getCity() + " ("+airport.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveAirport", "id", getPackageName()))).setText(airport.getAirportName());


                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
            } else if (i > 0 && i <= 5 && i >= reservation.getDepartFlightArray().length) {
                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
            }
        }

        //------------------------------------------------------------------------
        if(!reservation.isReturn()){
            findViewById(R.id.returnDetailInfoLayout).setVisibility(View.GONE);
        }
        else {
            ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns) + " - " + Utils.mediumDate(reservation.getReturnDate(), this));
            ((TextView) findViewById(R.id.returnHintTextView)).setText(airportModel.getAirport(reservation.getToPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + airportModel.getAirport(reservation.getFromPort()).getCity());

            ((TextView) findViewById(R.id.returnBookingCode)).setText(reservation.getCarrierID1().equals(reservation.getCarrierID2()) && (reservation.getBookingCode2()==null || reservation.getBookingCode2().equals("")) ? reservation.getBookingCode1() : reservation.getBookingCode2());

            for (int i = 0; i < reservation.getReturnFlightArray().length || i <= 5; i++) {
                if (i == 0) {
                    Flight fl = reservation.getReturnFlightArray()[i];
                    ((ImageView) findViewById(R.id.return_imageViewAirline)).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                    ((TextView) findViewById(R.id.return_textViewAirline)).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                    ((TextView) findViewById(R.id.return_textViewFlightNumber)).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                    ((TextView) findViewById(R.id.return_departTime)).setText(fl.getDepartTime());
                    ((TextView) findViewById(R.id.return_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(R.id.return_arriveTime)).setText(fl.getArriveTime());
                    ((TextView) findViewById(R.id.return_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Airport airport = airportModel.getAirport(fl.getDepartPort());
                    ((TextView) findViewById(R.id.return_departCity)).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(R.id.return_departAirport)).setText(airport.getAirportName());
                    airport = airportModel.getAirport(fl.getArrivePort());
                    ((TextView) findViewById(R.id.return_arriveCity)).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(R.id.return_arriveAirport)).setText(airport.getAirportName());

                } else if (i > 0 && i <= 5 && i < reservation.getReturnFlightArray().length) {
                    Flight fl = reservation.getReturnFlightArray()[i];


                    ((ImageView) findViewById(getResources().getIdentifier("return" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(getResources().getIdentifier(("ic_air_" + (fl.getAirlineCode().equals("AIR") ? "AIR" : fl.getFlightCode())).toLowerCase(), "drawable", getPackageName()));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewAirline", "id", getPackageName()))).setText(airlineModel.getAirline(fl.getFlightCode()).getName());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(fl.getFlightNumber() + " " + fl.getDurationTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Airport airport = airportModel.getAirport(fl.getDepartPort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departCity", "id", getPackageName()))).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departAirport", "id", getPackageName()))).setText(airport.getAirportName());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getReturnFlightArray()[i - 1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getReturnFlightArray()[i - 1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) + " " + airport.getCity());

                    airport = airportModel.getAirport(fl.getArrivePort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveCity", "id", getPackageName()))).setText(airport.getCity() + " (" + airport.getIataCode() + ")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveAirport", "id", getPackageName()))).setText(airport.getAirportName());


                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
                } else if (i > 0 && i <= 5 && i >= reservation.getReturnFlightArray().length) {
                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
                }
            }


        }

        ((HtmlTextView) findViewById(R.id.textViewReservationNotes)).setHtml(getString(R.string.important_notes_text));

        findViewById(R.id.scrollViewTicketIssued).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        ViewAnimation.showOut(findViewById(R.id.toolbarTicketIssued));
//                        findViewById(R.id.toolbarTicketIssued).setVisibility(View.GONE);
                        break;

                    case MotionEvent.ACTION_UP:
                        ViewAnimation.showIn(findViewById(R.id.toolbarTicketIssued));
//                        findViewById(R.id.toolbarTicketIssued).setVisibility(View.VISIBLE);
                        break;

                    case MotionEvent.ACTION_MOVE:
                        findViewById(R.id.toolbarTicketIssued).setVisibility(View.GONE);
                        break;



                    case MotionEvent.ACTION_CANCEL:
                        Log.d("action","Action was CANCEL");
                        break;

                    case MotionEvent.ACTION_OUTSIDE:
                        Log.d("action", "Movement occurred outside bounds of current screen element");
                        break;
                }
                return false;
            }
        });


    }
}
