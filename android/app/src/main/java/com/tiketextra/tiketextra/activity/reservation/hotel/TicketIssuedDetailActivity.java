package com.tiketextra.tiketextra.activity.reservation.hotel;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONException;

public class TicketIssuedDetailActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ticket_issued_detail_hotel);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTicketIssued);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.ticket_detail);

        ReservationModel reservationModel = new ReservationModel(this);
        final Reservation reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));

        ((TextView) findViewById(R.id.textViewName)).setText(reservation.getContactName());
        ((TextView) findViewById(R.id.textViewEmail)).setText(reservation.getContactEmail());
        ((TextView) findViewById(R.id.textViewPhone)).setText(reservation.getContactPhone());

        try {
            ((TextView) findViewById(R.id.textViewHotelName)).setText(reservation.getHotel().getName());
            RatingBar ratingBar = findViewById(R.id.ratingBar);
            if (reservation.getHotel().getRating() > 0) {
                ratingBar.setRating(reservation.getHotel().getRating());
                LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
                stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.grey_100), PorterDuff.Mode.SRC_ATOP);
            } else {
                ratingBar.setVisibility(View.GONE);
            }
            ((TextView) findViewById(R.id.textViewBookingCode)).setText(reservation.getBookingCode1());

            ((TextView) findViewById(R.id.textViewCheckinDate)).setText(Utils.mediumDate(reservation.getCheckinDate(), getApplicationContext()));
            ((TextView) findViewById(R.id.textViewCheckoutDate)).setText(Utils.mediumDate(reservation.getCheckoutDate(), getApplicationContext()));
            int dateDiff = Utils.dateDiff(reservation.getCheckinDate(), reservation.getCheckoutDate());
            ((TextView) findViewById(R.id.textViewDuration)).setText(dateDiff + " " + getString(dateDiff > 1 ? R.string.nights : R.string.night));
            ((TextView) findViewById(R.id.textViewNumberRooms)).setText(reservation.getNumberRooms() + " " + getString(reservation.getNumberRooms() > 1 ? R.string.rooms : R.string.room));

            ((TextView) findViewById(R.id.textViewRoomName)).setText(reservation.getRoom().getName());
            if (reservation.getRoom().isBreakfast()) {
                findViewById(R.id.layoutBreakfast).setVisibility(View.VISIBLE);
                findViewById(R.id.layoutNotBreakfast).setVisibility(View.GONE);
            } else {
                findViewById(R.id.layoutBreakfast).setVisibility(View.GONE);
                findViewById(R.id.layoutNotBreakfast).setVisibility(View.VISIBLE);
            }
            if (reservation.getRoom().getPolicy().equals("")) {
                findViewById(R.id.layoutPolicy).setVisibility(View.GONE);
            } else {
                findViewById(R.id.layoutPolicy).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.textViewPolicy)).setText(reservation.getRoom().getPolicy());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
