package com.tiketextra.tiketextra.activity.reservation.train;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.custom_view.ItemPassengerLayoutTrainDetail;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.helper.ViewAnimation;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Station;
import com.tiketextra.tiketextra.object.Train;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.util.Utils;

import org.sufficientlysecure.htmltextview.HtmlTextView;

public class TicketIssuedDetailActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ticket_issued_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTicketIssued);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.ticket_detail);

        ReservationModel reservationModel = new ReservationModel(this);
        final Reservation reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));

        ((ImageView) findViewById(R.id.imageViewDepartCarrierType)).setImageResource(R.drawable.ic_train_go);
        ((ImageView) findViewById(R.id.imageViewReturnCarrierType)).setImageResource(R.drawable.ic_train_go);
        findViewById(R.id.imageViewReturnCarrierType).setScaleX(-1);
        ((TextView) findViewById(R.id.departBookingCode)).setText(reservation.getBookingCode1());

        LinearLayout passengerLayout = (LinearLayout) findViewById(R.id.passengerLayout);
        int number = 0;
        for (Passenger entry : reservation.getPassengers()) {
            ItemPassengerLayoutTrainDetail item = new ItemPassengerLayoutTrainDetail(this);
            item.setText(++number+".", (entry.getType().equals("adult")? ItineraryHelper.decodeAdultTitle(entry.getTitle(), getApplicationContext()) : (entry.getType().equals("child") ? ItineraryHelper.decodeChildTitle(entry.getTitle(), getApplicationContext()) : ItineraryHelper.decodeInfantTitle(entry.getTitle(), getApplicationContext()) )) + " " + entry.getName(), entry.getIDCardNumber(), entry.getDepartSeatFormatted(), entry.getReturnSeatFormatted());
            passengerLayout.addView(item);
        }

        String passenger = reservation.getAdultCount()+" "+getResources().getString(R.string.adult).toLowerCase() + (reservation.getChildCount() > 0 ? ", "+reservation.getChildCount()+" "+getResources().getString(R.string.child).toLowerCase():"") + (reservation.getInfantCount() > 0 ? ", "+reservation.getInfantCount()+" "+getResources().getString(R.string.infant).toLowerCase():"");
        ((TextView) findViewById(R.id.textViewPassengerCount)).setText(passenger);

        StationModel stationModel = new StationModel(this);

        ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(reservation.getDepartDate(), this));
        ((TextView) findViewById(R.id.departHintTextView)).setText(stationModel.getStation(reservation.getFromPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ stationModel.getStation(reservation.getToPort()).getCity());


        for (int i = 0; i < reservation.getDepartTrainArray().length || i <= 5; i++) {
            if (i == 0) {
                Train fl = reservation.getDepartTrainArray()[i];
                ((ImageView) findViewById(R.id.depart_imageViewAirline)).setImageResource(R.drawable.ic_kai);
                ((TextView) findViewById(R.id.depart_textViewAirline)).setText(fl.getTrainNameNumber());
                ((TextView) findViewById(R.id.depart_textViewFlightNumber)).setText(getString(getResources().getIdentifier("class_" + reservation.getClass1().toLowerCase(), "string", getPackageName())));
                ((TextView) findViewById(R.id.depart_departTime)).setText(fl.getDepartTime());
                ((TextView) findViewById(R.id.depart_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(R.id.depart_arriveTime)).setText(fl.getArriveTime());
                ((TextView) findViewById(R.id.depart_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Station station = stationModel.getStation(fl.getDepartPort());
                ((TextView) findViewById(R.id.depart_departCity)).setText(station.getCity());
                ((TextView) findViewById(R.id.depart_departAirport)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                station = stationModel.getStation(fl.getArrivePort());
                ((TextView) findViewById(R.id.depart_arriveCity)).setText(station.getCity());
                ((TextView) findViewById(R.id.depart_arriveAirport)).setText(station.getStationName() + " ("+station.getIataCode()+")");

            } else if (i > 0 && i <= 5 && i < reservation.getDepartTrainArray().length) {
                Train fl = reservation.getDepartTrainArray()[i];



                ((ImageView) findViewById(getResources().getIdentifier("depart" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(R.drawable.ic_kai);
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewAirline", "id", getPackageName()))).setText(fl.getTrainNameNumber());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(getString(getResources().getIdentifier("class_" + reservation.getClass1().toLowerCase(), "string", getPackageName())));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Station station = stationModel.getStation(fl.getDepartPort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departCity", "id", getPackageName()))).setText(station.getCity());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departAirport", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getDepartTrainArray()[i-1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getDepartTrainArray()[i-1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + station.getCity());

                station = stationModel.getStation(fl.getArrivePort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveCity", "id", getPackageName()))).setText(station.getCity());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveAirport", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");


                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
            } else if (i > 0 && i <= 5 && i >= reservation.getDepartTrainArray().length) {
                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
            }
        }

        //------------------------------------------------------------------------
        if(!reservation.isReturn()){
            findViewById(R.id.returnDetailInfoLayout).setVisibility(View.GONE);
        }
        else {
            ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns) + " - " + Utils.mediumDate(reservation.getReturnDate(), this));
            ((TextView) findViewById(R.id.returnHintTextView)).setText(stationModel.getStation(reservation.getToPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + stationModel.getStation(reservation.getFromPort()).getCity());

            ((TextView) findViewById(R.id.returnBookingCode)).setText(reservation.getCarrierID1().equals(reservation.getCarrierID2()) && (reservation.getBookingCode2()==null || reservation.getBookingCode2().equals("")) ? reservation.getBookingCode1() : reservation.getBookingCode2());


            for (int i = 0; i < reservation.getReturnTrainArray().length || i <= 5; i++) {
                if (i == 0) {
                    Train fl = reservation.getReturnTrainArray()[i];
                    ((ImageView) findViewById(R.id.return_imageViewAirline)).setImageResource(R.drawable.ic_kai);
                    ((TextView) findViewById(R.id.return_textViewAirline)).setText(fl.getTrainNameNumber());
                    ((TextView) findViewById(R.id.return_textViewFlightNumber)).setText(getString(getResources().getIdentifier("class_" + reservation.getClass2().toLowerCase(), "string", getPackageName())));
                    ((TextView) findViewById(R.id.return_departTime)).setText(fl.getDepartTime());
                    ((TextView) findViewById(R.id.return_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(R.id.return_arriveTime)).setText(fl.getArriveTime());
                    ((TextView) findViewById(R.id.return_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Station station = stationModel.getStation(fl.getDepartPort());
                    ((TextView) findViewById(R.id.return_departCity)).setText(station.getCity());
                    ((TextView) findViewById(R.id.return_departAirport)).setText(station.getStationName() + " (" + station.getIataCode() + ")");
                    station = stationModel.getStation(fl.getArrivePort());
                    ((TextView) findViewById(R.id.return_arriveCity)).setText(station.getCity());
                    ((TextView) findViewById(R.id.return_arriveAirport)).setText(station.getStationName() + " (" + station.getIataCode() + ")");

                } else if (i > 0 && i <= 5 && i < reservation.getReturnTrainArray().length) {
                    Train fl = reservation.getReturnTrainArray()[i];


                    ((ImageView) findViewById(getResources().getIdentifier("return" + i + "_imageViewAirline", "id", getPackageName()))).setImageResource(R.drawable.ic_kai);
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewAirline", "id", getPackageName()))).setText(fl.getTrainNameNumber());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewFlightNumber", "id", getPackageName()))).setText(getString(getResources().getIdentifier("class_" + reservation.getClass2().toLowerCase(), "string", getPackageName())));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Station station = stationModel.getStation(fl.getDepartPort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departCity", "id", getPackageName()))).setText(station.getCity());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departAirport", "id", getPackageName()))).setText(station.getStationName() + " (" + station.getIataCode() + ")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getReturnTrainArray()[i - 1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getReturnTrainArray()[i - 1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) + " " + station.getCity());

                    station = stationModel.getStation(fl.getArrivePort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveCity", "id", getPackageName()))).setText(station.getCity());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveAirport", "id", getPackageName()))).setText(station.getStationName() + " (" + station.getIataCode() + ")");


                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.VISIBLE);
                } else if (i > 0 && i <= 5 && i >= reservation.getReturnTrainArray().length) {
                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_flightNumber", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailFlight", "id", getPackageName())).setVisibility(View.GONE);
                }
            }


        }

        ((HtmlTextView) findViewById(R.id.textViewReservationNotes)).setHtml(getString(R.string.train_important_notes_text));

        findViewById(R.id.scrollViewTicketIssued).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        ViewAnimation.showOut(findViewById(R.id.toolbarTicketIssued));
//                        findViewById(R.id.toolbarTicketIssued).setVisibility(View.GONE);
                        break;

                    case MotionEvent.ACTION_UP:
                        ViewAnimation.showIn(findViewById(R.id.toolbarTicketIssued));
//                        findViewById(R.id.toolbarTicketIssued).setVisibility(View.VISIBLE);
                        break;

                    case MotionEvent.ACTION_MOVE:
                        findViewById(R.id.toolbarTicketIssued).setVisibility(View.GONE);
                        break;



                    case MotionEvent.ACTION_CANCEL:
                        Log.d("action","Action was CANCEL");
                        break;

                    case MotionEvent.ACTION_OUTSIDE:
                        Log.d("action", "Movement occurred outside bounds of current screen element");
                        break;
                }
                return false;
            }
        });


    }
}
