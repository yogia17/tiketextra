package com.tiketextra.tiketextra.activity.train;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.StationListViewAdapter;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.object.Station;

import java.util.ArrayList;
import java.util.Locale;

public class ChooseStationActivity extends BaseActivity {
    private EditText inputStationName;
    private ArrayList<Station> allStation = new ArrayList<>();


    private StationListViewAdapter adapter;
    private ListView listViewStation;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_station);
        toolbar = findViewById(R.id.toolbarChooseStation);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().getStringExtra("stationType").equals("fromPort")){
            ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.choose_from);
        }
        else if(getIntent().getStringExtra("stationType").equals("toPort")){
            ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.choose_to);
        }

        StationModel stationModel = new StationModel(this);
        allStation = stationModel.getAllStations(stationModel.isZeroHit()?"region":"hit");
        adapter = new StationListViewAdapter(this, allStation);
        listViewStation = findViewById(R.id.listViewStation);
        listViewStation.setAdapter(adapter);


        inputStationName = findViewById(R.id.inputStationName);
        inputStationName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = inputStationName.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });
    }


}
