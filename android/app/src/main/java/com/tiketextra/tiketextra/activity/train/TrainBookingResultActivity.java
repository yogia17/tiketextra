package com.tiketextra.tiketextra.activity.train;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.custom_view.ItemPassengerLayoutTrainDetail;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.object.Station;
import com.tiketextra.tiketextra.object.Train;
import com.tiketextra.tiketextra.util.Utils;

public class TrainBookingResultActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_summary);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarResultSummary);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.my_booking);

        ReservationModel reservationModel = new ReservationModel(this);
        final Reservation reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));


        findViewById(R.id.contactLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.passengerLayout).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.contactNameTextView)).setText(ItineraryHelper.decodeAdultTitle(reservation.getContactTitle(), getApplicationContext()) + " " + reservation.getContactName());
        ((TextView) findViewById(R.id.contactEmailTextView)).setText(reservation.getContactEmail());
        ((TextView) findViewById(R.id.contactPhoneTextView)).setText(reservation.getContactPhone());

        LinearLayout passengerLayout = (LinearLayout) findViewById(R.id.passengerLayout);
        int number = 0;
        for (Passenger entry : reservation.getPassengers()) {
            ItemPassengerLayoutTrainDetail item = new ItemPassengerLayoutTrainDetail(this);
            item.setText(++number+".", (entry.getType().equals("adult")? ItineraryHelper.decodeAdultTitle(entry.getTitle(), getApplicationContext()) : (entry.getType().equals("child") ? ItineraryHelper.decodeChildTitle(entry.getTitle(), getApplicationContext()) : ItineraryHelper.decodeInfantTitle(entry.getTitle(), getApplicationContext()) )) + " " + entry.getName(), entry.getIDCardNumber(), entry.getDepartSeatFormatted(), entry.getReturnSeatFormatted());
            passengerLayout.addView(item);
        }

        String passenger = reservation.getAdultCount()+" "+getResources().getString(R.string.adult).toLowerCase() + (reservation.getChildCount() > 0 ? ", "+reservation.getChildCount()+" "+getResources().getString(R.string.child).toLowerCase():"") + (reservation.getInfantCount() > 0 ? ", "+reservation.getInfantCount()+" "+getResources().getString(R.string.infant).toLowerCase():"");
        ((TextView) findViewById(R.id.textViewPassengerCount)).setText(passenger);

        StationModel stationModel = new StationModel(this);

        ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(reservation.getDepartDate(), this));
        ((TextView) findViewById(R.id.departHintTextView)).setText(stationModel.getStation(reservation.getFromPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ stationModel.getStation(reservation.getToPort()).getCity());


        for (int i = 0; i < reservation.getDepartTrainArray().length || i <= 5; i++) {
            if (i == 0) {
                Train fl = reservation.getDepartTrainArray()[i];
                ((TextView) findViewById(R.id.depart_textViewTrainName)).setText(fl.getTrainNameNumber());
                ((TextView) findViewById(R.id.depart_textViewTrainClass)).setText(getString(getResources().getIdentifier(("class_" + reservation.getClass1()).toLowerCase(), "string", getPackageName())));
                ((TextView) findViewById(R.id.depart_departTime)).setText(fl.getDepartTime());
                ((TextView) findViewById(R.id.depart_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(R.id.depart_arriveTime)).setText(fl.getArriveTime());
                ((TextView) findViewById(R.id.depart_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Station station = stationModel.getStation(fl.getDepartPort());
                ((TextView) findViewById(R.id.depart_departStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_departCity)).setText(station.getCity());
                station = stationModel.getStation(fl.getArrivePort());
                ((TextView) findViewById(R.id.depart_arriveStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_arriveCity)).setText(station.getCity());

            } else if (i > 0 && i <= 5 && i < reservation.getDepartTrainArray().length) {
                Train fl = reservation.getDepartTrainArray()[i];((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTrainName", "id", getPackageName()))).setText(fl.getTrainNameNumber());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTrainClass", "id", getPackageName()))).setText(getString(getResources().getIdentifier(("class_" + reservation.getClass1()).toLowerCase(), "string", getPackageName())));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Station station = stationModel.getStation(fl.getDepartPort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departCity", "id", getPackageName()))).setText(station.getCity());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getDepartTrainArray()[i-1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getDepartTrainArray()[i-1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + station.getCity());

                station = stationModel.getStation(fl.getArrivePort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveCity", "id", getPackageName()))).setText(station.getCity());


                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.VISIBLE);
            } else if (i > 0 && i <= 5 && i >= reservation.getDepartTrainArray().length) {
                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.GONE);
            }
        }

        ((TextView) findViewById(R.id.departSummaryTextView)).setText(reservation.getFromPort() + " - " + reservation.getToPort() + (reservation.isReturn() && reservation.getCarrierID1().equals(reservation.getCarrierID2())?" - "+reservation.getFromPort():"") + " x " + (reservation.getAdultCount() + reservation.getChildCount() + reservation.getInfantCount()) + " " + getString(R.string.person).toLowerCase());
        ((TextView) findViewById(R.id.departFareTextView)).setText(Utils.numberFormatCurrency(reservation.getTotalDepartPrice() - reservation.getTotalDepartDiscount()));

        int total = reservation.getTotalDepartPrice();
        int discount = reservation.getTotalDepartDiscount();

        //------------------------------------------------------------------------
        if(!reservation.isReturn()){
            findViewById(R.id.returnDetailInfoLayout).setVisibility(View.GONE);
            findViewById(R.id.returnFareLayout).setVisibility(View.GONE);
        }
        else {
            ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns) + " - " + Utils.mediumDate(reservation.getReturnDate(), this));
            ((TextView) findViewById(R.id.returnHintTextView)).setText(stationModel.getStation(reservation.getToPort()).getCity() + " " + getResources().getString(R.string.to).toLowerCase() + " " + stationModel.getStation(reservation.getFromPort()).getCity());

            for (int i = 0; i < reservation.getReturnTrainArray().length || i <= 5; i++) {
                if (i == 0) {
                    Train fl = reservation.getReturnTrainArray()[i];
                    ((TextView) findViewById(R.id.return_textViewTrainName)).setText(fl.getTrainNameNumber());
                    ((TextView) findViewById(R.id.return_textViewTrainClass)).setText(getString(getResources().getIdentifier(("class_" + reservation.getClass2()).toLowerCase(), "string", getPackageName())));
                    ((TextView) findViewById(R.id.return_departTime)).setText(fl.getDepartTime());
                    ((TextView) findViewById(R.id.return_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(R.id.return_arriveTime)).setText(fl.getArriveTime());
                    ((TextView) findViewById(R.id.return_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Station station = stationModel.getStation(fl.getDepartPort());
                    ((TextView) findViewById(R.id.return_departStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(R.id.return_departCity)).setText(station.getCity());
                    station = stationModel.getStation(fl.getArrivePort());
                    ((TextView) findViewById(R.id.return_arriveStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(R.id.return_arriveCity)).setText(station.getCity());

                } else if (i > 0 && i <= 5 && i < reservation.getReturnTrainArray().length) {
                    Train fl = reservation.getReturnTrainArray()[i];
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTrainName", "id", getPackageName()))).setText(fl.getTrainNameNumber());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTrainClass", "id", getPackageName()))).setText(getString(getResources().getIdentifier(("class_" + reservation.getClass2()).toLowerCase(), "string", getPackageName())));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Station station = stationModel.getStation(fl.getDepartPort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departCity", "id", getPackageName()))).setText(station.getCity());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(reservation.getReturnTrainArray()[i - 1].getArriveDatetime(), fl.getDepartDatetime(), reservation.getReturnTrainArray()[i - 1].getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) + " " + station.getCity());

                    station = stationModel.getStation(fl.getArrivePort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveCity", "id", getPackageName()))).setText(station.getCity());


                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.VISIBLE);
                } else if (i > 0 && i <= 5 && i >= reservation.getReturnTrainArray().length) {
                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.GONE);
                }
            }


            if (!reservation.getCarrierID1().equals(reservation.getCarrierID2())) {
                ((TextView) findViewById(R.id.returnSummaryTextView)).setText(reservation.getToPort() + " - " + reservation.getFromPort() + " x " + (reservation.getAdultCount() + reservation.getChildCount() + reservation.getInfantCount()) + " " + getString(R.string.person).toLowerCase());
                ((TextView) findViewById(R.id.returnFareTextView)).setText(Utils.numberFormatCurrency(reservation.getTotalReturnPrice() - reservation.getTotalReturnDiscount()));

                total += reservation.getTotalReturnPrice();
                discount += reservation.getTotalReturnDiscount();
            }
            else{
                findViewById(R.id.returnFareLayout).setVisibility(View.GONE);
            }
        }

        ((TextView) findViewById(R.id.fareHintTextView)).setText(!reservation.isReturn()?R.string.oneway:R.string.roundtrip);

        if(discount < 0){
            findViewById(R.id.discountFareLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.discountFareTextView)).setText(Utils.numberFormatCurrency(discount));
        }


        if(reservation.getPromoAmount() != 0){
            findViewById(R.id.promoFareLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.promoSummaryTextView)).setText(getString(R.string.promo)+" : "+reservation.getPromoCode());
            ((TextView) findViewById(R.id.promoFareTextView)).setText(Utils.numberFormatCurrency(reservation.getPromoAmount()));
//            if(!reservation.getIsConfirmed().equals("Y")) {
                total += reservation.getPromoAmount();
//            }
        }
        int totalFare = total;
        if(reservation.getAmount() > 0){
            total = reservation.getAmount();
        }

        if(totalFare-total != 0){
            findViewById(R.id.paymentChannelFareLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.paymentChannelSummaryTextView)).setText(getString(total - totalFare < 0 ? R.string.bank_discount : R.string.transaction_fee));
            ((TextView) findViewById(R.id.paymentChannelFareTextView)).setText(Utils.numberFormatCurrency(total - totalFare));
        }

//        if(){

        ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / ( Integer.toString(total).length() >= 3 ? 1000 : 1 ) )));
        ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - ( Integer.toString(total).length() >= 3 ? 3 : 0 )));


        if(reservation.getBank()==null){
            ((TextView) findViewById(R.id.buttonBookingText)).setText(R.string.finish_payment);
        }
        else{
            ((TextView) findViewById(R.id.buttonBookingText)).setText(R.string.back);
        }
        findViewById(R.id.buttonBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reservation.getBank()==null){
                    finish();
                }
                else{
                    startActivity(new Intent(TrainBookingResultActivity.this, TicketBookingActivity.class));
                }
            }
        });

    }
}
