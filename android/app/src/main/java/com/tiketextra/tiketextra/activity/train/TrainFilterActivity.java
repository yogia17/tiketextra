package com.tiketextra.tiketextra.activity.train;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.train.TrainResultActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.model.AirlineModel;
import com.tiketextra.tiketextra.object.Airline;
import com.tiketextra.tiketextra.util.Utils;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.util.ArrayList;
import java.util.Collections;

public class TrainFilterActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_filter);
        Toolbar toolbar = findViewById(R.id.toolbarFilter);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(getString(R.string.filter)+" "+getString(getIntent().getStringExtra("mode").equals(Configuration.MODE_DEPART)?R.string.depart_train:R.string.return_train));

        final LinearLayout layoutAirline = findViewById(R.id.listCheckboxAirlines);
        final LinearLayout layoutClass = findViewById(R.id.listCheckboxClass);
        final LinearLayout layoutTransitNum = findViewById(R.id.listCheckboxTransitNum);
        final TextView tvPriceMin = findViewById(R.id.minPriceTextView);
        final TextView tvPriceMax = findViewById(R.id.maxPriceTextView);


        CheckBox checkBox;
        RangeSeekBar rangeSeekBar = findViewById(R.id.rangeSeekbarPrice);
//        AirlineModel airlineModel = new AirlineModel(this);
        switch (getIntent().getStringExtra("mode")) {
            case Configuration.MODE_DEPART: {
//                final ArrayList<Airline> airlines = airlineModel.getAirlines(AppHelper.departFilterAirline);
//                for (int i = 0; i < airlines.size(); i++) {
//                    checkBox = new CheckBox(this);
//                    checkBox.setId(i);
//                    checkBox.setText(airlines.get(i).getName());
//
//                    final Airline airline = airlines.get(i);
//
//                    if (AppHelper.departSelectedAirline.contains(airline.getCarrierID())) {
//                        checkBox.setChecked(true);
//                    }
//                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                        @Override
//                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                            if (b) {
//                                AppHelper.departSelectedAirline.add(airline.getCarrierID());
//                            } else {
//                                AppHelper.departSelectedAirline.remove(airline.getCarrierID());
//                            }
//                        }
//                    });
//                    layoutAirline.addView(checkBox);
//                }

                String[] cabinClass = {"eco", "bus", "exe"};

                for (int i = 0; i < cabinClass.length; i++) {
                    final String cabin = cabinClass[i];
                    if (AppHelper.departFilterClass.contains(cabin)) {
                        checkBox = new CheckBox(this);
                        checkBox.setText(getResources().getIdentifier("class_" + cabin, "string", getPackageName()));
                        if (AppHelper.departSelectedClass.contains(cabin)) {
                            checkBox.setChecked(true);
                        }
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    AppHelper.departSelectedClass.add(cabin);
                                } else {
                                    AppHelper.departSelectedClass.remove(cabin);
                                }
                            }
                        });
                        layoutClass.addView(checkBox);
                    }
                }

//                Collections.sort(AppHelper.departFilterTransitNum);
//                for (int i = 0; i < AppHelper.departFilterTransitNum.size(); i++) {
//                    checkBox = new CheckBox(this);
//                    final String transit = AppHelper.departFilterTransitNum.get(i);
//                    if (Integer.parseInt(transit) == 0) {
//                        checkBox.setText(R.string.direct);
//                    } else {
//                        checkBox.setText(transit + " " + (getResources().getString(R.string.transit)));
//                    }
//                    if (AppHelper.departSelectedTransitNum.contains(transit)) {
//                        checkBox.setChecked(true);
//                    }
//                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                        @Override
//                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                            if (b) {
//                                AppHelper.departSelectedTransitNum.add(transit);
//                            } else {
//                                AppHelper.departSelectedTransitNum.remove(transit);
//                            }
//                        }
//                    });
//                    layoutTransitNum.addView(checkBox);
//                }

                tvPriceMin.setText(Utils.numberFormat(AppHelper.departSelectedPriceMin));
                tvPriceMax.setText(Utils.numberFormat(AppHelper.departSelectedPriceMax));

                rangeSeekBar.setRangeValues(AppHelper.departPriceMin, AppHelper.departPriceMax);
                rangeSeekBar.setSelectedMinValue(AppHelper.departSelectedPriceMin);
                rangeSeekBar.setSelectedMaxValue(AppHelper.departSelectedPriceMax);

                rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                    @Override
                    public void onRangeSeekBarValuesChanged(RangeSeekBar<Integer> bar, Integer minValue, Integer maxValue) {
                        AppHelper.departSelectedPriceMin = minValue;
                        AppHelper.departSelectedPriceMax = maxValue;

                        tvPriceMin.setText(Utils.numberFormat(AppHelper.departSelectedPriceMin));
                        tvPriceMax.setText(Utils.numberFormat(AppHelper.departSelectedPriceMax));
                    }
                });
                break;

            }
            case Configuration.MODE_RETURN: {

//                final ArrayList<Airline> airlines = airlineModel.getAirlines(AppHelper.returnFilterAirline);
//                for (int i = 0; i < airlines.size(); i++) {
//                    checkBox = new CheckBox(this);
//                    checkBox.setId(i);
//                    checkBox.setText(airlines.get(i).getName());
//
//                    final Airline airline = airlines.get(i);
//
//                    if (AppHelper.returnSelectedAirline.contains(airline.getCarrierID())) {
//                        checkBox.setChecked(true);
//                    }
//                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                        @Override
//                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                            if (b) {
//                                AppHelper.returnSelectedAirline.add(airline.getCarrierID());
//                            } else {
//                                AppHelper.returnSelectedAirline.remove(airline.getCarrierID());
//                            }
//                        }
//                    });
//                    layoutAirline.addView(checkBox);
//                }

                String[] cabinClass = {"eco", "bus", "exe"};

                for (int i = 0; i < cabinClass.length; i++) {
                    final String cabin = cabinClass[i];
                    if (AppHelper.returnFilterClass.contains(cabin)) {
                        checkBox = new CheckBox(this);
                        checkBox.setText(getResources().getIdentifier("class_" + cabin, "string", getPackageName()));
                        if (AppHelper.returnSelectedClass.contains(cabin)) {
                            checkBox.setChecked(true);
                        }
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    AppHelper.returnSelectedClass.add(cabin);
                                } else {
                                    AppHelper.returnSelectedClass.remove(cabin);
                                }
                            }
                        });
                        layoutClass.addView(checkBox);
                    }
                }

//                Collections.sort(AppHelper.returnFilterTransitNum);
//                for (int i = 0; i < AppHelper.returnFilterTransitNum.size(); i++) {
//                    checkBox = new CheckBox(this);
//                    final String transit = AppHelper.returnFilterTransitNum.get(i);
//                    if (Integer.parseInt(transit) == 0) {
//                        checkBox.setText(R.string.direct);
//                    } else {
//                        checkBox.setText(transit + " " + (getResources().getString(R.string.transit)));
//                    }
//                    if (AppHelper.returnSelectedTransitNum.contains(transit)) {
//                        checkBox.setChecked(true);
//                    }
//                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                        @Override
//                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                            if (b) {
//                                AppHelper.returnSelectedTransitNum.add(transit);
//                            } else {
//                                AppHelper.returnSelectedTransitNum.remove(transit);
//                            }
//                        }
//                    });
//                    layoutTransitNum.addView(checkBox);
//                }

                tvPriceMin.setText(Utils.numberFormat(AppHelper.returnSelectedPriceMin));
                tvPriceMax.setText(Utils.numberFormat(AppHelper.returnSelectedPriceMax));

                rangeSeekBar.setRangeValues(AppHelper.returnPriceMin, AppHelper.returnPriceMax);
                rangeSeekBar.setSelectedMinValue(AppHelper.returnSelectedPriceMin);
                rangeSeekBar.setSelectedMaxValue(AppHelper.returnSelectedPriceMax);

                rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                    @Override
                    public void onRangeSeekBarValuesChanged(RangeSeekBar<Integer> bar, Integer minValue, Integer maxValue) {
                        AppHelper.returnSelectedPriceMin = minValue;
                        AppHelper.returnSelectedPriceMax = maxValue;

                        tvPriceMin.setText(Utils.numberFormat(AppHelper.returnSelectedPriceMin));
                        tvPriceMax.setText(Utils.numberFormat(AppHelper.returnSelectedPriceMax));
                    }
                });

                break;


            }

        }

//        RelativeLayout layoutChooseAirline = findViewById(R.id.airlineFilterChoiceLayout);
//        final ImageView ivAirline = findViewById(R.id.arrowRightAirlineImageView);

        RelativeLayout layoutChooseClass = findViewById(R.id.classFilterChoiceLayout);
        final ImageView ivClass = findViewById(R.id.arrowRightClassImageView);

//        RelativeLayout layoutChooseTransit = findViewById(R.id.transitFilterChoiceLayout);
//        final ImageView ivTransit = findViewById(R.id.arrowRightTransitImageView);
//
//
//        layoutChooseAirline.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                layoutAirline.setVisibility(View.VISIBLE);
//                layoutClass.setVisibility(View.GONE);
//                layoutTransitNum.setVisibility(View.GONE);
//
//                ivAirline.setVisibility(View.GONE);
//                ivClass.setVisibility(View.VISIBLE);
//                ivTransit.setVisibility(View.VISIBLE);
//            }
//        });

        layoutChooseClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                layoutAirline.setVisibility(View.GONE);
//                layoutTransitNum.setVisibility(View.GONE);
                layoutClass.setVisibility(View.VISIBLE);

//                ivAirline.setVisibility(View.VISIBLE);
//                ivTransit.setVisibility(View.VISIBLE);
                ivClass.setVisibility(View.GONE);
            }
        });

//        layoutChooseTransit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                layoutAirline.setVisibility(View.GONE);
//                layoutClass.setVisibility(View.GONE);
//                layoutTransitNum.setVisibility(View.VISIBLE);
//
//                ivAirline.setVisibility(View.VISIBLE);
//                ivClass.setVisibility(View.VISIBLE);
//                ivTransit.setVisibility(View.GONE);
//            }
//        });

        findViewById(R.id.buttonApplyFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TrainResultActivity.class);
                TrainFilterActivity.this.setResult(Activity.RESULT_OK, intent);
                TrainFilterActivity.this.finish();
            }
        });
    
    }
}
