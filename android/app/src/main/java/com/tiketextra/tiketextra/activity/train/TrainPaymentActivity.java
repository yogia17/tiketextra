package com.tiketextra.tiketextra.activity.train;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputFilter;
import android.view.Display;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.payment.BankCVSResultGuideActivity;
import com.tiketextra.tiketextra.activity.payment.CreditCardValidationActivity;
import com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.BankRecyclerViewAdapter;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.custom_view.AutoFitGridLayoutManager;
import com.tiketextra.tiketextra.custom_view.ItemPassengerLayoutTrainDetail;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CreditCardNumberFormattingTextWatcher;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.listener.BankItemListener;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Train;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class TrainPaymentActivity extends BaseActivity implements BankItemListener {

    private Reservation reservation;
    private TextView textViewTimeLimit;
    private int promoAmount = 0;
    private String imei;
    private Bank bank;
    private ArrayList<Bank> banks, transferBanks, virtualAccountBanks, cvsBanks, ccBanks;
    private String promoName="";
    private EditText editTextPromoCode;
    private BankRecyclerViewAdapter adapterVirtualAccount, adapterBankTransfer, adapterConvenienceStore, adapterCreditCard;
    private boolean bankChoose = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPayment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.payment);

        ReservationModel reservationModel = new ReservationModel(this);
        StationModel stationModel = new StationModel(this);
        banks = new Gson().fromJson(getIntent().getStringExtra("bank"), new TypeToken<ArrayList<Bank>>() {}.getType());

        transferBanks = new ArrayList<>();
        virtualAccountBanks = new ArrayList<>();
        cvsBanks = new ArrayList<>();
        ccBanks = new ArrayList<>();

        initBanks();

        reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));

        String passenger = reservation.getAdultCount()+" "+getResources().getString(R.string.adult).toLowerCase() + (reservation.getChildCount() > 0 ? ", "+reservation.getChildCount()+" "+getResources().getString(R.string.child).toLowerCase():"") + (reservation.getInfantCount() > 0 ? ", "+reservation.getInfantCount()+" "+getResources().getString(R.string.infant).toLowerCase():"");
        ((TextView) findViewById(R.id.textViewPassengerCount)).setText(passenger);


        LinearLayout passengerLayout = (LinearLayout) findViewById(R.id.passengerLayout);
        int number = 0;
        for (Passenger entry : reservation.getPassengers()) {
            ItemPassengerLayoutTrainDetail item = new ItemPassengerLayoutTrainDetail(this);
            item.setText(++number+".", (entry.getType().equals("adult")? ItineraryHelper.decodeAdultTitle(entry.getTitle(), getApplicationContext()) : (entry.getType().equals("child") ? ItineraryHelper.decodeChildTitle(entry.getTitle(), getApplicationContext()) : ItineraryHelper.decodeInfantTitle(entry.getTitle(), getApplicationContext()) )) + " " + entry.getName(), entry.getIDCardNumber(), entry.getDepartSeatFormatted(), entry.getReturnSeatFormatted());
            passengerLayout.addView(item);
        }

        findViewById(R.id.buttonChangeSeat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.scrollViewPayment).setVisibility(View.GONE);
                findViewById(R.id.progressBarChangeSeat).setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<>();
                params.put("reservation_id", Integer.toString(reservation.getReservationID()));
                params.put("direction", "depart");
                System.out.println("cembeliq :"+params);
                JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "train/seat_map", params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if(response.getInt("status")==1){
                                Intent seatMap = new Intent(TrainPaymentActivity.this, TrainSelectSeatActivity.class);
                                seatMap.putExtra("map", response.toString());
                                seatMap.putExtra("reservation_id", getIntent().getIntExtra("reservation_id", 0));
                                seatMap.putExtra("direction", "depart");
                                startActivity(seatMap);
                            }
                            else{
                                dialogErrorSeatMap();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialogErrorConnection();
                        }



                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError arg0) {
                        VolleyLog.d("Error: ", arg0.getMessage());
                        dialogErrorConnection();
                        findViewById(R.id.scrollViewPayment).setVisibility(View.VISIBLE);
                        findViewById(R.id.progressBarChangeSeat).setVisibility(View.GONE);
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonReq);

            }
        });

        ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(reservation.getDepartDate(), this));
        ((TextView) findViewById(R.id.departHintTextView)).setText(stationModel.getStation(reservation.getFromPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ stationModel.getStation(reservation.getToPort()).getCity());

        String routeStr, trainNumStr;
        trainNumStr = routeStr = "";
        Calendar basic = Calendar.getInstance();
        Calendar compareDepart = Calendar.getInstance();
        Calendar compareArrive = Calendar.getInstance();
        for (Train fl : reservation.getDepartTrainArray()) {
            String supDepart = "";
            String supArrive = "";
            compareDepart.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
            compareArrive.set(fl.getArriveYear(), fl.getArriveMonth(), fl.getArriveDay());
            long ex;
            if((ex = (compareDepart.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0){
                supDepart = "<sup><em><small>+" + ex + "</small></em></sup>";
            }
            if((ex = (compareArrive.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0){
                supArrive = "<sup><em><small>+" + ex + "</small></em></sup>";
            }
            trainNumStr += fl.getTrainNameNumber() + ", ";
            routeStr += fl.getDepartTime() + supDepart +" " + fl.getDepartPort()
                    + " - " + fl.getArriveTime() + supArrive + " "
                    + fl.getArrivePort() + "<br/>";

        }

        trainNumStr = trainNumStr.substring(0, trainNumStr.length() - 2);
        ((TextView) findViewById(R.id.textViewDepartTrainNameNumber)).setText(trainNumStr);
        routeStr = routeStr.length() > 0 ? routeStr.substring(0,
                routeStr.length() - 5) : routeStr;
        ((TextView) findViewById(R.id.textViewDepartRoute)).setText(Html.fromHtml(routeStr));
        ((TextView) findViewById(R.id.textViewDepartItinerary)).setText(Utils.strTimeDiff(reservation.getDepartTrainArray()[0].getDepartDatetime(), reservation
                .getDepartTrainArray()[reservation.getDepartTrainArray().length - 1]
                .getArriveDatetime(), reservation.getDepartTrainArray()[0].getDepartTimezone() - reservation.getDepartTrainArray()[reservation.getDepartTrainArray().length - 1].getArriveTimezone()));


        ((TextView) findViewById(R.id.textViewDepartBookingCode)).setText(reservation.getBookingCode1());

        if (reservation.getClass1().equals("pro")) {
            ((TextView) findViewById(R.id.textViewDepartClass)).setText(R.string.class_pro);
            ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.green_500));
        } else if (reservation.getClass1().equals("eco")) {
            ((TextView) findViewById(R.id.textViewDepartClass)).setText(R.string.class_eco);
            ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.blue_500));
        } else if (reservation.getClass1().equals("bus")) {
            ((TextView) findViewById(R.id.textViewDepartClass)).setText(R.string.class_bus);
            ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.orange_500));
        } else if (reservation.getClass1().equals("exe")) {
            ((TextView) findViewById(R.id.textViewDepartClass)).setText(getResources().getString(R.string.class_exe));
            ((TextView) findViewById(R.id.textViewDepartClass)).setTextColor(getResources().getColor(R.color.red_500));
        }

        if(reservation.isReturn()){
            findViewById(R.id.returnTrain).setVisibility(View.VISIBLE);

            ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns)+" - "+ Utils.mediumDate(reservation.getReturnDate(), this));
            ((TextView) findViewById(R.id.returnHintTextView)).setText(stationModel.getStation(reservation.getToPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ stationModel.getStation(reservation.getFromPort()).getCity());


            trainNumStr = routeStr = "";
            for (Train fl : reservation.getReturnTrainArray()) {
                String supDepart = "";
                String supArrive = "";
                compareDepart.set(fl.getDepartYear(), fl.getDepartMonth(), fl.getDepartDay());
                compareArrive.set(fl.getArriveYear(), fl.getArriveMonth(), fl.getArriveDay());
                long ex;
                if((ex = (compareDepart.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0){
                    supDepart = "<sup><em><small>+" + ex + "</small></em></sup>";
                }
                if((ex = (compareArrive.getTimeInMillis() - basic.getTimeInMillis()) / (1000 * 60 * 60 * 24)) > 0){
                    supArrive = "<sup><em><small>+" + ex + "</small></em></sup>";
                }
                trainNumStr += fl.getTrainNameNumber() + ", ";
                routeStr += fl.getDepartTime() + supDepart +" " + fl.getDepartPort()
                        + " - " + fl.getArriveTime() + supArrive + " "
                        + fl.getArrivePort() + "<br/>";

            }

            trainNumStr = trainNumStr.substring(0, trainNumStr.length() - 2);
            ((TextView) findViewById(R.id.textViewReturnTrainNameNumber)).setText(trainNumStr);
            routeStr = routeStr.length() > 0 ? routeStr.substring(0,
                    routeStr.length() - 5) : routeStr;
            ((TextView) findViewById(R.id.textViewReturnRoute)).setText(Html.fromHtml(routeStr));
            ((TextView) findViewById(R.id.textViewReturnItinerary)).setText(Utils.strTimeDiff(reservation.getReturnTrainArray()[0].getDepartDatetime(), reservation
                    .getReturnTrainArray()[reservation.getReturnTrainArray().length - 1]
                    .getArriveDatetime(), reservation.getReturnTrainArray()[0].getDepartTimezone() - reservation.getReturnTrainArray()[reservation.getReturnTrainArray().length - 1].getArriveTimezone()));


            ((TextView) findViewById(R.id.textViewReturnBookingCode)).setText(reservation.getCarrierID1().equals(reservation.getCarrierID2()) && (reservation.getBookingCode2()==null || reservation.getBookingCode2().equals("")) ? reservation.getBookingCode1() : reservation.getBookingCode2());

            if (reservation.getClass2().equals("pro")) {
                ((TextView) findViewById(R.id.textViewReturnClass)).setText(R.string.class_pro);
                ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.green_500));
            } else if (reservation.getClass2().equals("eco")) {
                ((TextView) findViewById(R.id.textViewReturnClass)).setText(R.string.class_eco);
                ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.blue_500));
            } else if (reservation.getClass2().equals("bus")) {
                ((TextView) findViewById(R.id.textViewReturnClass)).setText(R.string.class_bus);
                ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.orange_500));
            } else if (reservation.getClass2().equals("exe")) {
                ((TextView) findViewById(R.id.textViewReturnClass)).setText(getResources().getString(R.string.class_exe));
                ((TextView) findViewById(R.id.textViewReturnClass)).setTextColor(getResources().getColor(R.color.red_500));
            }
        }
        else{
            findViewById(R.id.returnTrain).setVisibility(View.GONE);
        }

        ((TextView) findViewById(R.id.departPortCodeTextView)).setText(reservation.getFromPort());
        ((TextView) findViewById(R.id.arrivePortCodeTextView)).setText(reservation.getToPort());
        if(reservation.isReturn()){
            ((ImageView)findViewById(R.id.directionArrowImageView)).setImageResource(R.drawable.ic_swap);
            ((TextView) findViewById(R.id.itineraryTripTextView)).setText(R.string.roundtrip);
        }
        ((TextView) findViewById(R.id.passengerCountTextView)).setText(reservation.getAdultCount()+" "+getResources().getString(R.string.adult).toLowerCase() + (reservation.getChildCount() > 0 ? ", "+reservation.getChildCount()+" "+getResources().getString(R.string.child).toLowerCase():"") + (reservation.getInfantCount() > 0 ? ", "+reservation.getInfantCount()+" "+getResources().getString(R.string.infant).toLowerCase():""));

        ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(reservation.getTotalFare() / ( Integer.toString(reservation.getTotalFare()).length() >= 3 ? 1000 : 1 ))));
        ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(reservation.getTotalFare()).substring(Integer.toString(reservation.getTotalFare()).length() - ( Integer.toString(reservation.getTotalFare()).length() >= 3 ? 3 : 0 )));

        findViewById(R.id.layoutTrainInfoFare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrainPaymentActivity.this, TrainBookingResultActivity.class);
                intent.putExtra("reservation_id", reservation.getReservationID());
                startActivity(intent);
            }
        });

        SettingModel settingModel = new SettingModel(TrainPaymentActivity.this);
        imei = settingModel.getValue("device_id");

        RecyclerView recyclerViewBankTransfer = (RecyclerView) findViewById(R.id.recyclerViewBankTransfer);
        adapterBankTransfer = new BankRecyclerViewAdapter(transferBanks, this, this);
        recyclerViewBankTransfer.setAdapter(adapterBankTransfer);

        RecyclerView recyclerViewVirtualAccount = (RecyclerView) findViewById(R.id.recyclerViewVirtualAccount);
        adapterVirtualAccount = new BankRecyclerViewAdapter(virtualAccountBanks, this, this);
        recyclerViewVirtualAccount.setAdapter(adapterVirtualAccount);

        RecyclerView recyclerViewConvenienceStore = (RecyclerView) findViewById(R.id.recyclerViewConvenienceStore);
        adapterConvenienceStore = new BankRecyclerViewAdapter(cvsBanks, this, this);
        recyclerViewConvenienceStore.setAdapter(adapterConvenienceStore);

        RecyclerView recyclerViewCreditCard = (RecyclerView) findViewById(R.id.recyclerViewCreditCard);
        adapterCreditCard = new BankRecyclerViewAdapter(ccBanks, this, this);
        recyclerViewCreditCard.setAdapter(adapterCreditCard);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int countPerRow = width <= 700 ? 2 : 3;
        recyclerViewBankTransfer.setLayoutManager(new GridLayoutManager(this, countPerRow));
        recyclerViewVirtualAccount.setLayoutManager(new GridLayoutManager(this, countPerRow));
        recyclerViewConvenienceStore.setLayoutManager(new GridLayoutManager(this, countPerRow));
        recyclerViewCreditCard.setLayoutManager(new GridLayoutManager(this, countPerRow));

//        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
//        AutoFitGridLayoutManager manager = new AutoFitGridLayoutManager(this, 250);
//        recyclerViewBankTransfer.setLayoutManager(new AutoFitGridLayoutManager(this, 250));
//        recyclerViewVirtualAccount.setLayoutManager(new AutoFitGridLayoutManager(this, 250));
//        recyclerViewConvenienceStore.setLayoutManager(new AutoFitGridLayoutManager(this, 250));
//        recyclerViewCreditCard.setLayoutManager(new AutoFitGridLayoutManager(this, 250));

        editTextPromoCode = findViewById(R.id.editTextPromo);
        editTextPromoCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        String promoCode = settingModel.getValue("promo_code");
        if(promoCode != null && reservation.getPromoAmount() == 0){
            editTextPromoCode.setText(promoCode);
            fetchPromo();
            new SettingModel(getApplicationContext()).setValue("promo_code", null);
        }

        if(reservation.getPromoAmount() !=0){
            ((TextView) findViewById(R.id.textViewPromoCode)).setText(reservation.getPromoCode());
            ((TextView) findViewById(R.id.textViewPromoDiscount)).setText(Utils.numberFormat(reservation.getPromoAmount()));

            promoName = reservation.getPromoCode();
            promoAmount = reservation.getPromoAmount();


            findViewById(R.id.inputPromoCode).setVisibility(View.GONE);
            findViewById(R.id.layoutPromoCode).setVisibility(View.VISIBLE);
            findViewById(R.id.layoutAmountPromo).setVisibility(View.VISIBLE);
        }

        findViewById(R.id.promoButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fetchPromo();

            }
        });

        textViewTimeLimit = findViewById(R.id.textViewTimeLimit);
        textViewTimeLimit.setText(Utils.longDateTime(reservation.getTimeLimit(), this) + (getCurLang().equals("en")?" GMT+7":" WIB"));

        findViewById(R.id.buttonBankTransfer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBank();
                findViewById(R.id.layoutBankTransfer).setVisibility(View.VISIBLE);
                ((NestedScrollView) findViewById(R.id.scrollViewPayment)).fullScroll(View.FOCUS_DOWN);
            }
        });

        findViewById(R.id.buttonVirtualAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBank();
                findViewById(R.id.layoutVirtualAccount).setVisibility(View.VISIBLE);
                ((NestedScrollView) findViewById(R.id.scrollViewPayment)).fullScroll(View.FOCUS_DOWN);

            }
        });

        findViewById(R.id.buttonConvenienceStore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBank();
                findViewById(R.id.layoutConvenienceStore).setVisibility(View.VISIBLE);
                ((NestedScrollView) findViewById(R.id.scrollViewPayment)).fullScroll(View.FOCUS_DOWN);

            }
        });

        findViewById(R.id.buttonCreditCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBank();
                findViewById(R.id.layoutCreditCard).setVisibility(View.VISIBLE);
                findViewById(R.id.layoutCreditCardForm).setVisibility(View.VISIBLE);
                ((NestedScrollView) findViewById(R.id.scrollViewPayment)).fullScroll(View.FOCUS_DOWN);

            }
        });

        hideBank();

        ArrayAdapter<String> spinnerMonthArrayAdapter = new ArrayAdapter<>(this, R.layout.my_spinner_item, ItineraryHelper.getMonthNumber(this));
        spinnerMonthArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerExpiryYearArrayAdapter= new ArrayAdapter<>(this, R.layout.my_spinner_item, ItineraryHelper.getPassportExpiryYear());
        spinnerExpiryYearArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        ((Spinner) findViewById(R.id.spinnerValidThruMonth)).setAdapter(spinnerMonthArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerValidThruYear)).setAdapter(spinnerExpiryYearArrayAdapter);

        CreditCardNumberFormattingTextWatcher tw = new CreditCardNumberFormattingTextWatcher();
        ((TextView) findViewById(R.id.creditCardNumberEditText)).addTextChangedListener(tw);
    }

    private void initBanks() {
        transferBanks.clear();
        virtualAccountBanks.clear();
        cvsBanks.clear();
        ccBanks.clear();
        for (int i = 0; i < banks.size(); i++) {
            if (banks.get(i).getGateway().equals("1")) {
                transferBanks.add(banks.get(i));
            } else if (banks.get(i).getGateway().equals("4")) {
                virtualAccountBanks.add(banks.get(i));
            } else if (banks.get(i).getGateway().equals("5")) {
                cvsBanks.add(banks.get(i));
            } else if (banks.get(i).getGateway().equals("6")) {
                ccBanks.add(banks.get(i));
            }
        }

        if (transferBanks.size() > 0) {
            findViewById(R.id.buttonBankTransfer).setVisibility(View.VISIBLE);
            findViewById(R.id.layoutBankTransfer).setVisibility(View.VISIBLE);
            findViewById(R.id.dividerBankTransfer).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.buttonBankTransfer).setVisibility(View.GONE);
            findViewById(R.id.layoutBankTransfer).setVisibility(View.GONE);
            findViewById(R.id.dividerBankTransfer).setVisibility(View.GONE);
        }

        if (virtualAccountBanks.size() > 0) {
            findViewById(R.id.buttonVirtualAccount).setVisibility(View.VISIBLE);
            findViewById(R.id.layoutVirtualAccount).setVisibility(View.VISIBLE);
            findViewById(R.id.dividerVirtualAccount).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.buttonVirtualAccount).setVisibility(View.GONE);
            findViewById(R.id.layoutBankTransfer).setVisibility(View.GONE);
            findViewById(R.id.dividerVirtualAccount).setVisibility(View.GONE);
        }

        if (cvsBanks.size() > 0) {
            findViewById(R.id.buttonConvenienceStore).setVisibility(View.VISIBLE);
            findViewById(R.id.layoutConvenienceStore).setVisibility(View.VISIBLE);
            findViewById(R.id.dividerConvenienceStore).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.buttonConvenienceStore).setVisibility(View.GONE);
            findViewById(R.id.layoutConvenienceStore).setVisibility(View.GONE);
            findViewById(R.id.dividerConvenienceStore).setVisibility(View.GONE);
        }

        if (ccBanks.size() > 0) {
            findViewById(R.id.buttonCreditCard).setVisibility(View.VISIBLE);
            findViewById(R.id.layoutCreditCard).setVisibility(View.VISIBLE);
            findViewById(R.id.dividerCreditCard).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.buttonCreditCard).setVisibility(View.GONE);
            findViewById(R.id.layoutCreditCard).setVisibility(View.GONE);
            findViewById(R.id.dividerCreditCard).setVisibility(View.GONE);
        }
    }

    private void hideBank() {
        findViewById(R.id.layoutBankTransfer).setVisibility(View.GONE);
        findViewById(R.id.layoutVirtualAccount).setVisibility(View.GONE);
        findViewById(R.id.layoutConvenienceStore).setVisibility(View.GONE);
        findViewById(R.id.layoutCreditCardForm).setVisibility(View.GONE);
        findViewById(R.id.layoutCreditCard).setVisibility(View.GONE);
    }

    private void fetchPromo(){
        if(!(editTextPromoCode.getText().toString().replaceAll("\\s+","")).equals("")){
            findViewById(R.id.layoutChooseBank).setVisibility(View.GONE);
            findViewById(R.id.bankDescriptionLayout).setVisibility(View.GONE);
            findViewById(R.id.progressBarBank).setVisibility(View.VISIBLE);

            promoName = editTextPromoCode.getText().toString();

            Map<String, String> params = new HashMap<>();
            params.put("reservation_id", Integer.toString(reservation.getReservationID()));
            params.put("device_id", imei);
            params.put("name", promoName);
            params.put("auth_mode", isLoggedIn()?"_auth":"");
            JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "promo", params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

//                            Log.e("response "+Configuration.WEB_SERVICE_CONTROLLER + "promo"+(isLoggedIn()?"/_auth":""), response.toString());

                    try {

                        if(response.getInt("status")==1){
                            banks.clear();
                            ArrayList<Bank> promoBanks = new Gson().fromJson(response.getString("bank"), new TypeToken<ArrayList<Bank>>() {}.getType());
                            for (int i = 0; i < promoBanks.size(); i++) {
                                banks.add(promoBanks.get(i));
                            }
                            initBanks();
                            adapterBankTransfer.notifyDataSetChanged();
                            adapterVirtualAccount.notifyDataSetChanged();
                            adapterCreditCard.notifyDataSetChanged();
                            adapterConvenienceStore.notifyDataSetChanged();

                            promoAmount = response.getInt("amount");

                            ((TextView) findViewById(R.id.textViewPromoCode)).setText(promoName);
                            ((TextView) findViewById(R.id.textViewPromoDiscount)).setText(Utils.numberFormat(response.getInt("amount")));


                            findViewById(R.id.inputPromoCode).setVisibility(View.GONE);
                            findViewById(R.id.layoutPromoCode).setVisibility(View.VISIBLE);
                            findViewById(R.id.layoutAmountPromo).setVisibility(View.VISIBLE);

                            findViewById(R.id.progressBarBank).setVisibility(View.GONE);
                            findViewById(R.id.layoutChooseBank).setVisibility(View.VISIBLE);

                            ReservationModel reservationModel = new ReservationModel(TrainPaymentActivity.this);
                            reservationModel.setReservationPromo(reservation.getReservationID(), promoAmount, promoName);

                        }
                        else{
                            dialogPromoEmpty(response.getInt("status")*-1, response.getInt("status")==-4 || response.getInt("status")==-5 || response.getInt("status")==-6 || response.getInt("status")==-7 ? response.getString("payload") : "");
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialogErrorConnection();
                        findViewById(R.id.layoutChooseBank).setVisibility(View.VISIBLE);
                        findViewById(R.id.progressBarBank).setVisibility(View.GONE);
                    }



                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError arg0) {
                    VolleyLog.d("Error: ", arg0.getMessage());
                    dialogErrorConnection();
                    findViewById(R.id.layoutChooseBank).setVisibility(View.VISIBLE);
                    findViewById(R.id.progressBarBank).setVisibility(View.GONE);
                }
            });
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
        else{
//                    //Log.e("error", "masukkan kode dgn benar");
        }
    }

    @Override
    public void onBackPressed() {
        if(bankChoose){
            bankChange();
        }
        else {
            finish();
            startActivity(new Intent(TrainPaymentActivity.this, TicketBookingActivity.class));
        }
    }

    private void bankChange(){
        findViewById(R.id.bankDescriptionLayout).setVisibility(View.GONE);
        findViewById(R.id.layoutChooseBank).setVisibility(View.VISIBLE);
        if(promoAmount==0) {
            findViewById(R.id.inputPromoCode).setVisibility(View.VISIBLE);
        }
        bankChoose = false;

        bank = null;

        textViewTimeLimit.setText(Utils.longDateTime(reservation.getTimeLimit(), this) + (getCurLang().equals("en")?" GMT+7":" WIB"));
    }

    @Override
    public void onItemClick(Bank item) {
        bankChoose = true;
        findViewById(R.id.bankDescriptionLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.layoutChooseBank).setVisibility(View.GONE);
        findViewById(R.id.inputPromoCode).setVisibility(View.GONE);
        bankDescription(item);
    }

    private void bankDescription(Bank item){
        bank = item;
        textViewTimeLimit.setText(Utils.longDateTime(bank.getTimeLimit(), this) + (getCurLang().equals("en")?" GMT+7":" WIB"));

        ImageUtil.displayImage((ImageView) findViewById(R.id.imageViewBank), Configuration.TARGET_URL+bank.getImage(), null);
        ((TextView) findViewById(R.id.dialog_info_title)).setText(bank.getName());
        ((HtmlTextView) findViewById(R.id.dialog_info_text)).setHtml(bank.getDescription(getCurLocale()));
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            ((TextView) findViewById(R.id.dialog_info_text)).setText(Html.fromHtml(bank.getDescription(getCurLocale()), Html.FROM_HTML_MODE_LEGACY));
//        }
//        else{
//            ((TextView) findViewById(R.id.dialog_info_text)).setText(Html.fromHtml(bank.getDescription(getCurLocale())));
//        }

        int discount = 0;
        int surcharge = 0;
        float discountValue = bank.getDiscountValue();
        float surchargeValue = bank.getSurchargeValue();
        if(discountValue>0){
            discount = (int)(bank.getDiscountType().equals("percent")?reservation.getTotalFare()*discountValue/100:discountValue);
            discount = -discount;
        }
        if(surchargeValue>0){
            surcharge = (int)(bank.getSurchargeType().equals("percent")?reservation.getTotalFare()*surchargeValue/100:surchargeValue);
        }

        if(discount == 0 && surcharge == 0){
            findViewById(R.id.bankFare).setVisibility(View.GONE);
        }
        else {
            findViewById(R.id.bankFare).setVisibility(View.VISIBLE);
            if(discount < 0){
                findViewById(R.id.bankDiscount).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.textViewBankDiscount)).setText(Utils.numberFormat(discount));
            }
            else {
                findViewById(R.id.bankDiscount).setVisibility(View.GONE);
            }
            if(surcharge > 0){
                findViewById(R.id.transactionFee).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.textViewTransactionFee)).setText(Utils.numberFormat(surcharge));
            }
            else {
                findViewById(R.id.transactionFee).setVisibility(View.GONE);
            }
        }

//        final int totalPayment = reservation.getTotalFare()+discount+surcharge+promoAmount;
//        final int totalPrice = reservation.getTotalFare() - reservation.getTotalDiscount();
//        final int totalDiscount = reservation.getTotalDiscount();
        final int bankDiscount = discount;
        final int bankSurcharge = surcharge;


        ((TextView) findViewById(R.id.textViewGrandTotal)).setText(getResources().getString(R.string.total)+ " : "+Utils.numberFormatCurrency(reservation.getTotalFare()+discount+surcharge+promoAmount));
        textViewTimeLimit.setText(Utils.longDateTime(bank.getTimeLimit(), this) + (getCurLang().equals("en")?" GMT+7":" WIB"));

        (findViewById(R.id.dialog_info_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!bank.getGateway().equals("6") && !bank.getGateway().equals("1")) {
                    if (!bank.getGateway().equals("6")){
                        findViewById(R.id.dialog_info_text).setVisibility(View.GONE);
                        findViewById(R.id.dividerBankButton).setVisibility(View.GONE);
                        findViewById(R.id.linearLayoutBankAction).setVisibility(View.GONE);
                        findViewById(R.id.layoutCreditCardForm).setVisibility(View.GONE);
                    }
                    findViewById(R.id.progressBarBank).setVisibility(View.VISIBLE);
                }

                final Map<String, String> params = new HashMap<>();
                params.put("reservation_id", Integer.toString(reservation.getReservationID()));
                params.put("bank_id", Integer.toString(bank.getBankID()));

                params.put("device_id", imei);

                if (bank.getGateway().equals("1")) {
                    final Dialog mDialog = new Dialog(TrainPaymentActivity.this, R.style.CustomDialogTheme);

                    mDialog.setContentView(R.layout.dialog_yes_no_question);
                    mDialog.setCancelable(true);
                    TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                    TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                    mDialogHeader.setText(R.string.confirm);
                    mDialogText.setText(Html.fromHtml(getString(R.string.immediately_transfer_payment)), TextView.BufferType.SPANNABLE);

                    ((TextView) mDialog.findViewById(R.id.dialog_info_ok)).setText(R.string.okay);
                    ((TextView) mDialog.findViewById(R.id.dialog_info_no)).setText(R.string.back);

                    mDialog.show();

                    mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialog.findViewById(R.id.progressDialogConfirmation).setVisibility(View.VISIBLE);
                            mDialog.findViewById(R.id.layoutConfirmButton).setVisibility(View.GONE);
                            findViewById(R.id.progressBarBank).setVisibility(View.VISIBLE);
                            mDialog.dismiss();

                            params.put("bank_surcharge", Integer.toString(bankSurcharge));
                            params.put("bank_discount", Integer.toString(bankDiscount));
                            System.out.println("cembeliq :" + params.toString());
                            JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "direct_transfer", params, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {

                                        if (response.getInt("status") == 1) {
                                            ReservationModel reservationModel = new ReservationModel(TrainPaymentActivity.this);
                                            reservationModel.updateReservationTimeLimit(reservation.getReservationID(), response.getString("time_limit"));
                                            reservationModel.setReservationBank(reservation.getReservationID(), bank.getBankID(), bank.getName(), response.getInt("amount"), response.getInt("discount"), response.getInt("surcharge"), response.getInt("unique_code"), "");

                                            JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "get_bank_user_guide", params, new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    try {

                                                        if (response.getInt("status") == 1) {
                                                            Gson gson = new Gson();
                                                            Intent intent = new Intent(TrainPaymentActivity.this, BankCVSResultGuideActivity.class);
                                                            intent.putExtra("reservation_id", reservation.getReservationID());
                                                            intent.putExtra("bank", gson.toJson(bank));
        //                                                    intent.putExtra("type", reservation.getCarrierType());

                                                            Bundle bundle = new Bundle();
                                                            bundle.putString("atm", response.getString("atm"));
                                                            bundle.putString("mobile_banking", response.getString("mobile_banking"));
                                                            bundle.putString("internet_banking", response.getString("internet_banking"));
                                                            bundle.putString("sms_banking", response.getString("sms_banking"));
                                                            bundle.putString("convenience_store", response.getString("convenience_store"));
                                                            intent.putExtra("guidance", bundle);
                                                            startActivity(intent);
                                                        } else {
                                                            dialogErrorConnection();
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                        dialogErrorConnection();
                                                    }


                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError arg0) {
                                                    VolleyLog.d("Error: ", arg0.getMessage());
                                                    dialogErrorConnection();
                                                }
                                            });
                                            AppController.getInstance().addToRequestQueue(jsonReq);

                                        } else {
                                            dialogErrorTransaction();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialogErrorConnection();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError arg0) {
                                    VolleyLog.d("Error: ", arg0.getMessage());
                                    dialogErrorConnection();
                                }
                            });
                            AppController.getInstance().addToRequestQueue(jsonReq);
                        }
                    });

                    mDialog.findViewById(R.id.dialog_info_no).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialog.dismiss();
                        }
                    });
                }
                else if (bank.getGateway().equals("4") || bank.getGateway().equals("5")) {
                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + (bank.getGateway().equals("4") ? "nicepay_va" : "nicepay_cvs"), params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                if (response.getInt("status") == 1) {
                                    ReservationModel reservationModel = new ReservationModel(TrainPaymentActivity.this);
                                    reservationModel.updateReservationTimeLimit(reservation.getReservationID(), response.getString("time_limit"));
                                    reservationModel.setReservationBank(reservation.getReservationID(), bank.getBankID(), bank.getName(), response.getInt("amount"), response.getInt("discount"), response.getInt("surcharge"), response.getInt("unique_code"), response.getString("va_number"));

                                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + (bank.getGateway().equals("4") ? "get_bank_user_guide" : "get_cvs_user_guide"), params, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {

                                                if (response.getInt("status") == 1) {
                                                    Gson gson = new Gson();
                                                    Intent intent = new Intent(TrainPaymentActivity.this, BankCVSResultGuideActivity.class);
                                                    intent.putExtra("reservation_id", reservation.getReservationID());
                                                    intent.putExtra("bank", gson.toJson(bank));
//                                                    intent.putExtra("type", reservation.getCarrierType());

                                                    Bundle bundle = new Bundle();
                                                    bundle.putString("atm", response.getString("atm"));
                                                    bundle.putString("mobile_banking", response.getString("mobile_banking"));
                                                    bundle.putString("internet_banking", response.getString("internet_banking"));
                                                    bundle.putString("sms_banking", response.getString("sms_banking"));
                                                    bundle.putString("convenience_store", response.getString("convenience_store"));
                                                    intent.putExtra("guidance", bundle);
                                                    startActivity(intent);
                                                } else {
                                                    dialogErrorConnection();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                dialogErrorConnection();
                                            }


                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError arg0) {
                                            VolleyLog.d("Error: ", arg0.getMessage());
                                            dialogErrorConnection();
                                        }
                                    });
                                    AppController.getInstance().addToRequestQueue(jsonReq);

                                } else {
                                    dialogErrorTransaction();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialogErrorConnection();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);
                }
                else if (bank.getGateway().equals("6")) {
                    boolean valid = true;
                    final EditText cardNumber = findViewById(R.id.creditCardNumberEditText);
                    final EditText cardHolder = findViewById(R.id.cardHolderEditText);
                    final EditText cvv = findViewById(R.id.cvvEditText);


                    if(cardNumber.getText().toString().isEmpty()){
                        cardNumber.setError(getResources().getString(R.string.passenger_empty_validation));
                        valid = false;
                        cardNumber.requestFocus();
                    }
                    if(cardHolder.getText().toString().isEmpty()){
                        cardHolder.setError(getResources().getString(R.string.passenger_empty_validation));
                        valid = false;
                        cardHolder.requestFocus();
                    }
                    if(cvv.getText().toString().isEmpty()){
                        cvv.setError(getResources().getString(R.string.passenger_empty_validation));
                        valid = false;
                        cvv.requestFocus();
                    }

                    if(valid) {


                        final Dialog mDialog = new Dialog(TrainPaymentActivity.this, R.style.CustomDialogTheme);

                        mDialog.setContentView(R.layout.dialog_yes_no_question);
                        mDialog.setCancelable(true);
                        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                        mDialogHeader.setText(R.string.continue_to_payment);
                        mDialogText.setText(bank.getDescription(getCurLocale()));

                        ((TextView) mDialog.findViewById(R.id.dialog_info_ok)).setText(R.string.okay);
                        ((TextView) mDialog.findViewById(R.id.dialog_info_no)).setText(R.string.cancel);

                        mDialog.show();

                        mDialog.findViewById(R.id.dialog_info_no).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mDialog.dismiss();
                            }
                        });

                        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mDialog.dismiss();

                                findViewById(R.id.dialog_info_text).setVisibility(View.GONE);
                                findViewById(R.id.dividerBankButton).setVisibility(View.GONE);
                                findViewById(R.id.linearLayoutBankAction).setVisibility(View.GONE);
                                findViewById(R.id.layoutCreditCardForm).setVisibility(View.GONE);

                                findViewById(R.id.progressBarBank).setVisibility(View.VISIBLE);

                                Spinner year = findViewById(R.id.spinnerValidThruYear);
                                Spinner month = findViewById(R.id.spinnerValidThruMonth);

                                params.put("card_no", cardNumber.getText().toString());
                                params.put("holder_name", cardHolder.getText().toString());
                                params.put("valid_thru", year.getSelectedItem().toString().substring(2)+month.getSelectedItem().toString().substring(1, 3));
                                params.put("cvv", cvv.getText().toString());

                                JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "nicepay_cc", params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {

                                            if (response.getInt("status") == 1) {
                                                JSONObject param = response.getJSONObject("param");
                                                Intent intent = new Intent(TrainPaymentActivity.this, CreditCardValidationActivity.class);
                                                intent.putExtra("url", response.getString("url"));
                                                intent.putExtra("timeStamp", param.getString("timeStamp"));
                                                intent.putExtra("tXid", param.getString("tXid"));
                                                intent.putExtra("cardNo", param.getString("cardNo"));
                                                intent.putExtra("cardExpYymm", param.getString("cardExpYymm"));
                                                intent.putExtra("cardCvv", param.getString("cardCvv"));
                                                intent.putExtra("cardHolderNm", param.getString("cardHolderNm"));
                                                intent.putExtra("merchantToken", param.getString("merchantToken"));
                                                intent.putExtra("callBackUrl", param.getString("callBackUrl"));
                                                intent.putExtra("bank", new Gson().toJson(bank));
                                                intent.putExtra("reservation_id", reservation.getReservationID());
                                                startActivity(intent);


                                            } else {
                                                dialog(getString(R.string.error_occurred), response.getString("message"));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            dialogErrorConnection();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError arg0) {
                                        VolleyLog.d("Error: ", arg0.getMessage());
                                        dialogErrorConnection();
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(jsonReq);
                            }
                        });
                    }
                }

            }
        });
        (findViewById(R.id.dialog_info_change)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bankChange();
            }
        });

        ((NestedScrollView) findViewById(R.id.scrollViewPayment)).fullScroll(View.FOCUS_DOWN);
    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogErrorTransaction(){
        dialog(R.string.error_occurred, R.string.error_transaction_handshaking);
    }

    private void dialogErrorSeatMap(){
        dialog(R.string.error_occurred, R.string.error_get_seat_map_handshaking);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        findViewById(R.id.dialog_info_text).setVisibility(View.VISIBLE);
        findViewById(R.id.dividerBankButton).setVisibility(View.VISIBLE);
        findViewById(R.id.linearLayoutBankAction).setVisibility(View.VISIBLE);
        findViewById(R.id.scrollViewPayment).setVisibility(View.VISIBLE);

        findViewById(R.id.progressBarBank).setVisibility(View.GONE);
        findViewById(R.id.progressBarChangeSeat).setVisibility(View.GONE);

        mDialog.show();

    }

    private void dialog(String title, String message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        findViewById(R.id.dialog_info_text).setVisibility(View.VISIBLE);
        findViewById(R.id.dividerBankButton).setVisibility(View.VISIBLE);
        findViewById(R.id.linearLayoutBankAction).setVisibility(View.VISIBLE);
        findViewById(R.id.scrollViewPayment).setVisibility(View.VISIBLE);

        findViewById(R.id.progressBarBank).setVisibility(View.GONE);
        findViewById(R.id.progressBarChangeSeat).setVisibility(View.GONE);

        mDialog.show();
    }



    private void dialogPromoEmpty(int index, String payload){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(R.string.promo_empty);

        String[] arrError = getResources().getStringArray(R.array.promo_error_code);

        mDialogText.setText(arrError[index].replace("#", payload));

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        findViewById(R.id.layoutChooseBank).setVisibility(View.VISIBLE);
        findViewById(R.id.progressBarBank).setVisibility(View.GONE);

        mDialog.show();

    }

    @Override
    public void onResume() {
        super.onResume();

        findViewById(R.id.dialog_info_text).setVisibility(View.VISIBLE);
        findViewById(R.id.dividerBankButton).setVisibility(View.VISIBLE);
        findViewById(R.id.linearLayoutBankAction).setVisibility(View.VISIBLE);

        findViewById(R.id.progressBarBank).setVisibility(View.GONE);
        findViewById(R.id.progressBarChangeSeat).setVisibility(View.GONE);
    }
}
