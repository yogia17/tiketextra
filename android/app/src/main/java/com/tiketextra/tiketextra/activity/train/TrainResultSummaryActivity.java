package com.tiketextra.tiketextra.activity.train;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.object.Station;
import com.tiketextra.tiketextra.object.FareRouteTrain;
import com.tiketextra.tiketextra.object.Train;
import com.tiketextra.tiketextra.util.Utils;

public class TrainResultSummaryActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_summary);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarResultSummary);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.ticket_detail);

        final Bundle parameter = getIntent().getBundleExtra("parameter");
        boolean discountFromProfit = false;


        final Gson gson = new Gson();
        FareRouteTrain fareDepart = gson.fromJson(parameter.getString("depart_choice"), FareRouteTrain.class);

        StationModel stationModel = new StationModel(this);

        ((TextView) findViewById(R.id.departTextView)).setText(getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(fareDepart.getDepartDatetime(), this));
        ((TextView) findViewById(R.id.departHintTextView)).setText(stationModel.getStation(fareDepart.getTrain().get(0).getDepartPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ stationModel.getStation(fareDepart.getTrain().get(fareDepart.getTrain().size() - 1).getArrivePort()).getCity());


        for (int i = 0; i < fareDepart.getTrain().size() || i <= 5; i++) {
            if (i == 0) {
                Train fl = fareDepart.getTrain().get(i);
                ((TextView) findViewById(R.id.depart_textViewTrainName)).setText(fl.getTrainNameNumber());
                ((TextView) findViewById(R.id.depart_textViewTrainClass)).setText(getString(getResources().getIdentifier(("class_" + fareDepart.getFareClass()).toLowerCase(), "string", getPackageName())) + " - " + fareDepart.getFareID());
                ((TextView) findViewById(R.id.depart_departTime)).setText(fl.getDepartTime());
                ((TextView) findViewById(R.id.depart_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(R.id.depart_arriveTime)).setText(fl.getArriveTime());
                ((TextView) findViewById(R.id.depart_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Station station = stationModel.getStation(fl.getDepartPort());
                ((TextView) findViewById(R.id.depart_departStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_departCity)).setText(station.getCity());
                station = stationModel.getStation(fl.getArrivePort());
                ((TextView) findViewById(R.id.depart_arriveStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(R.id.depart_arriveCity)).setText(station.getCity());

            } else if (i > 0 && i <= 5 && i < fareDepart.getTrain().size()) {
                Train fl = fareDepart.getTrain().get(i);



                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTrainName", "id", getPackageName()))).setText(fl.getTrainNameNumber());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTrainClass", "id", getPackageName()))).setText(getString(getResources().getIdentifier(("class_" + fareDepart.getFareClass()).toLowerCase(), "string", getPackageName())) + " - " + fareDepart.getFareID());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                Station station = stationModel.getStation(fl.getDepartPort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_departCity", "id", getPackageName()))).setText(station.getCity());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(fareDepart.getTrain().get(i-1).getArriveDatetime(), fl.getDepartDatetime(), fareDepart.getTrain().get(i-1).getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + station.getCity());

                station = stationModel.getStation(fl.getArrivePort());
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                ((TextView) findViewById(getResources().getIdentifier("depart" + i + "_arriveCity", "id", getPackageName()))).setText(station.getCity());


                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.VISIBLE);
            } else if (i > 0 && i <= 5 && i >= fareDepart.getTrain().size()) {
                findViewById(getResources().getIdentifier("depart" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.GONE);
                findViewById(getResources().getIdentifier("depart" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.GONE);
            }
        }

        ((TextView) findViewById(R.id.departSummaryTextView)).setText(fareDepart.getDepartPort() + " - " + fareDepart.getArrivePort() + " x " + (parameter.getInt("adult") + 0 + parameter.getInt("infant")) + " " + getString(R.string.person).toLowerCase());
        ((TextView) findViewById(R.id.departFareTextView)).setText(Utils.numberFormatCurrency(fareDepart.getTotalPriceBeforeDiscount(parameter.getInt("adult"), parameter.getInt("infant"))));

        int total = fareDepart.getTotalPrice(parameter.getInt("adult"), parameter.getInt("infant"));
        int discount = fareDepart.getTotalDiscount(parameter.getInt("adult"), parameter.getInt("infant"));

        discountFromProfit = discountFromProfit || fareDepart.getFare().get("adult").isDiscountRuleFromProfit();

        //------------------------------------------------------------------------
        if(parameter.getString("type").equals("oneway")){
            findViewById(R.id.returnDetailInfoLayout).setVisibility(View.GONE);
            findViewById(R.id.returnFareLayout).setVisibility(View.GONE);
        }
        else {
            FareRouteTrain fareReturn = gson.fromJson(parameter.getString("return_choice"), FareRouteTrain.class);

            ((TextView) findViewById(R.id.returnTextView)).setText(getResources().getString(R.string.returns)+" - "+ Utils.mediumDate(fareReturn.getDepartDatetime(), this));
            ((TextView) findViewById(R.id.returnHintTextView)).setText(stationModel.getStation(fareReturn.getTrain().get(0).getDepartPort()).getCity()+" "+getResources().getString(R.string.to).toLowerCase()+" "+ stationModel.getStation(fareReturn.getTrain().get(fareReturn.getTrain().size() - 1).getArrivePort()).getCity());



            for (int i = 0; i < fareReturn.getTrain().size() || i <= 5; i++) {
                if (i == 0) {
                    Train fl = fareReturn.getTrain().get(i);
                    ((TextView) findViewById(R.id.return_textViewTrainName)).setText(fl.getTrainNameNumber());
                    ((TextView) findViewById(R.id.return_textViewTrainClass)).setText(getString(getResources().getIdentifier(("class_" + fareReturn.getFareClass()).toLowerCase(), "string", getPackageName())) + " - " + fareReturn.getFareID());
                    ((TextView) findViewById(R.id.return_departTime)).setText(fl.getDepartTime());
                    ((TextView) findViewById(R.id.return_departDate)).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(R.id.return_arriveTime)).setText(fl.getArriveTime());
                    ((TextView) findViewById(R.id.return_arriveDate)).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Station station = stationModel.getStation(fl.getDepartPort());
                    ((TextView) findViewById(R.id.return_departStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(R.id.return_departCity)).setText(station.getCity());
                    station = stationModel.getStation(fl.getArrivePort());
                    ((TextView) findViewById(R.id.return_arriveStation)).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(R.id.return_arriveCity)).setText(station.getCity());

                } else if (i > 0 && i <= 5 && i < fareReturn.getTrain().size()) {
                    Train fl = fareReturn.getTrain().get(i);
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTrainName", "id", getPackageName()))).setText(fl.getTrainNameNumber());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTrainClass", "id", getPackageName()))).setText(getString(getResources().getIdentifier(("class_" + fareReturn.getFareClass()).toLowerCase(), "string", getPackageName())) + " - " + fareReturn.getFareID());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departTime", "id", getPackageName()))).setText(fl.getDepartTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getDepartDate(), this));
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveTime", "id", getPackageName()))).setText(fl.getArriveTime());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveDate", "id", getPackageName()))).setText(Utils.mediumMD(fl.getArriveDate(), this));

                    Station station = stationModel.getStation(fl.getDepartPort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_departCity", "id", getPackageName()))).setText(station.getCity());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_textViewTransit", "id", getPackageName()))).setText(Utils.strTimeDiff(fareReturn.getTrain().get(i-1).getArriveDatetime(), fl.getDepartDatetime(), fareReturn.getTrain().get(i-1).getArriveTimezone() - fl.getDepartTimezone()) + " " + getString(R.string.transit_at) +" " + station.getCity());

                    station = stationModel.getStation(fl.getArrivePort());
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveStation", "id", getPackageName()))).setText(station.getStationName() + " ("+station.getIataCode()+")");
                    ((TextView) findViewById(getResources().getIdentifier("return" + i + "_arriveCity", "id", getPackageName()))).setText(station.getCity());


                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.VISIBLE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.VISIBLE);
                } else if (i > 0 && i <= 5 && i >= fareReturn.getTrain().size()) {
                    findViewById(getResources().getIdentifier("return" + i + "_transitLayout", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_trainNumber", "id", getPackageName())).setVisibility(View.GONE);
                    findViewById(getResources().getIdentifier("return" + i + "_detailTrain", "id", getPackageName())).setVisibility(View.GONE);
                }
            }


            ((TextView) findViewById(R.id.returnSummaryTextView)).setText(fareReturn.getDepartPort() + " - " + fareReturn.getArrivePort() + " x " + (parameter.getInt("adult") + 0 + parameter.getInt("infant")) + " " + getString(R.string.person).toLowerCase());
            ((TextView) findViewById(R.id.returnFareTextView)).setText(Utils.numberFormatCurrency(fareReturn.getTotalPriceBeforeDiscount(parameter.getInt("adult"), parameter.getInt("infant"))));

            total += fareReturn.getTotalPrice(parameter.getInt("adult"), parameter.getInt("infant"));
            discount += fareReturn.getTotalDiscount(parameter.getInt("adult"), parameter.getInt("infant"));

            discountFromProfit = discountFromProfit || fareReturn.getFare().get("adult").isDiscountRuleFromProfit();
        }

        ((TextView) findViewById(R.id.fareHintTextView)).setText(parameter.getString("type").equals("oneway")?R.string.oneway:R.string.roundtrip);


            if(discount < 0 && discountFromProfit==false){
                findViewById(R.id.discountFareLayout).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.discountFareTextView)).setText(Utils.numberFormatCurrency(discount));
                ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / ( Integer.toString(total).length() >= 3 ? 1000 : 1 ) )));
                ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - ( Integer.toString(total).length() >= 3 ? 3 : 0 )));
            }
            else if(discountFromProfit){
//            findViewById(R.id.discountFareLayout).setVisibility(View.VISIBLE);
//            ((TextView) findViewById(R.id.discountFareTextView)).setText(R.string.price_before_discount);
                total = total - discount;
                ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / ( Integer.toString(total).length() >= 3 ? 1000 : 1 ) )));
                ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - ( Integer.toString(total).length() >= 3 ? 3 : 0 )));
            }
            else{
                ((TextView) findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor(total / ( Integer.toString(total).length() >= 3 ? 1000 : 1 ) )));
                ((TextView) findViewById(R.id.textViewPrice2)).setText(Integer.toString(total).substring(Integer.toString(total).length() - ( Integer.toString(total).length() >= 3 ? 3 : 0 )));
            }

        findViewById(R.id.buttonBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(isLoggedIn()){
//                    ContactModel contactModel = new ContactModel(TrainResultSummaryActivity.this);
//                    Contact cont = contactModel.getContactByName(Utils.upperCaseAllFirst(getSessionName().replaceAll("\\s+", " ")));
//
//                    if(cont == null){
//                        contactModel.insert(getSessionTitle(), Utils.upperCaseAllFirst(getSessionName().replaceAll("\\s+", " ")), getSessionMobile(), getSessionEmail());
//                    }
//                    else{
//                        contactModel.update(cont.getContactID(), getSessionTitle(), Utils.upperCaseAllFirst(getSessionName().replaceAll("\\s+", " ")), getSessionMobile(), getSessionEmail());
//
//                    }
//                    Contact contact = new Contact(getSessionTitle(), Utils.upperCaseAllFirst(getSessionName().replaceAll("\\s+", " ")), getSessionMobile(), getSessionEmail());
                    Intent intent = new Intent(TrainResultSummaryActivity.this, TrainPassengerActivity.class);
                    intent.putExtra("parameter", parameter);
//                    intent.putExtra("contact", gson.toJson(contact));
                    startActivity(intent);

//                }
//                else {
//                    Intent intent = new Intent(TrainResultSummaryActivity.this, TrainPassengerContactActivity.class);
//                    intent.putExtra("parameter", parameter);
//                    startActivity(intent);
//                }
            }
        });
    }
}
