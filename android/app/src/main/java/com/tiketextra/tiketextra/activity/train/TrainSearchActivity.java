package com.tiketextra.tiketextra.activity.train;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.format.ArrayWeekDayFormatter;
import com.prolificinteractive.materialcalendarview.format.MonthArrayTitleFormatter;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity.MenuOnlineActivity;
import com.tiketextra.tiketextra.activity.reservation.flight.TicketBookingActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.HighlightWeekendsDecorator;
import com.tiketextra.tiketextra.helper.SelectionDecorator;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.model.SettingModel;
import com.tiketextra.tiketextra.object.Station;
import com.tiketextra.tiketextra.service.GPSTracker;
import com.tiketextra.tiketextra.util.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TrainSearchActivity extends BaseActivity {

    private String from, to;
    private int adult = 1, infant = 0;
    private String calendarMode = "go";
    private MaterialCalendarView calendarWidget;
    private Date goDate;
    private Date backDate;
    private boolean isReturn = false;


    private ImageView adultIncreaseButton;
    private ImageView adultDecreaseButton;
    private ImageView infantIncreaseButton;
    private ImageView infantDecreaseButton;
    private TextView adultCountTextView;
    private TextView infantCountTextView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTrainSearch);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.train_search);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }

        adultIncreaseButton = findViewById(R.id.adultIncreaseButton);
        adultDecreaseButton = findViewById(R.id.adultDecreaseButton);
        infantIncreaseButton = findViewById(R.id.infantIncreaseButton);
        infantDecreaseButton = findViewById(R.id.infantDecreaseButton);
        adultCountTextView = findViewById(R.id.adultCountTextView);
        infantCountTextView = findViewById(R.id.infantCountTextView);



        calendarWidget = findViewById(R.id.calendarView);

        SettingModel settingModel = new SettingModel(this);
        this.from=settingModel.getValue("train_search_from_port");
        this.to=settingModel.getValue("train_search_to_port");
        initFromToPort();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Dialog mDialog = new Dialog(TrainSearchActivity.this, R.style.CustomDialogTheme);

                mDialog.setContentView(R.layout.dialog_info);
                mDialog.setCancelable(true);
                TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
                TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

                mDialogHeader.setText(R.string.get_location);
                mDialogText.setText(R.string.get_nearest_station);

                mDialogOKButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });

                mDialog.show();
                getStationRoute(mDialog);
                initFromToPort();
            }
        }, 0);

        findViewById(R.id.buttonFromPort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TrainSearchActivity.this, ChooseStationActivity.class);
                i.putExtra("stationType", "fromPort");
                startActivityForResult(i, 1);
            }
        });

        findViewById(R.id.buttonToPort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TrainSearchActivity.this, ChooseStationActivity.class);
                i.putExtra("stationType", "toPort");
                startActivityForResult(i, 2);
            }
        });

        findViewById(R.id.buttonRevert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String temp = from;
                from = to;
                to = temp;
                initFromToPort();
            }
        });



        findViewById(R.id.buttonOneway).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOneway();

            }
        });

        findViewById(R.id.buttonRoundtrip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickRoundtrip();
            }
        });

        initDate(Configuration.DEPART_EXTRA, Configuration.RETURN_EXTRA_FROM_DEPART);

        findViewById(R.id.buttonDepartDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarMode = "go";
                showCalendar();
            }
        });

        findViewById(R.id.buttonReturnDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarMode = "back";
                showCalendar();
            }
        });

        calendarWidget.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                calendarWidget.removeDecorators();
//                Log.e("date", date.toString());
//                Log.e("date", date.getDate().toString());
                if (calendarMode.equals("go")) {

                    ArrayList<CalendarDay> goSelectedDay = new ArrayList<>();
                    goSelectedDay.add(date);

                    SelectionDecorator goSelectedDecorator = new SelectionDecorator(TrainSearchActivity.this, SelectionDecorator.LEFT_DECORATOR, goSelectedDay);
                    calendarWidget.addDecorator(goSelectedDecorator);

                    goDate = date.getDate();
                    if (backDate.compareTo(date.getDate()) < 0) {
                        backDate = goDate;
                    }
                    if (isReturn) {




                        ArrayList<CalendarDay> rangeDays = Utils.getDates(goDate, backDate);


                        ArrayList<CalendarDay> backSelectedDay = new ArrayList<>();
                        backSelectedDay.add(CalendarDay.from(backDate));
                        SelectionDecorator rangeDecorator = new SelectionDecorator(TrainSearchActivity.this, SelectionDecorator.RANGE_DECORATOR, rangeDays);
                        SelectionDecorator backSelectedDecorator = new SelectionDecorator(TrainSearchActivity.this, SelectionDecorator.RIGHT_DECORATOR, backSelectedDay);
                        calendarWidget.addDecorator(rangeDecorator);
                        calendarWidget.addDecorator(backSelectedDecorator);
                    }
                    else{
                        clickOneway();
                    }
                }
                else{
                    calendarWidget.setDateSelected(backDate, false);
                    backDate = date.getDate();
                    if (goDate.compareTo(date.getDate()) > 0) {
                        goDate = backDate;
                    }

                    ArrayList<CalendarDay> rangeDays = Utils.getDates(goDate, backDate);
                    ArrayList<CalendarDay> goSelectedDay = new ArrayList<>();
                    goSelectedDay.add(CalendarDay.from(goDate));

                    ArrayList<CalendarDay> backSelectedDay = new ArrayList<>();
                    backSelectedDay.add(date);
                    SelectionDecorator rangeDecorator = new SelectionDecorator(TrainSearchActivity.this, SelectionDecorator.RANGE_DECORATOR,
                            rangeDays);
                    SelectionDecorator goSelectedDecorator = new SelectionDecorator(TrainSearchActivity.this, SelectionDecorator.LEFT_DECORATOR,
                            goSelectedDay);
                    SelectionDecorator backSelectedDecorator = new SelectionDecorator(TrainSearchActivity.this, SelectionDecorator.RIGHT_DECORATOR,
                            backSelectedDay);
                    calendarWidget.addDecorator(rangeDecorator);
                    calendarWidget.addDecorator(goSelectedDecorator);
                    calendarWidget.addDecorator(backSelectedDecorator);
                    clickRoundtrip();
                }
                addHolidayDecorator();
                initDate(Utils.getDateDifference(new Date(), goDate), Utils.getDateDifference(goDate, backDate));
                findViewById(R.id.calendarLayout).setVisibility(View.GONE);
            }
        });


        adultIncreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adult = Integer.parseInt(adultCountTextView.getText().toString());
                if(adult < Configuration.TRAIN_MAX_PASSENGER){
                    adult++;
                }
                if(adult==Configuration.TRAIN_MAX_PASSENGER){
//                    adultIncreaseButton.setVisibility(View.INVISIBLE);
//                    childIncreaseButton.setVisibility(View.INVISIBLE);
                }
                if(adult == 2){
//                    adultDecreaseButton.setVisibility(View.VISIBLE);
                }
                adultCountTextView.setText(Integer.toString(adult));

            }
        });

        adultDecreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adult = Integer.parseInt(adultCountTextView.getText().toString());
                if(adult==Configuration.TRAIN_MAX_PASSENGER){
//                    adultIncreaseButton.setVisibility(View.VISIBLE);
//                    childIncreaseButton.setVisibility(View.VISIBLE);
                }
                if(adult > 1){
                    if(adult==infant){
                        infant--;
                        infantCountTextView.setText(Integer.toString(infant));
                    }
                    adult--;

                }
                if(adult == 1){
//                    adultDecreaseButton.setVisibility(View.INVISIBLE);
                }
                adultCountTextView.setText(Integer.toString(adult));
            }
        });

        infantIncreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infant = Integer.parseInt(infantCountTextView.getText().toString());
                if(infant < adult){
                    infant++;
                }
                if(infant==adult){
//                    infantIncreaseButton.setVisibility(View.INVISIBLE);
                }
                if(infant == 0){
//                    infantDecreaseButton.setVisibility(View.VISIBLE);
                }
                infantCountTextView.setText(Integer.toString(infant));
            }
        });

        infantDecreaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infant = Integer.parseInt(infantCountTextView.getText().toString());
                if(infant==adult){
//                    infantIncreaseButton.setVisibility(View.VISIBLE);
                }
                if(infant > 0){
                    infant--;
                }
                if(infant == 0){
//                    infantDecreaseButton.setVisibility(View.INVISIBLE);
                }
                infantCountTextView.setText(Integer.toString(infant));
            }
        });

        findViewById(R.id.buttonTrainSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StationModel stationModel = new StationModel(getApplicationContext());
                if(!stationModel.existStation(from)){
                    dialogValidation(R.string.wrong_form_filling, R.string.from_station_empty);
                }
                else{
                    if(!stationModel.existStation(to)){
                        dialogValidation(R.string.wrong_form_filling, R.string.to_station_empty);
                    }
                    else {
                        if (from.equals(to)) {
                            dialogValidation(R.string.wrong_form_filling, R.string.from_to_station_match);
                        }
                        else{

                            SettingModel settingModel = new SettingModel(getApplicationContext());
                            settingModel.setValue("train_search_timestamp", Long.toString(System.currentTimeMillis()));


                            settingModel.setValue("train_search_from_port", from);
                            settingModel.setValue("train_search_to_port", to);





                            Intent i = new Intent(TrainSearchActivity.this, TrainResultActivity.class);
                            Bundle bundle = new Bundle();
                            Station fromStation = stationModel.getStation(from);
                            Station toStation = stationModel.getStation(to);

                            stationModel.updateStationHit(from, fromStation.getHit()+1);
                            stationModel.updateStationHit(to, toStation.getHit()+1);

                            if(fromStation.getCountryCode().equals(Configuration.COUNTRY_BASE)){
                                bundle.putBoolean("is_international", !(toStation.getCountryCode().equals(Configuration.COUNTRY_BASE)));
                            }
                            else{
                                bundle.putBoolean("is_international", true);
                            }

                            bundle.putString("from", from);
                            bundle.putString("to", to);
                            bundle.putString("depart", new SimpleDateFormat("yyyy-MM-dd").format(goDate.getTime()));
                            bundle.putString("return", isReturn?new SimpleDateFormat("yyyy-MM-dd").format(backDate.getTime()):"");
                            bundle.putString("type", isReturn?"roundtrip":"oneway");
                            bundle.putInt("adult", adult);
                            bundle.putInt("infant", infant);
                            i.putExtra("parameter", bundle);
                            startActivity(i);



                        }
                    }
                }
            }
        });
    }

    private void showCalendar(){
        findViewById(R.id.calendarLayout).setVisibility(View.VISIBLE);
    }

    private void addHolidayDecorator() {
        calendarWidget.addDecorator(new HighlightWeekendsDecorator());
    }

    private void initDate(int departExtra, int returnExtra) {
        GregorianCalendar cal = new GregorianCalendar();
        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.add(Calendar.DAY_OF_YEAR, 90);
        cal.setTime(new Date());
        calendarWidget.state().edit().setMinimumDate(cal).setMaximumDate(cal2).commit();
//        calendarWidget.state().edit().setMaximumDate(cal).commit();
        calendarWidget.setTitleFormatter(new MonthArrayTitleFormatter(getResources().getTextArray(R.array.custom_months)));
        calendarWidget.setWeekDayFormatter(new ArrayWeekDayFormatter(getResources().getTextArray(R.array.custom_weekdays)));

        addHolidayDecorator();

        cal.add(Calendar.DAY_OF_MONTH, departExtra);
        goDate = cal.getTime();
        calendarWidget.setDateSelected(goDate, true);

        ((TextView) findViewById(R.id.textDayDepart)).setText(Utils.getDayName(cal.get(Calendar.DAY_OF_WEEK), getApplicationContext()));
        ((TextView) findViewById(R.id.textDateMonthDepart)).setText(cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getUcMonthShortName(cal.get(Calendar.MONTH), getApplicationContext()));
        ((TextView) findViewById(R.id.textYearDepart)).setText(cal.get(Calendar.YEAR)+"");

        cal.add(Calendar.DAY_OF_MONTH, returnExtra);
        backDate = cal.getTime();
        ((TextView) findViewById(R.id.textDayReturn)).setText(Utils.getDayName(cal.get(Calendar.DAY_OF_WEEK), getApplicationContext()));
        ((TextView) findViewById(R.id.textDateMonthReturn)).setText(cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getUcMonthShortName(cal.get(Calendar.MONTH), getApplicationContext()));
        ((TextView) findViewById(R.id.textYearReturn)).setText(cal.get(Calendar.YEAR)+"");

    }

    private void clickOneway(){

        isReturn = false;
//        ((TextView) findViewById(R.id.textViewOneway)).setTypeface(((TextView) findViewById(R.id.textViewOneway)).getTypeface(), Typeface.BOLD);
        ((TextView) findViewById(R.id.textViewOneway)).setTextColor(getResources().getColor(R.color.grey_800));
        findViewById(R.id.buttonOneway).setBackground(getResources().getDrawable(R.drawable.capsule_active));
        findViewById(R.id.buttonOneway).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));

//        ((TextView) findViewById(R.id.textViewRoundtrip)).setTypeface(((TextView) findViewById(R.id.textViewRoundtrip)).getTypeface(), Typeface.NORMAL);
        ((TextView) findViewById(R.id.textViewRoundtrip)).setTextColor(getResources().getColor(R.color.grey_500));
        findViewById(R.id.buttonRoundtrip).setBackground(getResources().getDrawable(R.drawable.capsule_inactive));
        findViewById(R.id.buttonRoundtrip).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));


        ((TextView) findViewById(R.id.textViewReturn)).setTextColor(getResources().getColor(R.color.grey_500));
        ((TextView) findViewById(R.id.textDayReturn)).setTextColor(getResources().getColor(R.color.grey_500));
        ((TextView) findViewById(R.id.textDateMonthReturn)).setTextColor(getResources().getColor(R.color.grey_500));
        ((TextView) findViewById(R.id.textYearReturn)).setTextColor(getResources().getColor(R.color.grey_500));

        ((ImageView)findViewById(R.id.imageViewReturn)).setImageDrawable(getResources().getDrawable(R.drawable.ic_calendar_inactive));
    }

    private void clickRoundtrip(){

        isReturn = true;
//        ((TextView) findViewById(R.id.textViewOneway)).setTypeface(((TextView) findViewById(R.id.textViewOneway)).getTypeface(), Typeface.NORMAL);
        ((TextView) findViewById(R.id.textViewOneway)).setTextColor(getResources().getColor(R.color.grey_500));
        findViewById(R.id.buttonOneway).setBackground(getResources().getDrawable(R.drawable.capsule_inactive));
        findViewById(R.id.buttonOneway).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));

//        ((TextView) findViewById(R.id.textViewRoundtrip)).setTypeface(((TextView) findViewById(R.id.textViewRoundtrip)).getTypeface(), Typeface.BOLD);
        ((TextView) findViewById(R.id.textViewRoundtrip)).setTextColor(getResources().getColor(R.color.grey_800));
        findViewById(R.id.buttonRoundtrip).setBackground(getResources().getDrawable(R.drawable.capsule_active));
        findViewById(R.id.buttonRoundtrip).setPadding(getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin), getResources().getDimensionPixelSize(R.dimen.text_margin));


        ((TextView) findViewById(R.id.textViewReturn)).setTextColor(getResources().getColor(R.color.grey_700));
        ((TextView) findViewById(R.id.textDayReturn)).setTextColor(getResources().getColor(R.color.grey_700));
        ((TextView) findViewById(R.id.textDateMonthReturn)).setTextColor(getResources().getColor(R.color.grey_800));
        ((TextView) findViewById(R.id.textYearReturn)).setTextColor(getResources().getColor(R.color.grey_700));

        ((ImageView)findViewById(R.id.imageViewReturn)).setImageDrawable(getResources().getDrawable(R.drawable.ic_calendar_active));

    }

    private void initFromToPort(){
        StationModel stationModel = new StationModel(this);
        Station fromStation = stationModel.getStation(from);
        Station toStation = stationModel.getStation(to);
        ((TextView) findViewById(R.id.textFromCity)).setText(fromStation.getName());
        ((TextView) findViewById(R.id.textToCity)).setText(toStation.getName());
        ((TextView) findViewById(R.id.textFromPort)).setText((Configuration.COUNTRY_BASE.equals(fromStation.getCountryCode())?fromStation.getCity():fromStation.getCity()+" - "+fromStation.getCountry()));
        ((TextView) findViewById(R.id.textToPort)).setText((Configuration.COUNTRY_BASE.equals(toStation.getCountryCode())?toStation.getCity():toStation.getCity()+" - "+toStation.getCountry()));

    }

    private void dialogValidation(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);
        TextView mDialogOKButton = mDialog.findViewById(R.id.dialog_info_ok);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                if (data.getStringExtra("iataCode").equals(to)) {
                    dialogValidation(R.string.wrong_form_filling, R.string.from_to_station_match);
                }
                else {
                    this.from = data.getStringExtra("iataCode");
                    initFromToPort();
                }
            }
        }
        else if(requestCode==2){
            if(resultCode==RESULT_OK){
                if (from.equals(data.getStringExtra("iataCode"))) {
                    dialogValidation(R.string.wrong_form_filling, R.string.from_to_station_match);
                }
                else {
                    this.to = data.getStringExtra("iataCode");
                    initFromToPort();
                }
            }
        }
    }

    private void getStationRoute(Dialog mDialog){
        SettingModel settingModel = new SettingModel(this);
        StationModel stationModel = new StationModel(this);
        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()){
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            if(latitude==0 && longitude==0){
                this.from=settingModel.getValue("train_search_from_port");
                this.to=settingModel.getValue("train_search_to_port");
            }
            else{
                this.from = Utils.getNearestStation(latitude, longitude, stationModel.getAllStations("hit"));
                if(this.from.equals(settingModel.getValue("train_search_to_port"))){
                    this.to = settingModel.getValue("train_search_from_port");
                }
                else{
                    this.to = settingModel.getValue("train_search_to_port");
                }

            }
//            Log.e("latitude", latitude+"");
//            Log.e("longitude", longitude+"");
        }
        else{
            this.from=settingModel.getValue("train_search_from_port");
            this.to=settingModel.getValue("train_search_to_port");
        }
        mDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.calendarLayout).getVisibility() == View.VISIBLE) {
            findViewById(R.id.calendarLayout).setVisibility(View.GONE);
        }  else {
            Intent i = new Intent(this, com.tiketextra.tiketextra.MainActivity.class);
            // i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
            //startActivity(new Intent(this, MenuOnlineActivity.class));
        }
    }
}
