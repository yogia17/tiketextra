package com.tiketextra.tiketextra.activity.train;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.reservation.train.TicketBookingActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.adapter.TrainSelectSeatAdapter;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.listener.TrainPassengerSeat;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.object.Bank;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TrainSelectSeatActivity extends BaseActivity implements TrainPassengerSeat {

    private Reservation reservation;
    private TrainSelectSeatAdapter trainSelectSeatAdapter;
//    private ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_select_seat);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSelectSeat);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ReservationModel reservationModel = new ReservationModel(this);
        StationModel stationModel = new StationModel(this);

        reservation = reservationModel.getReservation(getIntent().getIntExtra("reservation_id", 0));
        String from = stationModel.getStation(reservation.getFromPort()).getStationName();
        String to = stationModel.getStation(reservation.getToPort()).getStationName();

        AppHelper.selectedSeat = new String[reservation.getAdultCount()];
        AppHelper.selectedWagon = new String[reservation.getAdultCount()];

        ((TextView) findViewById(R.id.toolbarTitleTextView)).setText(getIntent().getStringExtra("direction").equals("depart")?R.string.depart_seat:R.string.return_seat);
        if(getIntent().getStringExtra("direction").equals("depart")) {
            ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(from + " " + getString(R.string.to).toLowerCase() + " " + to);
        }
        else{
            ((TextView) findViewById(R.id.toolbarSubTitleTextView)).setText(to + " " + getString(R.string.to).toLowerCase() + " " + from);
        }
        try {
            JSONObject objSeatMap = new JSONObject(getIntent().getStringExtra("map"));
            trainSelectSeatAdapter = new TrainSelectSeatAdapter(getSupportFragmentManager(), getApplicationContext(), objSeatMap, reservation, getIntent().getStringExtra("direction"));
            ViewPager viewPager = findViewById(R.id.containerPassenger);
            viewPager.setAdapter(trainSelectSeatAdapter);

            TabLayout tabLayout = findViewById(R.id.tabsPassenger);
            tabLayout.setupWithViewPager(viewPager);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSubmitDepartOnlySeat() {
        setSeat("depart_only");
    }

    @Override
    public void onSubmitDepartSeat() {
        setSeat("depart");
    }

    @Override
    public void onSubmitReturnSeat() {
        setSeat("return");
    }

    @Override
    public void onBackPressed() {
        findViewById(R.id.containerPassenger).setVisibility(View.GONE);
        findViewById(R.id.tabsPassenger).setVisibility(View.GONE);
        findViewById(R.id.progressBarConfirmSeat).setVisibility(View.VISIBLE);

        Map<String, String> params = new HashMap<>();
        params.put("reservation_id", Integer.toString(reservation.getReservationID()));
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "reservation", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    Gson gson = new Gson();
                    ArrayList<Bank> banks = new ArrayList<>();
                    JSONArray bank = response.getJSONArray("bank");
                    for (int i = 0; i < bank.length(); i++) {
                        Bank curr = gson.fromJson(bank.get(i).toString(), Bank.class);
                        banks.add(curr);
                    }
                    if (reservation.getBank() == null) {
                        Intent intent = new Intent(TrainSelectSeatActivity.this, TrainPaymentActivity.class);
                        intent.putExtra("reservation_id", reservation.getReservationID());
                        intent.putExtra("bank", gson.toJson(banks));
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(TrainSelectSeatActivity.this, TicketBookingActivity.class));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    startActivity(new Intent(TrainSelectSeatActivity.this, TicketBookingActivity.class));
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                startActivity(new Intent(TrainSelectSeatActivity.this, TicketBookingActivity.class));
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);


    }

    private void setSeat(final String type){
        findViewById(R.id.containerPassenger).setVisibility(View.GONE);
        findViewById(R.id.tabsPassenger).setVisibility(View.GONE);
        findViewById(R.id.progressBarConfirmSeat).setVisibility(View.VISIBLE);
        Gson gson = new Gson();
        Map<String, String> params = new HashMap<>();
        params.put("reservation_id", Integer.toString(reservation.getReservationID()));
        params.put("direction", type.equals("depart_only")?"depart":type);
        params.put("passenger_id", gson.toJson(reservation.getAdultPassengerID()));
        params.put("seat", gson.toJson(AppHelper.selectedSeat));
        params.put("wagon", gson.toJson(AppHelper.selectedWagon));
        System.out.println("cembeliq :"+params.toString());
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "train/set_seat", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if(response.getInt("status")==1){
                        ReservationModel reservationModel = new ReservationModel(TrainSelectSeatActivity.this);
                        if(type.equals("depart_only")){
                            reservationModel.setPassengerSeat("depart", reservation.getAdultPassengerID(), AppHelper.selectedWagon, AppHelper.selectedSeat);
                            backToPayment();
                        }
                        else {
                            reservationModel.setPassengerSeat(type, reservation.getAdultPassengerID(), AppHelper.selectedWagon, AppHelper.selectedSeat);
                            if(type.equals("depart")){
                                Map<String, String> params = new HashMap<>();
                                params.put("reservation_id", Integer.toString(reservation.getReservationID()));
                                params.put("direction", "return");
                                JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "train/seat_map", params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        try {
                                            if(response.getInt("status")==1){
                                                Intent seatMap = new Intent(TrainSelectSeatActivity.this, TrainSelectSeatActivity.class);
                                                seatMap.putExtra("map", response.toString());
                                                seatMap.putExtra("reservation_id", getIntent().getIntExtra("reservation_id", 0));
                                                seatMap.putExtra("direction", "return");
                                                startActivity(seatMap);
                                            }
                                            else{
                                                dialogErrorSeatMap();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            dialogErrorConnection();
                                        }



                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError arg0) {
                                        VolleyLog.d("Error: ", arg0.getMessage());
                                        dialogErrorConnection();
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(jsonReq);
                            }
                            else{
                                backToPayment();
                            }
                        }

                    }
                    else{
                        dialogErrorSetSeat();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogErrorConnection();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                dialogErrorConnection();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogErrorSetSeat(){
        dialog(R.string.error_occurred, R.string.error_set_seat_handshaking);
    }

    private void dialogErrorSeatMap(){
        dialog(R.string.error_occurred, R.string.error_get_seat_map_handshaking);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        findViewById(R.id.containerPassenger).setVisibility(View.VISIBLE);
        findViewById(R.id.tabsPassenger).setVisibility(View.VISIBLE);
        findViewById(R.id.progressBarConfirmSeat).setVisibility(View.GONE);

        mDialog.show();

    }

    private void backToPayment() {
        Map<String, String> params = new HashMap<>();
        params.put("reservation_id", Integer.toString(reservation.getReservationID()));
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "reservation", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    Gson gson = new Gson();
                    ArrayList<Bank> banks = new ArrayList<>();
                    JSONArray bank = response.getJSONArray("bank");
                    for (int i = 0; i < bank.length(); i++) {
                        Bank curr = gson.fromJson(bank.get(i).toString(), Bank.class);
                        banks.add(curr);
                    }
                    if (reservation.getBank() == null) {
                        Intent intent = new Intent(TrainSelectSeatActivity.this, TrainPaymentActivity.class);
                        intent.putExtra("reservation_id", reservation.getReservationID());
                        intent.putExtra("bank", gson.toJson(banks));
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(TrainSelectSeatActivity.this, TicketBookingActivity.class);
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogErrorConnection();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
                dialogErrorConnection();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }
}
