package com.tiketextra.tiketextra.activity.user;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.ContactActivity;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Fare;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ActivationActivity extends BaseActivity {

    private HashMap<String, String> user;
    private SessionManager session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarUserAccount);
        setSupportActionBar(toolbar);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.activation);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : {startActivity(new Intent(getApplicationContext(), ContactActivity.class));break;}
                    case R.id.menu_activation : {return true;}
                    case R.id.menu_profile : {if(isLoggedIn()){startActivity(new Intent(getApplicationContext(), ProfileActivity.class));}else {startActivity(new Intent(getApplicationContext(), LoginActivity.class));}break;}
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_activation);

        session = new SessionManager(this);
        user = session.getUserDetails();

        ((TextView) findViewById(R.id.textViewActivationInstruction)).setText(Html.fromHtml(getString(R.string.activation_instruction, user.get(session.KEY_EMAIL))));

        final EditText editText1 = (EditText) findViewById(R.id.editText1);
        final EditText editText2 = (EditText) findViewById(R.id.editText2);
        final EditText editText3 = (EditText) findViewById(R.id.editText3);
        final EditText editText4 = (EditText) findViewById(R.id.editText4);
        final EditText editText5 = (EditText) findViewById(R.id.editText5);
        final EditText editText6 = (EditText) findViewById(R.id.editText6);

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==1){
                    editText2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==1){
                    editText3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==1){
                    editText4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==1){
                    editText5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editText5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==1){
                    editText6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editText6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==1){
                    findViewById(R.id.submitButton).requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        findViewById(R.id.submitButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String activationCode = editText1.getText().toString()+editText2.getText().toString()+editText3.getText().toString()+editText4.getText().toString()+editText5.getText().toString()+editText6.getText().toString();
                if(activationCode.equals("")){
                    dialogErrorActivationCode();
                }
                else {
                    Map<String, String> params = new HashMap<>();
                    params.put("title_id", getSessionTitle());
                    params.put("name", getSessionName());
                    params.put("mobile", getSessionMobile());
                    params.put("email", getSessionEmail());
                    params.put("activation_code", activationCode);

                    final ReservationModel reservationModel = new ReservationModel(ActivationActivity.this);
                    ArrayList<Integer> reservations = reservationModel.getNonAuthReservationID();
                    String reservationID = "";
                    if (reservations.size() > 0) {
                        for (int i = 0; i < reservations.size(); i++) {
                            reservationID += "," + reservations.get(i);
                        }
                        reservationID = reservationID.substring(1);
                    }
                    params.put("resID", reservationID);

                    findViewById(R.id.register_form).setVisibility(View.GONE);
                    findViewById(R.id.register_progress).setVisibility(View.VISIBLE);

                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "check_activation_code", params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //Log.e("response", response.toString());

                            try {
                                if (response.getInt("status") == 0) {
                                    dialogErrorActivationCode();
                                } else if (response.getInt("status") == 1) {
                                    SessionManager sessionManager = new SessionManager(ActivationActivity.this);
                                    sessionManager.createLoginSession(getSessionTitle(), getSessionName(), getSessionMobile(), getSessionEmail());
                                    Gson gson = new Gson();
                                    Reservation reservation;
                                    JSONArray arrReservation = response.getJSONArray("reservation");
                                    for (int i = 0; i < arrReservation.length(); i++) {
                                        reservation = gson.fromJson(arrReservation.get(i).toString(), Reservation.class);
                                        if (reservationModel.getReservationExists(reservation.getReservationID())) {
                                            reservationModel.updateReservationObject(reservation);
                                        } else {
                                            reservationModel.insertReservationObject(reservation);
                                        }

                                        if(reservation.getBankID() != 0){
                                            reservationModel.setReservationBankID(reservation.getReservationID(), reservation.getBankID());
                                        }

                                    }
                                    JSONArray passengers = response.getJSONArray("passenger");
                                    for (int i = 0; i < passengers.length(); i++) {
                                        Passenger passenger = gson.fromJson(passengers.get(i).toString(), Passenger.class);
                                        reservationModel.deletePassenger(passenger.getReservationID());
                                        reservationModel.insertPassengerObject(passenger);
                                    }
                                    JSONArray fares = response.getJSONArray("fare");
                                    for (int i = 0; i < fares.length(); i++) {
                                        Fare fare = gson.fromJson(fares.get(i).toString(), Fare.class);
                                        reservationModel.deleteFare(fare.getReservationID());
                                        reservationModel.insertFareObject(fare);
                                    }
                                    reservationModel.setReservationToAuthUser();
                                    startActivity(new Intent(ActivationActivity.this, ProfileActivity.class));

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialogErrorConnection();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);
                }

            }
        });

        findViewById(R.id.exitButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog mDialog = new Dialog(ActivationActivity.this, R.style.CustomDialogTheme);

                mDialog.setContentView(R.layout.dialog_yes_no_question);
                mDialog.setCancelable(true);
                TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                mDialogHeader.setText(getString(R.string.exit) +" ?");
                mDialogText.setText(getString(R.string.activation_exit_hint));

                ((TextView) mDialog.findViewById(R.id.dialog_info_ok)).setText(R.string.exit_now);
                ((TextView) mDialog.findViewById(R.id.dialog_info_no)).setText(R.string.back);

                mDialog.show();

                mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        finish();
//                        session.logoutUser();
                        mDialog.dismiss();
                        startActivity(new Intent(ActivationActivity.this, MainActivity.class));
                    }
                });

                mDialog.findViewById(R.id.dialog_info_no).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });


            }
        });

    }




    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogErrorActivationCode(){
        dialog(R.string.error_occurred, R.string.error_activation_code);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

        findViewById(R.id.register_form).setVisibility(View.VISIBLE);
        findViewById(R.id.register_progress).setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ActivationActivity.this, MainActivity.class));
    }
}
