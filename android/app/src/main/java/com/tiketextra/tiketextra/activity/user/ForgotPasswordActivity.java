package com.tiketextra.tiketextra.activity.user;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.ContactActivity;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.model.SettingModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends BaseActivity {

    private boolean valid;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarForgotPassword);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.forgot_my_password);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : {startActivity(new Intent(getApplicationContext(), ContactActivity.class));break;}
                    case R.id.menu_activation : {startActivity(new Intent(getApplicationContext(), ActivationActivity.class));break;}
                    case R.id.menu_profile : return true;
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_profile);

        findViewById(R.id.resetButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valid = true;
                final EditText emailPhoneEditText = findViewById(R.id.emailPhoneEditText);
                if(emailPhoneEditText.getText().toString().isEmpty()){
                    emailPhoneEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                else if (!emailPhoneEditText.getText().toString().matches("^[0]{1}[1-9]{1}[0-9]{7,15}$") && !emailPhoneEditText.getText().toString().matches("^([a-z0-9\\+_\\-]+)(\\.[a-z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$")) {

                    emailPhoneEditText.setError(getResources().getString(R.string.passenger_email_phone_validation));
                    valid = valid && false;
                }

                if(valid){
                    SettingModel settingModel = new SettingModel(ForgotPasswordActivity.this);
                    Map<String, String> params = new HashMap<>();
                    params.put("mobile", emailPhoneEditText.getText().toString());
                    params.put("email", emailPhoneEditText.getText().toString());
                    params.put("device_id", settingModel.getValue("device_id"));
                    params.put("lang", getCurLang());

                    findViewById(R.id.reset_password_form).setVisibility(View.GONE);
                    findViewById(R.id.reset_password_progress).setVisibility(View.VISIBLE);


                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "reset_password", params, new Response.Listener<JSONObject>() {


                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getInt("status") == 0) {
                                    emailPhoneEditText.setError(getResources().getString(R.string.account_not_found));
                                    findViewById(R.id.reset_password_form).setVisibility(View.VISIBLE);
                                    findViewById(R.id.reset_password_progress).setVisibility(View.GONE);
                                }
                                else if(response.getInt("status") == -1) {
                                    dialogQuotaExceed();
                                }
                                else if(response.getInt("status") == 1) {
                                    Intent intent = new Intent(ForgotPasswordActivity.this, RequestResetPasswordActivity.class);
                                    intent.putExtra("mobile", response.getString("mobile"));
                                    intent.putExtra("email", response.getString("email"));
                                    startActivity(intent);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialogErrorConnection();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);

                }
            }
        });

        findViewById(R.id.textViewLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
            }
        });
    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogQuotaExceed(){
        dialog(R.string.error_occurred, R.string.reset_password_quota_exceed);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

        findViewById(R.id.reset_password_form).setVisibility(View.VISIBLE);
        findViewById(R.id.reset_password_progress).setVisibility(View.GONE);

    }
}
