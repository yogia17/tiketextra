package com.tiketextra.tiketextra.activity.user;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.ContactActivity;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity.MenuOnlineActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Fare;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarLogin);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.login);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : {startActivity(new Intent(getApplicationContext(), ContactActivity.class));break;}
                    case R.id.menu_activation : {startActivity(new Intent(getApplicationContext(), ActivationActivity.class));break;}
                    case R.id.menu_profile : return true;
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_profile);

        final EditText email = (EditText) findViewById(R.id.emailEditText);
        final EditText password = (EditText) findViewById(R.id.passwordEditText);

        findViewById(R.id.loginButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean valid = true;

                if (email.getText().toString().isEmpty()) {
                    email.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                } else if (!email.getText().toString().matches("^([a-z0-9\\+_\\-]+)(\\.[a-z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$")) {
                    email.setError(getResources().getString(R.string.passenger_email_validation));
                    valid = valid && false;
                }

                if (password.getText().toString().isEmpty()) {
                    password.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }

                if (valid) {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", email.getText().toString());
                    params.put("password", password.getText().toString());

                    final ReservationModel reservationModel = new ReservationModel(LoginActivity.this);
                    ArrayList<Integer> reservations = reservationModel.getNonAuthReservationID();
                    String reservationID = "";
                    if(reservations.size() > 0) {
                        for (int i = 0; i < reservations.size(); i++) {
                            reservationID += "," + reservations.get(i);
                        }
                        reservationID = reservationID.substring(1);
                    }
                    params.put("resID", reservationID);

                    findViewById(R.id.login_form).setVisibility(View.GONE);
                    findViewById(R.id.login_progress).setVisibility(View.VISIBLE);

                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "user_login", params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                if (response.getInt("status") == 1) {
                                    SessionManager sessionManager = new SessionManager(LoginActivity.this);
                                    sessionManager.createLoginSession(response.getString("title"), response.getString("name"), response.getString("mobile"), email.getText().toString());
                                    Gson gson = new Gson();
                                    Reservation reservation;
                                    JSONArray arrReservation = response.getJSONArray("reservation");
                                    for (int i = 0; i < arrReservation.length(); i++) {
                                        reservation = gson.fromJson(arrReservation.get(i).toString(), Reservation.class);
                                        if(reservationModel.getReservationExists(reservation.getReservationID())){
                                            reservationModel.updateReservationObject(reservation);
                                        }
                                        else{
                                            reservationModel.insertReservationObject(reservation);
                                        }

                                        if(reservation.getBankID() != 0){
                                            reservationModel.setReservationBankID(reservation.getReservationID(), reservation.getBankID());
                                        }
                                    }
                                    JSONArray passengers = response.getJSONArray("passenger");
                                    for(int i=0; i<passengers.length(); i++){
                                        Passenger passenger = gson.fromJson(passengers.get(i).toString(), Passenger.class);
                                        reservationModel.deletePassenger(passenger.getReservationID());
                                        reservationModel.insertPassengerObject(passenger);
                                    }
                                    JSONArray fares = response.getJSONArray("fare");
                                    for(int i=0; i<fares.length(); i++){
                                        Fare fare = gson.fromJson(fares.get(i).toString(), Fare.class);
                                        reservationModel.deleteFare(fare.getReservationID());
                                        reservationModel.insertFareObject(fare);
                                    }
                                    reservationModel.setReservationToAuthUser();

//                                    UserModel userModel = new UserModel(LoginActivity.this);
//
//                                    JSONArray passengerArr = response.getJSONArray("user_passenger");
//                                    for(int i=0; i<passengerArr.length(); i++){
//                                        JSONObject passenger = (JSONObject) passengerArr.get(i);
//                                        userModel.insertPassenger(passenger.getInt("passenger_id"), passenger.getString("title_id"), passenger.getString("name"), passenger.getString("date_of_birth"), passenger.getString("nationality_id"), passenger.getString("passport_number"), passenger.getString("passport_issuing_country"), passenger.getString("passport_expiry_date"));
//                                    }

                                    startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
                                } else if (response.getInt("status") == 0) {
                                    dialogLoginIncorrect();
                                } else if (response.getInt("status") == -1) {
                                    SessionManager sessionManager = new SessionManager(LoginActivity.this);
                                    sessionManager.createActivationSession(response.getString("title"), response.getString("name"), response.getString("mobile"), email.getText().toString());

                                    startActivity(new Intent(LoginActivity.this, ActivationActivity.class));
//                                    dialogAccountInactive();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialogErrorConnection();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);

                }
                else{
                    //Log.e("not valid", "not valid");
                }

            }
        });

        findViewById(R.id.textViewNeedRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        findViewById(R.id.textViewRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        findViewById(R.id.textViewForgotPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        //startActivity(new Intent(this, MenuOnlineActivity.class));
    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogLoginIncorrect(){
        dialog(R.string.error_occurred, R.string.login_incorrect);
    }

    private void dialogAccountInactive(){
        dialog(R.string.error_occurred, R.string.login_user_inactive);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        findViewById(R.id.login_form).setVisibility(View.VISIBLE);
        findViewById(R.id.login_progress).setVisibility(View.GONE);

        mDialog.show();

    }
}
