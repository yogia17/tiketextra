package com.tiketextra.tiketextra.activity.user;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.ContactActivity;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity.MenuOnlineActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends BaseActivity {

    private HashMap<String, String> user;
    private SessionManager session;
    private String title;
    private boolean valid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarUserAccount);
        setSupportActionBar(toolbar);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.my_account);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : {startActivity(new Intent(getApplicationContext(), ContactActivity.class));break;}
                    case R.id.menu_activation : {startActivity(new Intent(getApplicationContext(), ActivationActivity.class));break;}
                    case R.id.menu_profile : return true;
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_profile);

        session = new SessionManager(this);
        user = session.getUserDetails();

        final EditText nameEditText = (EditText)findViewById(R.id.nameEditText);
        final EditText mobileEditText = (EditText) findViewById(R.id.phoneEditText);
        final EditText emailEditText = (EditText) findViewById(R.id.emailEditText);
        title = user.get(session.KEY_TITLE);

        ArrayAdapter<String> spinnerTitleAdapter = new ArrayAdapter<>(this, R.layout.my_spinner_item, ItineraryHelper.getAdultTitle(getApplicationContext()));
        final Spinner spinnerTitle = findViewById(R.id.spinnerTitle);
        spinnerTitle.setAdapter(spinnerTitleAdapter);

        spinnerTitle.setSelection(ItineraryHelper.gatAdultTitleIdx(title, getApplicationContext()));

        emailEditText.setKeyListener(null);

        nameEditText.setText(user.get(session.KEY_NAME));
        mobileEditText.setText(user.get(session.KEY_MOBILE));
        emailEditText.setText(user.get(session.KEY_EMAIL));


        findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valid = true;

                if(spinnerTitle.getSelectedItemPosition()==0){
                    valid = valid && false;
                    ((TextView) spinnerTitle.getSelectedView()).setError(getResources().getString(R.string.passenger_empty_validation));
                }

                if(nameEditText.getText().toString().isEmpty()){
                    nameEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                else if(!nameEditText.getText().toString().matches("[a-z A-Z]+?")) {
                    nameEditText.setError(getResources().getString(R.string.passenger_name_validation));
                    valid = valid && false;
                }


                if(mobileEditText.getText().toString().isEmpty()){
                    mobileEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                else if (!mobileEditText.getText().toString().matches("^[0]{1}[1-9]{1}[0-9]{7,15}$")) {
                    mobileEditText.setError(getResources().getString(R.string.passenger_mobile_validation));
                    valid = valid && false;
                }


                if(emailEditText.getText().toString().isEmpty()){
                    emailEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                else if (!emailEditText.getText().toString().matches("^([a-z0-9\\+_\\-]+)(\\.[a-z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$")) {
                    emailEditText.setError(getResources().getString(R.string.passenger_email_validation));
                    valid = valid && false;
                }

                if (valid) {
                    Map<String, String> params = new HashMap<>();
                    params.put("mobile", user.get(session.KEY_MOBILE));
                    params.put("mobile_target", mobileEditText.getText().toString());
                    params.put("email", user.get(session.KEY_EMAIL));
                    params.put("email_target", emailEditText.getText().toString());


                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "user_check_mobile_email", params, new Response.Listener<JSONObject>() {


                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getInt("mobile") == 0) {
                                    valid = valid && false;
                                    mobileEditText.setError(getResources().getString(R.string.mobile_in_use));
                                }
                                if (response.getInt("email") == 0) {
                                    valid = valid && false;
                                    emailEditText.setError(getResources().getString(R.string.email_in_use));
                                }
                                if(valid){
                                    title = ItineraryHelper.encodeAdultTitle(spinnerTitle.getSelectedItem().toString(), getApplicationContext());
                                    Map<String, String> params = new HashMap<>();
                                    params.put("title_id", title);
                                    params.put("name", Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")));
                                    params.put("mobile", mobileEditText.getText().toString());
                                    params.put("email_target", emailEditText.getText().toString());
                                    params.put("email", user.get(session.KEY_EMAIL));

                                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "user_update", params, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            final Dialog mDialog = new Dialog(ProfileActivity.this, R.style.CustomDialogTheme);

                                            mDialog.setContentView(R.layout.dialog_info);
                                            mDialog.setCancelable(true);
                                            TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                                            TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                                            mDialogHeader.setText(R.string.profile_updated);
                                            mDialogText.setText(title+" "+Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " "))+", "+mobileEditText.getText().toString()+", "+emailEditText.getText().toString());
                                            mDialog.show();
                                            mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    session.updateSession(title, Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobileEditText.getText().toString(), emailEditText.getText().toString());
                                                    mDialog.dismiss();
//                                                    finish();
//                                                    startActivity(getIntent());
                                                }
                                            });



                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError arg0) {
                                            VolleyLog.d("Error: ", arg0.getMessage());
                                            dialogErrorConnection();
                                        }
                                    });
                                    AppController.getInstance().addToRequestQueue(jsonReq);


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialogErrorConnection();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);
                }

            }
        });




    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        //startActivity(new Intent(ProfileActivity.this, MenuOnlineActivity.class));
    }
}
