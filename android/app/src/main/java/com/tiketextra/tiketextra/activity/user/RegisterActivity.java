package com.tiketextra.tiketextra.activity.user;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.ContactActivity;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.model.SettingModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarRegister);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.register);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : {startActivity(new Intent(getApplicationContext(), ContactActivity.class));break;}
                    case R.id.menu_activation : {startActivity(new Intent(getApplicationContext(), ActivationActivity.class));break;}
                    case R.id.menu_profile : return true;
                }
                return false;
            }
        });

        final EditText nameEditText = (EditText)findViewById(R.id.nameEditText);
        final EditText mobileEditText = (EditText) findViewById(R.id.phoneEditText);
        final EditText emailEditText = (EditText) findViewById(R.id.emailEditText);
        final EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        ArrayAdapter<String> spinnerTitleAdapter = new ArrayAdapter<>(this, R.layout.my_spinner_item, ItineraryHelper.getAdultTitle(getApplicationContext()));
        final Spinner spinnerTitle = findViewById(R.id.spinnerTitle);
        spinnerTitle.setAdapter(spinnerTitleAdapter);

        findViewById(R.id.registerButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean valid = true;

                if(spinnerTitle.getSelectedItemPosition()==0){
                    valid = valid && false;
                    ((TextView) spinnerTitle.getSelectedView()).setError(getResources().getString(R.string.passenger_empty_validation));

                }

                if(nameEditText.getText().toString().isEmpty()){
                    nameEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                else if(!nameEditText.getText().toString().matches("[a-z A-Z]+?")) {
                    nameEditText.setError(getResources().getString(R.string.passenger_name_validation));
                    valid = valid && false;
                }


                if(mobileEditText.getText().toString().isEmpty()){
                    mobileEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                else if (!mobileEditText.getText().toString().matches("^[0]{1}[1-9]{1}[0-9]{7,15}$")) {
                    mobileEditText.setError(getResources().getString(R.string.passenger_mobile_validation));
                    valid = valid && false;
                }


                if(emailEditText.getText().toString().isEmpty()){
                    emailEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                else if (!emailEditText.getText().toString().matches("^([a-z0-9\\+_\\-]+)(\\.[a-z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$")) {
                    emailEditText.setError(getResources().getString(R.string.passenger_email_validation));
                    valid = valid && false;
                }

                if(passwordEditText.getText().toString().isEmpty()){
                    passwordEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }

                if (valid) {
//                    //Log.e("valid", "eksekusi");
                    final String title = ItineraryHelper.encodeAdultTitle(spinnerTitle.getSelectedItem().toString(), getApplicationContext());
                    SettingModel settingModel = new SettingModel(RegisterActivity.this);
                    Map<String, String> params = new HashMap<>();
                    params.put("title_id", title);
                    params.put("name", Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")));
                    params.put("mobile", mobileEditText.getText().toString());
                    params.put("email", emailEditText.getText().toString());
                    params.put("password", passwordEditText.getText().toString());
                    params.put("device_id", settingModel.getValue("device_id"));
                    params.put("lang", getCurLang());

                    findViewById(R.id.register_form).setVisibility(View.GONE);
                    findViewById(R.id.register_progress).setVisibility(View.VISIBLE);

                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "user_register", params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //Log.e("response", response.toString());

                            try {
                                if (response.getInt("status") == 0) {
                                    if (response.getInt("mobile") == 0) {
                                        mobileEditText.setError(getResources().getString(R.string.mobile_in_use));
                                    }
                                    if (response.getInt("email") == 0) {
                                        emailEditText.setError(getResources().getString(R.string.email_in_use));
                                    }
                                    findViewById(R.id.register_form).setVisibility(View.VISIBLE);
                                    findViewById(R.id.register_progress).setVisibility(View.GONE);
                                }
                                else if(response.getInt("status") == -1) {
                                    dialogQuotaRegisterExceed();
                                }
//                                else if(response.getInt("status") == -1) {
//                                    dialogSendSMSError();
//                                }
                                else if(response.getInt("status") == 1) {
                                    SessionManager sessionManager = new SessionManager(RegisterActivity.this);
                                    sessionManager.createActivationSession(title, Utils.upperCaseAllFirst(nameEditText.getText().toString().replaceAll("\\s+", " ")), mobileEditText.getText().toString(), emailEditText.getText().toString());
                                    startActivity(new Intent(RegisterActivity.this, ActivationActivity.class));

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialogErrorConnection();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);
                }
                else{
                    //Log.e("not valid", "not valid");
                }
            }
        });


        findViewById(R.id.textViewHaveAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        findViewById(R.id.textViewLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

    }

    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialogQuotaRegisterExceed(){
        dialog(R.string.error_occurred, R.string.registration_quota_exceed);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

        findViewById(R.id.register_form).setVisibility(View.VISIBLE);
        findViewById(R.id.register_progress).setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
    }
}
