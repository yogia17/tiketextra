package com.tiketextra.tiketextra.activity.user;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.ContactActivity;
import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity_base.BaseActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordFormActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarResetPasswordForm);
        setSupportActionBar(toolbar);
        ((TextView) findViewById(R.id.textViewToolbarTitle)).setText(R.string.create_new_password);

        if(inActivation()){
            findViewById(R.id.bottomNavigationBar).setVisibility(View.GONE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.bottomNavigationBar).setVisibility(View.VISIBLE);
            findViewById(R.id.activationBottomNavigationBar).setVisibility(View.GONE);
        }

        BottomNavigationViewEx navigation = (BottomNavigationViewEx) (inActivation() ? findViewById(R.id.activationBottomNavigationBar) :  findViewById(R.id.bottomNavigationBar));
        navigation.enableShiftingMode(false);
        navigation.setTextSize(8);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home : {startActivity(new Intent(getApplicationContext(), MainActivity.class));break;}
                    case R.id.menu_contact : {startActivity(new Intent(getApplicationContext(), ContactActivity.class));break;}
                    case R.id.menu_activation : {startActivity(new Intent(getApplicationContext(), ActivationActivity.class));break;}
                    case R.id.menu_profile : return true;
                }
                return false;
            }
        });
        navigation.setSelectedItemId(R.id.menu_profile);

        final EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        final EditText confPasswordEditText = (EditText) findViewById(R.id.confPasswordEditText);

        findViewById(R.id.resetButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean valid = true;
                if(passwordEditText.getText().toString().isEmpty()){
                    passwordEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                if(confPasswordEditText.getText().toString().isEmpty()){
                    confPasswordEditText.setError(getResources().getString(R.string.passenger_empty_validation));
                    valid = valid && false;
                }
                if(!passwordEditText.getText().toString().equals(confPasswordEditText.getText().toString())){
                    confPasswordEditText.setError(getResources().getString(R.string.password_does_not_match));
                    valid = valid && false;
                }
                if(valid){
                    Map<String, String> params = new HashMap<>();
                    params.put("password", passwordEditText.getText().toString());
                    params.put("email", getIntent().getStringExtra("email"));

                    findViewById(R.id.reset_form).setVisibility(View.GONE);
                    findViewById(R.id.reset_progress).setVisibility(View.VISIBLE);

                    JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "change_password", params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            findViewById(R.id.reset_form).setVisibility(View.VISIBLE);
                            findViewById(R.id.reset_progress).setVisibility(View.GONE);


                            final Dialog mDialog = new Dialog(ResetPasswordFormActivity.this, R.style.CustomDialogTheme);

                            mDialog.setContentView(R.layout.dialog_info);
                            mDialog.setCancelable(true);
                            TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
                            TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

                            mDialogHeader.setText(R.string.reset_password_success);
                            mDialogText.setText(R.string.reset_password_success_hint);

                            mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mDialog.dismiss();
                                    startActivity(new Intent(ResetPasswordFormActivity.this, LoginActivity.class));
                                }
                            });

                            mDialog.show();






                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError arg0) {
                            VolleyLog.d("Error: ", arg0.getMessage());
                            dialogErrorConnection();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonReq);

                }

            }
        });




    }



    private void dialogErrorConnection(){
        dialog(R.string.error_occurred, R.string.error_data_communication);
    }

    private void dialog(int title, int message){
        final Dialog mDialog = new Dialog(this, R.style.CustomDialogTheme);

        mDialog.setContentView(R.layout.dialog_info);
        mDialog.setCancelable(true);
        TextView mDialogHeader = mDialog.findViewById(R.id.dialog_info_title);
        TextView mDialogText = mDialog.findViewById(R.id.dialog_info_text);

        mDialogHeader.setText(title);
        mDialogText.setText(message);

        mDialog.findViewById(R.id.dialog_info_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();

        findViewById(R.id.reset_form).setVisibility(View.VISIBLE);
        findViewById(R.id.reset_progress).setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {

    }
}
