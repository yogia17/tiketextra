package com.tiketextra.tiketextra.activity_base;

import android.os.Bundle;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.model.SettingModel;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by kurnia on 8/19/17.
 */

public abstract class BaseActivity extends LocalizationActivity {

    private SettingModel settingModel;
    private String curLang;
    private SessionManager sessionManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        settingModel = new SettingModel(this);
        curLang = settingModel.getValue("cur_lang");
        super.onCreate(savedInstanceState);
        setLanguage(curLang);
        sessionManager = new SessionManager(this);
    }

    public boolean isLoggedIn(){
        return sessionManager.isLoggedIn();
    }

    public boolean inActivation() {
        return sessionManager.inActivation();
    }

    public String getSessionEmail(){
        HashMap<String, String> user = sessionManager.getUserDetails();
        return user.get(sessionManager.KEY_EMAIL);
    }

    public String getSessionMobile(){
        HashMap<String, String> user = sessionManager.getUserDetails();
        return user.get(sessionManager.KEY_MOBILE);
    }

    public String getSessionName(){
        HashMap<String, String> user = sessionManager.getUserDetails();
        return user.get(sessionManager.KEY_NAME);
    }

    public String getSessionTitle(){
        HashMap<String, String> user = sessionManager.getUserDetails();
        return user.get(sessionManager.KEY_TITLE);
    }








    public String getCurLang() {
        return curLang;
    }

    public Locale getCurLocale(){
        return new Locale(getCurLang());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
