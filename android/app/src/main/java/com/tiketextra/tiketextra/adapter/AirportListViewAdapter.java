package com.tiketextra.tiketextra.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.flight.FlightSearchActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Airport;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by kurnia on 18/04/16.
 */
public class AirportListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Airport> airportList = null;
    private ArrayList<Airport> arraylist;

    public AirportListViewAdapter(Context context, List<Airport> airportList) {
        mContext = context;
        this.airportList = airportList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(airportList);
    }

    private static class ViewHolder{
        TextView iataCode, city, airportName;
    }

    @Override
    public int getCount() {
        return this.airportList.size();
    }

    @Override
    public Airport getItem(int position) {
        return this.airportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private ViewHolder holder;

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_airport, null);

            holder.iataCode = (TextView) view.findViewById(R.id.textAirportIataCode);
            holder.city = (TextView) view.findViewById(R.id.textCityName);
            holder.airportName = (TextView) view.findViewById(R.id.textAirportName);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.iataCode.setText(airportList.get(position).getIataCode());
        holder.city.setText(Configuration.COUNTRY_BASE.equals(airportList.get(position).getCountryCode())?airportList.get(position).getCity():airportList.get(position).getCity()+" - "+airportList.get(position).getCountry());
//        if(!airportList.get(position).getName().equals("")){
            holder.airportName.setText(airportList.get(position).getAirportName());
//        }
//        else{
//            holder.airportName.setVisibility(View.GONE);
//        }

        // Listen for ListView Item Click
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, FlightSearchActivity.class);
                intent.putExtra("iataCode",(airportList.get(position).getIataCode()));
                intent.putExtra("city",(airportList.get(position).getCity()));
                intent.putExtra("country",(airportList.get(position).getCountry()));
                intent.putExtra("countryCode", airportList.get(position).getCountryCode());
                ((Activity)mContext).setResult(Activity.RESULT_OK, intent);
                ((Activity)mContext).finish();
            }
        });

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        airportList.clear();
        if (charText.length() == 0) {
            airportList.addAll(arraylist);
        }
        else
        {
            for (Airport wp : arraylist)
            {
                if ((wp.getIataCode()+" "+wp.getName()+" "+wp.getNameAlt()+" "+wp.getCity()+" "+wp.getCityAlt()+" "+wp.getCountry()).toLowerCase(Locale.getDefault()).contains(charText))
                {
                    airportList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
