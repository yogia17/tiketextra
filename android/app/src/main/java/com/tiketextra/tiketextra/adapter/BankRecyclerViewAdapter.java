package com.tiketextra.tiketextra.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.listener.BankItemListener;
import com.tiketextra.tiketextra.object.Bank;

import java.util.ArrayList;

/**
 * Created by kurnia on 16/10/17.
 */

public class BankRecyclerViewAdapter extends RecyclerView.Adapter {

    private ArrayList<Bank> mValues;
    private Context mContext;
    protected BankItemListener mListener;

    public BankRecyclerViewAdapter(ArrayList values, Context context, BankItemListener itemListener) {
        mValues = values;
        mContext = context;
        mListener=itemListener;
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView textView;
        private ImageView imageView;
        private Bank bank;

        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
            textView = v.findViewById(R.id.textView);
            imageView = v.findViewById(R.id.imageView);

        }

        public void setData(Bank item){
            this.bank = item;
            textView.setText(item.getName());
            ImageUtil.displayImage(imageView, Configuration.TARGET_URL+item.getImage(), null);

//            //Log.e(bank.getName()+" data "+bank.getAccount(), bank.getTimeLimit());
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(bank);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_payment_bank, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder)holder).setData(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
