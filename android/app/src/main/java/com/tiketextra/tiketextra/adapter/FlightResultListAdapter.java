package com.tiketextra.tiketextra.adapter;


import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.listener.OnViewClickListener;
import com.tiketextra.tiketextra.model.AirlineModel;
import com.tiketextra.tiketextra.model.AirportModel;
import com.tiketextra.tiketextra.object.Airline;
import com.tiketextra.tiketextra.object.Airport;
import com.tiketextra.tiketextra.object.FareRoute;
import com.tiketextra.tiketextra.object.Flight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class FlightResultListAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<FareRoute> fareRoute;

	private OnViewClickListener itemListener;
	private String mode;

	public FlightResultListAdapter(LayoutInflater inflater, List<FareRoute> fareRoute, String mode, OnViewClickListener itemListener) {
		this.inflater = inflater;
		this.fareRoute = fareRoute;
		this.itemListener = itemListener;
		this.mode = mode;
	}

	@Override
	public int getCount() {
		return this.fareRoute.size();
	}

	@Override
	public Object getItem(int position) {
		return this.fareRoute.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}





	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		FareRoute fare = this.fareRoute.get(position);

		if(fare.isFilterClass() || fare.isFilterAirline() || fare.isFilterTransitNum() || fare.isFilterPrice()){
			convertView = inflater.inflate(R.layout.item_null, null);
			return convertView;
		}
		else {
//			if (convertView == null)
				convertView = inflater.inflate(R.layout.item_flight_result, null);

			final int pos = position;


			TextView tvDepartTime, tvDepartPort, tvArriveTime, tvArrivePort, tvPriceBefore, tvPrice1, tvPrice2, textViewItinerary, textViewFareClass;
			ImageView imageViewAirline;
			LinearLayout commonInfoLayout, detailInfoLayout, chooseButton;

			commonInfoLayout = convertView.findViewById(R.id.commonInfoLayout);
			commonInfoLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					itemListener.setOnViewClickListener(view, pos);
					itemListener.setOnViewClickListener(view, pos, mode);
				}
			});

			chooseButton = convertView.findViewById(R.id.chooseButton);
			chooseButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					itemListener.setOnViewClickListener(view, pos);
					itemListener.setOnViewClickListener(view, pos, mode);
				}
			});


			detailInfoLayout = convertView.findViewById(R.id.detailInfoLayout);

			imageViewAirline = convertView.findViewById(R.id.imageViewAirline);
			tvDepartTime = convertView.findViewById(R.id.textViewDepartTime);
			tvDepartPort = convertView.findViewById(R.id.textViewDepartPort);
			tvArriveTime = convertView.findViewById(R.id.textViewArriveTime);
			tvArrivePort = convertView.findViewById(R.id.textViewArrivePort);
			tvPriceBefore = convertView.findViewById(R.id.textViewPriceBefore);
			tvPrice1 = convertView.findViewById(R.id.textViewPrice1);
			tvPrice2 = convertView.findViewById(R.id.textViewPrice2);
			textViewItinerary = convertView.findViewById(R.id.textViewItinerary);
			textViewFareClass = convertView.findViewById(R.id.textViewCabinClass);

			if (fare.isSelected()) {
				detailInfoLayout.setVisibility(View.VISIBLE);
			} else {
				detailInfoLayout.setVisibility(View.GONE);
			}

			imageViewAirline.setImageResource(convertView.getResources().getIdentifier(("ic_air_" + (fare.getAirlineCode().equals("AIR") ? "AIR" : fare.getFlightCode())).toLowerCase(), "drawable", convertView.getContext().getPackageName()));
			tvDepartTime.setText(fare.getDepartTime());
			tvDepartPort.setText(fare.getDepartPort());
			int diff;
			tvArriveTime.setText((diff = Utils.dateDiff(fare.getDepartDatetime(), fare.getArriveDatetime())) > 0 ? Html.fromHtml(fare.getArriveTime() + "<sup><small>(+" + diff + ")</small></sup>") : fare.getArriveTime());
			tvArrivePort.setText(fare.getArrivePort());

			textViewItinerary.setText(Utils.strTimeDiff(fare.getFlight().get(0).getDepartDatetime(), fare
					.getFlight().get(fare.getFlight().size() - 1)
					.getArriveDatetime(), fare.getFlight().get(0).getDepartTimezone() - fare.getFlight().get(fare.getFlight().size() - 1).getArriveTimezone()) + "  ( " + ((fare.getFlight().size() + fare.getHiddenTransitCount())==1 ? parent.getResources().getString(R.string.direct) : (fare.getFlight().size() - 1 + fare.getHiddenTransitCount()) + " " + parent.getResources().getString(R.string.transit)) + " )");


			if (fare.getFareClass().equals("pro")) {
				textViewFareClass.setText(parent.getResources().getString(R.string.class_pro));
				textViewFareClass.setTextColor(parent.getResources().getColor(R.color.green_500));
			} else if (fare.getFareClass().equals("eco")) {
				textViewFareClass.setText(parent.getResources().getString(R.string.class_eco));
				textViewFareClass.setTextColor(parent.getResources().getColor(R.color.blue_500));
			} else if (fare.getFareClass().equals("bus")) {
				textViewFareClass.setText(parent.getResources().getString(R.string.class_bus));
				textViewFareClass.setTextColor(parent.getResources().getColor(R.color.orange_500));
			}

			tvPrice1.setText(Utils.numberFormat((int) Math.floor(fare.getFare().get("adult").getTotal() / 1000)));
			tvPrice2.setText(Integer.toString(fare.getFare().get("adult").getTotal()).substring(Integer.toString(fare.getFare().get("adult").getTotal()).length() - 3));

			if(fare.getFare().get("adult").isRuleFromProfit()){
				tvPriceBefore.setVisibility(View.GONE);
				tvPriceBefore.setText(R.string.price_before_discount);
			}
			else {
				if (fare.getFare().get("adult").getTotal() - fare.getFare().get("adult").getDiscount() > fare.getFare().get("adult").getTotal()) {
					tvPriceBefore.setText(Utils.numberFormatCurrency(fare.getFare().get("adult").getTotal() - fare.getFare().get("adult").getDiscount()));
					tvPriceBefore.setPaintFlags(tvPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
					tvPriceBefore.setVisibility(View.VISIBLE);
				} else {
					tvPriceBefore.setVisibility(View.GONE);
				}
			}

			AirportModel airportModel = new AirportModel(convertView.getContext());

			for (int i = 0; i < fare.getFlight().size() || i < 5; i++) {
				if (i == 0) {
					Flight fl = fare.getFlight().get(i);
					Airport airport = airportModel.getAirport(fl.getDepartPort());
					TextView tvFlightDepartPort = convertView.findViewById(R.id.departPortDetailTextView);
					tvFlightDepartPort.setText(airport.getAirportLite());
					TextView tvFlightAirlineNDuration = convertView.findViewById(R.id.airlineCodeNDurationTextView);
					tvFlightAirlineNDuration.setText(fl.getFlightNumber() + " " + fl.getDurationTime());
				} else if (i > 0 && i <= 5 && i < fare.getFlight().size()) {
					Flight fl = fare.getFlight().get(i);
					Airport airport = airportModel.getAirport(fl.getDepartPort());
					TextView tvTransit = convertView.findViewById(convertView.getResources().getIdentifier("transitPoint" + i + "TextView", "id", convertView.getContext().getPackageName()));
					tvTransit.setText(airport.getCity());
					TextView tvTransitDuration = convertView.findViewById(convertView.getResources().getIdentifier("transitDuration" + i + "TextView", "id", convertView.getContext().getPackageName()));
					tvTransitDuration.setText(convertView.getResources().getString(R.string.transit) + " " + Utils.strTimeDiff(fare.getFlight().get(i - 1).getArriveDatetime(), fl.getDepartDatetime(), 0));
					TextView tvFlightAirlineNDuration = convertView.findViewById(convertView.getResources().getIdentifier("airlineCodeNDuration" + i + "TextView", "id", convertView.getContext().getPackageName()));
					tvFlightAirlineNDuration.setText(fl.getFlightNumber() + " " + fl.getDurationTime());

					convertView.findViewById(convertView.getResources().getIdentifier("transitPoint" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.VISIBLE);
					convertView.findViewById(convertView.getResources().getIdentifier("transitRoute" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.VISIBLE);
				} else if (i > 0 && i <= 5 && i >= fare.getFlight().size()) {
					convertView.findViewById(convertView.getResources().getIdentifier("transitPoint" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.GONE);
					convertView.findViewById(convertView.getResources().getIdentifier("transitRoute" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.GONE);
				}
				if (i == fare.getFlight().size() - 1) {
					Flight fl = fare.getFlight().get(i);
					Airport airport = airportModel.getAirport(fl.getArrivePort());
					TextView tvFlightArrivePort = convertView.findViewById(R.id.arrivePortDetailTextView);
					tvFlightArrivePort.setText(airport.getAirportLite());
				}
			}

			AirlineModel airlineModel = new AirlineModel(convertView.getContext());
			boolean meal = true, airportTax = true;
			int baggage = Integer.MAX_VALUE;

			for (Flight fl : fare.getFlight()) {
				Airline airline = airlineModel.getAirline(fl.getFlightCode());
				if (airline.getMeal().equals("N")) {
					meal = false;
				}
				if (airline.getAirportTax().equals("N")) {
					airportTax = false;
				}

				if (airportModel.getAirport(fl.getDepartPort()).getCountryCode().equals(Configuration.COUNTRY_BASE) && airportModel.getAirport(fl.getArrivePort()).getCountryCode().equals(Configuration.COUNTRY_BASE)) {
					baggage = airline.getDomesticBaggage() < baggage ? airline.getDomesticBaggage() : baggage;
				} else {
					baggage = airline.getInternationalBaggage() < baggage ? airline.getInternationalBaggage() : baggage;
				}
			}

			if(fare.getHiddenTransitCount() > 0){
				((TextView) convertView.findViewById(R.id.transitNotMentioned)).setText(convertView.getResources().getString(R.string.transit_not_mentioned, ""+fare.getHiddenTransitCount()));
				convertView.findViewById(R.id.transitNotMentioned).setVisibility(View.VISIBLE);
			}
			else {
                convertView.findViewById(R.id.transitNotMentioned).setVisibility(View.GONE);
            }

			TextView tvBaggage = convertView.findViewById(R.id.baggageTextView);
			TextView tvAirportTax = convertView.findViewById(R.id.taxTextView);
			TextView tvMeal = convertView.findViewById(R.id.mealTextView);

			if (baggage > 0) {
				((ImageView) convertView.findViewById(R.id.baggageImageView)).setImageResource(R.drawable.ic_baggage_on_icon);
				tvBaggage.setText(convertView.getResources().getString(R.string.baggage) + " " + baggage + " kg");
				tvBaggage.setTextColor(convertView.getResources().getColor(R.color.colorPrimaryDark));
			} else {
				((ImageView) convertView.findViewById(R.id.baggageImageView)).setImageResource(R.drawable.ic_baggage_off_icon);
				tvBaggage.setText(R.string.baggage);
				tvBaggage.setTextColor(convertView.getResources().getColor(R.color.grey_400));
				tvBaggage.setPaintFlags(tvBaggage.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			}

			if (airportTax) {
				((ImageView) convertView.findViewById(R.id.taxImageView)).setImageResource(R.drawable.ic_tax_on_icon);
				tvAirportTax.setText(R.string.airport_tax);
				tvAirportTax.setTextColor(convertView.getResources().getColor(R.color.colorPrimaryDark));
			} else {
				((ImageView) convertView.findViewById(R.id.taxImageView)).setImageResource(R.drawable.ic_tax_off_icon);
				tvAirportTax.setText(R.string.airport_tax);
				tvAirportTax.setTextColor(convertView.getResources().getColor(R.color.grey_400));
				tvAirportTax.setPaintFlags(tvAirportTax.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			}

			if (meal) {
				((ImageView) convertView.findViewById(R.id.mealImageView)).setImageResource(R.drawable.ic_meal_on_icon);
				tvMeal.setText(R.string.meal);
				tvMeal.setTextColor(convertView.getResources().getColor(R.color.colorPrimaryDark));
			} else {
				((ImageView) convertView.findViewById(R.id.mealImageView)).setImageResource(R.drawable.ic_meal_off_icon);
				tvMeal.setText(R.string.meal);
				tvMeal.setTextColor(convertView.getResources().getColor(R.color.grey_400));
				tvMeal.setPaintFlags(tvMeal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			}
			return convertView;
		}
	}
	
	private Bitmap combineBitmap(ArrayList<Bitmap> src){
		Bitmap drawnBitmap = null;

	    try {
	        drawnBitmap = Bitmap.createBitmap(180, src.size()*60, Config.ARGB_8888);
	        Canvas canvas = new Canvas(drawnBitmap);
	        for(int b=0; b<src.size(); b++){
				canvas.drawBitmap(src.get(b), 0, b*60, null);
			}
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	    return drawnBitmap;
		
	}
	
	public void updateDataSetChanged() {
		Collections.sort(this.fareRoute);
		super.notifyDataSetChanged();
	}

}
