package com.tiketextra.tiketextra.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.flight.FlightSearchActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppController;
import com.tiketextra.tiketextra.helper.CustomRequest;
import com.tiketextra.tiketextra.object.HotelDestination;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Created by kurnia on 18/04/16.
 */
public class HotelCityListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<HotelDestination> destinationList = null;
    private String billerID;

    public HotelCityListViewAdapter(Context context) {
        mContext = context;
        this.destinationList = new ArrayList<>();
        inflater = LayoutInflater.from(mContext);
    }

    private static class ViewHolder{
        TextView category, value, location, count;
    }

    @Override
    public int getCount() {
        return this.destinationList.size();
    }

    @Override
    public HotelDestination getItem(int position) {
        return this.destinationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private ViewHolder holder;

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_destination, null);
            holder.value = (TextView) view.findViewById(R.id.textHotelValue);
            holder.location = (TextView) view.findViewById(R.id.textHotelLocation);
            holder.category = (TextView) view.findViewById(R.id.textHotelCategory);
            holder.count = (TextView) view.findViewById(R.id.textHotelCount);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.value.setText(destinationList.get(position).getValue());
        holder.location.setText(destinationList.get(position).getLocation());
        holder.category.setText(destinationList.get(position).getCategory());
        if(!destinationList.get(position).getType().equals("business")) {
            if(destinationList.get(position).getCount() > 0) {
                holder.count.setVisibility(View.VISIBLE);
                holder.count.setText(destinationList.get(position).getCount() + " " + mContext.getResources().getString(R.string.hotels));
            }
            else {
                holder.count.setVisibility(View.GONE);
            }
        }
        else {
            holder.count.setVisibility(View.GONE);
        }


        // Listen for ListView Item Click
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, FlightSearchActivity.class);
                intent.putExtra("key", destinationList.get(position).getKey());
                intent.putExtra("value", destinationList.get(position).getValue()+ (destinationList.get(position).getType().equals("business")?"":", "+destinationList.get(position).getLocation()));
                intent.putExtra("type", destinationList.get(position).getType());
                intent.putExtra("location", destinationList.get(position).getLocation());
                intent.putExtra("biller_id", billerID);
                ((Activity)mContext).setResult(Activity.RESULT_OK, intent);
                ((Activity)mContext).finish();
            }
        });

        return view;
    }

    public void addDefaultList(){
        destinationList.clear();
        destinationList.add(new HotelDestination("province:17", "Bali", "province", "Region", "Indonesia", 0));
        destinationList.add(new HotelDestination("city:165", "Bandung", "city", "Kota", "Jawa Barat, Indonesia", 0));
        destinationList.add(new HotelDestination("city:322", "Banjarmasin", "city", "Kota", "Kalimantan Selatan, Indonesia", 0));
        destinationList.add(new HotelDestination("city:347", "Balikpapan", "city", "Kota", "Kalimantan Timur, Indonesia", 0));
        destinationList.add(new HotelDestination("city:168", "Bogor", "city", "Kota", "Jawa Barat, Indonesia", 0));
        destinationList.add(new HotelDestination("province:13", "Jakarta", "province", "Region", "Indonesia", 0));
        destinationList.add(new HotelDestination("city:495", "Lombok", "city", "Kota", "Nusa Tenggara Barat, Indonesia", 0));
        destinationList.add(new HotelDestination("city:248", "Malang", "city", "Kota", "Jawa Timur, Indonesia", 0));
        destinationList.add(new HotelDestination("city:377", "Makassar", "city", "Kota", "Sulawesi Selatan, Indonesia", 0));
        destinationList.add(new HotelDestination("city:47", "Medan", "city", "Kota", "Sumatera Utara, Indonesia", 0));
        destinationList.add(new HotelDestination("city:115", "Palembang", "city", "Kota", "Sumatera Selatan, Indonesia", 0));
        destinationList.add(new HotelDestination("city:212", "Semarang", "city", "Kota", "Jawa Tengah, Indonesia", 0));
        destinationList.add(new HotelDestination("city:40698", "Solo - Surakarta", "city", "Kota", "Jawa Tengah, Indonesia", 0));
        destinationList.add(new HotelDestination("city:252", "Surabaya", "city", "Kota", "Jawa Timur, Indonesia", 0));
        destinationList.add(new HotelDestination("province:16", "Yogyakarta", "province", "Region", "Indonesia", 0));
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        if (charText.length() >= 3) {
            ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("listViewDestination", "id", mContext.getPackageName())).setVisibility(View.GONE);
            ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("layoutDestinationLoading", "id", mContext.getPackageName())).setVisibility(View.VISIBLE);
            ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("layoutDestinationNotFound", "id", mContext.getPackageName())).setVisibility(View.GONE);
            Map<String, String> params = new HashMap<>();
            params.put("keyword", charText);
            JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "hotel/destination", params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String status = response.getString("status");
                        if(status.equals("1")){
                            billerID = response.getString("biller_id");
                            JSONArray data = response.getJSONArray("data");
                            destinationList.clear();
                            for (int i=0; i<data.length(); i++){
                                JSONObject destination = (JSONObject) data.get(i);
                                HotelDestination hd = new HotelDestination(destination.getString("key"), destination.getString("value"), destination.getString("type"), destination.getString("category"), destination.getString("location"), destination.getInt("count"));
                                if(!destinationList.contains(hd)) {
                                    destinationList.add(hd);
                                }
                            }
                            ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("listViewDestination", "id", mContext.getPackageName())).setVisibility(View.VISIBLE);
                            if(destinationList.size()==0){
                                addDefaultList();

                                ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("layoutDestinationLoading", "id", mContext.getPackageName())).setVisibility(View.GONE);
                                ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("layoutDestinationNotFound", "id", mContext.getPackageName())).setVisibility(View.VISIBLE);
                            }
                            else {
                                ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("layoutDestinationLoading", "id", mContext.getPackageName())).setVisibility(View.GONE);
                                ((Activity) mContext).getWindow().getDecorView().findViewById(mContext.getResources().getIdentifier("layoutDestinationNotFound", "id", mContext.getPackageName())).setVisibility(View.GONE);
                            }
                            notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                    Toast.makeText(context, R.string.error_data_communication, Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError arg0) {
                    VolleyLog.d("Error: ", arg0.getMessage());
                    Toast.makeText(mContext, R.string.error_data_communication, Toast.LENGTH_LONG).show();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }
}
