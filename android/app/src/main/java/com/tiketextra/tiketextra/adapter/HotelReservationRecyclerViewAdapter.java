package com.tiketextra.tiketextra.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.listener.ReservationItemListener;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.util.Utils;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by kurnia on 26/12/17.
 */

public class HotelReservationRecyclerViewAdapter extends Adapter {

    private ArrayList<Reservation> reservations;
    private boolean isBooking;
    private Context mContext;

    protected ReservationItemListener mListener;

    public HotelReservationRecyclerViewAdapter(ArrayList<Reservation> reservations, boolean isBooking, Context mContext, ReservationItemListener mListener) {
        this.reservations = reservations;
        this.mContext = mContext;
        this.mListener = mListener;
        this.isBooking = isBooking;
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View convertView;
        private Reservation reservation;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            convertView = itemView;
        }

        public void setData(Reservation res){
            reservation = res;

            if(isBooking) {
                if (reservation.getBank() == null) {
                    ((TextView) convertView.findViewById(R.id.textViewTimeLimit)).setText(convertView.getContext().getString(R.string.time_limit_parameter, Utils.mediumDateTime(reservation.getTimeLimit(), convertView.getContext())));

                    convertView.findViewById(R.id.hotelHeader).setBackgroundColor(convertView.getResources().getColor(R.color.yellow_100));
                } else {
                    if (reservation.getIsConfirmed().equals("Y")) {
                        ((TextView) convertView.findViewById(R.id.textViewBill)).setText(R.string.already_paid);
                        ((TextView) convertView.findViewById(R.id.textViewTimeLimit)).setText(R.string.ticket_processing);
                        convertView.findViewById(R.id.hotelHeader).setBackgroundColor(convertView.getResources().getColor(R.color.green_100));
                    } else {
                        ((TextView) convertView.findViewById(R.id.textViewTimeLimit)).setText(convertView.getContext().getString(R.string.time_limit_parameter, Utils.mediumDateTime(reservation.getTimeLimit(), convertView.getContext())));
                        convertView.findViewById(R.id.hotelHeader).setBackgroundColor(convertView.getResources().getColor(R.color.yellow_100));
                    }
                }
            }
            else{
                convertView.findViewById(R.id.hotelHeader).setBackgroundColor(convertView.getResources().getColor(R.color.blue_50));
                convertView.findViewById(R.id.layoutPaymentInfo).setVisibility(View.GONE);
            }


            try {
                ((TextView) convertView.findViewById(R.id.textViewHotelName)).setText(reservation.getHotel().getName());
                ((TextView) convertView.findViewById(R.id.textViewRoomName)).setText(reservation.getRoom().getName());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ((TextView) convertView.findViewById(R.id.textViewNumberRooms)).setText(reservation.getNumberRooms() + " " + convertView.getContext().getString(reservation.getNumberRooms() > 1  ? R.string.rooms : R.string.room));
            ((TextView) convertView.findViewById(R.id.textViewBookingCode)).setText(reservation.getBookingCode1());

            ((TextView) convertView.findViewById(R.id.textViewCheckinDate)).setText(Utils.mediumMD(reservation.getCheckinDate(), convertView.getContext()));
            ((TextView) convertView.findViewById(R.id.textViewCheckoutDate)).setText(Utils.mediumMD(reservation.getCheckoutDate(), convertView.getContext()));

            if(reservation.getAmount() > 0){
                ((TextView) convertView.findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor((reservation.getAmount()) / 1000)));
                ((TextView) convertView.findViewById(R.id.textViewPrice2)).setText(Integer.toString(reservation.getAmount()).substring(Integer.toString(reservation.getAmount()).length() - 3));
            }
            else {
                ((TextView) convertView.findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor((reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount()) / 1000)));
                ((TextView) convertView.findViewById(R.id.textViewPrice2)).setText(Integer.toString(reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount()).substring(Integer.toString(reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount()).length() - 3));
            }
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(reservation);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_reservation_hotel, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder)holder).setData(reservations.get(position));
    }

    @Override
    public int getItemCount() {
        return reservations.size();
    }
}
