package com.tiketextra.tiketextra.adapter;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.listener.OnViewClickListener;
import com.tiketextra.tiketextra.object.Hotel;
import com.tiketextra.tiketextra.util.Utils;

import java.util.Collections;
import java.util.List;


public class HotelResultListAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<Hotel> hotel;

	private OnViewClickListener itemListener;

	public HotelResultListAdapter(LayoutInflater inflater, List<Hotel> hotel, OnViewClickListener itemListener) {
		this.inflater = inflater;
		this.hotel = hotel;
		this.itemListener = itemListener;
	}

	@Override
	public int getCount() {
		return this.hotel.size();
	}

	@Override
	public Object getItem(int position) {
		return this.hotel.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}





	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		Hotel fare = this.hotel.get(position);

//		Log.e(position+" position", fare.getPrice() + " "+AppHelper.hotelPriceMax);

//		if (!AppHelper.hotelFilterRating.contains(fare.getRating())) {
//			AppHelper.hotelFilterRating.add(fare.getRating());
//			AppHelper.hotelSelectedRating.add(fare.getRating());
//		}


		if(fare.isFilterRating() || fare.isFilterPrice() || fare.isFilterName()){
			convertView = inflater.inflate(R.layout.item_null, null);
			return convertView;
		}
		else {
			convertView = inflater.inflate(R.layout.item_hotel_result, null);



			ImageView imageView = convertView.findViewById(R.id.imageHotel);
			if(fare.getImage().toLowerCase().contains("default")){
				imageView.setBackground(convertView.getResources().getDrawable(R.drawable.ic_menu_hotel));
			}
			else {
				ImageUtil.displayImage(imageView, fare.getImage(), null);
			}
			AppCompatRatingBar ratingBar = convertView.findViewById(R.id.ratingHotel);
			if(fare.getRating() > 0) {
				ratingBar.setRating(fare.getRating());
				LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
				stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
				ratingBar.setVisibility(View.VISIBLE);
			}
			else {
				ratingBar.setVisibility(View.GONE);
			}
			TextView tvName = convertView.findViewById(R.id.textViewHotelName);
			tvName.setText(fare.getName());

			TextView tvAddress = convertView.findViewById(R.id.textViewAddress);
			tvAddress.setText(fare.getAddress());

			TextView tvPriceBefore = convertView.findViewById(R.id.textViewPriceBefore);
			if(fare.getPrice() < fare.getPriceBefore()){
				tvPriceBefore.setText(Utils.numberFormatCurrency(fare.getPriceBefore()));
				tvPriceBefore.setPaintFlags(tvPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
				tvPriceBefore.setVisibility(View.VISIBLE);
			}
			else {
				tvPriceBefore.setVisibility(View.GONE);
			}

			TextView tvPrice = convertView.findViewById(R.id.textViewPrice);
			tvPrice.setText(Utils.numberFormatCurrency(fare.getPrice()));

			RelativeLayout layout = convertView.findViewById(R.id.layoutParent);
			layout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					itemListener.setOnViewClickListener(view, position);

				}
			});

			return convertView;
		}
	}
	
	public void updateDataSetChanged() {
		Collections.sort(this.hotel);
		super.notifyDataSetChanged();
	}

}
