package com.tiketextra.tiketextra.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.object.Promo;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by kurnia on 07/11/17.
 */

public class PromoListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<Promo> promos;
    private Locale locale;

    public PromoListAdapter(Activity activity, ArrayList<Promo> promos, Locale locale){
        this.activity = activity;
        this.promos = promos;
        this.locale = locale;
    }

    @Override
    public int getCount() {
        return promos.size();
    }

    @Override
    public Object getItem(int i) {
        return promos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {


        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.item_promo, null);

        Promo promo = promos.get(i);

        ImageView image = convertView.findViewById(R.id.imageViewPromo);
        ImageUtil.displayImage(image, Configuration.SLIDESHOW_TARGET_URL+promo.getImage(), null);

        ((TextView) convertView.findViewById(R.id.textViewPromoTitle)).setText(promo.getTitle(locale));
        ((TextView) convertView.findViewById(R.id.textViewPromoSummary)).setText(promo.getSummary(locale));
        ((TextView) convertView.findViewById(R.id.btnViewPromo)).setText(promo.getName());

        return convertView;
    }
}
