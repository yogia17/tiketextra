package com.tiketextra.tiketextra.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.listener.RoomItemListener;
import com.tiketextra.tiketextra.object.Room;
import com.tiketextra.tiketextra.util.Utils;

import java.util.ArrayList;

/**
 * Created by kurnia on 16/10/17.
 */

public class RoomRecyclerViewAdapter extends RecyclerView.Adapter {

    private ArrayList<Room> mValues;
    private Context mContext;
    protected RoomItemListener mListener;

    public RoomRecyclerViewAdapter(ArrayList values, Context context, RoomItemListener itemListener) {
        mValues = values;
        mContext = context;
        mListener=itemListener;
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Room room;
        private View convertView;
        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
            convertView = v;

        }

        public void setData(Room item){
            this.room = item;


                TextView tvName = convertView.findViewById(R.id.textViewRoomName);
                tvName.setText(room.getName());

                ImageView imageView = convertView.findViewById(R.id.imageRoom);
                if (room.getImage().toLowerCase().contains("default")) {
                    imageView.setBackground(convertView.getResources().getDrawable(R.drawable.ic_menu_hotel));
                } else {
                    ImageUtil.displayImage(imageView, room.getImage(), null);
                }
                TextView tvOccupancy = convertView.findViewById(R.id.textViewOccupancy);
                tvOccupancy.setText(room.getOccupancy() + " " + convertView.getResources().getString(room.getOccupancy() > 1 ? R.string.guests_per_room : R.string.guest_per_room));
                if (room.isBreakfast()) {
                    convertView.findViewById(R.id.layoutBreakfast).setVisibility(View.VISIBLE);
                    convertView.findViewById(R.id.layoutNotBreakfast).setVisibility(View.GONE);
                } else {
                    convertView.findViewById(R.id.layoutBreakfast).setVisibility(View.GONE);
                    convertView.findViewById(R.id.layoutNotBreakfast).setVisibility(View.VISIBLE);
                }

                TextView tvRoomLeft = convertView.findViewById(R.id.textViewRoomLeft);
                if (room.getAllotment() > 10) {
                    tvRoomLeft.setVisibility(View.GONE);
                } else {
                    tvRoomLeft.setVisibility(View.VISIBLE);
                    tvRoomLeft.setText(room.getAllotment() + " " + convertView.getResources().getString(room.getAllotment() > 1 ? R.string.rooms_left : R.string.room_left));
                }
                TextView tvPrice = convertView.findViewById(R.id.textViewPrice);
                tvPrice.setText(Utils.numberFormatCurrency(room.getPrice()));
            Button button = convertView.findViewById(R.id.buttonChooseRoom);
            if(!room.isBookable()){
                button.setVisibility(View.GONE);
            }
            else {
                button.setVisibility(View.VISIBLE);
            }
            button.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(room);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_room, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder)holder).setData(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
