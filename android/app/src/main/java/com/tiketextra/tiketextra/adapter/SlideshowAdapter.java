package com.tiketextra.tiketextra.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tiketextra.tiketextra.fragment.SlideshowFragment;
import com.tiketextra.tiketextra.object.Slideshow;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by kurnia on 15/08/17.
 */

public class SlideshowAdapter extends FragmentPagerAdapter {

    private ArrayList<Slideshow> slideshows;

    public SlideshowAdapter(FragmentManager fm, ArrayList<Slideshow> slideshows) {
        super(fm);
        this.slideshows = slideshows;
    }

    @Override
    public Fragment getItem(int position) {
        Gson gson = new Gson();
        String json = gson.toJson(slideshows.get(position));
        return SlideshowFragment.newInstance(json);
    }

    @Override
    public int getCount() {
        return slideshows.size();
    }
}
