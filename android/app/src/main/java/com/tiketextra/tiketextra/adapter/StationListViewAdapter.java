package com.tiketextra.tiketextra.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.train.TrainSearchActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Station;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by kurnia on 18/04/16.
 */
public class StationListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Station> stationList = null;
    private ArrayList<Station> arraylist;

    public StationListViewAdapter(Context context, List<Station> stationList) {
        mContext = context;
        this.stationList = stationList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(stationList);
    }

    private static class ViewHolder{
        TextView iataCode, city, stationName;
    }

    @Override
    public int getCount() {
        return this.stationList.size();
    }

    @Override
    public Station getItem(int position) {
        return this.stationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private ViewHolder holder;

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_station, null);

            holder.iataCode = (TextView) view.findViewById(R.id.textStationIataCode);
            holder.city = (TextView) view.findViewById(R.id.textCityName);
            holder.stationName = (TextView) view.findViewById(R.id.textStationName);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.iataCode.setText(stationList.get(position).getIataCode());
        holder.city.setText(Configuration.COUNTRY_BASE.equals(stationList.get(position).getCountryCode())?stationList.get(position).getCity():stationList.get(position).getCity()+" - "+stationList.get(position).getCountry());
//        if(!stationList.get(position).getName().equals("")){
            holder.stationName.setText(stationList.get(position).getStationName());
//        }
//        else{
//            holder.stationName.setVisibility(View.GONE);
//        }

        // Listen for ListView Item Click
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, TrainSearchActivity.class);
                intent.putExtra("iataCode",(stationList.get(position).getIataCode()));
                intent.putExtra("city",(stationList.get(position).getCity()));
                intent.putExtra("country",(stationList.get(position).getCountry()));
                intent.putExtra("countryCode", stationList.get(position).getCountryCode());
                ((Activity)mContext).setResult(Activity.RESULT_OK, intent);
                ((Activity)mContext).finish();
            }
        });

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        stationList.clear();
        if (charText.length() == 0) {
            stationList.addAll(arraylist);
        }
        else
        {
            for (Station wp : arraylist)
            {
                if ((wp.getIataCode()+" "+wp.getName()+" "+wp.getCity()+" "+wp.getCountry()).toLowerCase(Locale.getDefault()).contains(charText))
                {
                    stationList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
