package com.tiketextra.tiketextra.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.listener.ReservationItemListener;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.object.Station;
import com.tiketextra.tiketextra.object.Reservation;
import com.tiketextra.tiketextra.util.Utils;

import java.util.ArrayList;

/**
 * Created by kurnia on 26/12/17.
 */

public class TrainReservationRecyclerViewAdapter extends Adapter {

    private ArrayList<Reservation> reservations;
    private boolean isBooking;
    private Context mContext;

    protected ReservationItemListener mListener;

    public TrainReservationRecyclerViewAdapter(ArrayList<Reservation> reservations, boolean isBooking, Context mContext, ReservationItemListener mListener) {
        this.reservations = reservations;
        this.mContext = mContext;
        this.mListener = mListener;
        this.isBooking = isBooking;
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View convertView;
        private Reservation reservation;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            convertView = itemView;
        }

        public void setData(Reservation res){
            reservation = res;
            StationModel StationModel = new StationModel(convertView.getContext());
            Station fromPort = StationModel.getStation(reservation.getFromPort());
            Station toPort = StationModel.getStation(reservation.getToPort());

            ((ImageView) convertView.findViewById(R.id.imageViewDepartCarrierType)).setImageResource(R.drawable.ic_train_go);
            ((ImageView) convertView.findViewById(R.id.imageViewReturnCarrierType)).setImageResource(R.drawable.ic_train_go);

            if(isBooking) {
                if (reservation.getBank() == null) {
                    ((TextView) convertView.findViewById(R.id.textViewTimeLimit)).setText(convertView.getContext().getString(R.string.time_limit_parameter, Utils.mediumDateTime(reservation.getTimeLimit(), convertView.getContext())));

                    convertView.findViewById(R.id.departHeader).setBackgroundColor(convertView.getResources().getColor(R.color.yellow_100));
                    if (reservation.isReturn()) {
                        convertView.findViewById(R.id.returnHeader).setBackgroundColor(convertView.getResources().getColor(R.color.yellow_100));
                    }
                } else {
                    if (reservation.getIsConfirmed().equals("Y")) {
                        ((TextView) convertView.findViewById(R.id.textViewBill)).setText(R.string.already_paid);
                        ((TextView) convertView.findViewById(R.id.textViewTimeLimit)).setText(R.string.ticket_processing);
                        convertView.findViewById(R.id.departHeader).setBackgroundColor(convertView.getResources().getColor(R.color.green_100));
                        if (reservation.isReturn()) {
                            convertView.findViewById(R.id.returnHeader).setBackgroundColor(convertView.getResources().getColor(R.color.green_100));
                        }
                    } else {
                        ((TextView) convertView.findViewById(R.id.textViewTimeLimit)).setText(convertView.getContext().getString(R.string.time_limit_parameter, Utils.mediumDateTime(reservation.getTimeLimit(), convertView.getContext())));
                        convertView.findViewById(R.id.departHeader).setBackgroundColor(convertView.getResources().getColor(R.color.yellow_100));
                        if (reservation.isReturn()) {
                            convertView.findViewById(R.id.returnHeader).setBackgroundColor(convertView.getResources().getColor(R.color.yellow_100));
                        }
                    }
                }
            }
            else{
                convertView.findViewById(R.id.departHeader).setBackgroundColor(convertView.getResources().getColor(R.color.blue_50));
                convertView.findViewById(R.id.returnHeader).setBackgroundColor(convertView.getResources().getColor(R.color.blue_50));
                convertView.findViewById(R.id.layoutPaymentInfo).setVisibility(View.GONE);
            }

            if (reservation.isReturn()) {
                convertView.findViewById(R.id.lowerLayout).setVisibility(View.VISIBLE);

                ((TextView) convertView.findViewById(R.id.returnTextView)).setText(convertView.getResources().getString(R.string.returns)+" - "+ Utils.mediumDate(reservation.getReturnDate(), convertView.getContext()));
                ((TextView) convertView.findViewById(R.id.returnHintTextView)).setText(toPort.getCity()+" "+convertView.getResources().getString(R.string.to).toLowerCase()+" "+ fromPort.getCity());

                ((ImageView) convertView.findViewById(R.id.imageViewReturnAirline)).setImageResource(R.drawable.ic_kai);
                ((TextView) convertView.findViewById(R.id.textViewReturnBookingCode)).setText(reservation.getCarrierID1().equals(reservation.getCarrierID2()) && (reservation.getBookingCode2()==null || reservation.getBookingCode2().equals("")) ? reservation.getBookingCode1() : reservation.getBookingCode2());

                ((TextView) convertView.findViewById(R.id.textViewReturnDepartTime)).setText(reservation.getReturnTrainArray()[0].getDepartTime());
                ((TextView) convertView.findViewById(R.id.textViewReturnFromPort)).setText(reservation.getToPort());

                ((TextView) convertView.findViewById(R.id.textViewReturnArriveTime)).setText(reservation.getReturnTrainArray()[reservation.getReturnTrainArray().length - 1].getArriveTime());
                ((TextView) convertView.findViewById(R.id.textViewReturnToPort)).setText(reservation.getFromPort());

            } else {
                convertView.findViewById(R.id.lowerLayout).setVisibility(View.GONE);
            }
            ((TextView) convertView.findViewById(R.id.departTextView)).setText(convertView.getResources().getString(R.string.departure)+" - "+ Utils.mediumDate(reservation.getDepartDate(), convertView.getContext()));
            ((TextView) convertView.findViewById(R.id.departHintTextView)).setText(fromPort.getCity()+" "+convertView.getResources().getString(R.string.to).toLowerCase()+" "+ toPort.getCity());

            ((ImageView) convertView.findViewById(R.id.imageViewDepartAirline)).setImageResource(R.drawable.ic_kai);
            ((TextView) convertView.findViewById(R.id.textViewDepartBookingCode)).setText(reservation.getBookingCode1());

            ((TextView) convertView.findViewById(R.id.textViewDepartDepartTime)).setText(reservation.getDepartTrainArray()[0].getDepartTime());
            ((TextView) convertView.findViewById(R.id.textViewDepartFromPort)).setText(reservation.getFromPort());

            ((TextView) convertView.findViewById(R.id.textViewDepartArriveTime)).setText(reservation.getDepartTrainArray()[reservation.getDepartTrainArray().length - 1].getArriveTime());
            ((TextView) convertView.findViewById(R.id.textViewDepartToPort)).setText(reservation.getToPort());

            if(reservation.getAmount() > 0){
                ((TextView) convertView.findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor((reservation.getAmount()) / ( Integer.toString(reservation.getAmount()).length() >= 3 ? 1000 : 1 ))));
                ((TextView) convertView.findViewById(R.id.textViewPrice2)).setText(Integer.toString(reservation.getAmount()).substring(Integer.toString(reservation.getAmount()).length() -  ( Integer.toString(reservation.getAmount()).length() >= 3 ? 3 : 0 )));
            }
            else {
                ((TextView) convertView.findViewById(R.id.textViewPrice1)).setText(Utils.numberFormat((int) Math.floor((reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount()) / ( Integer.toString((reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount())).length() >= 3 ? 1000 : 1 ))));
                ((TextView) convertView.findViewById(R.id.textViewPrice2)).setText(Integer.toString(reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount()).substring(Integer.toString(reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount()).length() -  ( Integer.toString(reservation.getTotalFare() + reservation.getBankDiscount() + reservation.getBankSurcharge() + reservation.getUniqueCode() + reservation.getPromoAmount()).length() >= 3 ? 3 : 0 )));
            }
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(reservation);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_reservation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder)holder).setData(reservations.get(position));
    }

    @Override
    public int getItemCount() {
        return reservations.size();
    }
}
