package com.tiketextra.tiketextra.adapter;


import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.listener.OnViewClickListener;
import com.tiketextra.tiketextra.model.StationModel;
import com.tiketextra.tiketextra.object.Station;
import com.tiketextra.tiketextra.object.FareRouteTrain;
import com.tiketextra.tiketextra.object.Train;
import com.tiketextra.tiketextra.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class TrainResultListAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<FareRouteTrain> fareRoute;

	private OnViewClickListener itemListener;
	private String mode;

	public TrainResultListAdapter(LayoutInflater inflater, List<FareRouteTrain> fareRoute, String mode, OnViewClickListener itemListener) {
		this.inflater = inflater;
		this.fareRoute = fareRoute;
		this.itemListener = itemListener;
		this.mode = mode;
	}

	@Override
	public int getCount() {
		return this.fareRoute.size();
	}

	@Override
	public Object getItem(int position) {
		return this.fareRoute.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}





	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		FareRouteTrain fare = this.fareRoute.get(position);

		if(fare.isFilterClass() || fare.isFilterCarrier() || fare.isFilterTransitNum() || fare.isFilterPrice()){
			convertView = inflater.inflate(R.layout.item_null, null);
			return convertView;
		}
		else {
//			if (convertView == null)
				convertView = inflater.inflate(R.layout.item_train_result, null);

			final int pos = position;


			TextView tvDepartTime, tvDepartPort, tvArriveTime, tvArrivePort, tvPriceBefore, tvPrice1, tvPrice2, textViewItinerary, textViewFareClass, textViewTrainNumber, textViewSeatAvail;
			LinearLayout commonInfoLayout, detailInfoLayout, chooseButton;

			commonInfoLayout = convertView.findViewById(R.id.commonInfoLayout);
			commonInfoLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					itemListener.setOnViewClickListener(view, pos);
					itemListener.setOnViewClickListener(view, pos, mode);
				}
			});

			chooseButton = convertView.findViewById(R.id.chooseButton);
			chooseButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					itemListener.setOnViewClickListener(view, pos);
					itemListener.setOnViewClickListener(view, pos, mode);
				}
			});


			detailInfoLayout = convertView.findViewById(R.id.detailInfoLayout);

			tvDepartTime = convertView.findViewById(R.id.textViewDepartTime);
			tvDepartPort = convertView.findViewById(R.id.textViewDepartPort);
			tvArriveTime = convertView.findViewById(R.id.textViewArriveTime);
			tvArrivePort = convertView.findViewById(R.id.textViewArrivePort);
			tvPriceBefore = convertView.findViewById(R.id.textViewPriceBefore);
			tvPrice1 = convertView.findViewById(R.id.textViewPrice1);
			tvPrice2 = convertView.findViewById(R.id.textViewPrice2);
			textViewItinerary = convertView.findViewById(R.id.textViewItinerary);
			textViewFareClass = convertView.findViewById(R.id.textViewCabinClass);
			textViewTrainNumber = convertView.findViewById(R.id.textViewTrainNumber);
			textViewSeatAvail = convertView.findViewById(R.id.textViewSeatAvail);

			if (fare.isSelected()) {
				detailInfoLayout.setVisibility(View.VISIBLE);
			} else {
				detailInfoLayout.setVisibility(View.GONE);
			}

			textViewTrainNumber.setText(fare.getTrainNameNumber());

			tvDepartTime.setText(fare.getDepartTime());
			tvDepartPort.setText(fare.getDepartPort());
			int diff;
			tvArriveTime.setText((diff = Utils.dateDiff(fare.getDepartDatetime(), fare.getArriveDatetime())) > 0 ? Html.fromHtml(fare.getArriveTime() + "<sup><small>(+" + diff + ")</small></sup>") : fare.getArriveTime());
			tvArrivePort.setText(fare.getArrivePort());

			textViewItinerary.setText(Utils.strTimeDiff(fare.getTrain().get(0).getDepartDatetime(), fare
					.getTrain().get(fare.getTrain().size() - 1)
					.getArriveDatetime(), fare.getTrain().get(0).getDepartTimezone() - fare.getTrain().get(fare.getTrain().size() - 1).getArriveTimezone()));// + "  ( " + (fare.getTrain().size()==1 ? parent.getResources().getString(R.string.direct) : (fare.getTrain().size() - 1) + " " + parent.getResources().getString(R.string.transit)) + " )");


			if (fare.getFareClass().equals("pro")) {
				textViewFareClass.setText(parent.getResources().getString(R.string.class_pro) + " - " + fare.getFareID());
				textViewFareClass.setTextColor(parent.getResources().getColor(R.color.green_500));
			} else if (fare.getFareClass().equals("eco")) {
				textViewFareClass.setText(parent.getResources().getString(R.string.class_eco) + " - " + fare.getFareID());
				textViewFareClass.setTextColor(parent.getResources().getColor(R.color.blue_500));
			} else if (fare.getFareClass().equals("bus")) {
				textViewFareClass.setText(parent.getResources().getString(R.string.class_bus) + " - " + fare.getFareID());
				textViewFareClass.setTextColor(parent.getResources().getColor(R.color.orange_500));
			} else if (fare.getFareClass().equals("exe")) {
				textViewFareClass.setText(parent.getResources().getString(R.string.class_exe) + " - " + fare.getFareID());
				textViewFareClass.setTextColor(parent.getResources().getColor(R.color.red_500));
			}

			if (Integer.toString(fare.getFare().get("adult").getTotal()).length() >= 3){
				tvPrice1.setText(Utils.numberFormat((int) Math.floor(fare.getFare().get("adult").getTotal() / 1000)));
				tvPrice2.setText(Integer.toString(fare.getFare().get("adult").getTotal()).substring(Integer.toString(fare.getFare().get("adult").getTotal()).length() - 3));
			}else{
				tvPrice1.setText(Utils.numberFormat((int) Math.floor(fare.getFare().get("adult").getTotal() )));
				tvPrice2.setText(Integer.toString(fare.getFare().get("adult").getTotal()).substring(Integer.toString(fare.getFare().get("adult").getTotal()).length() ));
			}


			textViewSeatAvail.setText(Integer.toString(fare.getSeatAvail()));


			if(fare.getFare().get("adult").isRuleFromProfit()){
				tvPriceBefore.setVisibility(View.GONE);
				tvPriceBefore.setText(R.string.price_before_discount);
			}
			else {
				if (fare.getFare().get("adult").getTotal() - fare.getFare().get("adult").getDiscount() > fare.getFare().get("adult").getTotal()) {
					tvPriceBefore.setText(Utils.numberFormatCurrency(fare.getFare().get("adult").getTotal() - fare.getFare().get("adult").getDiscount()));
					tvPriceBefore.setPaintFlags(tvPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
					tvPriceBefore.setVisibility(View.VISIBLE);
				} else {
					tvPriceBefore.setVisibility(View.GONE);
				}
			}

			StationModel stationModel = new StationModel(convertView.getContext());

			for (int i = 0; i < fare.getTrain().size() || i < 5; i++) {
				if (i == 0) {
					Train fl = fare.getTrain().get(i);
					Station station = stationModel.getStation(fl.getDepartPort());
					TextView tvTrainDepartPort = convertView.findViewById(R.id.departPortDetailTextView);
					tvTrainDepartPort.setText(station.getStation());
					TextView tvTrainAirlineNDuration = convertView.findViewById(R.id.airlineCodeNDurationTextView);
					tvTrainAirlineNDuration.setText(((diff = Utils.dateDiff(fl.getDepartDatetime(), fl.getArriveDatetime())) > 0 ? Html.fromHtml(fl.getDepartTime() + " - " + fl.getArriveTime() + "<sup><small>(+" + diff + ")</small></sup>" + ", " + fl.getDurationTime()) : fl.getDepartTime() + " - " + fl.getArriveTime() + ", " + fl.getDurationTime()));
				} else if (i > 0 && i <= 5 && i < fare.getTrain().size()) {
					Train fl = fare.getTrain().get(i);
					Station station = stationModel.getStation(fl.getDepartPort());
					TextView tvTransit = convertView.findViewById(convertView.getResources().getIdentifier("transitPoint" + i + "TextView", "id", convertView.getContext().getPackageName()));
					tvTransit.setText(station.getCity());
					TextView tvTransitDuration = convertView.findViewById(convertView.getResources().getIdentifier("transitDuration" + i + "TextView", "id", convertView.getContext().getPackageName()));
					tvTransitDuration.setText(convertView.getResources().getString(R.string.transit) + " " + Utils.strTimeDiff(fare.getTrain().get(i - 1).getArriveDatetime(), fl.getDepartDatetime(), 0));
					TextView tvTrainAirlineNDuration = convertView.findViewById(convertView.getResources().getIdentifier("airlineCodeNDuration" + i + "TextView", "id", convertView.getContext().getPackageName()));
					tvTrainAirlineNDuration.setText(((diff = Utils.dateDiff(fl.getDepartDatetime(), fl.getArriveDatetime())) > 0 ? Html.fromHtml(fl.getDepartTime() + " - " + fl.getArriveTime() + "<sup><small>(+" + diff + ")</small></sup>" + ", " + fl.getDurationTime()) : fl.getDepartTime() + " - " + fl.getArriveTime() + ", " + fl.getDurationTime()));

					convertView.findViewById(convertView.getResources().getIdentifier("transitPoint" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.VISIBLE);
					convertView.findViewById(convertView.getResources().getIdentifier("transitRoute" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.VISIBLE);
				} else if (i > 0 && i <= 5 && i >= fare.getTrain().size()) {
					convertView.findViewById(convertView.getResources().getIdentifier("transitPoint" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.GONE);
					convertView.findViewById(convertView.getResources().getIdentifier("transitRoute" + i + "Layout", "id", convertView.getContext().getPackageName())).setVisibility(View.GONE);
				}
				if (i == fare.getTrain().size() - 1) {
					Train fl = fare.getTrain().get(i);
					Station station = stationModel.getStation(fl.getArrivePort());
					TextView tvTrainArrivePort = convertView.findViewById(R.id.arrivePortDetailTextView);
					tvTrainArrivePort.setText(station.getStation());
				}
			}

			
			return convertView;
		}
	}
	
	private Bitmap combineBitmap(ArrayList<Bitmap> src){
		Bitmap drawnBitmap = null;

	    try {
	        drawnBitmap = Bitmap.createBitmap(180, src.size()*60, Config.ARGB_8888);
	        Canvas canvas = new Canvas(drawnBitmap);
	        for(int b=0; b<src.size(); b++){
				canvas.drawBitmap(src.get(b), 0, b*60, null);
			}
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	    return drawnBitmap;
		
	}
	
	public void updateDataSetChanged() {
		Collections.sort(this.fareRoute);
		super.notifyDataSetChanged();
	}

}
