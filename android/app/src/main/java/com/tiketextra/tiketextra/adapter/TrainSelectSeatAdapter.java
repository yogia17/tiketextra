package com.tiketextra.tiketextra.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.fragment.TrainSelectSeatFragment;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONObject;

public class TrainSelectSeatAdapter extends FragmentPagerAdapter {
    private Context context;
    private JSONObject jsonSeatMap;
    private Reservation reservation;
    private String direction;

    public TrainSelectSeatAdapter(FragmentManager fm, Context context, JSONObject jsonSeatMap, Reservation reservation, String direction) {
        super(fm);
        this.context = context;
        this.jsonSeatMap = jsonSeatMap;
        this.reservation = reservation;
        this.direction = direction;
    }

    @Override
    public Fragment getItem(int position) {
        return TrainSelectSeatFragment.newInstance(context, jsonSeatMap, reservation, direction, position);
    }

    @Override
    public int getCount() {
        return this.reservation.getAdultCount();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(R.string.adult) + " " + Integer.toString(position + 1);
    }
}
