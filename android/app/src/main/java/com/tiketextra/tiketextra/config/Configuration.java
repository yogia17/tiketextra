package com.tiketextra.tiketextra.config;

import android.os.Environment;

import com.tiketextra.tiketextra.model.SettingModel;

import java.io.File;

public final class Configuration {

//	public static final String DOKU_MALL_ID = "4094";//2749
//	public static final String DOKU_SHARED_KEY = "G1f1l2KSc0aV";


//	public static final String PACKAGE_NAME = "com.tiketextra.tiketextra";
//public static final String TARGET_URL = "http://10.0.3.2:8080/tiketextra/";
//		public static final String TARGET_URL = "http://10.0.2.2/tiketextra_prod/public/";
//	public static final String TARGET_URL = "http://192.168.0.3:8080/tiket/";
	public static final String TARGET_URL = "https://tiketextra.com/";
	public static final String TARGET_URL_WA = "https://wa.me/";
	public static final String TARGET_URL_TELE = "https://telegram.me/tiketextra2";
	public static final String TARGET_URL_FB = "https://m.me/tiketextra";
	public static final String TARGET_URL_LINE = "https://line.me/ti/p/~cembeliq19";
//	public static final String TARGET_URL_TELE = "https://web.telegram.org/#/im?p=u419498489_4710263236264616559";
	public static final String SLIDESHOW_TARGET_URL = "https://tiketextra.com/admin/";
	public static final String WEB_SERVICE_CONTROLLER = "android/";
//	public static final String TARGET_URL = "http://ws.tiketextra.com/ws.php/";
//	public static final String REGISTER_URL = "http://member.tiketextra.com/admin.php/register";
	public static final String APP_FOLDER = Environment.getExternalStorageDirectory()+ File.separator+"Tiketextra";
	public static final String INVOICE_FOLDER = Environment.getExternalStorageDirectory()+ File.separator+"Tiketextra"+ File.separator+"Invoices";
	public static final String TICKET_FOLDER = Environment.getExternalStorageDirectory()+ File.separator+"Tiketextra"+ File.separator+"Tickets";
	public static final String CLIENT_USERNAME = "android";
	public static final String CLIENT_PASSWORD = "klsjfouiwkmrtkhuYUJksdfjiUsduIJkijksdf";
	public static final int SPLASH_TIME_OUT = 3000;
	public static final int DEPART_EXTRA = 2;
	public static final int RETURN_EXTRA_FROM_DEPART = 2;
    public static final int MAX_PASSENGER = 9;
    public static final int TRAIN_MAX_PASSENGER = 4;

	public static final int FLIGHT_NUM_LINKS = 2;

	public static final int SORT_LOWEST_PRICE = 0;
	public static final int SORT_DEPART_TIME = 1;
	public static final int SORT_ARRIVE_TIME = 2;
	public static final int SORT_DURATION = 3;
	public static final int SORT_CABIN_CLASS = 4;

	public static final int SORT_HIGHEST_PRICE = 1;

	public static final int SORT_RATING_5_TO_1 = 2;
	public static final int SORT_RATING_1_TO_5 = 3;
	public static final int SORT_HIGHEST_DISCOUNT = 4;

	public static final String MODE_DEPART = "depart";
	public static final String MODE_RETURN = "return";

	public static final int SOCKET_TIMEOUT = 200000;
	public static final int DEFAULT_MAX_RETRIES = 20;
	public static final int DEFAULT_BACKOFF_MULT = 20;
	public static final int FLIGHT_SEARCH_CACHE_TIMEOUT = 600000;
	public static final int FLIGHT_SEARCH_CACHE_WAITING_TIME = 8000;
	public static final int RESERVATION_CHECK_DELAY = 60000;

	public static final String API_STATE_RUN = "1";
	public static final String API_STATE_FINISH = "3";
	public static final String API_STATE_ERROR = "4";

//	public static final String DEFAULT_FROM_PORT = "JOG";
//	public static final String DEFAULT_TO_PORT = "CGK";

	public static final int PREFETCH_UPDATE_DURATION_THRESHOLD_IN_MINUTES = 15;//480 minutes = 8 hours

	public static final int DATABASE_VERSION = 3;
	public static final String DATABASE_NAME = "new.sqli";
	public static final String COUNTRY_BASE = "ID";

	public static final String TABLE_PORT = "port";

	public static final String FIELD_IATA_CODE = "iata_code";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_NAME_ALT = "name_alt";
	public static final String FIELD_CITY = "city";
	public static final String FIELD_CITY_ALT = "city_alt";
	public static final String FIELD_REGION = "region";
	public static final String FIELD_COUNTRY_CODE = "country_code";
	public static final String FIELD_LATITUDE = "latitude";
	public static final String FIELD_LONGITUDE = "longitude";
	public static final String FIELD_TIMEZONE = "timezone";
	public static final String FIELD_TZ = "tz";
	public static final String FIELD_IS_ACTIVE = "is_active";
	public static final String FIELD_HIT = "hit";

	public static final String TABLE_COUNTRY = "country";

	public static final String FIELD_COUNTRY = "country";

	public static final String TABLE_SETTING = "setting";
	public static final String FIELD_PARAMETER = "parameter";
	public static final String FIELD_VALUE = "value";

	public static final String TABLE_AIRLINE = "airline";
	public static final String FIELD_FLIGHT_NUMBER = "flight_number";
	public static final String FIELD_CARRIER_ID = "carrier_id";
	public static final String FIELD_DOMESTIC_BAGGAGE = "domestic_baggage";
	public static final String FIELD_INTERNATIONAL_BAGGAGE = "international_baggage";
	public static final String FIELD_MEAL = "meal";
	public static final String FIELD_AIRPORT_TAX = "airport_tax";

	public static final String TABLE_CONTACT = "contact";
	public static final String FIELD_CONTACT_ID = "contact_id";
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_FULL_NAME = "full_name";
	public static final String FIELD_PHONE = "phone";
	public static final String FIELD_EMAIL = "email";



	public static final String TABLE_FARE = "fare";
	public static final String FIELD_FARE_ID = "fare_id";
    public static final String FIELD_RESERVATION_ID = "reservation_id";
    public static final String FIELD_BANK_ID = "bank_id";
	public static final String FIELD_PASSENGER_TYPE = "passenger_type";
	public static final String FIELD_BASIC = "basic";
	public static final String FIELD_TAX = "tax";
	public static final String FIELD_IWJR = "iwjr";
	public static final String FIELD_INSURANCE = "insurance";
	public static final String FIELD_FUEL = "fuel";
	public static final String FIELD_ADMINISTRATION = "administration";
	public static final String FIELD_SURCHARGE = "surcharge";
	public static final String FIELD_DISCOUNT = "discount";
	public static final String FIELD_COUNT = "count";

	public static final String TABLE_PASSENGER = "passenger";
	public static final String FIELD_PASSENGER_ID = "passenger_id";
	public static final String FIELD_PASSENGER_TITLE = "passenger_title";
	public static final String FIELD_BIRTH_DATE = "date_of_birth";
	public static final String FIELD_ID_CARD_NUMBER = "id_card_num";
	public static final String FIELD_ADULT_ASSOC = "adult_assoc_id";
	public static final String FIELD_GENDER = "gender";
	public static final String FIELD_NATIONALITY = "nationality";
	public static final String FIELD_LOYALTY_ID = "loyalty_id";
	public static final String FIELD_PASSPORT_NUMBER = "passport_number";
	public static final String FIELD_PASSPORT_ISSUING_COUNTRY = "passport_issuing_country";
	public static final String FIELD_PASSPORT_EXPIRY_DATE = "passport_expiry_date";
	public static final String FIELD_DEPART_SEAT = "depart_seat";
	public static final String FIELD_RETURN_SEAT = "return_seat";

	public static final String TABLE_RESERVATION = "reservation";
	public static final String FIELD_INVOICE = "invoice";
	public static final String FIELD_SLUG = "slug";
	public static final String FIELD_CARRIER_TYPE = "carrier_type";
	public static final String FIELD_BANK = "bank";
	public static final String FIELD_IS_CONFIRMED = "is_confirmed";
	public static final String FIELD_IS_PAID = "is_paid";
	public static final String FIELD_AMOUNT = "amount";
	public static final String FIELD_BANK_DISCOUNT = "bank_discount";
	public static final String FIELD_BANK_SURCHARGE = "bank_surcharge";
	public static final String FIELD_UNIQUE_CODE = "unique_code";
	public static final String FIELD_PROMO_AMOUNT = "promo_amount";
	public static final String FIELD_PROMO_CODE = "promo_code";
	public static final String FIELD_PAID_FROM = "paid_from";
	public static final String FIELD_PAID_TIME_STAMP = "paid_time_stamp";
	public static final String FIELD_CONFIRMED_TIME_STAMP = "confirmed_time_stamp";
	public static final String FIELD_STATUS_1 = "status_1";
	public static final String FIELD_STATUS_2 = "status_2";
	public static final String FIELD_IS_RETURN = "is_return";
	public static final String FIELD_FROM_PORT = "from_port";
	public static final String FIELD_TO_PORT = "to_port";
	public static final String FIELD_DEPART_DATE = "depart_date";
	public static final String FIELD_RETURN_DATE = "return_date";
	public static final String FIELD_DEPART_FLIGHT = "depart_flight";
	public static final String FIELD_RETURN_FLIGHT = "return_flight";
	public static final String FIELD_ADULT_COUNT = "adult_count";
	public static final String FIELD_CHILD_COUNT = "child_count";
	public static final String FIELD_INFANT_COUNT = "infant_count";
	public static final String FIELD_CONTACT_TITLE = "contact_title";
	public static final String FIELD_CONTACT_NAME = "contact_name";
	public static final String FIELD_CONTACT_PHONE = "contact_phone";
	public static final String FIELD_CONTACT_EMAIL = "contact_email";
	public static final String FIELD_CARRIER_ID_1 = "carrier_id_1";
	public static final String FIELD_CARRIER_ID_2 = "carrier_id_2";
	public static final String FIELD_CLASS_1 = "class_1";
	public static final String FIELD_CLASS_2 = "class_2";
	public static final String FIELD_BOOKING_CODE_1 = "booking_code_1";
	public static final String FIELD_BOOKING_CODE_2 = "booking_code_2";
	public static final String FIELD_TIME_BOOKING_1 = "time_booking_1";
	public static final String FIELD_TIME_BOOKING_2 = "time_booking_2";
	public static final String FIELD_TIME_LIMIT_1 = "time_limit_1";
	public static final String FIELD_TIME_LIMIT_2 = "time_limit_2";
	public static final String FIELD_USER_TYPE = "user_type";
	public static final String FIELD_DEPART_HIDDEN_TRANSIT = "depart_hidden_transit";
	public static final String FIELD_RETURN_HIDDEN_TRANSIT = "return_hidden_transit";


	public static final String TABLE_USER_PASSENGER = "user_passenger";

	public static final String TABLE_CARRIER = "carrier";


















//			'title' => 						0
//			'first_name' =>					1
//			'last_name' => 					2

//			'date_of_birth' => 				3
//			'id_card_num' => 				4
//			'adult_assoc_num' => 			5

//			'gender' => 					6
//			'nationality' => 				7
//			'loyalty_id' => 				8
//
//			'passport_number' => 			9
//			'passport_issuing_country' => 	10
//			'passport_expiry_date' =>		11


	public static final int[][] PASSENGER_DATA_KAI = {
			{1, 1, 1, 0, 1, 0},    //ADULT
			{1, 1, 1, 1, 0, 1}    //INFANT
	};

	public static final int[][][] PASSENGER_DATA_LOCAL = {
			{//LIO
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//SRI
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//GAR
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//AIR
					{1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0},	//INFANT
			},
			{//CIT
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0},	//INFANT
			},
			{//KAL
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//TRI
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//EXP
					{1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0},	//INFANT
			},
			{//TRA
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},	//INFANT
			},
	};

	public static final int[][][] PASSENGER_DATA_INTERNATIONAL = {
			{//LIO
					{1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1},	//ADULT
					{1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1},	//CHILD
					{1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1},	//INFANT
			},
			{//SRI
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//GAR
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//AIR
					{1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0},	//INFANT
			},
			{//CIT
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0},	//INFANT
			},
			{//KAL
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//TRI
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0},	//INFANT
			},
			{//EXP
					{1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1},	//ADULT
					{1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1},	//CHILD
					{1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1},	//INFANT
			},
			{//TRA
					{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},	//ADULT
					{1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0},	//CHILD
					{1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},	//INFANT
			},
	};
}
