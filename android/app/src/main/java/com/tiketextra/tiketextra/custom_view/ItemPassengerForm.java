package com.tiketextra.tiketextra.custom_view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.model.CountryModel;
import com.tiketextra.tiketextra.object.FareRoute;
import com.tiketextra.tiketextra.object.Passenger;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kurnia on 17/12/17.
 */

public class ItemPassengerForm extends CardView {
    private FareRoute departChoice, returnChoice;
    private int[] passengerData;
    private Passenger passenger;
    private String passengerType;
    private Context context;

    public ItemPassengerForm(@NonNull Context context, int[] iPassengerData, String iPassengerType, int passengerTypeCounter, int adultCount, Bundle parameter, Passenger iPassenger) {
        super(context);
        View.inflate(context, R.layout.item_passenger_form, this);
        this.context = context;
        this.passengerData = iPassengerData;
        this.passenger = iPassenger;
        this.passengerType = iPassengerType;
        
        Gson gson = new Gson();

        departChoice = gson.fromJson(parameter.getString("depart_choice"), FareRoute.class);
        if (parameter.getString("type").equals("roundtrip")) {
            returnChoice = gson.fromJson(parameter.getString("return_choice"), FareRoute.class);
        }

        ArrayAdapter<String> spinnerTitle = null;
        if(passengerType.equals("adult")){
            spinnerTitle= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getAdultTitle(getContext()));
        }
        else if(passengerType.equals("child")){
            spinnerTitle= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getChildTitle(getContext()));
        }
        else{
            spinnerTitle= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getInfantTitle(getContext()));
        }
        spinnerTitle.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerDayArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getDay());
        spinnerDayArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerMonthArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getMonth(context));
        spinnerMonthArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerBirthYearArrayAdapter = null;
        if(passengerType.equals("adult")){
            spinnerBirthYearArrayAdapter= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getAdultYearOfBirth());
        }
        else if(passengerType.equals("child")){
            spinnerBirthYearArrayAdapter= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getChildYearOfBirth());
        }
        else{
            spinnerBirthYearArrayAdapter= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getInfantYearOfBirth());
        }
        spinnerBirthYearArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerCountryArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getNationality(new CountryModel(context).getAllCountries()));
        spinnerCountryArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerPassportExpiryYearArrayAdapter= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getPassportExpiryYear());
        spinnerPassportExpiryYearArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        String[] adultAssocNumber = new String[adultCount];
        for(int i=1; i<=adultCount; i++){
            adultAssocNumber[i-1] = getResources().getString(R.string.adult) + " " + i;
        }
        ArrayAdapter<String> spinnerAssocArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, adultAssocNumber);
        spinnerAssocArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ((ImageButton) findViewById(R.id.imagePassenger)).setImageResource(getResources().getIdentifier("ic_"+passengerType, "drawable", context.getPackageName()));
        ((TextView) findViewById(R.id.passengerTypeTextView)).setText(context.getString(getResources().getIdentifier(passengerType, "string", context.getPackageName()))+" "+passengerTypeCounter);
        ((TextView) findViewById(R.id.passengerTypeHintTextView)).setText(getResources().getIdentifier(passengerType+"_year_hint", "string", context.getPackageName()));



        if(passengerData[3]==1) {
//            if(passengerType.equals("adult")) {
//                if (parameter.getString("type").equals("roundtrip")) {
//                    if (departChoice.getAirlineCode().equals("CIT") && returnChoice.getAirlineCode().equals("CIT")) {
//                        findViewById(R.id.birthDateTextView).setVisibility(View.GONE);
//                        findViewById(R.id.birthDateLayout).setVisibility(View.GONE);
//                        passenger.setPrintBirthDate(false);
//                    } else if (!departChoice.getAirlineCode().equals("CIT") && !returnChoice.getAirlineCode().equals("CIT")) {
//                        findViewById(R.id.birthDateTextView).setVisibility(View.VISIBLE);
//                        findViewById(R.id.birthDateLayout).setVisibility(View.VISIBLE);
//                        passenger.setPrintBirthDate(true);
//                    } else {
//                        int[] comparePassengerData;
//                        if (parameter.getBoolean("is_international")) {
//                            comparePassengerData = ItineraryHelper.getInternationalOnewayPassengerData(!departChoice.getAirlineCode().equals("CIT") ? departChoice.getAirlineCode() : returnChoice.getAirlineCode(), passengerType);
//                        } else {
//                            comparePassengerData = ItineraryHelper.getDomesticOnewayPassengerData(!departChoice.getAirlineCode().equals("CIT") ? departChoice.getAirlineCode() : returnChoice.getAirlineCode(), passengerType);
//                        }
//                        if (comparePassengerData[3] == 1) {
//                            findViewById(R.id.birthDateTextView).setVisibility(View.VISIBLE);
//                            findViewById(R.id.birthDateLayout).setVisibility(View.VISIBLE);
//                            passenger.setPrintBirthDate(true);
//                        } else {
//                            findViewById(R.id.birthDateTextView).setVisibility(View.GONE);
//                            findViewById(R.id.birthDateLayout).setVisibility(View.GONE);
//                            passenger.setPrintBirthDate(false);
//                        }
//                    }
//                } else {
//                    if (departChoice.getAirlineCode().equals("CIT")) {
//                        findViewById(R.id.birthDateTextView).setVisibility(View.GONE);
//                        findViewById(R.id.birthDateLayout).setVisibility(View.GONE);
//                        passenger.setPrintBirthDate(false);
//                    } else {
//                        findViewById(R.id.birthDateTextView).setVisibility(View.VISIBLE);
//                        findViewById(R.id.birthDateLayout).setVisibility(View.VISIBLE);
//                        passenger.setPrintBirthDate(true);
//                    }
//                }
//            }
//            else{
                findViewById(R.id.birthDateTextView).setVisibility(View.VISIBLE);
                findViewById(R.id.birthDateLayout).setVisibility(View.VISIBLE);
                passenger.setPrintBirthDate(true);
//            }
        }
        else{
            findViewById(R.id.birthDateTextView).setVisibility(View.GONE);
            findViewById(R.id.birthDateLayout).setVisibility(View.GONE);
            passenger.setPrintBirthDate(false);
        }
        
        if(passengerData[4]==1) {
            findViewById(R.id.idCardNumTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.idCardNumLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.idCardNumTextView).setVisibility(View.GONE);
            findViewById(R.id.idCardNumLayout).setVisibility(View.GONE);
        }

        if(passengerData[5]==1) {
            findViewById(R.id.assocTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.assocLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.assocTextView).setVisibility(View.GONE);
            findViewById(R.id.assocLayout).setVisibility(View.GONE);
        }

//        if(passengerData[6]==1) {
//            findViewById(R.id.genderTextView).setVisibility(View.VISIBLE);
//            findViewById(R.id.genderRadioGroup).setVisibility(View.VISIBLE);
//        }
//        else{
        findViewById(R.id.genderTextView).setVisibility(View.GONE);
        findViewById(R.id.genderRadioGroup).setVisibility(View.GONE);
//        }

        if(passengerData[7]==1) {
            findViewById(R.id.nationalityTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.nationalityLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.nationalityTextView).setVisibility(View.GONE);
            findViewById(R.id.nationalityLayout).setVisibility(View.GONE);
        }

        if(passengerData[9]==1) {
            findViewById(R.id.passportNumberTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.passportNumberLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.passportNumberTextView).setVisibility(View.GONE);
            findViewById(R.id.passportNumberLayout).setVisibility(View.GONE);
        }

        if(passengerData[10]==1) {
            findViewById(R.id.passportIssuingCountryTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.passportIssuingCountryLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.passportIssuingCountryTextView).setVisibility(View.GONE);
            findViewById(R.id.passportIssuingCountryLayout).setVisibility(View.GONE);
        }

        if(passengerData[11]==1) {
            findViewById(R.id.passportExpiryDateTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.passportExpiryDateLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.passportExpiryDateTextView).setVisibility(View.GONE);
            findViewById(R.id.passportExpiryDateLayout).setVisibility(View.GONE);
        }

        ((Spinner) findViewById(R.id.spinnerTitle)).setAdapter(spinnerTitle);

        //----------DOB
        ((Spinner) findViewById(R.id.spinnerBirthDay)).setAdapter(spinnerDayArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerBirthMonth)).setAdapter(spinnerMonthArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerBirthYear)).setAdapter(spinnerBirthYearArrayAdapter);

        // ---- nationality
        ((Spinner) findViewById(R.id.spinnerNationality)).setAdapter(spinnerCountryArrayAdapter);

        ((Spinner) findViewById(R.id.spinnerPassportExpiryDay)).setAdapter(spinnerDayArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerPassportExpiryMonth)).setAdapter(spinnerMonthArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerPassportExpiryYear)).setAdapter(spinnerPassportExpiryYearArrayAdapter);

        ((Spinner) findViewById(R.id.spinnerPassportIssuingCountry)).setAdapter(spinnerCountryArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerAssoc)).setAdapter(spinnerAssocArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerAssoc)).setSelection(passengerTypeCounter-1);

    }

    public Passenger getPassenger() {
        return passenger;
    }

    public boolean validate(){
        boolean valid = true;
        if(passengerData[0]==1) {
            if(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItemPosition()==0){
                valid = false;
                ((TextView)((Spinner) findViewById(R.id.spinnerTitle)).getSelectedView()).setError(getResources().getString(R.string.passenger_empty_validation));
                findViewById(R.id.spinnerTitle).requestFocus();

            }
            else {
                if (passengerType.equals("adult")) {
                    passenger.setTitle(ItineraryHelper.encodeAdultTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()));
                } else if (passengerType.equals("child")) {
                    passenger.setTitle(ItineraryHelper.encodeChildTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()));
                } else {
                    passenger.setTitle(ItineraryHelper.encodeInfantTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()));
                }

                if(ItineraryHelper.encodeAdultTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()).equals("Mr") || ItineraryHelper.encodeAdultTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()).equals("Mstr")){
                    passenger.setGender("L");
                }
                else {
                    passenger.setGender("P");
                }
            }
        }

        EditText fullName = findViewById(R.id.nameEditText);

        if (fullName.getText().toString().isEmpty()) {
            fullName.setError(getResources().getString(R.string.passenger_empty_validation));
            valid = false;
            fullName.requestFocus();
        }
        else if (!fullName.getText().toString().matches("[a-z A-Z]+?")) {
            fullName.setError(getResources().getString(R.string.passenger_name_validation));
            valid = false;
            fullName.requestFocus();
        }
        passenger.setName(Utils.upperCaseAllFirst(fullName.getText().toString().replaceAll("\\s+", " ")));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(passengerData[3]==1) {
            Spinner year = findViewById(R.id.spinnerBirthYear);
            Spinner month = findViewById(R.id.spinnerBirthMonth);
            Spinner day = findViewById(R.id.spinnerBirthDay);
            TextView tvDate = findViewById(R.id.birthDateTextView);
            String date = year.getSelectedItem().toString()+"-"+((month.getSelectedItemPosition()+1)<10?"0"+(month.getSelectedItemPosition()+1):(month.getSelectedItemPosition()+1))+"-"+day.getSelectedItem().toString();
            Date now = new Date();

            if (passengerType.equals("adult")) {
                try {
                    Date seek = dateFormat.parse(date);
                    int diff = Days.daysBetween(new DateTime(seek), new DateTime(now)).getDays();
                    if(!(Double.compare(diff/365.25, 12) >= 0)){
                        tvDate.setText(getResources().getString(R.string.passenger_adult_date));
                        tvDate.setTextColor(Color.RED);
                        tvDate.setTypeface(null, Typeface.BOLD);
                        valid = false;
                        fullName.requestFocus();
                    }
                    else {
                        tvDate.setText(R.string.birth_date);
                        tvDate.setTextColor(getResources().getColor(R.color.grey_600));
                        tvDate.setTypeface(null, Typeface.NORMAL);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            else if (passengerType.equals("child")) {
                try {
                    Date seek = dateFormat.parse(date);
                    int diff = Days.daysBetween(new DateTime(seek), new DateTime(now)).getDays();
                    if(!(Double.compare(diff/365.25, 2) >= 0  && Double.compare(diff/365.25, 12) < 0)){
                        tvDate.setText(getResources().getString(R.string.passenger_child_date));
                        tvDate.setTextColor(Color.RED);
                        tvDate.setTypeface(null, Typeface.BOLD);
                        valid = false;
                        fullName.requestFocus();
                    }
                    else {
                        tvDate.setText(R.string.birth_date);
                        tvDate.setTextColor(getResources().getColor(R.color.grey_600));
                        tvDate.setTypeface(null, Typeface.NORMAL);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            else{
                try {
                    Date seek = dateFormat.parse(date);
                    int diff = Days.daysBetween(new DateTime(seek), new DateTime(now)).getDays();
                    if(!(Double.compare(diff/365.25, 2) < 0)){
                        tvDate.setText(getResources().getString(R.string.passenger_infant_date));
                        tvDate.setTextColor(Color.RED);
                        tvDate.setTypeface(null, Typeface.BOLD);
                        valid = false;
                        fullName.requestFocus();
                    }
                    else {
                        tvDate.setText(R.string.birth_date);
                        tvDate.setTextColor(getResources().getColor(R.color.grey_600));
                        tvDate.setTypeface(null, Typeface.NORMAL);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            passenger.setBirthDate(date);
        }

        if(passengerData[4]==1) {
            EditText idCard = findViewById(R.id.idCardNumEditText);
            if (idCard.getText().toString().isEmpty()) {
                idCard.setError(getResources().getString(R.string.passenger_empty_validation));
                valid = false;
                idCard.requestFocus();
            }
            passenger.setIDCardNumber(idCard.getText().toString());
        }

        if(passengerData[5]==1) {
//                    passenger.setAdultAssocNumber(Integer.toString(getArguments().getInt(POSITION) + 1));
            passenger.setAdultAssocNumber(((Spinner) findViewById(R.id.spinnerAssoc)).getSelectedItemPosition()+1);

        }

        if(passengerData[6]==1) {
//            RadioGroup radioButtonGroup = findViewById(R.id.genderRadioGroup);
//            int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
//            View radioButton = radioButtonGroup.findViewById(radioButtonID);
//            int idx = radioButtonGroup.indexOfChild(radioButton);
//            passenger.setGender(idx==0?"L":"P");



            if(ItineraryHelper.encodeAdultTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()).equals("Mr") || ItineraryHelper.encodeAdultTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()).equals("Mstr")){
                passenger.setGender("L");
            }
            else {
                passenger.setGender("P");
            }
        }

        if(passengerData[7]==1) {
            passenger.setNationality(new CountryModel(this.context).getCountryByName(((Spinner) findViewById(R.id.spinnerNationality)).getSelectedItem().toString()).getCountryCode());
        }

        if(passengerData[9]==1) {
            EditText passportNumber = findViewById(R.id.passportNumberEditText);
            if (passportNumber.getText().toString().isEmpty()) {
                passportNumber.setError(getResources().getString(R.string.passenger_empty_validation));
                valid = false;
                passportNumber.requestFocus();
            }
            passenger.setPassportNumber(passportNumber.getText().toString().toUpperCase());
        }

        if(passengerData[10]==1) {
            passenger.setPassportIssuingCountry(new CountryModel(context).getCountryByName(((Spinner) findViewById(R.id.spinnerPassportIssuingCountry)).getSelectedItem().toString()).getCountryCode());
        }

        if(passengerData[11]==1) {
            Spinner year = findViewById(R.id.spinnerPassportExpiryYear);
            Spinner month = findViewById(R.id.spinnerPassportExpiryMonth);
            Spinner day = findViewById(R.id.spinnerPassportExpiryDay);
            passenger.setPassportExpiryDate(year.getSelectedItem().toString()+"-"+((month.getSelectedItemPosition()+1)<10?"0"+(month.getSelectedItemPosition()+1):(month.getSelectedItemPosition()+1))+"-"+day.getSelectedItem().toString());
        }

        return valid;
    }
}
