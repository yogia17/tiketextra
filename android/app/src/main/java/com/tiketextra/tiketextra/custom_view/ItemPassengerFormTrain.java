package com.tiketextra.tiketextra.custom_view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.helper.ItineraryHelper;
import com.tiketextra.tiketextra.model.CountryModel;
import com.tiketextra.tiketextra.object.FareRouteTrain;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.util.Utils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kurnia on 17/12/17.
 */

public class ItemPassengerFormTrain extends CardView {
    private FareRouteTrain departChoice, returnChoice;
    private int[] passengerData;
    private Passenger passenger;
    private String passengerType;
    private Context context;
    private boolean upper17Years = true;

    public ItemPassengerFormTrain(@NonNull Context context, int[] iPassengerData, String iPassengerType, int passengerTypeCounter, int adultCount, Bundle parameter, Passenger iPassenger) {
        super(context);
        View.inflate(context, R.layout.item_passenger_form_train, this);
        this.context = context;
        this.passengerData = iPassengerData;
        this.passenger = iPassenger;
        this.passengerType = iPassengerType;
        
        Gson gson = new Gson();

        departChoice = gson.fromJson(parameter.getString("depart_choice"), FareRouteTrain.class);
        if (parameter.getString("type").equals("roundtrip")) {
            returnChoice = gson.fromJson(parameter.getString("return_choice"), FareRouteTrain.class);
        }

        ArrayAdapter<String> spinnerTitle = null;
        if(passengerType.equals("adult")){
            spinnerTitle= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getAdultTitle(getContext()));
        }
        else{
            spinnerTitle= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getInfantTitle(getContext()));
        }
        spinnerTitle.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerDayArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getDay());
        spinnerDayArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerMonthArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getMonth(context));
        spinnerMonthArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerBirthYearArrayAdapter = null;
        if(passengerType.equals("adult")){
            spinnerBirthYearArrayAdapter= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getAdultYearOfBirthUnder17());
        }
        else{
            spinnerBirthYearArrayAdapter= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getInfantYearOfBirthTrain());
        }
        spinnerBirthYearArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerCountryArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getNationality(new CountryModel(context).getAllCountries()));
        spinnerCountryArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ArrayAdapter<String> spinnerPassportExpiryYearArrayAdapter= new ArrayAdapter<>(context, R.layout.my_spinner_item, ItineraryHelper.getPassportExpiryYear());
        spinnerPassportExpiryYearArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        String[] adultAssocNumber = new String[adultCount];
        for(int i=1; i<=adultCount; i++){
            adultAssocNumber[i-1] = getResources().getString(R.string.adult) + " " + i;
        }
        ArrayAdapter<String> spinnerAssocArrayAdapter = new ArrayAdapter<>(context, R.layout.my_spinner_item, adultAssocNumber);
        spinnerAssocArrayAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        ((ImageButton) findViewById(R.id.imagePassenger)).setImageResource(getResources().getIdentifier("ic_"+passengerType, "drawable", context.getPackageName()));
        ((TextView) findViewById(R.id.passengerTypeTextView)).setText(context.getString(getResources().getIdentifier(passengerType, "string", context.getPackageName()))+" "+passengerTypeCounter);
        ((TextView) findViewById(R.id.passengerTypeHintTextView)).setText(getResources().getIdentifier("train_"+passengerType+"_year_hint", "string", context.getPackageName()));



        if(passengerData[3]==1) {
                findViewById(R.id.birthDateTextView).setVisibility(View.VISIBLE);
                findViewById(R.id.birthDateLayout).setVisibility(View.VISIBLE);
                passenger.setPrintBirthDate(true);
        }
        else{
            findViewById(R.id.birthDateTextView).setVisibility(View.GONE);
            findViewById(R.id.birthDateLayout).setVisibility(View.GONE);
            passenger.setPrintBirthDate(false);
        }
        
        if(passengerData[4]==1) {
            findViewById(R.id.idCardNumTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.idCardNumLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.idCardNumTextView).setVisibility(View.GONE);
            findViewById(R.id.idCardNumLayout).setVisibility(View.GONE);
        }

        if(passengerData[5]==1) {
            findViewById(R.id.assocTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.assocLayout).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.assocTextView).setVisibility(View.GONE);
            findViewById(R.id.assocLayout).setVisibility(View.GONE);
        }


        ((Spinner) findViewById(R.id.spinnerTitle)).setAdapter(spinnerTitle);

        //----------DOB
        ((Spinner) findViewById(R.id.spinnerBirthDay)).setAdapter(spinnerDayArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerBirthMonth)).setAdapter(spinnerMonthArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerBirthYear)).setAdapter(spinnerBirthYearArrayAdapter);

        ((Spinner) findViewById(R.id.spinnerAssoc)).setAdapter(spinnerAssocArrayAdapter);
        ((Spinner) findViewById(R.id.spinnerAssoc)).setSelection(passengerTypeCounter-1);

        if (passengerType.equals("adult")) {

            ((Switch) findViewById(R.id.switch_adult)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        findViewById(R.id.idCardNumTextView).setVisibility(View.VISIBLE);
                        findViewById(R.id.idCardNumLayout).setVisibility(View.VISIBLE);
                        upper17Years = true;

                        findViewById(R.id.birthDateTextView).setVisibility(View.GONE);
                        findViewById(R.id.birthDateLayout).setVisibility(View.GONE);
                        passenger.setPrintBirthDate(false);
                    } else {
                        findViewById(R.id.idCardNumTextView).setVisibility(View.GONE);
                        findViewById(R.id.idCardNumLayout).setVisibility(View.GONE);
                        upper17Years = false;

                        findViewById(R.id.birthDateTextView).setVisibility(View.VISIBLE);
                        findViewById(R.id.birthDateLayout).setVisibility(View.VISIBLE);
                        passenger.setPrintBirthDate(true);

                    }
                }
            });
        }
        else {
            findViewById(R.id.switch_adult).setVisibility(GONE);
        }

    }

    public Passenger getPassenger() {
        return passenger;
    }

    public boolean validate(){
        boolean valid = true;
        if(passengerData[0]==1) {
            if(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItemPosition()==0){
                valid = false;
                ((TextView)((Spinner) findViewById(R.id.spinnerTitle)).getSelectedView()).setError(getResources().getString(R.string.passenger_empty_validation));
                findViewById(R.id.nameEditText).requestFocus();

            }
            else {
                if (passengerType.equals("adult")) {
                    passenger.setTitle(ItineraryHelper.encodeAdultTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()));
                } else {
                    passenger.setTitle(ItineraryHelper.encodeInfantTitle(((Spinner) findViewById(R.id.spinnerTitle)).getSelectedItem().toString(), getContext()));
                }
            }
        }

        EditText fullName = findViewById(R.id.nameEditText);

        if (fullName.getText().toString().isEmpty()) {
            fullName.setError(getResources().getString(R.string.passenger_empty_validation));
            valid = false;
            fullName.requestFocus();
        }
        else if (!fullName.getText().toString().matches("[a-z A-Z]+?")) {
            fullName.setError(getResources().getString(R.string.passenger_name_validation));
            valid = false;
            fullName.requestFocus();
        }
        passenger.setName(Utils.upperCaseAllFirst(fullName.getText().toString().replaceAll("\\s+", " ")));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(passengerData[3]==1 || upper17Years == false) {
            Spinner year = findViewById(R.id.spinnerBirthYear);
            Spinner month = findViewById(R.id.spinnerBirthMonth);
            Spinner day = findViewById(R.id.spinnerBirthDay);
            TextView tvDate = findViewById(R.id.birthDateTextView);
            String date = year.getSelectedItem().toString()+"-"+((month.getSelectedItemPosition()+1)<10?"0"+(month.getSelectedItemPosition()+1):(month.getSelectedItemPosition()+1))+"-"+day.getSelectedItem().toString();
            Date now = new Date();

            if (passengerType.equals("adult")) {
                try {
                    Date seek = dateFormat.parse(date);
                    int diff = Days.daysBetween(new DateTime(seek), new DateTime(now)).getDays();
                    if(Double.compare(diff/365.25, 3) < 0  || Double.compare(diff/365.25, 17) >= 0){
                        tvDate.setText(getResources().getString(R.string.train_passenger_adult_date_under17));
                        tvDate.setTextColor(Color.RED);
                        tvDate.setTypeface(null, Typeface.BOLD);
                        valid = false;
                        fullName.requestFocus();
                    }
                    else {
                        tvDate.setText(R.string.birth_date);
                        tvDate.setTextColor(getResources().getColor(R.color.grey_600));
                        tvDate.setTypeface(null, Typeface.NORMAL);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            else if (passengerType.equals("infant")) {
                try {
                    Date seek = dateFormat.parse(date);
                    int diff = Days.daysBetween(new DateTime(seek), new DateTime(now)).getDays();
                    if(Double.compare(diff/365.25, 3) >= 0){
                        tvDate.setText(getResources().getString(R.string.train_passenger_infant_date));
                        tvDate.setTextColor(Color.RED);
                        tvDate.setTypeface(null, Typeface.BOLD);
                        valid = false;
                        fullName.requestFocus();
                    }
                    else {
                        tvDate.setText(R.string.birth_date);
                        tvDate.setTextColor(getResources().getColor(R.color.grey_600));
                        tvDate.setTypeface(null, Typeface.NORMAL);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            passenger.setBirthDate(date);
        }

        if(passengerData[4]==1 && upper17Years) {
            EditText idCard = findViewById(R.id.idCardNumEditText);
            if (idCard.getText().toString().isEmpty()) {
                idCard.setError(getResources().getString(R.string.passenger_empty_validation));
                valid = false;
                idCard.requestFocus();
            }
            passenger.setIDCardNumber(idCard.getText().toString());
        }
        else {
            Spinner year = findViewById(R.id.spinnerBirthYear);
            Spinner month = findViewById(R.id.spinnerBirthMonth);
            Spinner day = findViewById(R.id.spinnerBirthDay);
            String date = day.getSelectedItem().toString()+((month.getSelectedItemPosition()+1)<10?"0"+(month.getSelectedItemPosition()+1):(month.getSelectedItemPosition()+1))+year.getSelectedItem().toString();

            passenger.setIDCardNumber(date);
        }



        if(passengerData[5]==1) {
//                    passenger.setAdultAssocNumber(Integer.toString(getArguments().getInt(POSITION) + 1));
            passenger.setAdultAssocNumber(((Spinner) findViewById(R.id.spinnerAssoc)).getSelectedItemPosition()+1);

        }
        return valid;
    }
}
