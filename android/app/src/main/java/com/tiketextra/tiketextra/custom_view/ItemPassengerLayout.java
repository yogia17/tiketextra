package com.tiketextra.tiketextra.custom_view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;

public class ItemPassengerLayout extends LinearLayout {

    private TextView birthDateTextView, nameTextView, numberTextView;

    public ItemPassengerLayout(Context context) {
        super(context);

        View.inflate(context, R.layout.item_passenger, this);
        nameTextView = findViewById(R.id.textViewName);
        birthDateTextView = findViewById(R.id.birthDateTextView);
        numberTextView = findViewById(R.id.numberTextView);
    }

    public void setText(String number, String name, String birthDate) {
        numberTextView.setText(number);
        nameTextView.setText(name);
        birthDateTextView.setText(birthDate);
    }
}