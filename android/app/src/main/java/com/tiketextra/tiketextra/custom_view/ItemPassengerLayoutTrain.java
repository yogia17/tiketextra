package com.tiketextra.tiketextra.custom_view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;

public class ItemPassengerLayoutTrain extends LinearLayout {

    private TextView additionalInfoTextView, nameTextView, numberTextView, IDCardTextView;

    public ItemPassengerLayoutTrain(Context context) {
        super(context);

        View.inflate(context, R.layout.item_passenger_train, this);
        nameTextView = findViewById(R.id.textViewName);
        additionalInfoTextView = findViewById(R.id.additionalInfoTextView);
        numberTextView = findViewById(R.id.numberTextView);
        IDCardTextView = findViewById(R.id.IDCardTextView);
    }

    public void setText(String number, String name, String info, String IDCardNumber) {
        numberTextView.setText(number);
        nameTextView.setText(name);
        additionalInfoTextView.setText(info);
        IDCardTextView.setText(IDCardNumber);
    }
}