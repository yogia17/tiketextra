package com.tiketextra.tiketextra.custom_view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiketextra.tiketextra.R;

public class ItemPassengerLayoutTrainDetail extends LinearLayout {

    private TextView nameTextView, numberTextView, IDCardTextView, departSeatTextView, returnSeatTextView;

    public ItemPassengerLayoutTrainDetail(Context context) {
        super(context);

        View.inflate(context, R.layout.item_passenger_train_detail, this);
        nameTextView = findViewById(R.id.textViewName);
        numberTextView = findViewById(R.id.numberTextView);
        IDCardTextView = findViewById(R.id.IDCardTextView);
        departSeatTextView = findViewById(R.id.departSeatTextView);
        returnSeatTextView = findViewById(R.id.returnSeatTextView);
    }

    public void setText(String number, String name, String IDCardNumber, String departSeat, String returnSeat) {
        numberTextView.setText(number);
        nameTextView.setText(name);
        IDCardTextView.setText(IDCardNumber);
        if(departSeat != null && !departSeat.equals("")) {
            departSeatTextView.setText(departSeat);
            findViewById(R.id.layoutDepartSeat).setVisibility(VISIBLE);
        }
        else {
            findViewById(R.id.layoutDepartSeat).setVisibility(GONE);
        }
        if(returnSeat != null && !returnSeat.equals("")) {
            returnSeatTextView.setText(returnSeat);
            findViewById(R.id.layoutReturnSeat).setVisibility(VISIBLE);
        }
        else {
            findViewById(R.id.layoutReturnSeat).setVisibility(GONE);
        }
    }
}