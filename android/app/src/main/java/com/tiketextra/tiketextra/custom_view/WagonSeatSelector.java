package com.tiketextra.tiketextra.custom_view;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.tiketextra.tiketextra.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class WagonSeatSelector extends RadioGroup {

    private OnCheckedChangeListener mOnCheckedChangeListener;
    private OnClickListener mOnClickListener;
    private int mMaxInRow;
    private TableLayout mTableLayout;
    private List<RadioButton> mRadioButtons;
    private RadioButton mCheckedButton;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    public WagonSeatSelector(Context context) {
        super(context);
        init(null);
    }

    public WagonSeatSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        mRadioButtons = new ArrayList<>();

        mTableLayout = getTableLayout();
        addView(mTableLayout);
    }

    public void addActiveRadioButton(String name){
        int index = getRadioButtonCount();
        RadioButton radioButton = getRadioButton();
        radioButton.setBackgroundColor(getResources().getColor(R.color.colorAccentLight));
        radioButton.setTypeface(null, Typeface.BOLD);
        radioButton.setText(name);
        radioButton.setId(generateId());
        initRadioButton(radioButton);
        mRadioButtons.add(index, radioButton);
        arrangeButtons();
    }

    public void addEnabledRadioButton(String name){
        int index = getRadioButtonCount();
        RadioButton radioButton = getRadioButton();
        radioButton.setText(name);
        radioButton.setId(generateId());
        initRadioButton(radioButton);
        mRadioButtons.add(index, radioButton);
        arrangeButtons();
    }

    public void addDisabledRadioButton(String name){
        int index = getRadioButtonCount();
        RadioButton radioButton = getRadioButton();
        radioButton.setBackgroundColor(getResources().getColor(R.color.grey_500));
        radioButton.setEnabled(false);
        radioButton.setText(name);
        radioButton.setId(generateId());
        initRadioButton(radioButton);
        mRadioButtons.add(index, radioButton);
        arrangeButtons();
    }

    public void addVoidRadioButton(){
        int index = getRadioButtonCount();
        RadioButton radioButton = getRadioButton();
        radioButton.setVisibility(INVISIBLE);
        radioButton.setId(generateId());
        initRadioButton(radioButton);
        mRadioButtons.add(index, radioButton);
        arrangeButtons();
    }

    private int generateId() {
        // for API 17 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return View.generateViewId();

            // for API lower than 17
        } else {

            while (true) {
                final int result = sNextGeneratedId.get();

                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF)
                    newValue = 1; // Roll over to 1, not 0.

                if (sNextGeneratedId.compareAndSet(result, newValue))
                    return result;
            }

        }
    }

    private void initRadioButton(RadioButton radioButton) {
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean didCheckStateChange = checkButton((RadioButton) v);
                if (didCheckStateChange && mOnCheckedChangeListener != null) {
                    mOnCheckedChangeListener.onCheckedChanged(WagonSeatSelector.this, mCheckedButton);
                }
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(WagonSeatSelector.this, mCheckedButton);
                }
            }
        });
    }

    public void check(CharSequence text) {
        if (text == null)
            return;

        for (RadioButton radioButton : mRadioButtons) {
            if (radioButton.getText().equals(text)) {
                if (checkButton(radioButton)) { // True if the button wasn't already checked.
                    if (mOnCheckedChangeListener != null) {
                        mOnCheckedChangeListener.onCheckedChanged(
                                WagonSeatSelector.this, radioButton);
                    }
                }
                return;
            }
        }
    }

    private boolean checkButton(RadioButton button) {
        if (button == null || button == mCheckedButton) {
            return false;
        }

        // if the button to check is different from the current checked button
        // if exists un-checks mCheckedButton
        if (mCheckedButton != null) {
            mCheckedButton.setChecked(false);
        }

        button.setChecked(true);
        mCheckedButton = button;
        return true;
    }

    public RadioButton getRadioButton(CharSequence button) {
        for (RadioButton radioButton : mRadioButtons)
            if (radioButton.getText().equals(button))
                return radioButton;
        return null;
    }

    public void removeButtons(int start, int count) {
        if (count == 0) {
            return;
        }
        if (start < 0 || start >= mRadioButtons.size())
            throw new IllegalArgumentException("start index must be between 0 to getRadioButtonCount() - 1 [" +
                    (getRadioButtonCount() - 1) + "]: " + start);

        if (count < 0)
            throw new IllegalArgumentException("count must not be negative: " + count);

        int endIndex = start + count - 1;
        // if endIndex is not in the range of the radio buttons sets it to the last index
        if (endIndex >= mRadioButtons.size())
            endIndex = mRadioButtons.size() - 1;

        // iterates over the buttons to remove
        for (int i = endIndex; i >= start; i--) {
            RadioButton radiobutton = mRadioButtons.get(i);
            // if the button to remove is the checked button set mCheckedButton to null
            if (radiobutton == mCheckedButton)
                mCheckedButton = null;
            // removes the button from the list
            mRadioButtons.remove(i);
        }

        arrangeButtons();
    }

    /**
     * Removes all the radio buttons from the layout.
     */
    public void removeAllButtons() {
        removeButtons(0, mRadioButtons.size());
    }

    private TableLayout getTableLayout() {
        return (TableLayout) LayoutInflater.from(getContext())
                .inflate(R.layout.table_layout, this, false);
    }

    protected TableRow getTableRow() {
        return (TableRow) LayoutInflater.from(getContext())
                .inflate(R.layout.table_row, mTableLayout, false);
    }

    protected RadioButton getRadioButton() {
        return (RadioButton) LayoutInflater.from(getContext())
                .inflate(R.layout.radio_button, null);
    }

    private void arrangeButtons() {
        // iterates over each button and puts it in the right place
        for (int i = 0, len = mRadioButtons.size(); i < len; i++) {
            RadioButton radioButtonToPlace = mRadioButtons.get(i);
            int rowToInsert = (mMaxInRow != 0) ? i / mMaxInRow : 0;
            int columnToInsert = (mMaxInRow != 0) ? i % mMaxInRow : i;
            // gets the row to insert. if there is no row create one
            TableRow tableRowToInsert = (mTableLayout.getChildCount() <= rowToInsert)
                    ? addTableRow() : (TableRow) mTableLayout.getChildAt(rowToInsert);
            int tableRowChildCount = tableRowToInsert.getChildCount();

            // if there is already a button in the position
            if (tableRowChildCount > columnToInsert) {
                RadioButton currentButton = (RadioButton) tableRowToInsert.getChildAt(columnToInsert);

                // insert the button just if the current button is different
                if (currentButton != radioButtonToPlace) {
                    // removes the current button
                    removeButtonFromParent(currentButton, tableRowToInsert);
                    // removes the button to place from its current position
                    removeButtonFromParent(radioButtonToPlace, (ViewGroup) radioButtonToPlace.getParent());
                    // adds the button to the right place
                    tableRowToInsert.addView(radioButtonToPlace, columnToInsert);
                }

                // if there isn't already a button in the position
            } else {
                // removes the button to place from its current position
                removeButtonFromParent(radioButtonToPlace, (ViewGroup) radioButtonToPlace.getParent());
                // adds the button to the right place
                tableRowToInsert.addView(radioButtonToPlace, columnToInsert);
            }
        }

        removeRedundancies();
    }

    private void removeRedundancies() {
        // the number of rows to fit the buttons
        int rows;
        if (mRadioButtons.size() == 0)
            rows = 0;
        else if (mMaxInRow == 0)
            rows = 1;
        else
            rows = (mRadioButtons.size() - 1) / mMaxInRow + 1;

        int tableChildCount = mTableLayout.getChildCount();
        // if there are redundant rows remove them
        if (tableChildCount > rows)
            mTableLayout.removeViews(rows, tableChildCount - rows);

        tableChildCount = mTableLayout.getChildCount();
        int maxInRow = (mMaxInRow != 0) ? mMaxInRow : mRadioButtons.size();

        // iterates over the rows
        for (int i = 0; i < tableChildCount; i++) {
            TableRow tableRow = (TableRow) mTableLayout.getChildAt(i);
            int tableRowChildCount = tableRow.getChildCount();

            int startIndexToRemove;
            int count;

            // if it is the last row removes all redundancies after the last button in the list
            if (i == tableChildCount - 1) {
                startIndexToRemove = (mRadioButtons.size() - 1) % maxInRow + 1;
                count = tableRowChildCount - startIndexToRemove;

                // if it is not the last row removes all the buttons after maxInRow position
            } else {
                startIndexToRemove = maxInRow;
                count = tableRowChildCount - maxInRow;
            }

            if (count > 0)
                tableRow.removeViews(startIndexToRemove, count);
        }
    }

    // adds and returns a table row
    private TableRow addTableRow() {
        TableRow tableRow = getTableRow();
        mTableLayout.addView(tableRow);
        return tableRow;
    }

    // removes a radio button from a parent
    private void removeButtonFromParent(RadioButton radioButton, ViewGroup parent) {
        if (radioButton == null || parent == null)
            return;

        parent.removeView(radioButton);
    }

    /**
     * Returns the number of radio buttons.
     *
     * @return the number of radio buttons
     */
    public int getRadioButtonCount() {
        return mRadioButtons.size();
    }


    public void setMaxInRow(int maxInRow) {
        if (maxInRow < 0)
            throw new IllegalArgumentException("maxInRow must not be negative: " + maxInRow);
        this.mMaxInRow = maxInRow;
        arrangeButtons();
    }

    public CharSequence getCheckedRadioButtonText() {
        if (mCheckedButton == null)
            return null;

        return mCheckedButton.getText();
    }

    @Override
    public void addView(View child) {
        addView(child, -1, child.getLayoutParams());
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        this.mOnCheckedChangeListener = onCheckedChangeListener;
    }

    public void setOnClickListener(OnClickListener listener) {
        this.mOnClickListener = listener;
    }


    public interface OnCheckedChangeListener {

        /**
         * Called when a radio button is checked.
         *
         * @param group  the group that stores the radio button
         * @param button the radio button that was checked
         */
        void onCheckedChanged(ViewGroup group, RadioButton button);
    }

    /**
     * Interface definition for a callback to be invoked when a radio button is clicked.
     * Clicking a radio button multiple times will result in multiple callbacks.
     */
    public interface OnClickListener {

        /**
         * Called when a radio button is clicked.
         *
         * @param group  the group that stores the radio button
         * @param button the radio button that was clicked
         */
        void onClick(ViewGroup group, RadioButton button);
    }
}
