package com.tiketextra.tiketextra.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.activity.PromoInfoActivity;
import com.tiketextra.tiketextra.activity.SlideInfoActivity;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.ImageUtil;
import com.tiketextra.tiketextra.object.Promo;
import com.tiketextra.tiketextra.object.Slideshow;
import com.google.gson.Gson;

public class SlideshowFragment extends Fragment {

	private static final String ARG_JSON_SLIDE = "json";

	private String jsonSlide;
	private ImageView image;

	public static SlideshowFragment newInstance(String jsonSlide) {
		SlideshowFragment f = new SlideshowFragment();
		Bundle b = new Bundle();
		b.putString(ARG_JSON_SLIDE, jsonSlide);
		f.setArguments(b);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		jsonSlide = getArguments().getString(ARG_JSON_SLIDE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_slideshow,
				container, false);
		image = (ImageView) rootView
				.findViewById(R.id.fragment_slideshow_image);

		final Gson gson = new Gson();
		final Slideshow slide = gson.fromJson(jsonSlide, Slideshow.class);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	if(slide.getKind().equals("slideshow")) {
					Intent intent = new Intent(getActivity(), SlideInfoActivity.class);
					intent.putExtra("slide", jsonSlide);
					startActivity(intent);
				}
				else{
            		Promo promo = new Promo(slide.getKind(), slide.getImage(), slide.getEnTitle(), slide.getInTitle(), "", "", slide.getEnDescription(), slide.getInDescription());
					Intent intent = new Intent(getActivity(), PromoInfoActivity.class);
					intent.putExtra("promo", gson.toJson(promo));
					startActivity(intent);
				}

            }
        });

		ImageUtil.displayImage(image, Configuration.SLIDESHOW_TARGET_URL+slide.getImage(), null);

		ViewCompat.setElevation(rootView, 50);
		return rootView;
	}

}