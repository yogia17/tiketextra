package com.tiketextra.tiketextra.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.custom_view.WagonSeatSelector;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.listener.TrainPassengerSeat;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TrainSelectSeatFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String SEAT_MAP = "seat_map";
    private static final String RESERVATION = "reservation";
    private static final String POSITION = "position";
    private static final String DIRECTION = "direction";
    private static Context context;
    private View rootView;

    private Reservation reservation;
    private JSONObject jsonSeatMap;
    private Passenger passenger;

    private TrainPassengerSeat mListener;
    private CharSequence pressedSeatLetter, currentSeatLetter, pressedWagon, currentWagon;
    private int wagonPosition;

    public TrainSelectSeatFragment() {
        // Required empty public constructor
    }

    public static TrainSelectSeatFragment newInstance(Context mContext, JSONObject jsonSeatMap, Reservation reservation, String direction, int position) {
        Gson gson = new Gson();
        TrainSelectSeatFragment fragment = new TrainSelectSeatFragment();
        Bundle args = new Bundle();
        args.putString(SEAT_MAP, jsonSeatMap.toString());
        args.putString(RESERVATION, gson.toJson(reservation));
        args.putString(DIRECTION, direction);
        args.putInt(POSITION, position);
        fragment.setArguments(args);
        context = mContext;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Gson gson = new Gson();
            try {
                jsonSeatMap = new JSONObject(getArguments().getString(SEAT_MAP));
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            (getArguments().getString(SEAT_MAP), JSONObject.class);
            reservation = gson.fromJson(getArguments().getString(RESERVATION), Reservation.class);
            passenger = reservation.getAdultPassengers().get(getArguments().getInt(POSITION));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_train_select_seat, container, false);
        ((TextView) rootView.findViewById(R.id.textViewPassengerName)).setText(passenger.getName());

        pressedWagon = currentWagon = getArguments().getString(DIRECTION).equals("depart")?passenger.getDepartWagon():passenger.getReturnWagon();
        pressedSeatLetter = currentSeatLetter = getArguments().getString(DIRECTION).equals("depart")?passenger.getDepartSeatLetter():passenger.getReturnSeatLetter();

        AppHelper.selectedSeat[getArguments().getInt(POSITION)] = (String) pressedSeatLetter;
        AppHelper.selectedWagon[getArguments().getInt(POSITION)] = (String) pressedWagon;

        final TextView seatNumberTextView = (TextView) rootView.findViewById(R.id.textViewSeatNumber);
        seatNumberTextView.setText(getArguments().getString(DIRECTION).equals("depart")?passenger.getDepartSeatFormatted():passenger.getReturnSeatFormatted());

        try {
            final JSONArray jsonArrayWagon = jsonSeatMap.getJSONArray("wagon");
            final String[] wagon = new String[jsonArrayWagon.length()];
            wagonPosition = 0;
            for (int i=0; i < jsonArrayWagon.length(); i++){
                JSONObject jWagon = (JSONObject) jsonArrayWagon.get(i);
                wagon[i] = jWagon.getString("wagon_code")+"-"+jWagon.getString("wagon_number");
                if(getArguments().getString(DIRECTION).equals("depart")){
                    if(jWagon.getString("wagon_code").equals(passenger.getDepartWagon())){
                        wagonPosition = i;
                    }
                }
                else {
                    if(jWagon.getString("wagon_code").equals(passenger.getReturnWagon())){
                        wagonPosition = i;
                    }
                }
            }

            ArrayAdapter<String> spinnerWagonAdapter = new ArrayAdapter<>(context, R.layout.wagon_spinner_item, wagon);
            spinnerWagonAdapter.setDropDownViewResource(R.layout.wagon_spinner_dropdown_item);

            final Spinner wagonSpinner = (Spinner) rootView.findViewById(R.id.spinnerWagon);
            wagonSpinner.setAdapter(spinnerWagonAdapter);
            wagonSpinner.setSelection(wagonPosition);

            final WagonSeatSelector wagonSeatSelector = rootView.findViewById(R.id.layoutWagonSeat);

            final JSONArray jsonArrayMap = jsonSeatMap.getJSONArray("map");
            JSONObject jWagonMap = (JSONObject) jsonArrayMap.get(wagonPosition);
            JSONArray jSeat = jWagonMap.getJSONArray("seat");

            wagonSeatSelector.setMaxInRow(jWagonMap.getInt("column_per_row"));

            for (int i=0; i < jSeat.length(); i++){
                JSONArray jRowSeat = (JSONArray) jSeat.get(i);
                for (int j=0; j < jRowSeat.length(); j++){
                    JSONObject seat = (JSONObject) jRowSeat.get(j);
                    if(seat.has("void")){
                        wagonSeatSelector.addVoidRadioButton();
                    }
                    else{
                        if(seat.getInt("assigned") == 1){
                            wagonSeatSelector.addActiveRadioButton(seat.getString("seat_code"));
                        }
                        else {
                            if(seat.getInt("booked") == 0){
                                wagonSeatSelector.addEnabledRadioButton(seat.getString("seat_code"));
                            }
                            else {
                                wagonSeatSelector.addDisabledRadioButton(seat.getString("seat_code"));
                            }
                        }
                    }
                }
            }



            wagonSeatSelector.setOnCheckedChangeListener(new WagonSeatSelector.OnCheckedChangeListener(){

                @Override
                public void onCheckedChanged(ViewGroup group, RadioButton button) {
                    if(pressedWagon.equals(wagon[wagonSpinner.getSelectedItemPosition()])){

                        if(currentWagon.equals(wagon[wagonSpinner.getSelectedItemPosition()]) && currentSeatLetter.equals(button.getText()) && (wagonSeatSelector.getCheckedRadioButtonText() == null || wagonSeatSelector.getCheckedRadioButtonText().equals(button.getText()))){
                            button.setBackgroundColor(getResources().getColor(R.color.colorAccentLight));
                            button.setTypeface(null, Typeface.BOLD);
                        }
                        else {
                            button.setBackgroundColor(getResources().getColor(R.color.green_100));
                            button.setTypeface(null, Typeface.BOLD);
                        }


                        RadioButton prev = wagonSeatSelector.getRadioButton(pressedSeatLetter);
                        if(pressedWagon.equals(currentWagon) && pressedSeatLetter.equals(currentSeatLetter)) {
                            prev.setBackgroundColor(getResources().getColor(R.color.colorAccentLight));
                            prev.setTypeface(null, Typeface.BOLD);
                        }
                        else {
                            prev.setBackgroundColor(getResources().getColor(R.color.white));
                            prev.setTypeface(null, Typeface.NORMAL);
                        }
                    }
                    else {

                        button.setBackgroundColor(getResources().getColor(R.color.green_100));
                        button.setTypeface(null, Typeface.BOLD);
                    }
                    pressedWagon = wagon[wagonSpinner.getSelectedItemPosition()];
                    pressedSeatLetter = button.getText();

                    wagonPosition = wagonSpinner.getSelectedItemPosition();
                    seatNumberTextView.setText(pressedWagon + " / " + pressedSeatLetter);


                    AppHelper.selectedSeat[getArguments().getInt(POSITION)] = (String) pressedSeatLetter;
                    AppHelper.selectedWagon[getArguments().getInt(POSITION)] = (String) pressedWagon;
                }
            });

            wagonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        JSONObject jWagonMap = (JSONObject) jsonArrayMap.get(position);
                        JSONArray jSeat = jWagonMap.getJSONArray("seat");

                        wagonSeatSelector.removeAllButtons();

                        wagonSeatSelector.setMaxInRow(jWagonMap.getInt("column_per_row"));

                        for (int i = 0; i < jSeat.length(); i++) {
                            JSONArray jRowSeat = (JSONArray) jSeat.get(i);
                            for (int j = 0; j < jRowSeat.length(); j++) {
                                JSONObject seat = (JSONObject) jRowSeat.get(j);
                                if (seat.has("void")) {
                                    wagonSeatSelector.addVoidRadioButton();
                                } else {
                                    if (seat.getInt("assigned") == 1) {
                                        wagonSeatSelector.addActiveRadioButton(seat.getString("seat_code"));
                                    } else {
                                        if (seat.getInt("booked") == 0) {
                                            wagonSeatSelector.addEnabledRadioButton(seat.getString("seat_code"));
                                        } else {
                                            wagonSeatSelector.addDisabledRadioButton(seat.getString("seat_code"));
                                        }
                                    }
                                }
                            }
                        }
                        if(wagonPosition == position) {
                            wagonSeatSelector.check(pressedSeatLetter);
                            if(!(pressedWagon.equals(currentWagon) && pressedSeatLetter.equals(currentSeatLetter))) {
                                RadioButton button = wagonSeatSelector.getRadioButton(pressedSeatLetter);
                                button.setBackgroundColor(getResources().getColor(R.color.green_100));
                                button.setTypeface(null, Typeface.BOLD);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            if(reservation.isReturn() && getArguments().getString(DIRECTION).equals("depart")) {
                rootView.findViewById(R.id.returnSeatButton).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.returnSeatButton).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            AppHelper.selectedSeat[getArguments().getInt(POSITION)] = (String) pressedSeatLetter;
                            AppHelper.selectedWagon[getArguments().getInt(POSITION)] = (String) pressedWagon;
                            mListener.onSubmitDepartSeat();
                        }
                    }
                });
            }

            if(reservation.isReturn() && getArguments().getString(DIRECTION).equals("return")) {
                rootView.findViewById(R.id.submitButton).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.submitButton).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            AppHelper.selectedSeat[getArguments().getInt(POSITION)] = (String) pressedSeatLetter;
                            AppHelper.selectedWagon[getArguments().getInt(POSITION)] = (String) pressedWagon;
                            mListener.onSubmitReturnSeat();
                        }
                    }
                });
            }

            if(!reservation.isReturn()) {
                rootView.findViewById(R.id.submitButton).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.submitButton).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            AppHelper.selectedSeat[getArguments().getInt(POSITION)] = (String) pressedSeatLetter;
                            AppHelper.selectedWagon[getArguments().getInt(POSITION)] = (String) pressedWagon;
                            mListener.onSubmitDepartOnlySeat();
                        }
                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TrainPassengerSeat) {
            mListener = (TrainPassengerSeat) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
