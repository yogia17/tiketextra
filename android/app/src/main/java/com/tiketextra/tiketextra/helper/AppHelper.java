package com.tiketextra.tiketextra.helper;

import com.tiketextra.tiketextra.config.Configuration;

import java.util.ArrayList;


public class AppHelper {

    public static String mode = Configuration.MODE_DEPART;

    public static int departSort = 0; // [0 - 3]
    public static int returnSort = 0; // [0 - 3]

    public static ArrayList<String> departFilterAirline, returnFilterAirline;
    public static ArrayList<String> departSelectedAirline, returnSelectedAirline;
    public static ArrayList<String> departSelectedAirlineAtFlightForm, returnSelectedAirlineAtFlightForm;

    public static ArrayList<String> departFilterClass, returnFilterClass;
    public static ArrayList<String> departSelectedClass, returnSelectedClass;

    public static ArrayList<String> departFilterTransitNum, returnFilterTransitNum;
    public static ArrayList<String> departSelectedTransitNum, returnSelectedTransitNum;




    public static int departPriceMin = 0, departPriceMax = 0, departSelectedPriceMin = 0, departSelectedPriceMax = 0;
    public static int returnPriceMin = 0, returnPriceMax = 0, returnSelectedPriceMin = 0, returnSelectedPriceMax = 0;


    public static String departChoice;


    public static String[] selectedSeat, selectedWagon;

    public static String hotelFilterName = "";

    public static int hotelSort = 0;
    public static int hotelPriceMin = 0, hotelPriceMax = 0, hotelSelectedPriceMin = 0, hotelSelectedPriceMax = 0;
    public static ArrayList<String> hotelFilterRating, hotelSelectedRating;
}