package com.tiketextra.tiketextra.helper;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.tiketextra.tiketextra.config.Configuration;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class CustomRequest {

	public static JsonObjectRequest post(String path, final Map<String, String> params, Listener<JSONObject> reponseListener, ErrorListener errorListener){
		params.put("client_username", Configuration.CLIENT_USERNAME);
		params.put("client_password", Configuration.CLIENT_PASSWORD);
		Log.e("CEMBELIQ", Configuration.CLIENT_USERNAME);
		JsonObjectRequest ret = new JsonObjectRequest(Method.POST, Configuration.TARGET_URL + path, new JSONObject(params), reponseListener, errorListener);
		ret.setRetryPolicy(new DefaultRetryPolicy(Configuration.SOCKET_TIMEOUT, Configuration.DEFAULT_MAX_RETRIES, Configuration.DEFAULT_BACKOFF_MULT));
		return ret;
	}

	public static JsonObjectRequest get(String path, Listener<JSONObject> reponseListener, ErrorListener errorListener){
		Map<String, String> params = new HashMap<>();
		params.put("client_username", Configuration.CLIENT_USERNAME);
		params.put("client_password", Configuration.CLIENT_PASSWORD);
		JsonObjectRequest ret = new JsonObjectRequest(Method.POST, Configuration.TARGET_URL + path, new JSONObject(params), reponseListener, errorListener);
		ret.setRetryPolicy(new DefaultRetryPolicy(Configuration.SOCKET_TIMEOUT, Configuration.DEFAULT_MAX_RETRIES, Configuration.DEFAULT_BACKOFF_MULT));
		return ret;
	}

}