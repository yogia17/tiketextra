package com.tiketextra.tiketextra.helper;

import android.os.AsyncTask;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by kurnia on 17/11/17.
 */

public class DownloadFile extends AsyncTask<String, Void, Void> {

    private File filePdf, filePdfRename;

    private void initiate(){
        boolean success = true;
        File folder = new File(Configuration.APP_FOLDER);
        if(!folder.exists()){
            success = folder.mkdir();
            Log.e("filePdf folder", Configuration.APP_FOLDER);
        }
        if(success){
            File folder_child = new File(Configuration.INVOICE_FOLDER);
            if(!folder_child.exists()){
                folder_child.mkdir();

            }
        }
        if(success){
            File folder_child = new File(Configuration.TICKET_FOLDER);
            if(!folder_child.exists()){
                folder_child.mkdir();
            }
        }

    }
    @Override
    protected Void doInBackground(String... strings) {
        String fileUrl = strings[0];
        String fileName = strings[1];
        String extStorageDirectory = strings[2];

        this.initiate();

        File folder = new File(extStorageDirectory);

        filePdf = new File(folder, fileName);
        filePdfRename = new File(folder, "ticket-"+fileName);

        try{
            filePdf.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }
        new FileDownloader().downloadFile(fileUrl, filePdf);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        filePdf.renameTo(filePdfRename);
    }

    private class FileDownloader {
        private static final int  MEGABYTE = 1024 * 1024;

        public void downloadFile(String fileUrl, File directory){
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                //urlConnection.setRequestMethod("GET");
                //urlConnection.setDoOutput(true);
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(directory);
                int totalSize = urlConnection.getContentLength();

                byte[] buffer = new byte[MEGABYTE];
                int bufferLength = 0;
                while((bufferLength = inputStream.read(buffer))>0 ){
                    fileOutputStream.write(buffer, 0, bufferLength);
                }
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
