package com.tiketextra.tiketextra.helper;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.model.ReservationModel;
import com.tiketextra.tiketextra.object.Country;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kurnia on 04/05/16.
 */
public class ItineraryHelper {


    public static int[] getDomesticOnewayPassengerData(String departCarrier, String passengerType){
        return Configuration.PASSENGER_DATA_LOCAL[ItineraryHelper.getCarrierIndex(departCarrier)][ItineraryHelper.getPassengerTypeIndex(passengerType)];
    }


    public static int[] getInternationalOnewayPassengerData(String departCarrier, String passengerType){
        return Configuration.PASSENGER_DATA_INTERNATIONAL[ItineraryHelper.getCarrierIndex(departCarrier)][ItineraryHelper.getPassengerTypeIndex(passengerType)];
    }


    public static int[] getDomesticRoundtripPassengerData(String departCarrier, String returnCarrier, String passengerType){
        int[] departField = Configuration.PASSENGER_DATA_LOCAL[ItineraryHelper.getCarrierIndex(departCarrier)][ItineraryHelper.getPassengerTypeIndex(passengerType)];
        int[] returnField = Configuration.PASSENGER_DATA_LOCAL[ItineraryHelper.getCarrierIndex(returnCarrier)][ItineraryHelper.getPassengerTypeIndex(passengerType)];
        int[] field = new int[departField.length];
        for(int i=0; i<departField.length; i++){
            field[i] = (departField[i]+returnField[i])>0?1:0;
        }
        return field;
    }


    public static int[] getInternationalRoundtripPassengerData(String departCarrier, String returnCarrier, String passengerType){
        int[] departField = Configuration.PASSENGER_DATA_INTERNATIONAL[ItineraryHelper.getCarrierIndex(departCarrier)][ItineraryHelper.getPassengerTypeIndex(passengerType)];
        int[] returnField = Configuration.PASSENGER_DATA_INTERNATIONAL[ItineraryHelper.getCarrierIndex(returnCarrier)][ItineraryHelper.getPassengerTypeIndex(passengerType)];
        int[] field = new int[departField.length];
        for(int i=0; i<departField.length; i++){
            field[i] = (departField[i]+returnField[i])>0?1:0;
        }
        return field;
    }

    private static int getCarrierIndex(String carrierID) {
        if (carrierID.equals("LIO")) {
            return 0;
        } else if (carrierID.equals("SRI")) {
            return 1;
        } else if (carrierID.equals("GAR")) {
            return 2;
        } else if (carrierID.equals("AIR")) {
            return 3;
        } else if (carrierID.equals("CIT")) {
            return 4;
        } else if (carrierID.equals("KAL")) {
            return 5;
        } else if (carrierID.equals("TRI")) {
            return 6;
        } else if (carrierID.equals("EXP")) {
            return 7;
        } else {
            return 8;
        }
    }

    private static int getPassengerTypeIndex(String passengerType){
        if(passengerType.equals("adult")){
            return 0;
        }
        else if(passengerType.equals("child")){
            return 1;
        }
        else{
            return 2;
        }
    }

    public static String[] getNationality(ArrayList<Country> countries){
        String[] ret = new String[countries.size()];
        int j=-1;
        for (int i=0; i<countries.size(); i++){
            if((countries.get(i).getCountryCode()).equals("ID")){
                ret[0] = countries.get(i).getCountry();
                j = i;
            }
            else {
                if(j==-1) {
                    ret[i + 1] = countries.get(i).getCountry();
                }
                else{
                    ret[i] = countries.get(i).getCountry();
                }

            }
        }
        return ret;
    }

    public static String[] getAdultYearOfBirth(){
        String[] ret = new String[65];
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        for(int i=0; i<65; i++){
            ret[i] = (year-12-i)+"";
        }
        return ret;
    }

    public static String[] getChildYearOfBirth(){
        String[] ret = new String[10];
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        for(int i=0; i<10; i++){
            ret[i] = (year-2-i)+"";
        }
        return ret;
    }

    public static String[] getInfantYearOfBirth(){
        String[] ret = new String[3];
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        for(int i=0; i<3; i++){
            ret[i] = (year-i)+"";
        }
        return ret;
    }

    public static String[] getAdultYearOfBirthUnder17(){
        String[] ret = new String[15];
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        for(int i=0; i<15; i++){
            ret[i] = (year-3-i)+"";
        }
        return ret;
    }

    public static String[] getInfantYearOfBirthTrain(){
        String[] ret = new String[4];
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        for(int i=0; i<4; i++){
            ret[i] = (year-i)+"";
        }
        return ret;
    }

    public static String[] getMonth(Context context){

        String[] months = {
                context.getString(R.string.date_january),
                context.getString(R.string.date_february),
                context.getString(R.string.date_march),
                context.getString(R.string.date_april),
                context.getString(R.string.date_may1),
                context.getString(R.string.date_june),
                context.getString(R.string.date_july),
                context.getString(R.string.date_august),
                context.getString(R.string.date_september),
                context.getString(R.string.date_october),
                context.getString(R.string.date_november),
                context.getString(R.string.date_december)
        };
        return months;
    }

    public static String[] getMonthNumber(Context context){

        String[] months = {
                "[01] "+context.getString(R.string.date_january),
                "[02] "+context.getString(R.string.date_february),
                "[03] "+context.getString(R.string.date_march),
                "[04] "+context.getString(R.string.date_april),
                "[05] "+context.getString(R.string.date_may1),
                "[06] "+context.getString(R.string.date_june),
                "[07] "+context.getString(R.string.date_july),
                "[08] "+context.getString(R.string.date_august),
                "[09] "+context.getString(R.string.date_september),
                "[10] "+context.getString(R.string.date_october),
                "[11] "+context.getString(R.string.date_november),
                "[12] "+context.getString(R.string.date_december)
        };
        return months;
    }

    public static String[] getDay(){
        String[] ret = new String[31];
        for (int i=1; i<=31; i++){
            ret[i-1] = i<10?"0"+i:""+i;
        }
        return ret;
    }

    public static String[] getPassportExpiryYear(){
        String[] ret = new String[20];
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        for(int i=0; i<20; i++){
            ret[i] = (year+i)+"";
        }
        return ret;
    }

    public static String[][] passengerToArray(Map<Integer, Passenger> passengers){
        String[][] ret = new String[passengers.size()][10];
        for (Map.Entry<Integer, Passenger> entry : passengers.entrySet()) {
            ret[entry.getKey()][0] = entry.getValue().getTitle();
            ret[entry.getKey()][1] = entry.getValue().getName();
            ret[entry.getKey()][2] = entry.getValue().getBirthDate();
            ret[entry.getKey()][3] = entry.getValue().getIDCardNumber();
            ret[entry.getKey()][4] = Integer.toString(entry.getValue().getAdultAssocNumber());
            ret[entry.getKey()][5] = entry.getValue().getGender();
            ret[entry.getKey()][6] = entry.getValue().getNationality();
            ret[entry.getKey()][7] = entry.getValue().getPassportNumber();
            ret[entry.getKey()][8] = entry.getValue().getPassportIssuingCountry();
            ret[entry.getKey()][9] = entry.getValue().getPassportExpiryDate();
        }
//        for(int i=0; i<passengers.size(); i++){
//            ret[i][0] = passengers.get(i).getTitle();
//            ret[i][1] = passengers.get(i).getName();
//            ret[i][2] = passengers.get(i).getBirthDate();
//            ret[i][3] = passengers.get(i).getIDCardNumber();
//            ret[i][4] = Integer.toString(passengers.get(i).getAdultAssocNumber());
//            ret[i][5] = passengers.get(i).getGender();
//            ret[i][6] = passengers.get(i).getNationality();
//            ret[i][7] = passengers.get(i).getPassportNumber();
//            ret[i][8] = passengers.get(i).getPassportIssuingCountry();
//            ret[i][9] = passengers.get(i).getPassportExpiryDate();
//        }


        return ret;
    }

    public static String decodeAdultTitle(String systemTitle, Context context){
        if(systemTitle.equals("Mr")){
            return context.getString(R.string.adult_mr);
        }
        else if(systemTitle.equals("Mrs")){
            return context.getString(R.string.adult_mrs);
        }
        else {
            return context.getString(R.string.adult_ms);
        }
    }

    public static String encodeAdultTitle(String plainTitle, Context context){
        if(plainTitle.equals(context.getString(R.string.adult_mr))){
            return "Mr";
        }
        else if(plainTitle.equals(context.getString(R.string.adult_mrs))){
            return "Mrs";
        }
        else {
            return "Ms";
        }
    }

    public static String decodeChildTitle(String systemTitle, Context context){
        if(systemTitle.equals("Mstr")){
            return context.getString(R.string.child_mstr);
        }
        else {
            return context.getString(R.string.child_miss);
        }
    }

    public static String encodeChildTitle(String plainTitle, Context context){
        if(plainTitle.equals(context.getString(R.string.child_mstr))){
            return "Mstr";
        }
        else {
            return "Miss";
        }
    }

    public static String decodeInfantTitle(String systemTitle, Context context){
        if(systemTitle.equals("Mstr")){
            return context.getString(R.string.infant_mstr);
        }
        else {
            return context.getString(R.string.infant_miss);
        }
    }

    public static String encodeInfantTitle(String plainTitle, Context context){
        if(plainTitle.equals(context.getString(R.string.infant_mstr))){
            return "Mstr";
        }
        else {
            return "Miss";
        }
    }



    public static String[] getAdultTitle(Context context){
        String[] ret = {
                context.getString(R.string.choose),
                context.getString(R.string.adult_mr),
                context.getString(R.string.adult_mrs),
                context.getString(R.string.adult_ms)

                /*, "Miss", "Dr"*/};
        return ret;
    }

    public static int gatAdultTitleIdx(String title, Context context){
        int index = 0;
        String[] titleArr = ItineraryHelper.getAdultTitle(context);
        for (int i=0;i<titleArr.length;i++) {
            if (titleArr[i].equals(ItineraryHelper.decodeAdultTitle(title, context))) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static String[] getChildTitle(Context context){
        String[] ret = {
                context.getString(R.string.choose),
                context.getString(R.string.child_mstr),
                context.getString(R.string.child_miss)
        };
        return ret;
    }

    public static String[] getInfantTitle(Context context){
        String[] ret = {
                context.getString(R.string.choose),
                context.getString(R.string.infant_mstr),
                context.getString(R.string.infant_miss)
        };
        return ret;
    }

    public static void checkReservation(final Context context, int reservationID){
        final ReservationModel reservationModel = new ReservationModel(context);
        Map<String, String> params = new HashMap<>();
        params.put("reservation_id", Integer.toString(reservationID));
        JsonObjectRequest jsonReq = new CustomRequest().post(Configuration.WEB_SERVICE_CONTROLLER + "reservation", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("resp", response.toString());

                try {
                    Gson gson = new Gson();
                    Reservation reservation = gson.fromJson(response.getString("reservation"), Reservation.class);
                    reservationModel.updateReservationObject(reservation);
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                VolleyLog.d("Error: ", arg0.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonReq);
    }
}
