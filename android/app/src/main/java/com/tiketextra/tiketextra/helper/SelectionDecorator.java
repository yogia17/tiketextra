package com.tiketextra.tiketextra.helper;

import android.content.Context;

import com.tiketextra.tiketextra.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.HashSet;

public class SelectionDecorator implements DayViewDecorator {

//    public static final int SELECTED_DECORATOR = 0;
//    public static final int CIRCLE_DECORATOR = 2;


    public static final int RANGE_DECORATOR = 1;
    public static final int LEFT_DECORATOR = 3;
    public static final int RIGHT_DECORATOR = 4;
    private final int type;
    private final HashSet<CalendarDay> dates;
    private final Context context;

    public SelectionDecorator(Context context, int type, ArrayList<CalendarDay> dates) {
        this.context = context;
        this.type = type;
        this.dates = new HashSet<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        if (type == RANGE_DECORATOR) {
            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.circle_accent_light));
        }
//        else if (type == SELECTED_DECORATOR) {
//            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.circle_blue_tile));
//        }
//        else if (type == CIRCLE_DECORATOR) {
//            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.circle_transparent_blue_tile));
//        }
        else if (type == LEFT_DECORATOR) {
            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.circle_accent));
        }else if (type == RIGHT_DECORATOR) {
            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.circle_accent_dark));
        }
    }
}