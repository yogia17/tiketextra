package com.tiketextra.tiketextra.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.tiketextra.tiketextra.activity.MainActivity;
import com.tiketextra.tiketextra.activity.user.LoginActivity;

import java.util.HashMap;

public class SessionManager {
	SharedPreferences pref;

	Editor editor;
	Context context;
	
	// Shared pref mode
	int PRIVATE_MODE = 0;

	private static final String PREF_NAME = "LoginPref";

	private static final String IS_LOGIN = "IsLoggedIn";
	private static final String ACTIVATION = "Activation";

    public static final String KEY_EMAIL = "email";
    public static final String KEY_TITLE = "title";
    public static final String KEY_NAME = "name";
    public static final String KEY_MOBILE = "mobile";

	public SessionManager(Context context){
		this.context = context;
		pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void createActivationSession(String title, String name, String mobile, String email){
		editor.putBoolean(IS_LOGIN, false);
		editor.putBoolean(ACTIVATION, true);
		editor.putString(KEY_TITLE, title);
		editor.putString(KEY_NAME, name);
		editor.putString(KEY_MOBILE, mobile);
		editor.putString(KEY_EMAIL, email);
		editor.commit();
	}

	public boolean inActivation(){
		return pref.getBoolean(ACTIVATION, false);
	}

	public void createLoginSession(String title, String name, String mobile, String email){
		// set login value as TRUE
		editor.putBoolean(IS_LOGIN, true);
		editor.putBoolean(ACTIVATION, false);
		editor.putString(KEY_TITLE, title);
		editor.putString(KEY_NAME, name);
		editor.putString(KEY_MOBILE, mobile);
		editor.putString(KEY_EMAIL, email);
		editor.commit();
	}

	public void updateSession(String title, String name, String mobile, String email){
		editor.putString(KEY_TITLE, title);
		editor.putString(KEY_NAME, name);
		editor.putString(KEY_MOBILE, mobile);
		editor.putString(KEY_EMAIL, email);
		editor.apply();
	}

	public void checkLogin(){
		if(!this.isLoggedIn()){
			Intent i = new Intent(context, LoginActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}
	}

	public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<>();
		user.put(KEY_TITLE, pref.getString(KEY_TITLE, null));
		user.put(KEY_NAME, pref.getString(KEY_NAME, null));
		user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, null));
		user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
		return user;
	}

	public void logoutUser(){
		editor.clear();
		editor.commit();
		Intent i = new Intent(context, LoginActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}

	public void logout(){
		editor.clear();
		editor.commit();
		Intent i = new Intent(context, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}

	public boolean isLoggedIn(){
		return pref.getBoolean(IS_LOGIN, false);
	}
}
