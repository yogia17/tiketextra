package com.tiketextra.tiketextra.listener;

import com.tiketextra.tiketextra.object.Bank;

/**
 * Created by kurnia on 16/10/17.
 */

public interface BankItemListener {
    void onItemClick(Bank item);
}
