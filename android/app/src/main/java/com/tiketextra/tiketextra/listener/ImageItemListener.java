package com.tiketextra.tiketextra.listener;

import com.tiketextra.tiketextra.object.Image;

/**
 * Created by kurnia on 16/10/17.
 */

public interface ImageItemListener {
    void onItemClick(Image item);
}
