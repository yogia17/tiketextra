package com.tiketextra.tiketextra.listener;

import android.view.View;

/**
 * Created by kurnia on 04/09/17.
 */

public interface OnViewClickListener {
    void setOnViewClickListener(View view, int position);
    void setOnViewClickListener(View view, int position, String mode);
}
