package com.tiketextra.tiketextra.listener;

import com.tiketextra.tiketextra.object.Reservation;

/**
 * Created by kurnia on 26/12/17.
 */

public interface ReservationItemListener {
    void onItemClick(Reservation item);
}
