package com.tiketextra.tiketextra.listener;

import com.tiketextra.tiketextra.object.Room;

/**
 * Created by kurnia on 16/10/17.
 */

public interface RoomItemListener {
    void onItemClick(Room item);
}
