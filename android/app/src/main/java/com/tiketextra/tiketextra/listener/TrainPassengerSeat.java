package com.tiketextra.tiketextra.listener;

import com.tiketextra.tiketextra.object.Passenger;

import java.util.ArrayList;

/**
 * Created by kurnia on 26/09/17.
 */

public interface TrainPassengerSeat {
    public void onSubmitDepartSeat();
    public void onSubmitDepartOnlySeat();
    public void onSubmitReturnSeat();
}
