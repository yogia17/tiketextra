package com.tiketextra.tiketextra.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Airline;
import com.tiketextra.tiketextra.object.Carrier;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kurnia on 16/04/16.
 */
public class AirlineModel {
    private final String TAG = "AirlineModel";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public AirlineModel(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
    }

    private AirlineModel createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    private AirlineModel open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private AirlineModel openWrite() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getWritableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private void close()
    {
        mDb.close();
        mDbHelper.close();
    }

    public Airline getAirline(String flightCode) {

        String selectQuery = "SELECT " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_FLIGHT_NUMBER + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_CARRIER_ID + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_NAME + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_MEAL + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_AIRPORT_TAX + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_DOMESTIC_BAGGAGE + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_INTERNATIONAL_BAGGAGE
                + " FROM " + Configuration.TABLE_AIRLINE
                + " WHERE " + Configuration.FIELD_FLIGHT_NUMBER + " = '" + flightCode + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        Airline ret = new Airline(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6));
        cursor.close();

        this.close();

        return ret;
    }



    public ArrayList<Airline> getAirlines(ArrayList<String> flightCodes){
        ArrayList<Airline> airlines = new ArrayList<>();

        String fCode = "";
        for (int i=0; i<flightCodes.size(); i++){
            if(i==0){
                fCode += "'"+flightCodes.get(i)+"'";
            }
            else{
                fCode += ", '"+flightCodes.get(i)+"'";
            }
        }



        String selectQuery = "SELECT " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_FLIGHT_NUMBER + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_CARRIER_ID + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_NAME + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_MEAL + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_AIRPORT_TAX + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_DOMESTIC_BAGGAGE + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_INTERNATIONAL_BAGGAGE
                + " FROM " + Configuration.TABLE_AIRLINE
                + " WHERE " + Configuration.FIELD_FLIGHT_NUMBER + " IN (" + fCode + ") ORDER BY " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_NAME;

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                airlines.add(new Airline(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return airlines;

    }

    public ArrayList<Airline> getAirlineExcludeCarrierID(String carrierID){
        ArrayList<Airline> airlines = new ArrayList<>();

        String selectQuery = "SELECT " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_FLIGHT_NUMBER + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_CARRIER_ID + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_NAME + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_MEAL + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_AIRPORT_TAX + ", "
                + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_DOMESTIC_BAGGAGE + ", " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_INTERNATIONAL_BAGGAGE
                + " FROM " + Configuration.TABLE_AIRLINE
                + " WHERE " + Configuration.FIELD_CARRIER_ID + "='"+carrierID+"'"+" ORDER BY " + Configuration.TABLE_AIRLINE + "." + Configuration.FIELD_NAME;

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                airlines.add(new Airline(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return airlines;

    }


    public ArrayList<Carrier> getAllAirlineCarriers() {
        ArrayList<Carrier> countries = new ArrayList<>();

        String selectQuery = "SELECT "
                + Configuration.FIELD_CARRIER_ID + ", "
                + Configuration.FIELD_NAME
                + " FROM " + Configuration.TABLE_CARRIER
                + " WHERE " + Configuration.FIELD_CARRIER_TYPE + "='flight' AND " + Configuration.FIELD_IS_ACTIVE + "='Y'"
                + " ORDER BY "
                + Configuration.FIELD_NAME;
        this.createDatabase();
        this.open();
        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                countries.add(new Carrier(cursor.getString(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return countries;
    }

    public Carrier getCarrierByName(String carrier) {

        String selectQuery = "SELECT "
                + Configuration.FIELD_CARRIER_ID + ", "
                + Configuration.FIELD_NAME
                + " FROM " + Configuration.TABLE_CARRIER
                + " WHERE " + Configuration.FIELD_NAME + " = '" + carrier + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        Carrier ret = new Carrier(cursor.getString(0), cursor.getString(1));
        cursor.close();

        this.close();

        return ret;
    }






}
