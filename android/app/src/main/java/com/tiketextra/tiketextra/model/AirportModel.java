package com.tiketextra.tiketextra.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Airport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kurnia on 16/04/16.
 */
public class AirportModel {
    private final String TAG = "AirportModel";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public AirportModel(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
    }

    private AirportModel createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    private AirportModel open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private AirportModel openWrite() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getWritableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private void close()
    {
        mDb.close();
        mDbHelper.close();
    }

    public boolean isZeroHit(){
        String countQuery = "SELECT SUM(" + Configuration.FIELD_HIT + ") AS count FROM " + Configuration.TABLE_PORT + " WHERE "+Configuration.FIELD_CARRIER_TYPE + "='flight'";
        this.createDatabase();
        this.open();
        Cursor cursor = mDb.rawQuery(countQuery, null);
        cursor.moveToFirst();
        boolean ret = cursor.getInt(0) == 0;
        cursor.close();
        this.close();
        return ret;
    }

    public Airport getAirport(String iataCode) {

        String selectQuery = "SELECT " + Configuration.TABLE_PORT + "." + Configuration.FIELD_IATA_CODE + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_NAME + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_NAME_ALT + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_CITY + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_CITY_ALT + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_REGION + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_COUNTRY_CODE + ", " + Configuration.TABLE_COUNTRY + "."+ Configuration.FIELD_COUNTRY + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_LATITUDE + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_LONGITUDE + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_TIMEZONE + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_TZ + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_IS_ACTIVE + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_HIT
                + " FROM " + Configuration.TABLE_PORT
                + " JOIN " + Configuration.TABLE_COUNTRY + " ON " + Configuration.TABLE_COUNTRY + "." + Configuration.FIELD_COUNTRY_CODE + " = " + Configuration.TABLE_PORT + "." + Configuration.FIELD_COUNTRY_CODE
                + " WHERE " + Configuration.FIELD_IATA_CODE + " = '" + iataCode + "'"
                + " AND " + Configuration.FIELD_IS_ACTIVE + "='Y' AND "+Configuration.FIELD_CARRIER_TYPE + "='flight'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        Log.e("cembeliq", "Jumlah :"+cursor.getCount());
        cursor.moveToFirst();
        Airport ret = new Airport(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(6), cursor.getString(7), cursor.getDouble(8), cursor.getDouble(9), cursor.getDouble(10), cursor.getInt(13));

        cursor.close();

        this.close();

        return ret;
    }





    public boolean existAirport(String iataCode){
        String countQuery = "SELECT * FROM " + Configuration.TABLE_PORT
                + " WHERE " + Configuration.FIELD_IATA_CODE + "='"
                + iataCode + "'"
                + " AND " + Configuration.FIELD_IS_ACTIVE + "='Y' AND "+Configuration.FIELD_CARRIER_TYPE + "='flight'";
        this.createDatabase();
        this.open();
        Cursor cursor = mDb.rawQuery(countQuery, null);
        boolean ret = cursor.getCount() > 0;
        cursor.close();
        this.close();
        return ret;
    }

    public ArrayList<Airport> getAllAirports(String order) {
        ArrayList<Airport> port = new ArrayList<>();

        String selectQuery = "SELECT " + Configuration.TABLE_PORT + "." + Configuration.FIELD_IATA_CODE + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_NAME + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_NAME_ALT + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_CITY + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_CITY_ALT + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_REGION + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_COUNTRY_CODE + ", " + Configuration.TABLE_COUNTRY + "."+ Configuration.FIELD_COUNTRY + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_LATITUDE + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_LONGITUDE + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_TIMEZONE + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_TZ + ", "
                + Configuration.TABLE_PORT + "." + Configuration.FIELD_IS_ACTIVE + ", " + Configuration.TABLE_PORT + "." + Configuration.FIELD_HIT
                + " FROM " + Configuration.TABLE_PORT
                + " JOIN " + Configuration.TABLE_COUNTRY + " ON " + Configuration.TABLE_COUNTRY + "." + Configuration.FIELD_COUNTRY_CODE + " = " + Configuration.TABLE_PORT + "." + Configuration.FIELD_COUNTRY_CODE
                + " WHERE " + Configuration.FIELD_IS_ACTIVE + "='Y' AND "+Configuration.FIELD_CARRIER_TYPE + "='flight' ORDER BY "
                + (order.equals("hit")?Configuration.TABLE_PORT + "."+ Configuration.FIELD_HIT + " DESC, ":"")+"CASE WHEN "+Configuration.TABLE_PORT + "." + Configuration.FIELD_COUNTRY_CODE+" = 'ID' THEN 1 ELSE 2 END, "+Configuration.TABLE_PORT + "."+ Configuration.FIELD_CITY;

        this.createDatabase();
        this.open();
        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                port.add(new Airport(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(6), cursor.getString(7), cursor.getDouble(8), cursor.getDouble(9), cursor.getDouble(10), cursor.getInt(13)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return port;
    }

    public void updateAirportHit(String iataCode, int hit){
        this.createDatabase();
        this.openWrite();
        ContentValues values = new ContentValues();
        values.put(Configuration.FIELD_HIT, hit);
        mDb.update(Configuration.TABLE_PORT, values,
                Configuration.FIELD_IATA_CODE + " = ? AND "+Configuration.FIELD_IS_ACTIVE + " = ? AND "+Configuration.FIELD_CARRIER_TYPE + "= ?",
                new String[] { iataCode, "Y", "flight" });
        this.close();
    }
}
