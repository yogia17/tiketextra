package com.tiketextra.tiketextra.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Contact;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kurnia on 16/04/16.
 */
public class ContactModel {
    private final String TAG = "ContactModel";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public ContactModel(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
    }

    private ContactModel createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    private ContactModel open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private ContactModel openWrite() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getWritableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private void close()
    {
        mDb.close();
        mDbHelper.close();
    }

    public Contact getContactByName(String name) {

        String selectQuery = "SELECT "
                + Configuration.FIELD_CONTACT_ID + ", "
                + Configuration.FIELD_TITLE + ", "
                + Configuration.FIELD_FULL_NAME + ", "
                + Configuration.FIELD_PHONE + ", "
                + Configuration.FIELD_EMAIL
                + " FROM " + Configuration.TABLE_CONTACT
                + " WHERE " + Configuration.FIELD_FULL_NAME + " LIKE '%" + name + "%'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            Contact ret = new Contact(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            cursor.close();

            this.close();

            return ret;
        }
        else{
            return null;
        }
    }

    public Contact getContactByContactID(int id) {

        String selectQuery = "SELECT "
                + Configuration.FIELD_CONTACT_ID + ", "
                + Configuration.FIELD_TITLE + ", "
                + Configuration.FIELD_FULL_NAME + ", "
                + Configuration.FIELD_PHONE + ", "
                + Configuration.FIELD_EMAIL
                + " FROM " + Configuration.TABLE_CONTACT
                + " WHERE " + Configuration.FIELD_CONTACT_ID + " = '" + id + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        Contact ret = new Contact(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
        cursor.close();

        this.close();

        return ret;
    }


    public void insert(String title, String fullName, String phone, String email){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_TITLE, title);
        cv.put(Configuration.FIELD_FULL_NAME, fullName);
        cv.put(Configuration.FIELD_PHONE, phone);
        cv.put(Configuration.FIELD_EMAIL, email);
        mDb.insert(Configuration.TABLE_CONTACT, null, cv);
        this.close();
    }


    public void update(int contactID, String title, String fullName, String phone, String email){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_TITLE, title);
        cv.put(Configuration.FIELD_FULL_NAME, fullName);
        cv.put(Configuration.FIELD_PHONE, phone);
        cv.put(Configuration.FIELD_EMAIL, email);
        mDb.update(Configuration.TABLE_CONTACT, cv, Configuration.FIELD_CONTACT_ID + "= ?", new String[]{Integer.toString(contactID)});
        this.close();
    }

    public ArrayList<Contact> getAllContacts() {
        ArrayList<Contact> contacts = new ArrayList<>();

        String selectQuery = "SELECT "
                + Configuration.FIELD_CONTACT_ID + ", "
                + Configuration.FIELD_TITLE + ", "
                + Configuration.FIELD_FULL_NAME + ", "
                + Configuration.FIELD_PHONE + ", "
                + Configuration.FIELD_EMAIL
                + " FROM " + Configuration.TABLE_CONTACT + " ORDER BY "
                + Configuration.FIELD_FULL_NAME;
        this.createDatabase();
        this.open();
        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                contacts.add(new Contact(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return contacts;
    }
}
