package com.tiketextra.tiketextra.model;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Country;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kurnia on 16/04/16.
 */
public class CountryModel {
    private final String TAG = "CountryModel";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public CountryModel(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
    }

    private CountryModel createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    private CountryModel open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private CountryModel openWrite() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getWritableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private void close()
    {
        mDb.close();
        mDbHelper.close();
    }

    public Country getCountryByCode(String countryCode) {

        String selectQuery = "SELECT "
                + Configuration.FIELD_COUNTRY_CODE + ", "
                + Configuration.FIELD_COUNTRY
                + " FROM " + Configuration.TABLE_COUNTRY
                + " WHERE " + Configuration.FIELD_COUNTRY_CODE + " = '" + countryCode + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        Country ret = new Country(cursor.getString(0), cursor.getString(1));
        cursor.close();

        this.close();

        return ret;
    }

    public Country getCountryByName(String country) {

        String selectQuery = "SELECT "
                + Configuration.FIELD_COUNTRY_CODE + ", "
                + Configuration.FIELD_COUNTRY
                + " FROM " + Configuration.TABLE_COUNTRY
                + " WHERE " + Configuration.FIELD_COUNTRY + " = '" + country + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        Country ret = new Country(cursor.getString(0), cursor.getString(1));
        cursor.close();

        this.close();

        return ret;
    }

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> countries = new ArrayList<>();

        String selectQuery = "SELECT "
                + Configuration.FIELD_COUNTRY_CODE + ", "
                + Configuration.FIELD_COUNTRY
                + " FROM " + Configuration.TABLE_COUNTRY + " ORDER BY "
                + Configuration.FIELD_COUNTRY;
        this.createDatabase();
        this.open();
        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                countries.add(new Country(cursor.getString(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return countries;
    }
}
