package com.tiketextra.tiketextra.model;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by kurnia on 17/04/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private SQLiteDatabase myDatabase;
    private String DB_PATH, DB_NAME;

    private static final int DATABASE_VERSION = Configuration.DATABASE_VERSION;

    private final Context myContext;

    private static final String CREATE_TABLE_PORT =
            "CREATE TABLE "+Configuration.TABLE_PORT +" ("+Configuration.FIELD_IATA_CODE+" TEXT NOT NULL, " +
            Configuration.FIELD_CARRIER_TYPE+" TEXT, " +
            Configuration.FIELD_NAME+" TEXT, " +
            Configuration.FIELD_NAME_ALT+" TEXT, " +
            Configuration.FIELD_CITY+" TEXT, " +
            Configuration.FIELD_CITY_ALT+" TEXT, " +
            Configuration.FIELD_REGION+" TEXT, " +
            Configuration.FIELD_COUNTRY_CODE+" TEXT, " +
            Configuration.FIELD_LATITUDE+" REAL, " +
            Configuration.FIELD_LONGITUDE+" REAL, " +
            Configuration.FIELD_TIMEZONE+" REAL, " +
            Configuration.FIELD_TZ+" TEXT, " +
            Configuration.FIELD_IS_ACTIVE+" TEXT, " +
            Configuration.FIELD_HIT+" INTEGER, PRIMARY KEY("+Configuration.FIELD_IATA_CODE+", "+Configuration.FIELD_CARRIER_TYPE+", "+Configuration.FIELD_COUNTRY_CODE+"));";

    private static final String MIGRATE_TABLE =
            "INSERT INTO "+Configuration.TABLE_PORT +" ("+Configuration.FIELD_IATA_CODE+", " +
            Configuration.FIELD_CARRIER_TYPE+", " +
            Configuration.FIELD_NAME+", " +
            Configuration.FIELD_NAME_ALT+", " +
            Configuration.FIELD_CITY+", " +
            Configuration.FIELD_CITY_ALT+", " +
            Configuration.FIELD_REGION+", " +
            Configuration.FIELD_COUNTRY_CODE+", " +
            Configuration.FIELD_LATITUDE+", " +
            Configuration.FIELD_LONGITUDE+", " +
            Configuration.FIELD_TIMEZONE+", " +
            Configuration.FIELD_TZ+", " +
            Configuration.FIELD_IS_ACTIVE+", " +
            Configuration.FIELD_HIT+") SELECT "+
                    Configuration.FIELD_IATA_CODE+", " +
                    "'flight', " +
                    Configuration.FIELD_NAME+", " +
                    Configuration.FIELD_NAME_ALT+", " +
                    Configuration.FIELD_CITY+", " +
                    Configuration.FIELD_CITY_ALT+", " +
                    Configuration.FIELD_REGION+", " +
                    Configuration.FIELD_COUNTRY_CODE+", " +
                    Configuration.FIELD_LATITUDE+", " +
                    Configuration.FIELD_LONGITUDE+", " +
                    Configuration.FIELD_TIMEZONE+", " +
                    Configuration.FIELD_TZ+", " +
                    Configuration.FIELD_IS_ACTIVE+", " +
                    Configuration.FIELD_HIT+" FROM airport;";

    private static final String DROP_TABLE_AIRPORT = "DROP TABLE airport;";


//    private static final String UPDATE_ROW_SETTING_OPEN_FIRST_1 = "UPDATE "+Configuration.TABLE_SETTING+" SET "+Configuration.FIELD_VALUE + "='1' WHERE "+Configuration.FIELD_PARAMETER +"='open_first';";
    private static final String INSERT_TRAIN_SEARCH_FROM_PORT = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('train_search_from_port', 'YK')";
    private static final String INSERT_TRAIN_SEARCH_TO_PORT = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('train_search_to_port', 'GMR')";
    private static final String INSERT_TRAIN_SEARCH_TIMESTAMP = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('train_search_timestamp', NULL)";

    private static final String ALTER_TABLE_PASSENGER_DEPART_SEAT = "ALTER TABLE "+Configuration.TABLE_PASSENGER+" ADD COLUMN "+Configuration.FIELD_DEPART_SEAT+" TEXT;";
    private static final String ALTER_TABLE_PASSENGER_RETURN_SEAT = "ALTER TABLE "+Configuration.TABLE_PASSENGER+" ADD COLUMN "+Configuration.FIELD_RETURN_SEAT+" TEXT;";

    private static final String INSERT_FLIGHT_FEATURE = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('flight_feature', '1')";
    private static final String INSERT_TRAIN_FEATURE = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('train_feature', '0')";
    private static final String INSERT_HOTEL_FEATURE = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('hotel_feature', '1')";
    private static final String INSERT_TOUR_FEATURE = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('tour_feature', '1')";
    private static final String INSERT_SHIP_FEATURE = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('ship_feature', '0')";
    private static final String INSERT_CARGO_FEATURE = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('cargo_feature', '0')";
    private static final String INSERT_PPOB_FEATURE = "INSERT INTO "+Configuration.TABLE_SETTING +"("+Configuration.FIELD_PARAMETER+", " +Configuration.FIELD_VALUE+") VALUES ('ppob_feature', '0')";


    private static final String ALTER_TABLE_RESERVATION_ADD_COLUMN_DEPART_HIDDEN_TRANSIT = "ALTER TABLE "+ Configuration.TABLE_RESERVATION + " ADD COLUMN " + Configuration.FIELD_DEPART_HIDDEN_TRANSIT + " INTEGER;";
    private static final String ALTER_TABLE_RESERVATION_ADD_COLUMN_RETURN_HIDDEN_TRANSIT = "ALTER TABLE "+ Configuration.TABLE_RESERVATION + " ADD COLUMN " + Configuration.FIELD_RETURN_HIDDEN_TRANSIT + " INTEGER;";


    private static final String CREATE_TABLE_CARRIER = "CREATE TABLE " + Configuration.TABLE_CARRIER+" ("+Configuration.FIELD_CARRIER_ID+" TEXT, " +
            Configuration.FIELD_CARRIER_TYPE+" TEXT, " +
            Configuration.FIELD_NAME+" TEXT, " +
            Configuration.FIELD_IS_ACTIVE+" TEXT, PRIMARY KEY("+Configuration.FIELD_CARRIER_ID+") )";

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     */
    public DatabaseHelper(Context context) {

        super(context, Configuration.DATABASE_NAME, null, DATABASE_VERSION);
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        DB_NAME = Configuration.DATABASE_NAME;
        this.myContext = context;
    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if(dbExist){


        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DB_NAME;
            File file = new File(myPath);
            if (file.exists() && !file.isDirectory()) {
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            }

        }catch(SQLiteException e){

            //database does't exist yet.

        }

        if(checkDB != null){

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    public void openWriteDataBase() throws SQLException {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public synchronized void close() {

        if(myDatabase != null)
            myDatabase.close();

        super.close();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("upgrade db", oldVersion + " to " + newVersion);

//        if(oldVersion >= 2) {
            if (oldVersion < 2) {
                db.execSQL(CREATE_TABLE_PORT);
                db.execSQL(MIGRATE_TABLE);
                db.execSQL(DROP_TABLE_AIRPORT);

                db.execSQL(INSERT_TRAIN_SEARCH_FROM_PORT);
                db.execSQL(INSERT_TRAIN_SEARCH_TO_PORT);
                db.execSQL(INSERT_TRAIN_SEARCH_TIMESTAMP);
                db.execSQL(ALTER_TABLE_PASSENGER_DEPART_SEAT);
                db.execSQL(ALTER_TABLE_PASSENGER_RETURN_SEAT);
                db.execSQL(INSERT_FLIGHT_FEATURE);
                db.execSQL(INSERT_TRAIN_FEATURE);
                db.execSQL(INSERT_HOTEL_FEATURE);
                db.execSQL(INSERT_TOUR_FEATURE);
                db.execSQL(INSERT_SHIP_FEATURE);
                db.execSQL(INSERT_CARGO_FEATURE);
                db.execSQL(INSERT_PPOB_FEATURE);

                db.execSQL(ALTER_TABLE_RESERVATION_ADD_COLUMN_DEPART_HIDDEN_TRANSIT);
                db.execSQL(ALTER_TABLE_RESERVATION_ADD_COLUMN_RETURN_HIDDEN_TRANSIT);
            }

            if (oldVersion < 3) {
                db.execSQL(CREATE_TABLE_CARRIER);

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;

                try {
                    date = format.parse(Utils.getDateTimeNowInUTCPlus7());

                    GregorianCalendar cal = new GregorianCalendar();
                    cal.setTime(date);
                    cal.add(Calendar.MINUTE, -(Configuration.PREFETCH_UPDATE_DURATION_THRESHOLD_IN_MINUTES + 10));

                    db.execSQL("UPDATE "+Configuration.TABLE_SETTING+" SET "+Configuration.FIELD_VALUE + "='"+format.format(cal.getTime())+"' WHERE "+Configuration.FIELD_PARAMETER + "='update_prefetch';");


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            else {
                this.getReadableDatabase();

                try {

                    copyDataBase();

                } catch (IOException e) {

                    throw new Error("Error copying database");

                }
            }

//        }
    }

    // Add your public helper methods to access and get content from the database.
    // You could return cursors by doing "return myDatabase.query(....)" so it'd be easy
    // to you to create adapters for your views.

}