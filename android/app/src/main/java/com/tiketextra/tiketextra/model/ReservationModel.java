package com.tiketextra.tiketextra.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.SessionManager;
import com.tiketextra.tiketextra.util.Utils;
import com.tiketextra.tiketextra.object.Fare;
import com.tiketextra.tiketextra.object.Passenger;
import com.tiketextra.tiketextra.object.Reservation;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kurnia on 8/8/17.
 */

public class ReservationModel {
    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    private final String TAG = "ReservationModel";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;
    private SessionManager sessionManager;

    public ReservationModel(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
        sessionManager = new SessionManager(mContext);
    }

    private ReservationModel createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    private ReservationModel open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private ReservationModel openWrite() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getWritableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private void close()
    {
        mDb.close();
        mDbHelper.close();
    }

    public void insertReservationObject(Reservation reservation){
        this.insertReservation(
                reservation.getReservationID(), reservation.getAmount(), reservation.getBankDiscount(), reservation.getBankSurcharge(), reservation.getUniqueCode(), reservation.getAdultCount(), reservation.getChildCount(), reservation.getInfantCount(), reservation.getDepartHiddenTransit(), reservation.getReturnHiddenTransit(), reservation.getInvoice(), reservation.getSlug(),
                reservation.getCarrierType(), reservation.getBank(), reservation.getIsConfirmed(), reservation.getIsPaid(), reservation.getPaidFrom(), reservation.getPaidTimeStamp(), reservation.getConfirmedPaidTimeStamp(), reservation.getStatus1(), reservation.getStatus2(), reservation.getIsReturn(),
                reservation.getFromPort(), reservation.getToPort(), reservation.getDepartDate(), reservation.getReturnDate(), reservation.getDepartFlight(), reservation.getReturnFlight(), reservation.getContactTitle(), reservation.getContactName(), reservation.getContactPhone(), reservation.getContactEmail(),
                reservation.getCarrierID1(), reservation.getCarrierID2(), reservation.getClass1(), reservation.getClass2(), reservation.getBookingCode1(), reservation.getBookingCode2(), reservation.getTimeBooking1(), reservation.getTimeBooking2(), reservation.getTimeLimit1(), reservation.getTimeLimit2()
        );
    }

    private void insertReservation(int reservationID, int amount, int bankDiscount, int bankSurcharge, int uniqueCode, int adultCount, int childCount, int infantCount, int departHiddenTransit, int returnHiddenTransit, String invoice, String slug,
                                  String carrierType, String bank, String isConfirmed, String isPaid, String paidFrom, String paidTimeStamp, String confirmedPaidTimeStamp, String status1, String status2, String isReturn,
                                  String fromPort, String toPort, String departDate, String returnDate, String departFlight, String returnFlight, String contactTitle, String contactName, String contactPhone, String contactEmail,
                                  String carrierID1, String carrierID2, String class1, String class2, String bookingCode1, String bookingCode2, String timeBooking1, String timeBooking2, String timeLimit1, String timeLimit2) {
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_RESERVATION_ID, reservationID);
        cv.put(Configuration.FIELD_AMOUNT, amount);
        cv.put(Configuration.FIELD_BANK_DISCOUNT, bankDiscount);
        cv.put(Configuration.FIELD_BANK_SURCHARGE, bankSurcharge);
        cv.put(Configuration.FIELD_UNIQUE_CODE, uniqueCode);
        cv.put(Configuration.FIELD_ADULT_COUNT, adultCount);
        cv.put(Configuration.FIELD_CHILD_COUNT, childCount);
        cv.put(Configuration.FIELD_INFANT_COUNT, infantCount);
        cv.put(Configuration.FIELD_DEPART_HIDDEN_TRANSIT, departHiddenTransit);
        cv.put(Configuration.FIELD_RETURN_HIDDEN_TRANSIT, returnHiddenTransit);

        cv.put(Configuration.FIELD_INVOICE, invoice);
        cv.put(Configuration.FIELD_SLUG, slug);
        cv.put(Configuration.FIELD_CARRIER_TYPE, carrierType);
        cv.put(Configuration.FIELD_BANK, bank);
        cv.put(Configuration.FIELD_IS_CONFIRMED, isConfirmed);
        cv.put(Configuration.FIELD_IS_PAID, isPaid);
        cv.put(Configuration.FIELD_PAID_FROM, paidFrom);
        cv.put(Configuration.FIELD_PAID_TIME_STAMP, paidTimeStamp);
        cv.put(Configuration.FIELD_CONFIRMED_TIME_STAMP, confirmedPaidTimeStamp);
        cv.put(Configuration.FIELD_STATUS_1, status1);
        cv.put(Configuration.FIELD_STATUS_2, status2);
        cv.put(Configuration.FIELD_IS_RETURN, isReturn);
        cv.put(Configuration.FIELD_FROM_PORT, fromPort);
        cv.put(Configuration.FIELD_TO_PORT, toPort);
        cv.put(Configuration.FIELD_DEPART_DATE, departDate);
        cv.put(Configuration.FIELD_RETURN_DATE, returnDate);
        cv.put(Configuration.FIELD_DEPART_FLIGHT, departFlight);
        cv.put(Configuration.FIELD_RETURN_FLIGHT, returnFlight);
        cv.put(Configuration.FIELD_CONTACT_TITLE, contactTitle);
        cv.put(Configuration.FIELD_CONTACT_NAME, contactName);
        cv.put(Configuration.FIELD_CONTACT_PHONE, contactPhone);
        cv.put(Configuration.FIELD_CONTACT_EMAIL, contactEmail);
        cv.put(Configuration.FIELD_CARRIER_ID_1, carrierID1);
        cv.put(Configuration.FIELD_CARRIER_ID_2, carrierID2);
        cv.put(Configuration.FIELD_CLASS_1, class1);
        cv.put(Configuration.FIELD_CLASS_2, class2);
        cv.put(Configuration.FIELD_BOOKING_CODE_1, bookingCode1);
        cv.put(Configuration.FIELD_BOOKING_CODE_2, bookingCode2);
        cv.put(Configuration.FIELD_TIME_BOOKING_1, timeBooking1);
        cv.put(Configuration.FIELD_TIME_BOOKING_2, timeBooking2);
        cv.put(Configuration.FIELD_TIME_LIMIT_1, timeLimit1);
        cv.put(Configuration.FIELD_TIME_LIMIT_2, timeLimit2);
        cv.put(Configuration.FIELD_USER_TYPE, sessionManager.isLoggedIn()?"auth":"origin");

        mDb.insert(Configuration.TABLE_RESERVATION, null, cv);
        this.close();
    }

    public void updateReservationObject(Reservation reservation){
        this.updateReservation(
                reservation.getReservationID(), reservation.getAmount(), reservation.getBankDiscount(), reservation.getBankSurcharge(), reservation.getUniqueCode(), reservation.getAdultCount(), reservation.getChildCount(), reservation.getInfantCount(), reservation.getDepartHiddenTransit(), reservation.getReturnHiddenTransit(), reservation.getInvoice(), reservation.getSlug(),
                reservation.getCarrierType(), reservation.getBank(), reservation.getIsConfirmed(), reservation.getIsPaid(), reservation.getPaidFrom(), reservation.getPaidTimeStamp(), reservation.getConfirmedPaidTimeStamp(), reservation.getStatus1(), reservation.getStatus2(), reservation.getIsReturn(),
                reservation.getFromPort(), reservation.getToPort(), reservation.getDepartDate(), reservation.getReturnDate(), reservation.getDepartFlight(), reservation.getReturnFlight(), reservation.getContactTitle(), reservation.getContactName(), reservation.getContactPhone(), reservation.getContactEmail(),
                reservation.getCarrierID1(), reservation.getCarrierID2(), reservation.getClass1(), reservation.getClass2(), reservation.getBookingCode1(), reservation.getBookingCode2(), reservation.getTimeBooking1(), reservation.getTimeBooking2(), reservation.getTimeLimit1(), reservation.getTimeLimit2()
        );
    }

    private void updateReservation(int reservationID, int amount, int bankDiscount, int bankSurcharge, int uniqueCode, int adultCount, int childCount, int infantCount, int departHiddenTransit, int returnHiddenTransit, String invoice, String slug,
                                  String carrierType, String bank, String isConfirmed, String isPaid, String paidFrom, String paidTimeStamp, String confirmedPaidTimeStamp, String status1, String status2, String isReturn,
                                  String fromPort, String toPort, String departDate, String returnDate, String departFlight, String returnFlight, String contactTitle, String contactName, String contactPhone, String contactEmail,
                                  String carrierID1, String carrierID2, String class1, String class2, String bookingCode1, String bookingCode2, String timeBooking1, String timeBooking2, String timeLimit1, String timeLimit2) {
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_RESERVATION_ID, reservationID);
        cv.put(Configuration.FIELD_AMOUNT, amount);
        cv.put(Configuration.FIELD_BANK_DISCOUNT, bankDiscount);
        cv.put(Configuration.FIELD_BANK_SURCHARGE, bankSurcharge);
        cv.put(Configuration.FIELD_UNIQUE_CODE, uniqueCode);
        cv.put(Configuration.FIELD_ADULT_COUNT, adultCount);
        cv.put(Configuration.FIELD_CHILD_COUNT, childCount);
        cv.put(Configuration.FIELD_INFANT_COUNT, infantCount);
        cv.put(Configuration.FIELD_DEPART_HIDDEN_TRANSIT, departHiddenTransit);
        cv.put(Configuration.FIELD_RETURN_HIDDEN_TRANSIT, returnHiddenTransit);

        cv.put(Configuration.FIELD_INVOICE, invoice);
        cv.put(Configuration.FIELD_SLUG, slug);
        cv.put(Configuration.FIELD_CARRIER_TYPE, carrierType);
        cv.put(Configuration.FIELD_BANK, bank);
        cv.put(Configuration.FIELD_IS_CONFIRMED, isConfirmed);
        cv.put(Configuration.FIELD_IS_PAID, isPaid);
        cv.put(Configuration.FIELD_PAID_FROM, paidFrom);
        cv.put(Configuration.FIELD_PAID_TIME_STAMP, paidTimeStamp);
        cv.put(Configuration.FIELD_CONFIRMED_TIME_STAMP, confirmedPaidTimeStamp);
        cv.put(Configuration.FIELD_STATUS_1, status1);
        cv.put(Configuration.FIELD_STATUS_2, status2);
        cv.put(Configuration.FIELD_IS_RETURN, isReturn);
        cv.put(Configuration.FIELD_FROM_PORT, fromPort);
        cv.put(Configuration.FIELD_TO_PORT, toPort);
        cv.put(Configuration.FIELD_DEPART_DATE, departDate);
        cv.put(Configuration.FIELD_RETURN_DATE, returnDate);
        cv.put(Configuration.FIELD_DEPART_FLIGHT, departFlight);
        cv.put(Configuration.FIELD_RETURN_FLIGHT, returnFlight);
        cv.put(Configuration.FIELD_CONTACT_TITLE, contactTitle);
        cv.put(Configuration.FIELD_CONTACT_NAME, contactName);
        cv.put(Configuration.FIELD_CONTACT_PHONE, contactPhone);
        cv.put(Configuration.FIELD_CONTACT_EMAIL, contactEmail);
        cv.put(Configuration.FIELD_CARRIER_ID_1, carrierID1);
        cv.put(Configuration.FIELD_CARRIER_ID_2, carrierID2);
        cv.put(Configuration.FIELD_CLASS_1, class1);
        cv.put(Configuration.FIELD_CLASS_2, class2);
        cv.put(Configuration.FIELD_BOOKING_CODE_1, bookingCode1);
        cv.put(Configuration.FIELD_BOOKING_CODE_2, bookingCode2);
        cv.put(Configuration.FIELD_TIME_BOOKING_1, timeBooking1);
        cv.put(Configuration.FIELD_TIME_BOOKING_2, timeBooking2);
        cv.put(Configuration.FIELD_TIME_LIMIT_1, timeLimit1);
        cv.put(Configuration.FIELD_TIME_LIMIT_2, timeLimit2);

        mDb.update(Configuration.TABLE_RESERVATION, cv,
                Configuration.FIELD_RESERVATION_ID + " = ?",
                new String[] { reservationID+"" });

        this.close();
    }



    public void updateReservationTimeLimit(int reservationID, String timeLimit){
        this.createDatabase();
        this.openWrite();
        ContentValues values = new ContentValues();
        values.put(Configuration.FIELD_TIME_LIMIT_1, timeLimit);
        mDb.update(Configuration.TABLE_RESERVATION, values,
                Configuration.FIELD_RESERVATION_ID + " = ?",
                new String[] { reservationID+"" });


        values.put(Configuration.FIELD_TIME_LIMIT_2, timeLimit);
        mDb.update(Configuration.TABLE_RESERVATION, values,
                Configuration.FIELD_RESERVATION_ID + " = ? AND "+Configuration.FIELD_IS_RETURN+" = \"Y\" AND "+Configuration.FIELD_CARRIER_ID_1+" != "+Configuration.FIELD_CARRIER_ID_2,
                new String[] { reservationID+"" });

        this.close();
    }

    public void confirmPayment(int reservationID, int bankID, int amount, String paidFrom, String paidTimeStamp, String isPaid){
        this.createDatabase();
        this.openWrite();
        ContentValues values = new ContentValues();
        values.put(Configuration.FIELD_BANK_ID, bankID);
        values.put(Configuration.FIELD_AMOUNT, amount);
        values.put(Configuration.FIELD_PAID_FROM, paidFrom);
        values.put(Configuration.FIELD_PAID_TIME_STAMP, paidTimeStamp);
        values.put(Configuration.FIELD_IS_PAID, isPaid);
        values.put(Configuration.FIELD_IS_CONFIRMED, "Y");
        mDb.update(Configuration.TABLE_RESERVATION, values,
                Configuration.FIELD_RESERVATION_ID + " = ?",
                new String[] { reservationID+"" });

        this.close();
    }

    public void setReservationPromo(int reservationID, int promoAmount, String promoCode){
        this.createDatabase();
        this.openWrite();
        ContentValues values = new ContentValues();
        values.put(Configuration.FIELD_PROMO_AMOUNT, promoAmount);
        values.put(Configuration.FIELD_PROMO_CODE, promoCode);
        mDb.update(Configuration.TABLE_RESERVATION, values,
                Configuration.FIELD_RESERVATION_ID + " = ?",
                new String[] { reservationID+"" });

        this.close();
    }

    public void setReservationBank(int reservationID, int bankID, String bank, int amount, int bankDiscount, int bankSurcharge, int uniqueCode, String paidFrom){
        this.createDatabase();
        this.openWrite();
        ContentValues values = new ContentValues();
        values.put(Configuration.FIELD_BANK_ID, bankID);
        values.put(Configuration.FIELD_BANK, bank);
        values.put(Configuration.FIELD_AMOUNT, amount);
        values.put(Configuration.FIELD_BANK_DISCOUNT, bankDiscount);
        values.put(Configuration.FIELD_BANK_SURCHARGE, bankSurcharge);
        values.put(Configuration.FIELD_UNIQUE_CODE, uniqueCode);
        values.put(Configuration.FIELD_PAID_FROM, paidFrom);
        mDb.update(Configuration.TABLE_RESERVATION, values,
                Configuration.FIELD_RESERVATION_ID + " = ?",
                new String[] { reservationID+"" });

        this.close();
    }

    public void setReservationBankID(int reservationID, int bankID){
        this.createDatabase();
        this.openWrite();
        ContentValues values = new ContentValues();
        values.put(Configuration.FIELD_BANK_ID, bankID);
        mDb.update(Configuration.TABLE_RESERVATION, values,
                Configuration.FIELD_RESERVATION_ID + " = ?",
                new String[] { reservationID+"" });

        this.close();
    }

    public Reservation getReservation(int reservationID){
        String selectQuery = "SELECT " + Configuration.FIELD_RESERVATION_ID + ", "
                + Configuration.FIELD_BANK_ID + ","
                + Configuration.FIELD_AMOUNT + ","
                + Configuration.FIELD_BANK_DISCOUNT + ","
                + Configuration.FIELD_BANK_SURCHARGE + ","
                + Configuration.FIELD_UNIQUE_CODE + ","
                + Configuration.FIELD_PROMO_AMOUNT + ","
                + Configuration.FIELD_ADULT_COUNT + ","
                + Configuration.FIELD_CHILD_COUNT + ","
                + Configuration.FIELD_INFANT_COUNT + ","
                + Configuration.FIELD_DEPART_HIDDEN_TRANSIT + ","
                + Configuration.FIELD_RETURN_HIDDEN_TRANSIT + ","
                + Configuration.FIELD_INVOICE + ","
                + Configuration.FIELD_SLUG + ","
                + Configuration.FIELD_CARRIER_TYPE + ","
                + Configuration.FIELD_BANK + ","
                + Configuration.FIELD_IS_CONFIRMED + ","
                + Configuration.FIELD_IS_PAID + ","
                + Configuration.FIELD_PROMO_CODE + ","
                + Configuration.FIELD_PAID_FROM + ","
                + Configuration.FIELD_PAID_TIME_STAMP + ","
                + Configuration.FIELD_CONFIRMED_TIME_STAMP + ","
                + Configuration.FIELD_STATUS_1 + ","
                + Configuration.FIELD_STATUS_2 + ","
                + Configuration.FIELD_IS_RETURN + ","
                + Configuration.FIELD_FROM_PORT + ","
                + Configuration.FIELD_TO_PORT + ","
                + Configuration.FIELD_DEPART_DATE + ","
                + Configuration.FIELD_RETURN_DATE + ","
                + Configuration.FIELD_DEPART_FLIGHT + ","
                + Configuration.FIELD_RETURN_FLIGHT + ","
                + Configuration.FIELD_CONTACT_TITLE + ","
                + Configuration.FIELD_CONTACT_NAME + ","
                + Configuration.FIELD_CONTACT_PHONE + ","
                + Configuration.FIELD_CONTACT_EMAIL + ","
                + Configuration.FIELD_CARRIER_ID_1 + ","
                + Configuration.FIELD_CARRIER_ID_2 + ","
                + Configuration.FIELD_CLASS_1 + ","
                + Configuration.FIELD_CLASS_2 + ","
                + Configuration.FIELD_BOOKING_CODE_1 + ","
                + Configuration.FIELD_BOOKING_CODE_2 + ","
                + Configuration.FIELD_TIME_BOOKING_1 + ","
                + Configuration.FIELD_TIME_BOOKING_2 + ","
                + Configuration.FIELD_TIME_LIMIT_1 + ","
                + Configuration.FIELD_TIME_LIMIT_2
                + " FROM " + Configuration.TABLE_RESERVATION + " WHERE " + Configuration.FIELD_RESERVATION_ID + " = '" + reservationID + "' AND "+Configuration.FIELD_USER_TYPE+"='"+(sessionManager.isLoggedIn()?"auth":"origin")+"'"+(sessionManager.isLoggedIn()?" AND "+Configuration.FIELD_CONTACT_EMAIL+"='"+sessionManager.getUserDetails().get(sessionManager.KEY_EMAIL)+"'":"");

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        Reservation reservation = new Reservation(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getInt(2),
                cursor.getInt(3),
                cursor.getInt(4),
                cursor.getInt(5),
                cursor.getInt(6),
                cursor.getInt(7),
                cursor.getInt(8),
                cursor.getInt(9),
                cursor.getInt(10),
                cursor.getInt(11),
                cursor.getString(12),
                cursor.getString(13),
                cursor.getString(14),
                cursor.getString(15),
                cursor.getString(16),
                cursor.getString(17),
                cursor.getString(18),
                cursor.getString(19),
                cursor.getString(20),
                cursor.getString(21),
                cursor.getString(22),
                cursor.getString(23),
                cursor.getString(24),
                cursor.getString(25),
                cursor.getString(26),
                cursor.getString(27),
                cursor.getString(28),
                cursor.getString(29),
                cursor.getString(30),
                cursor.getString(31),
                cursor.getString(32),
                cursor.getString(33),
                cursor.getString(34),
                cursor.getString(35),
                cursor.getString(36),
                cursor.getString(37),
                cursor.getString(38),
                cursor.getString(39),
                cursor.getString(40),
                cursor.getString(41),
                cursor.getString(42),
                cursor.getString(43),
                cursor.getString(44)
                );
        cursor.close();
        this.close();
        reservation.setFares(this.getFare(reservationID));
        reservation.setPassengers(this.getPassenger(reservationID));
        return reservation;
    }

    public boolean getReservationExists(int reservationID){
        String selectQuery = "SELECT * FROM " + Configuration.TABLE_RESERVATION + " WHERE " + Configuration.FIELD_RESERVATION_ID + " = '" + reservationID + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    public ArrayList<Reservation> getFlightReservationBooking(){
        return this.getReservationBooking("flight");
    }

    public ArrayList<Reservation> getFlightReservationIssued(boolean active){
        return this.getReservationIssued(active, "flight");
    }

    public ArrayList<Reservation> getTrainReservationBooking(){
        return this.getReservationBooking("train");
    }

    public ArrayList<Reservation> getTrainReservationIssued(boolean active){
        return this.getReservationIssued(active, "train");
    }

    public ArrayList<Reservation> getHotelReservationBooking(){
        return this.getReservationBooking("hotel");
    }

    public ArrayList<Reservation> getHotelReservationIssued(boolean active){
        return this.getReservationIssued(active, "hotel");
    }

    private ArrayList<Reservation> getReservationBooking(String carrierType){
        ArrayList<Reservation> reservations = new ArrayList<>();
        String selectQuery = "SELECT " + Configuration.FIELD_RESERVATION_ID + ", "
                + Configuration.FIELD_BANK_ID + ","
                + Configuration.FIELD_AMOUNT + ","
                + Configuration.FIELD_BANK_DISCOUNT + ","
                + Configuration.FIELD_BANK_SURCHARGE + ","
                + Configuration.FIELD_UNIQUE_CODE + ","
                + Configuration.FIELD_PROMO_AMOUNT + ","
                + Configuration.FIELD_ADULT_COUNT + ","
                + Configuration.FIELD_CHILD_COUNT + ","
                + Configuration.FIELD_INFANT_COUNT + ","
                + Configuration.FIELD_DEPART_HIDDEN_TRANSIT + ","
                + Configuration.FIELD_RETURN_HIDDEN_TRANSIT + ","
                + Configuration.FIELD_INVOICE + ","
                + Configuration.FIELD_SLUG + ","
                + Configuration.FIELD_CARRIER_TYPE + ","
                + Configuration.FIELD_BANK + ","
                + Configuration.FIELD_IS_CONFIRMED + ","
                + Configuration.FIELD_IS_PAID + ","
                + Configuration.FIELD_PROMO_CODE + ","
                + Configuration.FIELD_PAID_FROM + ","
                + Configuration.FIELD_PAID_TIME_STAMP + ","
                + Configuration.FIELD_CONFIRMED_TIME_STAMP + ","
                + Configuration.FIELD_STATUS_1 + ","
                + Configuration.FIELD_STATUS_2 + ","
                + Configuration.FIELD_IS_RETURN + ","
                + Configuration.FIELD_FROM_PORT + ","
                + Configuration.FIELD_TO_PORT + ","
                + Configuration.FIELD_DEPART_DATE + ","
                + Configuration.FIELD_RETURN_DATE + ","
                + Configuration.FIELD_DEPART_FLIGHT + ","
                + Configuration.FIELD_RETURN_FLIGHT + ","
                + Configuration.FIELD_CONTACT_TITLE + ","
                + Configuration.FIELD_CONTACT_NAME + ","
                + Configuration.FIELD_CONTACT_PHONE + ","
                + Configuration.FIELD_CONTACT_EMAIL + ","
                + Configuration.FIELD_CARRIER_ID_1 + ","
                + Configuration.FIELD_CARRIER_ID_2 + ","
                + Configuration.FIELD_CLASS_1 + ","
                + Configuration.FIELD_CLASS_2 + ","
                + Configuration.FIELD_BOOKING_CODE_1 + ","
                + Configuration.FIELD_BOOKING_CODE_2 + ","
                + Configuration.FIELD_TIME_BOOKING_1 + ","
                + Configuration.FIELD_TIME_BOOKING_2 + ","
                + Configuration.FIELD_TIME_LIMIT_1 + ","
                + Configuration.FIELD_TIME_LIMIT_2
                + " FROM " + Configuration.TABLE_RESERVATION + " WHERE /*" + Configuration.FIELD_IS_CONFIRMED + " = 'Y' OR*/ (" + Configuration.FIELD_CARRIER_TYPE + " = '" + carrierType +"' AND (" + Configuration.FIELD_STATUS_1 + " = 'booked' OR " + Configuration.FIELD_STATUS_2 + " = 'booked') AND (DATETIME(IFNULL(MIN("+Configuration.FIELD_TIME_LIMIT_1+","+Configuration.FIELD_TIME_LIMIT_2+"), "+Configuration.FIELD_TIME_LIMIT_1+")) > DATETIME('" + Utils.getDateTimeNowInUTCPlus7()+"') OR DATETIME(IFNULL(MIN("+Configuration.FIELD_TIME_LIMIT_1+","+Configuration.FIELD_TIME_LIMIT_2+"), "+Configuration.FIELD_TIME_LIMIT_1+"), '+10 minutes') > DATETIME('" + Utils.getDateTimeNowInUTCPlus7()+"')) AND "+Configuration.FIELD_USER_TYPE+"='"+(sessionManager.isLoggedIn()?"auth":"origin")+"'"+(sessionManager.isLoggedIn()?" AND "+Configuration.FIELD_CONTACT_EMAIL+"='"+sessionManager.getUserDetails().get(sessionManager.KEY_EMAIL)+"'":"")+")";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Reservation reservation = new Reservation(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getInt(2),
                        cursor.getInt(3),
                        cursor.getInt(4),
                        cursor.getInt(5),
                        cursor.getInt(6),
                        cursor.getInt(7),
                        cursor.getInt(8),
                        cursor.getInt(9),
                        cursor.getInt(10),
                        cursor.getInt(11),
                        cursor.getString(12),
                        cursor.getString(13),
                        cursor.getString(14),
                        cursor.getString(15),
                        cursor.getString(16),
                        cursor.getString(17),
                        cursor.getString(18),
                        cursor.getString(19),
                        cursor.getString(20),
                        cursor.getString(21),
                        cursor.getString(22),
                        cursor.getString(23),
                        cursor.getString(24),
                        cursor.getString(25),
                        cursor.getString(26),
                        cursor.getString(27),
                        cursor.getString(28),
                        cursor.getString(29),
                        cursor.getString(30),
                        cursor.getString(31),
                        cursor.getString(32),
                        cursor.getString(33),
                        cursor.getString(34),
                        cursor.getString(35),
                        cursor.getString(36),
                        cursor.getString(37),
                        cursor.getString(38),
                        cursor.getString(39),
                        cursor.getString(40),
                        cursor.getString(41),
                        cursor.getString(42),
                        cursor.getString(43),
                        cursor.getString(44)
                );
                reservation.setFares(this.getFare(reservation.getReservationID()));
                reservation.setPassengers(this.getPassenger(reservation.getReservationID()));
                reservations.add(reservation);
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return reservations;
    }

    private ArrayList<Reservation> getReservationIssued(boolean active, String carrierType){
        ArrayList<Reservation> reservations = new ArrayList<>();
        String selectQuery = "SELECT " + Configuration.FIELD_RESERVATION_ID + ", "
                + Configuration.FIELD_BANK_ID + ","
                + Configuration.FIELD_AMOUNT + ","
                + Configuration.FIELD_BANK_DISCOUNT + ","
                + Configuration.FIELD_BANK_SURCHARGE + ","
                + Configuration.FIELD_UNIQUE_CODE + ","
                + Configuration.FIELD_PROMO_AMOUNT + ","
                + Configuration.FIELD_ADULT_COUNT + ","
                + Configuration.FIELD_CHILD_COUNT + ","
                + Configuration.FIELD_INFANT_COUNT + ","
                + Configuration.FIELD_DEPART_HIDDEN_TRANSIT + ","
                + Configuration.FIELD_RETURN_HIDDEN_TRANSIT + ","
                + Configuration.FIELD_INVOICE + ","
                + Configuration.FIELD_SLUG + ","
                + Configuration.FIELD_CARRIER_TYPE + ","
                + Configuration.FIELD_BANK + ","
                + Configuration.FIELD_IS_CONFIRMED + ","
                + Configuration.FIELD_IS_PAID + ","
                + Configuration.FIELD_PROMO_CODE + ","
                + Configuration.FIELD_PAID_FROM + ","
                + Configuration.FIELD_PAID_TIME_STAMP + ","
                + Configuration.FIELD_CONFIRMED_TIME_STAMP + ","
                + Configuration.FIELD_STATUS_1 + ","
                + Configuration.FIELD_STATUS_2 + ","
                + Configuration.FIELD_IS_RETURN + ","
                + Configuration.FIELD_FROM_PORT + ","
                + Configuration.FIELD_TO_PORT + ","
                + Configuration.FIELD_DEPART_DATE + ","
                + Configuration.FIELD_RETURN_DATE + ","
                + Configuration.FIELD_DEPART_FLIGHT + ","
                + Configuration.FIELD_RETURN_FLIGHT + ","
                + Configuration.FIELD_CONTACT_TITLE + ","
                + Configuration.FIELD_CONTACT_NAME + ","
                + Configuration.FIELD_CONTACT_PHONE + ","
                + Configuration.FIELD_CONTACT_EMAIL + ","
                + Configuration.FIELD_CARRIER_ID_1 + ","
                + Configuration.FIELD_CARRIER_ID_2 + ","
                + Configuration.FIELD_CLASS_1 + ","
                + Configuration.FIELD_CLASS_2 + ","
                + Configuration.FIELD_BOOKING_CODE_1 + ","
                + Configuration.FIELD_BOOKING_CODE_2 + ","
                + Configuration.FIELD_TIME_BOOKING_1 + ","
                + Configuration.FIELD_TIME_BOOKING_2 + ","
                + Configuration.FIELD_TIME_LIMIT_1 + ","
                + Configuration.FIELD_TIME_LIMIT_2
                + " FROM " + Configuration.TABLE_RESERVATION + " WHERE " + Configuration.FIELD_CARRIER_TYPE + " = '" + carrierType +"' AND (" + Configuration.FIELD_STATUS_1 + " = 'issued' OR " + Configuration.FIELD_STATUS_2 + " = 'isseud') AND DATE(IFNULL(MAX("+Configuration.FIELD_DEPART_DATE+","+Configuration.FIELD_RETURN_DATE+"), "+Configuration.FIELD_DEPART_DATE+")) " + (active?">=":"<") + " DATE('" + Utils.getDateTimeNowInUTCPlus7()+"') AND "+Configuration.FIELD_USER_TYPE+"='"+(sessionManager.isLoggedIn()?"auth":"origin")+"'"+(sessionManager.isLoggedIn()?" AND "+Configuration.FIELD_CONTACT_EMAIL+"='"+sessionManager.getUserDetails().get(sessionManager.KEY_EMAIL)+"'":"");

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Reservation reservation = new Reservation(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getInt(2),
                        cursor.getInt(3),
                        cursor.getInt(4),
                        cursor.getInt(5),
                        cursor.getInt(6),
                        cursor.getInt(7),
                        cursor.getInt(8),
                        cursor.getInt(9),
                        cursor.getInt(10),
                        cursor.getInt(11),
                        cursor.getString(12),
                        cursor.getString(13),
                        cursor.getString(14),
                        cursor.getString(15),
                        cursor.getString(16),
                        cursor.getString(17),
                        cursor.getString(18),
                        cursor.getString(19),
                        cursor.getString(20),
                        cursor.getString(21),
                        cursor.getString(22),
                        cursor.getString(23),
                        cursor.getString(24),
                        cursor.getString(25),
                        cursor.getString(26),
                        cursor.getString(27),
                        cursor.getString(28),
                        cursor.getString(29),
                        cursor.getString(30),
                        cursor.getString(31),
                        cursor.getString(32),
                        cursor.getString(33),
                        cursor.getString(34),
                        cursor.getString(35),
                        cursor.getString(36),
                        cursor.getString(37),
                        cursor.getString(38),
                        cursor.getString(39),
                        cursor.getString(40),
                        cursor.getString(41),
                        cursor.getString(42),
                        cursor.getString(43),
                        cursor.getString(44)
                );
                reservation.setFares(this.getFare(reservation.getReservationID()));
                reservation.setPassengers(this.getPassenger(reservation.getReservationID()));
                reservations.add(reservation);
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return reservations;
    }

    public ArrayList<Integer> getNonAuthReservationID(){
        ArrayList<Integer> reservations = new ArrayList<>();
        String selectQuery = "SELECT " + Configuration.FIELD_RESERVATION_ID
                + " FROM " + Configuration.TABLE_RESERVATION + " WHERE "+Configuration.FIELD_USER_TYPE+"='origin'";
        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                reservations.add(cursor.getInt(0));

            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return reservations;
    }

    public void setReservationToAuthUser(){
        this.createDatabase();
        this.openWrite();
        ContentValues values = new ContentValues();
        values.put(Configuration.FIELD_CONTACT_EMAIL, sessionManager.getUserDetails().get(sessionManager.KEY_EMAIL));
        values.put(Configuration.FIELD_USER_TYPE, "auth");
        mDb.update(Configuration.TABLE_RESERVATION, values,
                Configuration.FIELD_USER_TYPE + " = ?",
                new String[] { "origin" });

        this.close();
    }

    public void insertFareObject(Fare fare){
        this.insertFare(fare.getFareID(), fare.getReservationID(), fare.getCarrierID(), fare.getPassengerType(), fare.getBasic(), fare.getTax(), fare.getIwjr(), fare.getInsurance(), fare.getFuel(), fare.getAdministration(), fare.getSurcharge(), fare.getDiscount(), fare.getCount());
    }

    public void insertFare(int fareID, int reservationID, String carrierID, String passengerType, int basic, int tax, int iwjr, int insurance, int fuel, int administration, int surcharge, int discount, int count){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_FARE_ID, fareID);
        cv.put(Configuration.FIELD_RESERVATION_ID, reservationID);
        cv.put(Configuration.FIELD_CARRIER_ID, carrierID);
        cv.put(Configuration.FIELD_PASSENGER_TYPE, passengerType);
        cv.put(Configuration.FIELD_BASIC, basic);
        cv.put(Configuration.FIELD_TAX, tax);
        cv.put(Configuration.FIELD_IWJR, iwjr);
        cv.put(Configuration.FIELD_INSURANCE, insurance);
        cv.put(Configuration.FIELD_FUEL, fuel);
        cv.put(Configuration.FIELD_ADMINISTRATION, administration);
        cv.put(Configuration.FIELD_SURCHARGE, surcharge);
        cv.put(Configuration.FIELD_DISCOUNT, discount);
        cv.put(Configuration.FIELD_COUNT, count);

        mDb.insert(Configuration.TABLE_FARE, null, cv);
        this.close();
    }



    public void updateFareObject(Fare fare){
        this.updateFare(fare.getFareID(), fare.getReservationID(), fare.getCarrierID(), fare.getPassengerType(), fare.getBasic(), fare.getTax(), fare.getIwjr(), fare.getInsurance(), fare.getFuel(), fare.getAdministration(), fare.getSurcharge(), fare.getDiscount(), fare.getCount());
    }

    private void updateFare(int fareID, int reservationID, String carrierID, String passengerType, int basic, int tax, int iwjr, int insurance, int fuel, int administration, int surcharge, int discount, int count){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_FARE_ID, fareID);
        cv.put(Configuration.FIELD_RESERVATION_ID, reservationID);
        cv.put(Configuration.FIELD_CARRIER_ID, carrierID);
        cv.put(Configuration.FIELD_PASSENGER_TYPE, passengerType);
        cv.put(Configuration.FIELD_BASIC, basic);
        cv.put(Configuration.FIELD_TAX, tax);
        cv.put(Configuration.FIELD_IWJR, iwjr);
        cv.put(Configuration.FIELD_INSURANCE, insurance);
        cv.put(Configuration.FIELD_FUEL, fuel);
        cv.put(Configuration.FIELD_ADMINISTRATION, administration);
        cv.put(Configuration.FIELD_SURCHARGE, surcharge);
        cv.put(Configuration.FIELD_DISCOUNT, discount);
        cv.put(Configuration.FIELD_COUNT, count);

        mDb.update(Configuration.TABLE_FARE, cv,
                Configuration.FIELD_FARE_ID + " = ?",
                new String[] { fareID+"" });

        this.close();
    }

    public void deleteFare(int reservationID){
        this.createDatabase();
        this.openWrite();
        mDb.delete(Configuration.TABLE_FARE, Configuration.FIELD_RESERVATION_ID + "= ?", new String[]{Integer.toString(reservationID)});
        this.close();
    }

    private ArrayList<Fare> getFare(int reservationID){
        ArrayList<Fare> fares = new ArrayList<>();
        String selectQuery = "SELECT " + Configuration.FIELD_FARE_ID + ", "
                + Configuration.FIELD_RESERVATION_ID + ","
                + Configuration.FIELD_CARRIER_ID + ","
                + Configuration.FIELD_PASSENGER_TYPE + ","
                + Configuration.FIELD_BASIC + ","
                + Configuration.FIELD_TAX + ","
                + Configuration.FIELD_IWJR + ","
                + Configuration.FIELD_INSURANCE + ","
                + Configuration.FIELD_FUEL + ","
                + Configuration.FIELD_ADMINISTRATION + ","
                + Configuration.FIELD_SURCHARGE + ","
                + Configuration.FIELD_DISCOUNT + ","
                + Configuration.FIELD_COUNT
                + " FROM " + Configuration.TABLE_FARE+ " WHERE " + Configuration.FIELD_RESERVATION_ID + " = '" + reservationID + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                fares.add(new Fare(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getInt(4),
                        cursor.getInt(5),
                        cursor.getInt(6),
                        cursor.getInt(7),
                        cursor.getInt(8),
                        cursor.getInt(9),
                        cursor.getInt(10),
                        cursor.getInt(11),
                        cursor.getInt(12)
                ));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return fares;
    }

    public boolean getFareExists(int fareID){
        String selectQuery = "SELECT * FROM " + Configuration.TABLE_FARE + " WHERE " + Configuration.FIELD_FARE_ID + " = '" + fareID + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    public void insertPassengerObject(Passenger passenger){
        this.insertPassenger(passenger.getPassengerID(), passenger.getReservationID(), passenger.getType(), passenger.getTitle(), passenger.getName(), passenger.getBirthDate(), passenger.getIDCardNumber(), passenger.getAdultAssocNumber(), passenger.getGender(), passenger.getNationality(), passenger.getPassportNumber(), passenger.getPassportIssuingCountry(), passenger.getPassportExpiryDate(), passenger.getDepartSeat(), passenger.getReturnSeat());
    }

    public void insertPassenger(int passengerID, int reservationID, String type, String title, String name, String birthDate, String IDCardNumber, int adultAssocNumber, String gender, String nationality, String passportNumber, String passportIssuingCountry, String passportExpiryDate, String departSeat, String returnSeat){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_PASSENGER_ID, passengerID);
        cv.put(Configuration.FIELD_RESERVATION_ID, reservationID);
        cv.put(Configuration.FIELD_PASSENGER_TYPE, type);
        cv.put(Configuration.FIELD_PASSENGER_TITLE, title);
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_BIRTH_DATE, birthDate);
        cv.put(Configuration.FIELD_ID_CARD_NUMBER, IDCardNumber);
        cv.put(Configuration.FIELD_ADULT_ASSOC, adultAssocNumber);
        cv.put(Configuration.FIELD_GENDER, gender);
        cv.put(Configuration.FIELD_NATIONALITY, nationality);
        cv.put(Configuration.FIELD_LOYALTY_ID, "");
        cv.put(Configuration.FIELD_PASSPORT_NUMBER, passportNumber);
        cv.put(Configuration.FIELD_PASSPORT_ISSUING_COUNTRY, passportIssuingCountry);
        cv.put(Configuration.FIELD_PASSPORT_EXPIRY_DATE, passportExpiryDate);
        cv.put(Configuration.FIELD_DEPART_SEAT, departSeat);
        cv.put(Configuration.FIELD_RETURN_SEAT, returnSeat);

        mDb.insert(Configuration.TABLE_PASSENGER, null, cv);
        this.close();
    }

    public void updatePassengerObject(Passenger passenger){
        this.updatePassenger(passenger.getPassengerID(), passenger.getReservationID(), passenger.getType(), passenger.getTitle(), passenger.getName(), passenger.getBirthDate(), passenger.getIDCardNumber(), passenger.getAdultAssocNumber(), passenger.getGender(), passenger.getNationality(), passenger.getPassportNumber(), passenger.getPassportIssuingCountry(), passenger.getPassportExpiryDate(), passenger.getDepartSeat(), passenger.getReturnSeat());
    }

    private void updatePassenger(int passengerID, int reservationID, String type, String title, String name, String birthDate, String IDCardNumber, int adultAssocNumber, String gender, String nationality, String passportNumber, String passportIssuingCountry, String passportExpiryDate, String departSeat, String returnSeat){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_PASSENGER_ID, passengerID);
        cv.put(Configuration.FIELD_RESERVATION_ID, reservationID);
        cv.put(Configuration.FIELD_PASSENGER_TYPE, type);
        cv.put(Configuration.FIELD_PASSENGER_TITLE, title);
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_BIRTH_DATE, birthDate);
        cv.put(Configuration.FIELD_ID_CARD_NUMBER, IDCardNumber);
        cv.put(Configuration.FIELD_ADULT_ASSOC, adultAssocNumber);
        cv.put(Configuration.FIELD_GENDER, gender);
        cv.put(Configuration.FIELD_NATIONALITY, nationality);
        cv.put(Configuration.FIELD_LOYALTY_ID, "");
        cv.put(Configuration.FIELD_PASSPORT_NUMBER, passportNumber);
        cv.put(Configuration.FIELD_PASSPORT_ISSUING_COUNTRY, passportIssuingCountry);
        cv.put(Configuration.FIELD_PASSPORT_EXPIRY_DATE, passportExpiryDate);
        cv.put(Configuration.FIELD_DEPART_SEAT, departSeat);
        cv.put(Configuration.FIELD_RETURN_SEAT, returnSeat);

        mDb.update(Configuration.TABLE_PASSENGER, cv,
                Configuration.FIELD_PASSENGER_ID + " = ?",
                new String[] { passengerID+"" });

        this.close();
    }

    public void setPassengerSeat(String direction, int[] passengerID, String[] wagon, String[] seat){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        for(int i=0; i<passengerID.length; i++) {
            cv.put(direction.equals("depart")?Configuration.FIELD_DEPART_SEAT:Configuration.FIELD_RETURN_SEAT, wagon[i]+","+seat[i]);

            mDb.update(Configuration.TABLE_PASSENGER, cv,
                    Configuration.FIELD_PASSENGER_ID + " = ?",
                    new String[]{passengerID[i] + ""});
        }

        this.close();
    }

    public void deletePassenger(int reservationID){
        this.createDatabase();
        this.openWrite();
        mDb.delete(Configuration.TABLE_PASSENGER, Configuration.FIELD_RESERVATION_ID + "= ?", new String[]{Integer.toString(reservationID)});
        this.close();
    }

    private ArrayList<Passenger> getPassenger(int reservationID){
        ArrayList<Passenger> passengers = new ArrayList<>();
        String selectQuery = "SELECT " + Configuration.FIELD_PASSENGER_ID + ", "
                + Configuration.FIELD_RESERVATION_ID + ","
                + Configuration.FIELD_PASSENGER_TYPE + ","
                + Configuration.FIELD_PASSENGER_TITLE + ","
                + Configuration.FIELD_NAME + ","
                + Configuration.FIELD_BIRTH_DATE + ","
                + Configuration.FIELD_ID_CARD_NUMBER + ","
                + Configuration.FIELD_ADULT_ASSOC + ","
                + Configuration.FIELD_GENDER + ","
                + Configuration.FIELD_NATIONALITY + ","
                + Configuration.FIELD_PASSPORT_NUMBER + ","
                + Configuration.FIELD_PASSPORT_ISSUING_COUNTRY + ","
                + Configuration.FIELD_PASSPORT_EXPIRY_DATE + ","
                + Configuration.FIELD_DEPART_SEAT + ","
                + Configuration.FIELD_RETURN_SEAT
                + " FROM " + Configuration.TABLE_PASSENGER + " WHERE " + Configuration.FIELD_RESERVATION_ID + " = '" + reservationID + "' ORDER BY "+Configuration.FIELD_PASSENGER_ID;

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                passengers.add(new Passenger(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getInt(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11),
                        cursor.getString(12),
                        cursor.getString(13),
                        cursor.getString(14)
                ));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return passengers;
    }

    public boolean getPassengerExists(int passengerID){
        String selectQuery = "SELECT * FROM " + Configuration.TABLE_PASSENGER + " WHERE " + Configuration.FIELD_PASSENGER_ID + " = '" + passengerID + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }
}
