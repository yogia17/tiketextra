package com.tiketextra.tiketextra.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Promo;
import com.tiketextra.tiketextra.object.Slideshow;
import com.tiketextra.tiketextra.util.Utils;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kurnia on 8/8/17.
 */

public class SettingModel {
    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    private final String TAG = "SettingModel";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public SettingModel(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
    }

    private SettingModel createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    private SettingModel open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private SettingModel openWrite() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getWritableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private void close()
    {
        mDb.close();
        mDbHelper.close();
    }

    public void setValue(String parameter, String value){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_VALUE, value);
        mDb.update(Configuration.TABLE_SETTING, cv,
                Configuration.FIELD_PARAMETER + " = ?",
                new String[] { parameter });
        this.close();
    }

    public String getValue(String parameter){
        String selectQuery = "SELECT " + Configuration.FIELD_VALUE
                + " FROM " + Configuration.TABLE_SETTING + " WHERE " + Configuration.FIELD_PARAMETER + " = '" + parameter + "'";

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        String value = cursor.getString(0);
        cursor.close();
        this.close();
        return value;
    }

    public boolean valueExists(String parameter){
        String countQuery = "SELECT " + Configuration.FIELD_VALUE
                + " FROM " + Configuration.TABLE_SETTING + " WHERE " + Configuration.FIELD_PARAMETER + " = '"+ parameter + "'";
        this.createDatabase();
        this.openWrite();
        Cursor cursor = mDb.rawQuery(countQuery, null);


        boolean isEmpty = cursor.getCount() == 0;
        cursor.close();
        this.close();
        return !isEmpty;
    }

    public void updateValue(String parameter, String value){
        String countQuery = "SELECT " + Configuration.FIELD_VALUE
                + " FROM " + Configuration.TABLE_SETTING + " WHERE " + Configuration.FIELD_PARAMETER + " = '"+ parameter + "'";
        this.createDatabase();
        this.openWrite();
        Cursor cursor = mDb.rawQuery(countQuery, null);

        boolean isEmpty = cursor.getCount() == 0;
        cursor.close();
        if(isEmpty){
            ContentValues cv = new ContentValues();
            cv.put(Configuration.FIELD_PARAMETER, parameter);
            cv.put(Configuration.FIELD_VALUE, value);
            mDb.insert(Configuration.TABLE_SETTING, null, cv);
            this.close();
        }
        else{
            this.setValue(parameter, value);
        }
    }

    public boolean updatePrefetch(){
        String countQuery = "SELECT " + Configuration.FIELD_VALUE
                + " FROM " + Configuration.TABLE_SETTING + " WHERE " + Configuration.FIELD_PARAMETER + " = 'update_prefetch'";
        this.createDatabase();
        this.openWrite();
        Cursor cursor = mDb.rawQuery(countQuery, null);


        boolean isEmpty = cursor.getCount() == 0;
        cursor.close();
        if(isEmpty){
            Log.e("insert",  "update prefetch");
            ContentValues cv = new ContentValues();
            cv.put(Configuration.FIELD_PARAMETER, "update_prefetch");
            cv.put(Configuration.FIELD_VALUE, Utils.getDateTimeNowInUTCPlus7());
            mDb.insert(Configuration.TABLE_SETTING, null, cv);
            this.close();
            return true;
        }
//        else if(this.getValue("open_first").equals("1")){
//            this.setValue("open_first", "0");
//            return true;
//        }
        else{
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = this.getValue("update_prefetch");
            Date d1 = null;
            Date d2 = null;

            try {
                d1 = format.parse(date);
                d2 = format.parse(Utils.getDateTimeNowInUTCPlus7());

                DateTime dt1 = new DateTime(d1);
                DateTime dt2 = new DateTime(d2);

                int minutes = Minutes.minutesBetween(dt1, dt2).getMinutes();
                if(minutes > Configuration.PREFETCH_UPDATE_DURATION_THRESHOLD_IN_MINUTES){
                    this.setValue("update_prefetch", Utils.getDateTimeNowInUTCPlus7());
                    return true;
                }
                else{
                    return false;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }


    public boolean updateBookingSessionTime(){
        return updateReservationSessionTime("update_booking_time");
    }

    public boolean updateIssuedSessionTime(){
        return updateReservationSessionTime("update_booking_time");
//        return updateReservationSessionTime("update_issued_time");
    }

    private boolean updateReservationSessionTime(String mode){
        String countQuery = "SELECT " + Configuration.FIELD_VALUE
                + " FROM " + Configuration.TABLE_SETTING + " WHERE " + Configuration.FIELD_PARAMETER + " = '"+ mode + "'";
        this.createDatabase();
        this.openWrite();
        Cursor cursor = mDb.rawQuery(countQuery, null);


        boolean isEmpty = cursor.getCount() == 0;
        cursor.close();
        if(isEmpty){
            ContentValues cv = new ContentValues();
            cv.put(Configuration.FIELD_PARAMETER, mode);
            cv.put(Configuration.FIELD_VALUE, Utils.getDateTimeNowInUTCPlus7());
            mDb.insert(Configuration.TABLE_SETTING, null, cv);
            this.close();
            return true;
        }
        else{
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = this.getValue(mode);
            Date d1 = null;
            Date d2 = null;

            try {
                d1 = format.parse(date);
                d2 = format.parse(Utils.getDateTimeNowInUTCPlus7());

                DateTime dt1 = new DateTime(d1);
                DateTime dt2 = new DateTime(d2);

                int minutes = Minutes.minutesBetween(dt1, dt2).getMinutes();
                if(minutes > 60){
                    this.setValue(mode, Utils.getDateTimeNowInUTCPlus7());
                    return true;
                }
                else{
                    return false;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
    }

    private boolean existAirline(String flightNumber, String carrierID){
        String countQuery = "SELECT * FROM " + Configuration.TABLE_AIRLINE
                + " WHERE " + Configuration.FIELD_FLIGHT_NUMBER + "='" + flightNumber + "'"
                + " AND " + Configuration.FIELD_CARRIER_ID + "='" + carrierID + "'";
        Cursor cursor = mDb.rawQuery(countQuery, null);
        boolean ret = cursor.getCount() > 0;
        cursor.close();
        return ret;
    }

    private void setAirline(String flightNumber, String carrierID, String name, String meal, String airportTax, int domesticBaggage, int internationalBaggage){
        
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_FLIGHT_NUMBER, flightNumber);
        cv.put(Configuration.FIELD_CARRIER_ID, carrierID);
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_MEAL, meal);
        cv.put(Configuration.FIELD_AIRPORT_TAX, airportTax);
        cv.put(Configuration.FIELD_DOMESTIC_BAGGAGE, domesticBaggage);
        cv.put(Configuration.FIELD_INTERNATIONAL_BAGGAGE, internationalBaggage);



        if(this.existAirline(flightNumber, carrierID)){
//            Log.e("update "+flightNumber, carrierID);
            mDb.update(Configuration.TABLE_AIRLINE, cv,
                    Configuration.FIELD_FLIGHT_NUMBER + " = '"+flightNumber+"' AND "+Configuration.FIELD_CARRIER_ID + " = '"+carrierID+"'",
                    null);
        }
        else{
//            Log.e("insert "+flightNumber, carrierID);
            mDb.insert(Configuration.TABLE_AIRLINE, null, cv);
        }
    }

    private void setAirport(String iataCode, String name, String nameAlt, String city, String cityAlt, String region,
                            String countryCode, double latitude, double longitude, double timezone, String tz, String isActive){
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_IATA_CODE, iataCode);
        cv.put(Configuration.FIELD_CARRIER_TYPE, "flight");
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_NAME_ALT, nameAlt);
        cv.put(Configuration.FIELD_CITY, city);
        cv.put(Configuration.FIELD_CITY_ALT, cityAlt);
        cv.put(Configuration.FIELD_REGION, region);
        cv.put(Configuration.FIELD_COUNTRY_CODE, countryCode);
        cv.put(Configuration.FIELD_LATITUDE, latitude);
        cv.put(Configuration.FIELD_LONGITUDE, longitude);
        cv.put(Configuration.FIELD_TIMEZONE, timezone);
        cv.put(Configuration.FIELD_TZ, tz);
        cv.put(Configuration.FIELD_IS_ACTIVE, isActive);

        if(this.existAirport(iataCode, countryCode)){
            Log.e("update "+iataCode, countryCode + " " + isActive);
            mDb.update(Configuration.TABLE_PORT, cv,
                    Configuration.FIELD_IATA_CODE + " = '"+iataCode+"' AND "+Configuration.FIELD_COUNTRY_CODE + " = '"+countryCode+"' AND "+Configuration.FIELD_CARRIER_TYPE + "='flight'",
                    null);
        }
        else{
            Log.e("insert "+iataCode, countryCode + " " + isActive);
            mDb.insert(Configuration.TABLE_PORT, null, cv);
        }
    }

    private boolean existAirport(String iataCode, String countryCode){
        String countQuery = "SELECT * FROM " + Configuration.TABLE_PORT
                + " WHERE " + Configuration.FIELD_IATA_CODE + "='" + iataCode + "'"
                + " AND " + Configuration.FIELD_COUNTRY_CODE + "='" + countryCode + "' AND "+Configuration.FIELD_CARRIER_TYPE + "='flight'";
        Cursor cursor = mDb.rawQuery(countQuery, null);
        boolean ret = cursor.getCount() > 0;
        cursor.close();
        return ret;
    }



    private void setStation(String iataCode, String name, String nameAlt, String city, String cityAlt, String region,
                            String countryCode, double latitude, double longitude, double timezone, String tz, String isActive){
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_IATA_CODE, iataCode);
        cv.put(Configuration.FIELD_CARRIER_TYPE, "train");
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_NAME_ALT, nameAlt);
        cv.put(Configuration.FIELD_CITY, city);
        cv.put(Configuration.FIELD_CITY_ALT, cityAlt);
        cv.put(Configuration.FIELD_REGION, region);
        cv.put(Configuration.FIELD_COUNTRY_CODE, countryCode);
        cv.put(Configuration.FIELD_LATITUDE, latitude);
        cv.put(Configuration.FIELD_LONGITUDE, longitude);
        cv.put(Configuration.FIELD_TIMEZONE, timezone);
        cv.put(Configuration.FIELD_TZ, tz);
        cv.put(Configuration.FIELD_IS_ACTIVE, isActive);

        if(this.existStation(iataCode, countryCode)){
            Log.e("update "+iataCode, countryCode + " " + isActive);
            mDb.update(Configuration.TABLE_PORT, cv,
                    Configuration.FIELD_IATA_CODE + " = '"+iataCode+"' AND "+Configuration.FIELD_COUNTRY_CODE + " = '"+countryCode+"' AND "+Configuration.FIELD_CARRIER_TYPE + "='train'",
                    null);
        }
        else{
            Log.e("insert station "+iataCode, countryCode + " " + isActive);
            mDb.insert(Configuration.TABLE_PORT, null, cv);
        }
    }

    private boolean existStation(String iataCode, String countryCode){
        String countQuery = "SELECT * FROM " + Configuration.TABLE_PORT
                + " WHERE " + Configuration.FIELD_IATA_CODE + "='" + iataCode + "'"
                + " AND " + Configuration.FIELD_COUNTRY_CODE + "='" + countryCode + "' AND "+Configuration.FIELD_CARRIER_TYPE + "='train'";
        Cursor cursor = mDb.rawQuery(countQuery, null);
        boolean ret = cursor.getCount() > 0;
        cursor.close();
        return ret;
    }
    
    public void updatePrefetchData(JSONObject response){
        this.createDatabase();
        this.openWrite();
        try {
            mDb.beginTransaction();

            JSONArray carrierArr = response.getJSONArray("carrier");
            for (int i = 0; i < carrierArr.length(); i++){
                JSONObject carrier = (JSONObject) carrierArr.get(i);
                this.setCarrier(carrier.getString("carrier_id"), carrier.getString("carrier_type"), carrier.getString("name"), carrier.getString("is_active"));

            }

            JSONArray airlinesArr = response.getJSONArray("airline");
            for (int i = 0; i < airlinesArr.length(); i++) {
                JSONObject airline = (JSONObject) airlinesArr.get(i);
                this.setAirline(airline.getString("flight_number"), airline.getString("carrier_id"), airline.getString("name"),
                        airline.getString("meal"), airline.getString("airport_tax"), airline.getInt("domestic_baggage"), airline.getInt("international_baggage"));
            }

            JSONArray airportsArr = response.getJSONArray("airport");
            if (airportsArr.length() > 0) {
                ContentValues cv = new ContentValues();
                cv.put(Configuration.FIELD_IS_ACTIVE, "N");
                mDb.update(Configuration.TABLE_PORT, cv,
                        Configuration.FIELD_IS_ACTIVE + " = 'Y' AND "+Configuration.FIELD_CARRIER_TYPE + "='flight'",
                        null);

                for (int i = 0; i < airportsArr.length(); i++) {
                    JSONObject airport = (JSONObject) airportsArr.get(i);

                    this.setAirport(airport.getString("iata_code"), airport.getString("airport_name"), airport.getString("name_alt"), airport.getString("city"), airport.getString("city_alt"), airport.getString("search"),
                            airport.getString("country_code"), airport.getDouble("latitude"), airport.getDouble("longitude"), airport.getDouble("timezone"), airport.getString("tz"), airport.getString("is_active"));

                }
            }
            JSONArray stationsArr = response.getJSONArray("station");
            if (stationsArr.length() > 0) {
                ContentValues cv = new ContentValues();
                cv.put(Configuration.FIELD_IS_ACTIVE, "N");
                mDb.update(Configuration.TABLE_PORT, cv,
                        Configuration.FIELD_IS_ACTIVE + " = 'Y' AND "+Configuration.FIELD_CARRIER_TYPE + "='train'",
                        null);
                for (int i = 0; i < stationsArr.length(); i++) {
                    JSONObject station = (JSONObject) stationsArr.get(i);
                    this.setStation(station.getString("iata_code"), station.getString("station_name"), station.getString("name_alt"), station.getString("city"), station.getString("city_alt"), station.getString("search"),
                            station.getString("country_code"), station.getDouble("latitude"), station.getDouble("longitude"), station.getDouble("timezone"), station.getString("tz"), station.getString("is_active"));

                }
            }

            if (this.getSettingValue("device_id").equals("000000000000000")) {
                this.setSettingValue("device_id", response.getString("device_id"));
            }
            Gson gson = new Gson();
            JSONObject jsonSlideshow = response.getJSONObject("slideshow");

            if (jsonSlideshow.getInt("count") == 0) {
                this.setSettingValue("slideshow", null);
            } else {
                ArrayList<Slideshow> slideshow = new ArrayList<>();
                JSONArray data = jsonSlideshow.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject slide = (JSONObject) data.get(i);
                    Slideshow slideObj = new Slideshow(slide.getString("image"), slide.getString("kind"), slide.getString("en_title"), slide.getString("in_title"), slide.getString("en_description"), slide.getString("in_description"));
                    slideshow.add(slideObj);
                }
                this.setSettingValue("slideshow", gson.toJson(slideshow));
            }

            JSONObject jsonPromo = response.getJSONObject("promo");

            if (jsonPromo.getInt("count") == 0) {
                this.setSettingValue("promo", null);
            } else {
                ArrayList<Promo> promos = new ArrayList<>();
                JSONArray data = jsonPromo.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject promo = (JSONObject) data.get(i);
                    Promo promoObj = new Promo(promo.getString("name"), promo.getString("image"), promo.getString("en_title"), promo.getString("in_title"), promo.getString("en_summary"), promo.getString("in_summary"), promo.getString("en_description"), promo.getString("in_description"));
                    promos.add(promoObj);
                }
                this.setSettingValue("promo", gson.toJson(promos));
            }



            JSONObject jsonSlideshowAuth = response.getJSONObject("slideshow_auth");

            if (jsonSlideshowAuth.getInt("count") == 0) {
                this.updateSettingValue("slideshow_auth", null);
            } else {
                ArrayList<Slideshow> slideshow = new ArrayList<>();
                JSONArray data = jsonSlideshowAuth.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject slide = (JSONObject) data.get(i);
                    Slideshow slideObj = new Slideshow(slide.getString("image"), slide.getString("kind"), slide.getString("en_title"), slide.getString("in_title"), slide.getString("en_description"), slide.getString("in_description"));
                    slideshow.add(slideObj);
                }
                this.updateSettingValue("slideshow_auth", gson.toJson(slideshow));
            }

            JSONObject jsonPromoAuth = response.getJSONObject("promo_auth");

            if (jsonPromoAuth.getInt("count") == 0) {
                this.updateSettingValue("promo_auth", null);
            } else {
                ArrayList<Promo> promos = new ArrayList<>();
                JSONArray data = jsonPromoAuth.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject promo = (JSONObject) data.get(i);
                    Promo promoObj = new Promo(promo.getString("name"), promo.getString("image"), promo.getString("en_title"), promo.getString("in_title"), promo.getString("en_summary"), promo.getString("in_summary"), promo.getString("en_description"), promo.getString("in_description"));
                    promos.add(promoObj);
                }
                this.updateSettingValue("promo_auth", gson.toJson(promos));
            }

            this.setSettingValue("contact_phone", response.getString("contact_phone"));
            this.setSettingValue("contact_phone_strip", response.getString("contact_phone_strip"));
            this.setSettingValue("contact_mobile", response.getString("contact_mobile"));
            this.setSettingValue("contact_mobile_strip", response.getString("contact_mobile_strip"));
            this.setSettingValue("contact_wa", response.getString("contact_wa"));
            this.setSettingValue("contact_wa_strip", response.getString("contact_wa_strip"));
            this.setSettingValue("contact_pin_bbm", response.getString("contact_pin_bbm"));
            this.setSettingValue("facebook_url", response.getString("facebook_url"));
            this.setSettingValue("facebook_id", response.getString("facebook_id"));
            this.setSettingValue("twitter_id", response.getString("twitter_id"));
            this.setSettingValue("instagram_id", response.getString("instagram_id"));
            this.setSettingValue("googleplus_id", response.getString("googleplus_id"));
            this.setSettingValue("contact_address", response.getString("contact_address"));

            this.setSettingValue("flight_feature", response.getString("flight_feature"));
            this.setSettingValue("train_feature", response.getString("train_feature"));
            this.setSettingValue("hotel_feature", response.getString("hotel_feature"));
            this.setSettingValue("tour_feature", response.getString("tour_feature"));
            this.setSettingValue("ship_feature", response.getString("ship_feature"));
            this.setSettingValue("cargo_feature", response.getString("cargo_feature"));



            
            mDb.setTransactionSuccessful();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {
            mDb.endTransaction();
            this.close();
        }
    }



    private void setSettingValue(String parameter, String value){
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_VALUE, value);
        mDb.update(Configuration.TABLE_SETTING, cv,
                Configuration.FIELD_PARAMETER + " = ?",
                new String[] { parameter });
    }

    private void updateSettingValue(String parameter, String value){
        String countQuery = "SELECT " + Configuration.FIELD_VALUE
                + " FROM " + Configuration.TABLE_SETTING + " WHERE " + Configuration.FIELD_PARAMETER + " = '"+ parameter + "'";
        Cursor cursor = mDb.rawQuery(countQuery, null);

        boolean isEmpty = cursor.getCount() == 0;
        cursor.close();
        if(isEmpty){
            ContentValues cv = new ContentValues();
            cv.put(Configuration.FIELD_PARAMETER, parameter);
            cv.put(Configuration.FIELD_VALUE, value);
            mDb.insert(Configuration.TABLE_SETTING, null, cv);
        }
        else{
            this.setSettingValue(parameter, value);
        }
    }

    public String getSettingValue(String parameter){
        String selectQuery = "SELECT " + Configuration.FIELD_VALUE
                + " FROM " + Configuration.TABLE_SETTING + " WHERE " + Configuration.FIELD_PARAMETER + " = '" + parameter + "'";

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        String value = cursor.getString(0);
        cursor.close();
        return value;
    }

    private void setCarrier(String carrierID, String carrierType, String name, String isActive){

        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_CARRIER_ID, carrierID);
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_CARRIER_TYPE, carrierType);
        cv.put(Configuration.FIELD_IS_ACTIVE, isActive);



        if(this.existCarrier(carrierID)){
//            Log.e("update "+flightNumber, carrierID);
            mDb.update(Configuration.TABLE_CARRIER, cv,
                    Configuration.FIELD_CARRIER_ID + " = '"+carrierID+"'",
                    null);
        }
        else{
//            Log.e("insert "+flightNumber, carrierID);
            mDb.insert(Configuration.TABLE_CARRIER, null, cv);
        }
    }

    private boolean existCarrier(String carrierID){
        String countQuery = "SELECT * FROM " + Configuration.TABLE_CARRIER
                + " WHERE " + Configuration.FIELD_CARRIER_ID + "='" + carrierID + "'";
        Cursor cursor = mDb.rawQuery(countQuery, null);
        boolean ret = cursor.getCount() > 0;
        cursor.close();
        return ret;
    }
}
