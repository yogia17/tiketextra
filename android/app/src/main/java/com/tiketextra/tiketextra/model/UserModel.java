package com.tiketextra.tiketextra.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.object.Passenger;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kurnia on 20/11/17.
 */

public class UserModel {
    private final String TAG = "UserModel";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public UserModel(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
    }

    private UserModel createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    private UserModel open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private UserModel openWrite() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getWritableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    private void close()
    {
        mDb.close();
        mDbHelper.close();
    }

    public void insertPassenger(int passengerID, String title, String name, String birthDate, String nationality, String passportNumber, String passportIssuingCountry, String passportExpiryDate){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_PASSENGER_ID, passengerID);
        cv.put(Configuration.FIELD_PASSENGER_TITLE, title);
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_BIRTH_DATE, birthDate);
        cv.put(Configuration.FIELD_NATIONALITY, nationality);
        cv.put(Configuration.FIELD_PASSPORT_NUMBER, passportNumber==null?"":passportNumber);
        cv.put(Configuration.FIELD_PASSPORT_ISSUING_COUNTRY, passportIssuingCountry==null?"":passportIssuingCountry);
        cv.put(Configuration.FIELD_PASSPORT_EXPIRY_DATE, passportExpiryDate==null?"":passportExpiryDate);

        mDb.insert(Configuration.TABLE_USER_PASSENGER, null, cv);
        this.close();
    }

    public void updatePassenger(int passengerID, String title, String name, String birthDate, String nationality, String passportNumber, String passportIssuingCountry, String passportExpiryDate){
        this.createDatabase();
        this.openWrite();
        ContentValues cv = new ContentValues();
        cv.put(Configuration.FIELD_PASSENGER_TITLE, title);
        cv.put(Configuration.FIELD_NAME, name);
        cv.put(Configuration.FIELD_BIRTH_DATE, birthDate);
        cv.put(Configuration.FIELD_NATIONALITY, nationality);
        cv.put(Configuration.FIELD_PASSPORT_NUMBER, passportNumber==null?"":passportNumber);
        cv.put(Configuration.FIELD_PASSPORT_ISSUING_COUNTRY, passportIssuingCountry==null?"":passportIssuingCountry);
        cv.put(Configuration.FIELD_PASSPORT_EXPIRY_DATE, passportExpiryDate==null?"":passportExpiryDate);
        mDb.update(Configuration.TABLE_USER_PASSENGER, cv, Configuration.FIELD_PASSENGER_ID + "= ?", new String[]{Integer.toString(passengerID)});
        this.close();
    }

    public void deletePassenger(int passengerID){
        this.createDatabase();
        this.openWrite();
        mDb.delete(Configuration.TABLE_USER_PASSENGER, Configuration.FIELD_PASSENGER_ID + "= ?", new String[]{Integer.toString(passengerID)});
        this.close();
    }

    public void deletePassenger(){
        this.createDatabase();
        this.openWrite();
        mDb.delete(Configuration.TABLE_USER_PASSENGER, null, null);
        this.close();
    }

    public ArrayList<Passenger> getPassenger(){
        ArrayList<Passenger> passengers = new ArrayList<>();
        String selectQuery = "SELECT " + Configuration.FIELD_PASSENGER_ID + ", "
                + Configuration.FIELD_PASSENGER_TITLE + ","
                + Configuration.FIELD_NAME + ","
                + Configuration.FIELD_BIRTH_DATE + ","
                + Configuration.FIELD_NATIONALITY + ","
                + Configuration.FIELD_PASSPORT_NUMBER + ","
                + Configuration.FIELD_PASSPORT_ISSUING_COUNTRY + ","
                + Configuration.FIELD_PASSPORT_EXPIRY_DATE
                + " FROM " + Configuration.TABLE_USER_PASSENGER;

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                passengers.add(new Passenger(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7)
                ));
            } while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return passengers;
    }

    public Passenger getPassengerByName(String name){

        String selectQuery = "SELECT " + Configuration.FIELD_PASSENGER_ID + ", "
                + Configuration.FIELD_PASSENGER_TITLE + ","
                + Configuration.FIELD_NAME + ","
                + Configuration.FIELD_BIRTH_DATE + ","
                + Configuration.FIELD_NATIONALITY + ","
                + Configuration.FIELD_PASSPORT_NUMBER + ","
                + Configuration.FIELD_PASSPORT_ISSUING_COUNTRY + ","
                + Configuration.FIELD_PASSPORT_EXPIRY_DATE
                + " FROM " + Configuration.TABLE_USER_PASSENGER
                + " WHERE " + Configuration.FIELD_NAME + " LIKE '%" + name + "%'";;

        this.createDatabase();
        this.open();

        Cursor cursor = mDb.rawQuery(selectQuery, null);
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            Passenger passenger = new Passenger(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7)
                );
            cursor.close();
            this.close();
            return passenger;
        }
        else{
            return null;
        }
    }
}
