package com.tiketextra.tiketextra.object;

/**
 * Created by kurnia on 05/09/17.
 */

public class Airline {
    private String flightCode, carrierID, name, meal, airportTax;
    private int domesticBaggage, internationalBaggage;

    public Airline(String flightCode, String carrierID, String name, String meal, String airportTax, int domesticBaggage, int internationalBaggage) {
        this.flightCode = flightCode;
        this.carrierID = carrierID;
        this.name = name;
        this.meal = meal;
        this.airportTax = airportTax;
        this.domesticBaggage = domesticBaggage;
        this.internationalBaggage = internationalBaggage;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public String getCarrierID() {
        return carrierID;
    }

    public String getName() {
        return name;
    }

    public String getMeal() {
        return meal;
    }

    public String getAirportTax() {
        return airportTax;
    }

    public int getDomesticBaggage() {
        return domesticBaggage;
    }

    public int getInternationalBaggage() {
        return internationalBaggage;
    }
}
