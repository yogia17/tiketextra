package com.tiketextra.tiketextra.object;

public class Airport {
	private String iataCode, name, nameAlt, city, cityAlt, countryCode, country;
	private int hit;
	private double timezone, latitude, longitude;
//	public Airport(String iataCode, String name, String city,
//				   String countryCode, String country, double latitude, double longitude, double timezone, int hit) {
//		this.iataCode = iataCode;
//		this.name = name;
//		this.city = city;
//		this.countryCode = countryCode;
//		this.country = country;
//		this.timezone = timezone;
//		this.latitude = latitude;
//		this.longitude = longitude;
//		this.hit = hit;
//	}

	public Airport(String iataCode, String name, String nameAlt, String city, String cityAlt, String countryCode, String country, double latitude, double longitude, double timezone, int hit) {
		this.iataCode = iataCode;
		this.name = name;
		this.nameAlt = nameAlt;
		this.city = city;
		this.cityAlt = cityAlt;
		this.countryCode = countryCode;
		this.country = country;
		this.hit = hit;
		this.timezone = timezone;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getIataCode() {
		return iataCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public String getCountry() {
		return country;
	}
	public double getTimezone() {
		return timezone;
	}
	public int getHit() {
		return hit;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String getAirportName(){
		if(getName()==null || getName().equals("") || getName().equals("null")){
			return getCity() + " Airport";
		}
		else {
			return getName();
		}
	}

	public String getAirport(){
		if(getName()==null || getName().equals("") || getName().equals("null")){
			return getCity();
		}
		else if(getName().equals(getCity())){
			return getCity();
		}
		else {
			return getName()+", "+getCity();
		}
	}

	public String getAirportLite(){
		if(getName()==null || getName().equals("") || getName().equals("null")){
			return getCity();
		}
		else if(getName().equals(getCity())){
			return getCity();
		}
		else {
			return getName().replace(" International", "").replace(" Airport", "")+", "+getCity();
		}
	}

	public String getNameAlt() {
		return nameAlt;
	}

	public String getCityAlt() {
		return cityAlt;
	}
}