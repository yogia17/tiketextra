package com.tiketextra.tiketextra.object;

import java.util.Locale;

/**
 * Created by kurnia on 14/10/17.
 */

public class Bank {
    private int bankID;
    private float discountValue, surchargeValue;
    private String gateway, name, owner, account, discountType, surchargeType, offlineHour, image, enDescription, inDescription, timeLimit;

    public Bank(int bankID, float discountValue, float surchargeValue, String gateway, String name, String owner, String account, String discountType, String surchargeType, String offlineHour, String image, String enDescription, String inDescription, String timeLimit) {
        this.bankID = bankID;
        this.timeLimit = timeLimit;
        this.discountValue = discountValue;
        this.surchargeValue = surchargeValue;
        this.gateway = gateway;
        this.name = name;
        this.owner = owner;
        this.account = account;
        this.discountType = discountType;
        this.surchargeType = surchargeType;
        this.offlineHour = offlineHour;
        this.image = image;
        this.enDescription = enDescription;
        this.inDescription = inDescription;
    }

    public int getBankID() {
        return bankID;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public float getDiscountValue() {
        return discountValue;
    }

    public float getSurchargeValue() {
        return surchargeValue;
    }

    public String getGateway() {
        return gateway;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getAccount() {
        return account;
    }

    public String getDiscountType() {
        return discountType;
    }

    public String getSurchargeType() {
        return surchargeType;
    }

    public String getOfflineHour() {
        return offlineHour;
    }

    public String getImage() {
        return image;
    }

    public String getDescription(Locale locale){
        return locale.getLanguage().equals("en")?enDescription:inDescription;
    }
}
