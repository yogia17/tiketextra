package com.tiketextra.tiketextra.object;

public class Carrier {
    private String carrierID, carrierType, name, isActive;

    public Carrier(String carrierID, String carrierType, String name, String isActive) {
        this.carrierID = carrierID;
        this.carrierType = carrierType;
        this.name = name;
        this.isActive = isActive;
    }

    public Carrier(String carrierID, String name) {
        this.carrierID = carrierID;
        this.name = name;
    }

    public String getCarrierID() {
        return carrierID;
    }

    public String getName() {
        return name;
    }
}
