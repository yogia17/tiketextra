package com.tiketextra.tiketextra.object;

/**
 * Created by kurnia on 21/09/17.
 */

public class Contact {
    private int contactID;
    private String title, fullName, phone, email, city;

    public Contact(int contactID, String title, String fullName, String phone, String email) {
        this.contactID = contactID;
        this.title = title;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
    }

    public Contact(String title, String fullName, String phone, String email) {
        this.title = title;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
    }

    public Contact(String title, String fullName, String phone, String email, String city) {
        this.title = title;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.city = city;
    }

    public int getContactID() {
        return contactID;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getTitle() {
        return title;
    }

    public String getCity() {
        return city;
    }
}
