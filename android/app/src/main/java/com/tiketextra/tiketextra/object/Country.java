package com.tiketextra.tiketextra.object;

public class Country {
	private String countryCode, country;

	public Country(String countryCode, String country) {
		this.countryCode = countryCode;
		this.country = country;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}