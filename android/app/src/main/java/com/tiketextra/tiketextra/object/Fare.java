package com.tiketextra.tiketextra.object;

/**
 * Created by kurnia on 03/10/17.
 */

public class Fare {
    private int fareID, reservationID, basic, tax, iwjr, insurance, fuel, administration, surcharge, discount, count;
    private String carrierID, passengerType;

    public Fare(int fareID, int reservationID, String carrierID, String passengerType, int basic, int tax, int iwjr, int insurance, int fuel, int administration, int surcharge, int discount, int count) {
        this.fareID = fareID;
        this.reservationID = reservationID;
        this.basic = basic;
        this.tax = tax;
        this.iwjr = iwjr;
        this.insurance = insurance;
        this.fuel = fuel;
        this.administration = administration;
        this.surcharge = surcharge;
        this.discount = discount;
        this.count = count;
        this.carrierID = carrierID;
        this.passengerType = passengerType;
    }

    public int getFareID() {
        return fareID;
    }

    public int getReservationID() {
        return reservationID;
    }

    public int getBasic() {
        return basic;
    }

    public int getTax() {
        return tax;
    }

    public int getIwjr() {
        return iwjr;
    }

    public int getInsurance() {
        return insurance;
    }

    public int getFuel() {
        return fuel;
    }

    public int getAdministration() {
        return administration;
    }

    public int getSurcharge() {
        return surcharge;
    }

    public int getDiscount() {
        return discount;
    }

    public int getCount() {
        return count;
    }

    public String getCarrierID() {
        return carrierID;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public int getTotal(){
        return (basic + tax + iwjr + insurance + fuel + administration + surcharge + discount) * count;
    }

    public int getTotalDiscount(){
        return discount * count;
    }
}
