package com.tiketextra.tiketextra.object;

public class FareComponent {
	private int basic, tax, fuel, adm, iwjr, surcharge, discount, total;
	private boolean ruleFromProfit = false, discountRuleFromProfit = false, surchargeRuleFromProfit = false;

	public FareComponent(int basic, int tax, int fuel, int adm, int iwjr,
						 int surcharge, int discount, int total, String discountRuleFrom, String surchargeRuleFrom) {
		this.basic = basic;
		this.tax = tax;
		this.fuel = fuel;
		this.adm = adm;
		this.iwjr = iwjr;
		this.surcharge = surcharge;
		this.discount = discount;
		this.total = total;
		this.ruleFromProfit = discountRuleFrom.equals("profit") || surchargeRuleFrom.equals("profit");
		this.discountRuleFromProfit = discountRuleFrom.equals("profit");
		this.surchargeRuleFromProfit = surchargeRuleFrom.equals("profit");
	}

	public boolean isDiscountRuleFromProfit() {
		return discountRuleFromProfit;
	}

	public boolean isSurchargeRuleFromProfit() {
		return surchargeRuleFromProfit;
	}

	public boolean isRuleFromProfit() {
		return ruleFromProfit;
	}

	public int getBasic() {
		return basic;
	}

	public void setBasic(int basic) {
		this.basic = basic;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getAdm() {
		return adm;
	}

	public void setAdm(int adm) {
		this.adm = adm;
	}

	public int getIwjr() {
		return iwjr;
	}

	public void setIwjr(int iwjr) {
		this.iwjr = iwjr;
	}

	public int getSurcharge() {
		return surcharge;
	}

	public void setSurcharge(int surcharge) {
		this.surcharge = surcharge;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}	
	
}
