package com.tiketextra.tiketextra.object;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.util.Utils;

import java.util.ArrayList;
import java.util.Map;

public class FareRoute implements Comparable<FareRoute> {
	private String fareClass, fareID;
	private Map<String, FareComponent> fare;
	private ArrayList<Flight> flight;
	private boolean selected = false;
	private boolean filterAirline = false;
	private boolean filterClass = false;
	private boolean filterTransitNum = false;

	private boolean filterPrice = false;

	private int hiddenTransitCount = 0;

	public FareRoute(String fareClass, String fareID, Map<String, FareComponent> fare,
			ArrayList<Flight> flight, int hiddenTransitCount) {
		super();
		this.fareClass = fareClass;
		this.fareID = fareID;
		this.fare = fare;
		this.flight = flight;
		this.hiddenTransitCount = hiddenTransitCount;
	}


	public int getHiddenTransitCount() {
		return hiddenTransitCount;
	}

	public String getDepartPort(){
		return flight.get(0).getDepartPort();
	}

    public String getDepartTime(){
        return flight.get(0).getDepartTime();
    }

    public String getDepartDatetime(){
        return flight.get(0).getDepartDatetime();
    }

    public String getDepartDate(){
		return flight.get(0).getDepartDatetime().substring(0, 10);
	}

    public String getArrivePort(){
        return flight.get(flight.size()-1).getArrivePort();
    }

    public String getArriveTime(){
        return flight.get(flight.size()-1).getArriveTime();
    }

    public String getArriveDatetime(){
        return flight.get(flight.size()-1).getArriveDatetime();
    }

    public String getAirlineCode(){
        return flight.get(0).getAirlineCode();
    }

    public String getFlightCode(){
        return flight.get(0).getFlightCode();
    }

    public String getFareClass() {
		return fareClass;
	}

	public String getFareID() {
		return fareID;
	}

	public Map<String, FareComponent> getFare() {
		return fare;
	}

	public ArrayList<Flight> getFlight() {
		return flight;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setFilterClass(boolean filterClass) {
		this.filterClass = filterClass;
	}

	public boolean isFilterClass() {
		return filterClass;
	}

	public boolean isFilterAirline() {
		return filterAirline;
	}

	public void setFilterAirline(boolean filterAirline) {
		this.filterAirline = filterAirline;
	}

	public boolean isFilterTransitNum() {
		return filterTransitNum;
	}

	public void setFilterTransitNum(boolean filterTransitNum) {
		this.filterTransitNum = filterTransitNum;
	}

	public boolean isFilterPrice() {
		return filterPrice;
	}

	public void setFilterPrice(boolean filterPrice) {
		this.filterPrice = filterPrice;
	}

	public int getTotalPriceBeforeDiscount(int adult, int child, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += (fare.get("adult").getTotal() - (fare.get("adult").isDiscountRuleFromProfit()?0:fare.get("adult").getDiscount())) * adult;
		}
		if(fare.containsKey("child") && child > 0){
			ret += (fare.get("child").getTotal() - (fare.get("child").isDiscountRuleFromProfit()?0:fare.get("child").getDiscount())) * child;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += (fare.get("infant").getTotal() - (fare.get("infant").isDiscountRuleFromProfit()?0:fare.get("infant").getDiscount())) * infant;
		}
		return ret;
	}

	public int getTotalDiscount(int adult, int child, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += (fare.get("adult").isDiscountRuleFromProfit()?0:fare.get("adult").getDiscount()) * adult;
		}
		if(fare.containsKey("child") && child > 0){
			ret += (fare.get("child").isDiscountRuleFromProfit()?0:fare.get("child").getDiscount()) * child;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += (fare.get("infant").isDiscountRuleFromProfit()?0:fare.get("infant").getDiscount()) * infant;
		}
		return ret;
	}



	public int getTotalPrice(int adult, int child, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += fare.get("adult").getTotal() * adult;
		}
		if(fare.containsKey("child") && child > 0){
			ret += fare.get("child").getTotal() * child;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += fare.get("infant").getTotal() * infant;
		}
		return ret;
	}

	@Override
	public int compareTo(FareRoute another) {

		switch (AppHelper.mode) {
			default:
			case Configuration.MODE_DEPART: {
				switch (AppHelper.departSort) {
					default:
					case Configuration.SORT_LOWEST_PRICE: {
						int compare = another.getFare().get("adult").getTotal();
						return this.fare.get("adult").getTotal() - compare;

					}
					case Configuration.SORT_DEPART_TIME: {
						int compare = another.getFlight().get(0).getDepartTimeMinute();
						return this.flight.get(0).getDepartTimeMinute() - compare;

					}
					case Configuration.SORT_ARRIVE_TIME: {
						int compare = another.getFlight().get(another.getFlight().size()-1).getArriveTimeMinute();
						return this.flight.get(this.flight.size()-1).getArriveTimeMinute() - compare;

					}
					case Configuration.SORT_DURATION: {
						int compare = Utils.timeDiffMinutes(another.getFlight().get(0).getDepartDatetime(), another.getFlight().get(another.getFlight().size()-1).getArriveDatetime(), another.getFlight().get(0).getDepartTimezone() - another.getFlight().get(another.getFlight().size()-1).getArriveTimezone());
						return Utils.timeDiffMinutes(this.getFlight().get(0).getDepartDatetime(), this.getFlight().get(this.getFlight().size()-1).getArriveDatetime(), this.getFlight().get(0).getDepartTimezone() - this.getFlight().get(this.getFlight().size()-1).getArriveTimezone()) - compare;

					}
					case Configuration.SORT_CABIN_CLASS: {
						int compare = another.getFareClass().equals("pro")?0:(another.getFareClass().equals("eco")?1:(another.getFareClass().equals("bus")?2:3));
						return (this.getFareClass().equals("pro")?0:(this.getFareClass().equals("eco")?1:(this.getFareClass().equals("bus")?2:3))) - compare;

					}
				}
			}
			case Configuration.MODE_RETURN:{
				switch (AppHelper.returnSort) {
					default:
					case Configuration.SORT_LOWEST_PRICE: {
						int compare = another.getFare().get("adult").getTotal();
						return this.fare.get("adult").getTotal() - compare;

					}
					case Configuration.SORT_DEPART_TIME: {
						int compare = another.getFlight().get(0).getDepartTimeMinute();
						return this.flight.get(0).getDepartTimeMinute() - compare;

					}
					case Configuration.SORT_ARRIVE_TIME: {
						int compare = another.getFlight().get(another.getFlight().size()-1).getArriveTimeMinute();
						return this.flight.get(this.flight.size()-1).getArriveTimeMinute() - compare;

					}
					case Configuration.SORT_DURATION: {
						int compare = Utils.timeDiffMinutes(another.getFlight().get(0).getDepartDatetime(), another.getFlight().get(another.getFlight().size()-1).getArriveDatetime(), another.getFlight().get(0).getDepartTimezone() - another.getFlight().get(another.getFlight().size()-1).getArriveTimezone());
						return Utils.timeDiffMinutes(this.getFlight().get(0).getDepartDatetime(), this.getFlight().get(this.getFlight().size()-1).getArriveDatetime(), this.getFlight().get(0).getDepartTimezone() - this.getFlight().get(this.getFlight().size()-1).getArriveTimezone()) - compare;

					}
					case Configuration.SORT_CABIN_CLASS: {
						int compare = another.getFareClass().equals("pro")?0:(another.getFareClass().equals("eco")?1:(another.getFareClass().equals("bus")?2:3));
						return (this.getFareClass().equals("pro")?0:(this.getFareClass().equals("eco")?1:(this.getFareClass().equals("bus")?2:3))) - compare;

					}
				}
			}
		}

	}
	
}
