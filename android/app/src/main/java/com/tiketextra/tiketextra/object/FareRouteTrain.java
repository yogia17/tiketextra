package com.tiketextra.tiketextra.object;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;
import com.tiketextra.tiketextra.util.Utils;

import java.util.ArrayList;
import java.util.Map;

public class FareRouteTrain implements Comparable<FareRouteTrain> {
	private String fareClass, fareID;
	private int seatAvail;
	private Map<String, FareComponent> fare;
	private ArrayList<Train> train;
	private boolean selected = false;
	private boolean filterCarrier = false;
	private boolean filterClass = false;
	private boolean filterTransitNum = false;

	private boolean filterPrice = false;

	public FareRouteTrain(String fareClass, String fareID, int seatAvail, Map<String, FareComponent> fare,
                          ArrayList<Train> train) {
		super();
		this.fareClass = fareClass;
		this.fareID = fareID;
		this.seatAvail = seatAvail;
		this.fare = fare;
		this.train = train;
	}

	public int getSeatAvail() {
		return seatAvail;
	}

	public String getDepartPort(){
		return train.get(0).getDepartPort();
	}

    public String getDepartTime(){
        return train.get(0).getDepartTime();
    }

    public String getDepartDatetime(){
        return train.get(0).getDepartDatetime();
    }

    public String getDepartDate(){
		return train.get(0).getDepartDatetime().substring(0, 10);
	}

    public String getArrivePort(){
        return train.get(train.size()-1).getArrivePort();
    }

    public String getArriveTime(){
        return train.get(train.size()-1).getArriveTime();
    }

    public String getArriveDatetime(){
        return train.get(train.size()-1).getArriveDatetime();
    }

    public String getCarrierCode(){
        return train.get(0).getCarrierCode();
	}

	public String getTrainNameNumber(){
		return train.get(0).getTrainNameNumber();
	}

    public String getFareClass() {
		return fareClass;
	}

	public String getFareID() {
		return fareID;
	}

	public Map<String, FareComponent> getFare() {
		return fare;
	}

	public ArrayList<Train> getTrain() {
		return train;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setFilterClass(boolean filterClass) {
		this.filterClass = filterClass;
	}

	public boolean isFilterClass() {
		return filterClass;
	}

	public boolean isFilterCarrier() {
		return filterCarrier;
	}

	public void setFilterCarrier(boolean filterCarrier) {
		this.filterCarrier = filterCarrier;
	}

	public boolean isFilterTransitNum() {
		return filterTransitNum;
	}

	public void setFilterTransitNum(boolean filterTransitNum) {
		this.filterTransitNum = filterTransitNum;
	}

	public boolean isFilterPrice() {
		return filterPrice;
	}

	public void setFilterPrice(boolean filterPrice) {
		this.filterPrice = filterPrice;
	}

	public int getTotalPriceBeforeDiscount(int adult, int child, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += (fare.get("adult").getTotal() - (fare.get("adult").isDiscountRuleFromProfit()?0:fare.get("adult").getDiscount())) * adult;
		}
		if(fare.containsKey("child") && child > 0){
			ret += (fare.get("child").getTotal() - (fare.get("child").isDiscountRuleFromProfit()?0:fare.get("child").getDiscount())) * child;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += (fare.get("infant").getTotal() - (fare.get("infant").isDiscountRuleFromProfit()?0:fare.get("infant").getDiscount())) * infant;
		}
		return ret;
	}

	public int getTotalPriceBeforeDiscount(int adult, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += (fare.get("adult").getTotal() - (fare.get("adult").isDiscountRuleFromProfit()?0:fare.get("adult").getDiscount())) * adult;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += (fare.get("infant").getTotal() - (fare.get("infant").isDiscountRuleFromProfit()?0:fare.get("infant").getDiscount())) * infant;
		}
		return ret;
	}

	public int getTotalDiscount(int adult, int child, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += (fare.get("adult").isDiscountRuleFromProfit()?0:fare.get("adult").getDiscount()) * adult;
		}
		if(fare.containsKey("child") && child > 0){
			ret += (fare.get("child").isDiscountRuleFromProfit()?0:fare.get("child").getDiscount()) * child;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += (fare.get("infant").isDiscountRuleFromProfit()?0:fare.get("infant").getDiscount()) * infant;
		}
		return ret;
	}

	public int getTotalDiscount(int adult, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += (fare.get("adult").isDiscountRuleFromProfit()?0:fare.get("adult").getDiscount()) * adult;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += (fare.get("infant").isDiscountRuleFromProfit()?0:fare.get("infant").getDiscount()) * infant;
		}
		return ret;
	}

	public int getTotalPrice(int adult, int child, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += fare.get("adult").getTotal() * adult;
		}
		if(fare.containsKey("child") && child > 0){
			ret += fare.get("child").getTotal() * child;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += fare.get("infant").getTotal() * infant;
		}
		return ret;
	}

	public int getTotalPrice(int adult, int infant){
		int ret = 0;
		if(fare.containsKey("adult") && adult > 0){
			ret += fare.get("adult").getTotal() * adult;
		}
		if(fare.containsKey("infant") && infant > 0){
			ret += fare.get("infant").getTotal() * infant;
		}
		return ret;
	}

	@Override
	public int compareTo(FareRouteTrain another) {

		switch (AppHelper.mode) {
			default:
			case Configuration.MODE_DEPART: {
				switch (AppHelper.departSort) {
					default:
					case Configuration.SORT_LOWEST_PRICE: {
						int compare = another.getFare().get("adult").getTotal();
						return this.fare.get("adult").getTotal() - compare;

					}
					case Configuration.SORT_DEPART_TIME: {
						int compare = another.getTrain().get(0).getDepartTimeMinute();
						return this.train.get(0).getDepartTimeMinute() - compare;

					}
					case Configuration.SORT_ARRIVE_TIME: {
						int compare = another.getTrain().get(another.getTrain().size()-1).getArriveTimeMinute();
						return this.train.get(this.train.size()-1).getArriveTimeMinute() - compare;

					}
					case Configuration.SORT_DURATION: {
						int compare = Utils.timeDiffMinutes(another.getTrain().get(0).getDepartDatetime(), another.getTrain().get(another.getTrain().size()-1).getArriveDatetime(), another.getTrain().get(0).getDepartTimezone() - another.getTrain().get(another.getTrain().size()-1).getArriveTimezone());
						return Utils.timeDiffMinutes(this.getTrain().get(0).getDepartDatetime(), this.getTrain().get(this.getTrain().size()-1).getArriveDatetime(), this.getTrain().get(0).getDepartTimezone() - this.getTrain().get(this.getTrain().size()-1).getArriveTimezone()) - compare;

					}
					case Configuration.SORT_CABIN_CLASS: {
						int compare = another.getFareClass().equals("pro")?0:(another.getFareClass().equals("eco")?1:(another.getFareClass().equals("bus")?2:3));
						return (this.getFareClass().equals("pro")?0:(this.getFareClass().equals("eco")?1:(this.getFareClass().equals("bus")?2:3))) - compare;

					}
				}
			}
			case Configuration.MODE_RETURN:{
				switch (AppHelper.returnSort) {
					default:
					case Configuration.SORT_LOWEST_PRICE: {
						int compare = another.getFare().get("adult").getTotal();
						return this.fare.get("adult").getTotal() - compare;

					}
					case Configuration.SORT_DEPART_TIME: {
						int compare = another.getTrain().get(0).getDepartTimeMinute();
						return this.train.get(0).getDepartTimeMinute() - compare;

					}
					case Configuration.SORT_ARRIVE_TIME: {
						int compare = another.getTrain().get(another.getTrain().size()-1).getArriveTimeMinute();
						return this.train.get(this.train.size()-1).getArriveTimeMinute() - compare;

					}
					case Configuration.SORT_DURATION: {
						int compare = Utils.timeDiffMinutes(another.getTrain().get(0).getDepartDatetime(), another.getTrain().get(another.getTrain().size()-1).getArriveDatetime(), another.getTrain().get(0).getDepartTimezone() - another.getTrain().get(another.getTrain().size()-1).getArriveTimezone());
						return Utils.timeDiffMinutes(this.getTrain().get(0).getDepartDatetime(), this.getTrain().get(this.getTrain().size()-1).getArriveDatetime(), this.getTrain().get(0).getDepartTimezone() - this.getTrain().get(this.getTrain().size()-1).getArriveTimezone()) - compare;

					}
					case Configuration.SORT_CABIN_CLASS: {
						int compare = another.getFareClass().equals("pro")?0:(another.getFareClass().equals("eco")?1:(another.getFareClass().equals("bus")?2:3));
						return (this.getFareClass().equals("pro")?0:(this.getFareClass().equals("eco")?1:(this.getFareClass().equals("bus")?2:3))) - compare;

					}
				}
			}
		}

	}
	
}
