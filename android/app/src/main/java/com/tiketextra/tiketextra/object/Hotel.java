package com.tiketextra.tiketextra.object;

import com.tiketextra.tiketextra.config.Configuration;
import com.tiketextra.tiketextra.helper.AppHelper;

public class Hotel implements Comparable <Hotel> {
    private String hotelID, name, address, region, image, billerID, fpID;
    private int rating, priceBefore, price;

    private boolean filterName = false;
    private boolean filterRating = false;
    private boolean filterPrice = false;

    public Hotel(String hotelID, String name, String address, String region, String image, String billerID, String fpID, int rating, int priceBefore, int price) {
        this.hotelID = hotelID;
        this.name = name;
        this.address = address;
        this.region = region;
        this.image = image;
        this.billerID = billerID;
        this.fpID = fpID;
        this.rating = rating;
        this.priceBefore = priceBefore;
        this.price = price;
    }

    public Hotel(String name, String address, String region, String image, int rating) {
        this.name = name;
        this.address = address;
        this.region = region;
        this.image = image;
        this.rating = rating;
    }


    public void setHotelID(String hotelID) {
        this.hotelID = hotelID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setBillerID(String billerID) {
        this.billerID = billerID;
    }

    public void setFpID(String fpID) {
        this.fpID = fpID;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setPriceBefore(int priceBefore) {
        this.priceBefore = priceBefore;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getHotelID() {
        return hotelID;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getImage() {
        return image;
    }

    public String getBillerID() {
        return billerID;
    }

    public String getFpID() {
        return fpID;
    }

    public int getRating() {
        return rating;
    }

//    public String getRating() {
//        return Integer.toString(rating);
//    }

    public int getPriceBefore() {
        return priceBefore;
    }

    public int getPrice() {
        return price;
    }


    public boolean isFilterName() {
        return filterName;
    }

    public void setFilterName(boolean filterName) {
        this.filterName = filterName;
    }

    public boolean isFilterRating() {
        return filterRating;
    }

    public void setFilterRating(boolean filterRating) {
        this.filterRating = filterRating;
    }

    public boolean isFilterPrice() {
        return filterPrice;
    }

    public void setFilterPrice(boolean filterPrice) {
        this.filterPrice = filterPrice;
    }

    public String getRegion() {
        return region;
    }

    @Override
    public int compareTo(Hotel another) {
        switch (AppHelper.hotelSort) {
            default:
            case Configuration.SORT_LOWEST_PRICE: {
                int compare = another.getPrice();
                return this.price - compare;
            }
            case Configuration.SORT_HIGHEST_PRICE: {
                int compare = another.getPrice();
                return compare - this.price;
            }
            case Configuration.SORT_RATING_5_TO_1: {
                int compare = another.getRating();
                return compare - this.rating;
            }
            case Configuration.SORT_RATING_1_TO_5: {
                int compare = another.getRating();
                return this.rating - compare;
            }
            case Configuration.SORT_HIGHEST_DISCOUNT: {
                int compare = another.getPriceBefore() - another.getPrice();
                return compare - (this.priceBefore - this.price);
            }
        }
    }
}
