package com.tiketextra.tiketextra.object;

public class HotelDestination {
    private String key, value, type, category, location;
    private int count;

    public HotelDestination(String key, String value, String type, String category, String location, int count) {
        this.key = key;
        this.value = value;
        this.type = type;
        this.category = category;
        this.location = location;
        this.count = count;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }

    public String getLocation() {
        return location;
    }

    public int getCount() {
        return count;
    }

//    @Override
//    public boolean equals(Object object) {
//        boolean isEqual= false;
//
//        if (object != null && object instanceof HotelDestination)
//        {
//            isEqual = (this.key == ((HotelDestination) object).key);
//        }
//
//        return isEqual;
//    }
//
//    @Override
//    public int hashCode() {
//        return this.key;
//    }
}
