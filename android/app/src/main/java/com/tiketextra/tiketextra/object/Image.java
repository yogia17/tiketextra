package com.tiketextra.tiketextra.object;

import android.support.annotation.NonNull;

public class Image implements Comparable<Image> {
    private String name, path;

    public Image(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    @Override
    public int compareTo(@NonNull Image image) {
        return name.compareTo(image.getName());
    }
}
