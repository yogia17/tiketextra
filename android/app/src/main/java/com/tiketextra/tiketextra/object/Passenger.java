package com.tiketextra.tiketextra.object;

/**
 * Created by kurnia on 25/09/17.
 */

public class Passenger {
    private int passengerID, reservationID, position, adultAssocNumber;
    private String type, title, name, birthDate, IDCardNumber, gender, nationality, passportNumber, passportIssuingCountry, passportExpiryDate, departSeat, returnSeat;
    private boolean printBirthDate;


    public Passenger(int passengerID, int reservationID, String type, String title, String name, String birthDate, String IDCardNumber, int adultAssocNumber, String gender, String nationality, String passportNumber, String passportIssuingCountry, String passportExpiryDate, String departSeat, String returnSeat) {
        this.passengerID = passengerID;
        this.reservationID = reservationID;
        this.adultAssocNumber = adultAssocNumber;
        this.type = type;
        this.title = title;
        this.name = name;
        this.birthDate = birthDate;
        this.IDCardNumber = IDCardNumber;
        this.gender = gender;
        this.nationality = nationality;
        this.passportNumber = passportNumber;
        this.passportIssuingCountry = passportIssuingCountry;
        this.passportExpiryDate = passportExpiryDate;
        this.departSeat = departSeat;
        this.returnSeat = returnSeat;
    }

    public Passenger(int passengerID, String title, String name, String birthDate, String nationality, String passportNumber, String passportIssuingCountry, String passportExpiryDate) {
        this.passengerID = passengerID;
        this.title = title;
        this.name = name;
        this.birthDate = birthDate;
        this.nationality = nationality;
        this.passportNumber = passportNumber;
        this.passportIssuingCountry = passportIssuingCountry;
        this.passportExpiryDate = passportExpiryDate;
    }

    public Passenger(int position, String type) {
        this.position = position;
        this.type = type;
    }

    public Passenger(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getIDCardNumber() {
        return IDCardNumber;
    }

    public void setIDCardNumber(String IDCardNumber) {
        this.IDCardNumber = IDCardNumber;
    }

    public int getAdultAssocNumber() {
        return adultAssocNumber;
    }

    public void setAdultAssocNumber(int adultAssocNumber) {
        this.adultAssocNumber = adultAssocNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportIssuingCountry() {
        return passportIssuingCountry;
    }

    public void setPassportIssuingCountry(String passportIssuingCountry) {
        this.passportIssuingCountry = passportIssuingCountry;
    }

    public String getPassportExpiryDate() {
        return passportExpiryDate;
    }

    public void setPassportExpiryDate(String passportExpiryDate) {
        this.passportExpiryDate = passportExpiryDate;
    }

    public int getPosition() {
        return position;
    }

    public String getType() {
        return type;
    }

    public int getPassengerID() {
        return passengerID;
    }

    public int getReservationID() {
        return reservationID;
    }

    public boolean isPrintBirthDate() {
        return printBirthDate;
    }

    public void setPrintBirthDate(boolean printBirthDate) {
        this.printBirthDate = printBirthDate;
    }

    public String getDepartSeat() {
        return departSeat;
    }

    public String getReturnSeat() {
        return returnSeat;
    }

    public String getDepartSeatFormatted(){
        if(departSeat != null && !departSeat.equals("")) {
            String[] parts = departSeat.split(",");
            if (parts.length == 2) {
                return parts[0] + " / " + parts[1];
            }
        }
        return departSeat;
    }

    public String getReturnSeatFormatted(){
        if(returnSeat != null && !returnSeat.equals("")) {
            String[] parts = returnSeat.split(",");
            if (parts.length == 2) {
                return parts[0] + " / " + parts[1];
            }
        }
        return returnSeat;
    }

    public String getDepartWagon(){
        if(departSeat != null && !departSeat.equals("")) {
            String[] parts = departSeat.split(",");
            if (parts.length == 2) {
                return parts[0];
            }
        }
        return departSeat;
    }

    public String getReturnWagon(){
        if(returnSeat != null && !returnSeat.equals("")) {
            String[] parts = returnSeat.split(",");
            if (parts.length == 2) {
                return parts[0];
            }
        }
        return returnSeat;
    }

    public String getDepartSeatLetter(){
        if(departSeat != null && !departSeat.equals("")) {
            String[] parts = departSeat.split(",");
            if (parts.length == 2) {
                return parts[1];
            }
        }
        return departSeat;
    }

    public String getReturnSeatLetter(){
        if(returnSeat != null && !returnSeat.equals("")) {
            String[] parts = returnSeat.split(",");
            if (parts.length == 2) {
                return parts[1];
            }
        }
        return returnSeat;
    }


}

