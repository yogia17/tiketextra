package com.tiketextra.tiketextra.object;

import java.util.Locale;

/**
 * Created by kurnia on 8/12/17.
 */

public class Promo {
    private String name, image, enTitle, inTitle, enSummary, inSummary, enDescription, inDescription;

    public Promo(String name, String image, String enTitle, String inTitle, String enSummary, String inSummary, String enDescription, String inDescription) {
        this.name = name;
        this.image = image;
        this.enTitle = enTitle;
        this.inTitle = inTitle;
        this.enSummary = enSummary;
        this.inSummary = inSummary;
        this.enDescription = enDescription;
        this.inDescription = inDescription;
    }

    public String getImage() {
        return image;
    }

    public String getTitle(Locale locale){
        return locale.getLanguage().equals("en")?enTitle:inTitle;
    }

    public String getSummary(Locale locale){
        return locale.getLanguage().equals("en")?enSummary:inSummary;
    }

    public String getDescription(Locale locale){
        return locale.getLanguage().equals("en")?enDescription:inDescription;
    }

    public String getName() {
        return name;
    }
}
