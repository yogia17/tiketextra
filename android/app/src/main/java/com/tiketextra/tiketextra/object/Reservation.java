package com.tiketextra.tiketextra.object;

import com.tiketextra.tiketextra.util.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kurnia on 03/10/17.
 */

public class Reservation implements Comparable<Reservation> {
    private int reservationID, bankID, amount, bankDiscount, bankSurcharge, uniqueCode, promoAmount, adultCount, childCount, infantCount, departHiddenTransit, returnHiddenTransit;;
    private String invoice, slug, carrierType, bank, isConfirmed, isPaid, promoCode, paidFrom, paidTimeStamp, confirmedPaidTimeStamp,
    status1, status2, isReturn, fromPort, toPort, departDate, returnDate, departFlight, returnFlight,
    contactTitle, contactName, contactPhone, contactEmail, carrierID1, carrierID2, class1, class2,
    bookingCode1, bookingCode2, timeBooking1, timeBooking2, timeLimit1, timeLimit2;

    private ArrayList<Passenger> passengers;
    private ArrayList<Fare> fares;

    public Reservation(int reservationID, int bankID, int amount, int bankDiscount, int bankSurcharge, int uniqueCode, int promoAmount, int adultCount, int childCount, int infantCount, int departHiddenTransit, int returnHiddenTransit, String invoice, String slug, String carrierType, String bank, String isConfirmed, String isPaid, String promoCode, String paidFrom, String paidTimeStamp, String confirmedPaidTimeStamp, String status1, String status2, String isReturn, String fromPort, String toPort, String departDate, String returnDate, String departFlight, String returnFlight, String contactTitle, String contactName, String contactPhone, String contactEmail, String carrierID1, String carrierID2, String class1, String class2, String bookingCode1, String bookingCode2, String timeBooking1, String timeBooking2, String timeLimit1, String timeLimit2) {
        this.reservationID = reservationID;
        this.bankID = bankID;
        this.amount = amount;
        this.bankDiscount = bankDiscount;
        this.bankSurcharge = bankSurcharge;
        this.uniqueCode = uniqueCode;
        this.promoAmount = promoAmount;
        this.adultCount = adultCount;
        this.childCount = childCount;
        this.infantCount = infantCount;
        this.departHiddenTransit = departHiddenTransit;
        this.returnHiddenTransit = returnHiddenTransit;
        this.invoice = invoice;
        this.slug = slug;
        this.carrierType = carrierType;
        this.bank = bank;
        this.isConfirmed = isConfirmed;
        this.isPaid = isPaid;
        this.promoCode = promoCode;
        this.paidFrom = paidFrom;
        this.paidTimeStamp = paidTimeStamp;
        this.confirmedPaidTimeStamp = confirmedPaidTimeStamp;
        this.status1 = status1;
        this.status2 = status2;
        this.isReturn = isReturn;
        this.fromPort = fromPort;
        this.toPort = toPort;
        this.departDate = departDate;
        this.returnDate = returnDate;
        this.departFlight = departFlight;
        this.returnFlight = returnFlight;
        this.contactTitle = contactTitle;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.contactEmail = contactEmail;
        this.carrierID1 = carrierID1;
        this.carrierID2 = carrierID2;
        this.class1 = class1;
        this.class2 = class2;
        this.bookingCode1 = bookingCode1;
        this.bookingCode2 = bookingCode2;
        this.timeBooking1 = timeBooking1;
        this.timeBooking2 = timeBooking2;
        this.timeLimit1 = timeLimit1;
        this.timeLimit2 = timeLimit2;
    }

    public int getDepartHiddenTransit() {
        return departHiddenTransit;
    }

    public int getReturnHiddenTransit() {
        return returnHiddenTransit;
    }

    public void setPassengers(ArrayList<Passenger> passengers) {
        this.passengers = passengers;
    }

    public void setFares(ArrayList<Fare> fares) {
        this.fares = fares;
    }

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public ArrayList<Passenger> getAdultPassengers() {
        ArrayList<Passenger> ret = new ArrayList<>();
        for(int i=0; i<passengers.size(); i++){
            if(passengers.get(i).getType().equals("adult")){
                ret.add(passengers.get(i));
            }
        }
        return ret;
    }

    public int[] getAdultPassengerID(){
        ArrayList<Passenger> adult = getAdultPassengers();
        int[] ret = new int[adultCount];
        for(int i=0; i<adult.size(); i++){
            ret[i] = adult.get(i).getPassengerID();
        }
        return ret;
    }

    public ArrayList<Fare> getFares() {
        return fares;
    }

    public int getReservationID() {
        return reservationID;
    }

    public int getBankID() {
        return bankID;
    }

    public int getAmount() {
        return amount;
    }

    public int getBankDiscount() {
        return bankDiscount;
    }

    public int getBankSurcharge() {
        return bankSurcharge;
    }

    public int getUniqueCode() {
        return uniqueCode;
    }

    public int getPromoAmount() {
        return promoAmount;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public int getChildCount() {
        return childCount;
    }

    public int getInfantCount() {
        return infantCount;
    }

    public String getInvoice() {
        return invoice;
    }

    public String getSlug() {
        return slug;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public String getBank() {
        return bank;
    }

    public String getIsConfirmed() {
        return isConfirmed;
    }

    public String getIsPaid() {
        return isPaid;
    }

    public String getPaidFrom() {
        return paidFrom;
    }

    public String getPaidTimeStamp() {
        return paidTimeStamp;
    }

    public String getConfirmedPaidTimeStamp() {
        return confirmedPaidTimeStamp;
    }

    public String getStatus1() {
        return status1;
    }

    public String getStatus2() {
        return status2;
    }

    public String getIsReturn() {
        return isReturn;
    }

    public String getFromPort() {
        return fromPort;
    }

    public String getToPort() {
        return toPort;
    }

    public String getDepartDate() {
        return departDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public Flight[] getDepartFlightArray() {
        Gson gson = new Gson();
        return gson.fromJson(departFlight, Flight[].class);
    }

    public Flight[] getReturnFlightArray() {
        Gson gson = new Gson();
        return gson.fromJson(returnFlight, Flight[].class);
    }

    public Train[] getDepartTrainArray() {
        Gson gson = new Gson();
        return gson.fromJson(departFlight, Train[].class);
    }

    public Train[] getReturnTrainArray() {
        Gson gson = new Gson();
        return gson.fromJson(returnFlight, Train[].class);
    }

    public Hotel getHotel() throws JSONException {
        JSONObject jsonObject = new JSONObject(departFlight);
        JSONObject json = jsonObject.getJSONObject("hotel");
        return new Hotel(json.getString("name"), json.getString("address"), json.getString("region"), json.getString("image"), json.getInt("rating"));
    }

    public Room getRoom() throws JSONException {
        JSONObject jsonObject = new JSONObject(departFlight);
        JSONObject json = jsonObject.getJSONObject("room");
        return new Room(json.getString("type"), json.getString("policy"), json.getInt("breakfast")==1?true:false);
    }

    public String getCheckinDate(){
        return departDate;
    }

    public String getCheckoutDate(){
        return returnDate;
    }

    public int getNumberRooms(){
        return adultCount;
    }

    public String getDepartFlight() {
        return departFlight;
    }

    public String getReturnFlight() {
        return returnFlight;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getCarrierID1() {
        return carrierID1;
    }

    public String getCarrierID2() {
        return carrierID2;
    }

    public String getClass1() {
        return class1;
    }

    public String getClass2() {
        return class2;
    }

    public String getBookingCode1() {
        return bookingCode1;
    }

    public String getBookingCode2() {
        return bookingCode2;
    }

    public String getTimeBooking1() {
        return timeBooking1;
    }

    public String getTimeBooking2() {
        return timeBooking2;
    }

    public String getTimeLimit1() {
        return timeLimit1;
    }

    public String getTimeLimit2() {
        return timeLimit2;
    }

    public int getTotalFare(){
        int total = 0;
        for(int i=0; i<fares.size(); i++){
            total += fares.get(i).getTotal();
        }

        return total;
    }

    public int getTotalDiscount(){
        int total = 0;
        for(int i=0; i<fares.size(); i++){
            total += fares.get(i).getTotalDiscount();
        }

        return total;
    }

    public boolean isReturn(){
        return this.getIsReturn().equals("Y");
    }

    public String getTimeLimit(){
        if(this.getIsReturn().equals("Y") && !this.carrierID1.equals(this.carrierID2)){
            long time1 = Utils.dateToLong(this.getTimeLimit1());
            long time2 = Utils.dateToLong(this.getTimeLimit2());
            return (time1 - time2 < 0)?this.getTimeLimit1():this.getTimeLimit2();
        }
        else{
            return this.getTimeLimit1();
        }
    }


    @Override
    public int compareTo(Reservation reservation) {
        long compare = Utils.dateToLong(reservation.getTimeLimit());
        return (int)(Utils.dateToLong(this.getTimeLimit()) - compare);
    }

    public int getTotalDepartPrice(){
        int total = 0;
        for(int i=0; i<fares.size(); i++){
            if(this.carrierID1.equals(fares.get(i).getCarrierID())){
                total += fares.get(i).getTotal();
            }
        }
        return total;
    }

    public int getTotalReturnPrice(){
        int total = 0;
        for(int i=0; i<fares.size(); i++){
            if(this.carrierID2.equals(fares.get(i).getCarrierID())){
                total += fares.get(i).getTotal();
            }
        }
        return total;
    }

    public int getTotalDepartDiscount(){
        int total = 0;
        for(int i=0; i<fares.size(); i++){
            if(this.carrierID1.equals(fares.get(i).getCarrierID())){
                total += fares.get(i).getTotalDiscount();
            }
        }
        return total;
    }

    public int getTotalReturnDiscount(){
        int total = 0;
        for(int i=0; i<fares.size(); i++){
            if(this.carrierID2.equals(fares.get(i).getCarrierID())){
                total += fares.get(i).getTotalDiscount();
            }
        }
        return total;
    }



}
