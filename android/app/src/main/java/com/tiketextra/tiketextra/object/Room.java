package com.tiketextra.tiketextra.object;

import android.support.annotation.NonNull;

public class Room implements Comparable<Room>{
    private String roomID, categoryID, name, image, billerID, internalCode, policy;
    private int occupancy, allotment, priceBefore, price, totalPrice;
    boolean breakfast, bookable;

    public Room(String roomID, String categoryID, String name, String image, String billerID, String internalCode, String policy, int occupancy, int allotment, int priceBefore, int price, int totalPrice, boolean breakfast, boolean bookable) {
        this.roomID = roomID;
        this.categoryID = categoryID;
        this.name = name;
        this.image = image;
        this.billerID = billerID;
        this.internalCode = internalCode;
        this.policy = policy;
        this.occupancy = occupancy;
        this.allotment = allotment;
        this.priceBefore = priceBefore;
        this.price = price;
        this.totalPrice = totalPrice;
        this.breakfast = breakfast;
        this.bookable = bookable;
    }

    public Room(String type, String policy, boolean breakfast) {
        this.name = type;
        this.policy = policy;
        this.breakfast = breakfast;
    }

    public String getRoomID() {
        return roomID;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getBillerID() {
        return billerID;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public String getPolicy() {
        return policy;
    }

    public int getOccupancy() {
        return occupancy;
    }

    public int getAllotment() {
        return allotment;
    }

    public int getPriceBefore() {
        return priceBefore;
    }

    public int getPrice() {
        return price;
    }

    public boolean isBreakfast() {
        return breakfast;
    }

    public boolean isBookable() {
        return bookable;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    @Override
    public int compareTo(@NonNull Room room) {
        return price - room.getPrice();
    }
}
