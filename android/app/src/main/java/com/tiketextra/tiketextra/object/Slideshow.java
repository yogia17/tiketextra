package com.tiketextra.tiketextra.object;

import java.util.Locale;

/**
 * Created by kurnia on 8/12/17.
 */

public class Slideshow {
    private String image, enTitle, inTitle, enDescription, inDescription, kind;

    public Slideshow(String image, String kind, String enTitle, String inTitle, String enDescription, String inDescription) {
        this.image = image;
        this.enTitle = enTitle;
        this.inTitle = inTitle;
        this.enDescription = enDescription;
        this.inDescription = inDescription;
        this.kind = kind;
    }

    public String getKind() {
        return kind;
    }

    public String getEnTitle() {
        return enTitle;
    }

    public String getInTitle() {
        return inTitle;
    }

    public String getEnDescription() {
        return enDescription;
    }

    public String getInDescription() {
        return inDescription;
    }

    public String getImage() {
        return image;
    }

    public String getTitle(Locale locale){
        return locale.getLanguage().equals("en")?enTitle:inTitle;
    }

    public String getDescription(Locale locale){
        return locale.getLanguage().equals("en")?enDescription:inDescription;
    }
}
