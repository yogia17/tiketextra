package com.tiketextra.tiketextra.object;

public class Station {
	private String iataCode, name, city, countryCode, country;
	private int hit;
	private double timezone, latitude, longitude;
	public Station(String iataCode, String name, String city,
                   String countryCode, String country, double latitude, double longitude, double timezone, int hit) {
		this.iataCode = iataCode;
		this.name = name;
		this.city = city;
		this.countryCode = countryCode;
		this.country = country;
		this.timezone = timezone;
		this.latitude = latitude;
		this.longitude = longitude;
		this.hit = hit;
	}

	public String getIataCode() {
		return iataCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public String getCountry() {
		return country;
	}
	public double getTimezone() {
		return timezone;
	}
	public int getHit() {
		return hit;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String getStationName(){
		if(getName()==null || getName().equals("") || getName().equals("null")){
			return getCity() + " Station";
		}
		else {
			return getName();
		}
	}

	public String getStation(){
		if(getName()==null || getName().equals("") || getName().equals("null")){
			return getCity();
		}
		else if(getName().equals(getCity())){
			return getCity();
		}
		else {
			return getName()+", "+getCity();
		}
	}
}