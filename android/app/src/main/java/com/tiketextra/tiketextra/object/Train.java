package com.tiketextra.tiketextra.object;

import com.tiketextra.tiketextra.util.Utils;

public class Train {
	private String carrierCode, trainName, trainNumber, departDatetime, departDate, departTime, departCity, departPort, arriveDatetime, arriveDate, arriveTime, arriveCity, arrivePort;
	private double departTimezone, arriveTimezone;
	
	public Train(String carrierCode, String trainName, String trainNumber, String departDatetime, double departTimezone,
                 String departCity, String departPort, String arriveDatetime, double arriveTimezone,
                 String arriveCity, String arrivePort) {
		this.carrierCode = carrierCode;
		this.trainName = trainName;
		this.trainNumber = trainNumber;
		this.departDatetime = departDatetime;
		this.departDate = departDatetime.substring(0, 10);
		this.departTime = departDatetime.substring(11, 16);
		this.departTimezone = departTimezone;
		this.departCity = departCity;
		this.departPort = departPort;
		this.arriveDatetime = arriveDatetime;
		this.arriveDate = arriveDatetime.substring(0, 10);
		this.arriveTime = arriveDatetime.substring(11, 16);
		this.arriveTimezone = arriveTimezone;
		this.arriveCity = arriveCity;
		this.arrivePort = arrivePort;
	}

	public String getDepartDate() {
		return departDate;
	}
	
	public int getDepartYear(){
		return Integer.parseInt(this.departDatetime.substring(0, 4));
	}
	
	public int getDepartMonth(){
		return Integer.parseInt(this.departDatetime.substring(5, 7)) - 1;
	}
	
	public int getDepartDay(){
		return Integer.parseInt(this.departDatetime.substring(8, 10));
	}

	public void setDepartDate(String departDate) {
		this.departDate = departDate;
	}

	public String getArriveDate() {
		return arriveDate;
	}
	
	public int getArriveYear(){
		return Integer.parseInt(this.arriveDatetime.substring(0, 4));
	}
	
	public int getArriveMonth(){
		return Integer.parseInt(this.arriveDatetime.substring(5, 7)) - 1;
	}
	
	public int getArriveDay(){
		return Integer.parseInt(this.arriveDatetime.substring(8, 10));
	}

	public void setArriveDate(String arriveDate) {
		this.arriveDate = arriveDate;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public double getDepartTimezone() {
		return departTimezone;
	}

	public void setDepartTimezone(int departTimezone) {
		this.departTimezone = departTimezone;
	}

	public double getArriveTimezone() {
		return arriveTimezone;
	}

	public void setArriveTimezone(int arriveTimezone) {
		this.arriveTimezone = arriveTimezone;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public String getTrainName() {
		return trainName;
	}

	public String getTrainNameNumber() {
		return trainName + " " + trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public String getDepartDatetime() {
		return departDatetime;
	}

	public void setDepartDatetime(String departDatetime) {
		this.departDatetime = departDatetime;
	}

	public String getDepartTime() {
		return departTime;
	}

	public void setDepartTime(String departTime) {
		this.departTime = departTime;
	}

	public String getDepartCity() {
		return departCity;
	}

	public void setDepartCity(String departCity) {
		this.departCity = departCity;
	}

	public String getDepartPort() {
		return departPort;
	}

	public void setDepartPort(String departPort) {
		this.departPort = departPort;
	}

	public String getArriveDatetime() {
		return arriveDatetime;
	}

	public void setArriveDatetime(String arriveDatetime) {
		this.arriveDatetime = arriveDatetime;
	}

	public String getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(String arriveTime) {
		this.arriveTime = arriveTime;
	}

	public String getArriveCity() {
		return arriveCity;
	}

	public void setArriveCity(String arriveCity) {
		this.arriveCity = arriveCity;
	}

	public String getArrivePort() {
		return arrivePort;
	}

	public void setArrivePort(String arrivePort) {
		this.arrivePort = arrivePort;
	}


	public String getDurationTime(){
		return Utils.strTimeDiff(this.getDepartDatetime(), this.getArriveDatetime(), this.getDepartTimezone() - this.getArriveTimezone());
	}

	public int getDepartTimeMinute() {
		return Integer.parseInt(getDepartTime().substring(0, 2))*60 + Integer.parseInt(getDepartTime().substring(3, 5));
	}

	public int getArriveTimeMinute() {
		return Integer.parseInt(getArriveTime().substring(0, 2))*60 + Integer.parseInt(getArriveTime().substring(3, 5));
	}
	
}
