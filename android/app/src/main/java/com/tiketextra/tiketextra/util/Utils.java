package com.tiketextra.tiketextra.util;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.tiketextra.tiketextra.R;
import com.tiketextra.tiketextra.object.Airport;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.tiketextra.tiketextra.object.Station;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Minutes;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class Utils {
    private static String sMessage;

    public static void logd(String message) {
        Log.d("tiketextra", message);
    }




    public static void longLog(String data) {
        if (data.length() > 4000) {
            logd(data.substring(0, 4000));
            longLog(data.substring(4000));
        } else
            logd(data);
    }

    public static String mediumMD(String date, Context context){
        return Integer.parseInt(date.substring(8, 10))+" "+ getUcMonthShortName(Integer.parseInt(date.substring(5, 7))-1, context);
    }

    public static String mediumDate(String date, Context context){
        return Integer.parseInt(date.substring(8, 10))+" "+ getMonthShortName(Integer.parseInt(date.substring(5, 7))-1, context)+" "+date.substring(0, 4);
    }

    public static String dayDate(String date){
        return Integer.parseInt(date.substring(8, 10))+"";
    }

    public static String mediumMY(String date, Context context){
        return getUcMonthShortName(Integer.parseInt(date.substring(5, 7))-1, context)+" "+date.substring(0, 4);
    }

    public static String mediumDateTime(String date, Context context){
        return Integer.parseInt(date.substring(8, 10))+" "+ getMonthShortName(Integer.parseInt(date.substring(5, 7))-1, context)+" "+date.substring(0, 4)+" "+date.substring(11, 16);
    }

    public static String longDateTime(String date, Context context){
        return Integer.parseInt(date.substring(8, 10))+" "+ getMonthName(Integer.parseInt(date.substring(5, 7))-1, context)+" "+date.substring(0, 4)+" "+date.substring(11, 16);
    }

    public static String getDateTimeNowInUTCPlus7(){
        Date date = new Date();
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        return formatter.format(date);
    }

    public static String getUcMonthShortName(int index, Context context) {
        String[] months = {
                context.getString(R.string.date_jan).toUpperCase(),
                context.getString(R.string.date_feb).toUpperCase(),
                context.getString(R.string.date_mar).toUpperCase(),
                context.getString(R.string.date_apr).toUpperCase(),
                context.getString(R.string.date_may).toUpperCase(),
                context.getString(R.string.date_jun).toUpperCase(),
                context.getString(R.string.date_jul).toUpperCase(),
                context.getString(R.string.date_aug).toUpperCase(),
                context.getString(R.string.date_sep).toUpperCase(),
                context.getString(R.string.date_oct).toUpperCase(),
                context.getString(R.string.date_nov).toUpperCase(),
                context.getString(R.string.date_dec).toUpperCase()
        };
        return months[index];
    }

    public static String getMonthShortName(int index, Context context) {
        String[] months = {
                context.getString(R.string.date_jan),
                context.getString(R.string.date_feb),
                context.getString(R.string.date_mar),
                context.getString(R.string.date_apr),
                context.getString(R.string.date_may),
                context.getString(R.string.date_jun),
                context.getString(R.string.date_jul),
                context.getString(R.string.date_aug),
                context.getString(R.string.date_sep),
                context.getString(R.string.date_oct),
                context.getString(R.string.date_nov),
                context.getString(R.string.date_dec)
        };
        return months[index];
    }

    public static String getMonthName(int index, Context context) {
        String[] months = {
                context.getString(R.string.date_january),
                context.getString(R.string.date_february),
                context.getString(R.string.date_march),
                context.getString(R.string.date_april),
                context.getString(R.string.date_may1),
                context.getString(R.string.date_june),
                context.getString(R.string.date_july),
                context.getString(R.string.date_august),
                context.getString(R.string.date_september),
                context.getString(R.string.date_october),
                context.getString(R.string.date_november),
                context.getString(R.string.date_december)
        };
        return months[index];
    }

    public static String getDayName(int index, Context context) {
        String[] days = {
                context.getString(R.string.date_sunday),
                context.getString(R.string.date_monday),
                context.getString(R.string.date_tuesday),
                context.getString(R.string.date_wednesday),
                context.getString(R.string.date_thursday),
                context.getString(R.string.date_friday),
                context.getString(R.string.date_saturday)
        };
        return days[index-1];
    }

    public static Date addDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }

    public static Date subtractDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, -days);

        return cal.getTime();
    }

    public static final String strTimeDiff(String dateStart, String dateEnd){
        String ret = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);

            ret = String.format("%02d", (Hours.hoursBetween(dt1, dt2).getHours()))+":"+String.format("%02d",  Minutes.minutesBetween(dt1, dt2).getMinutes() % 60);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static final String strTimeDiff(String dateStart, String dateEnd, double extra) {
        String ret = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);

            DateTime dt1 = new DateTime(d1);
            DateTime dt = new DateTime(d2);
            DateTime dt2 = dt.plus((long) (extra * 3600000));

            ret = Hours.hoursBetween(dt1, dt2).getHours() > 0 ? (String.format("%02d", Hours.hoursBetween(dt1, dt2).getHours())
                    + "h"
                    + (Minutes.minutesBetween(dt1, dt2).getMinutes() % 60 == 0 ? ""
                    : ","
                    + String.format("%02d", Minutes.minutesBetween(dt1, dt2)
                    .getMinutes() % 60) + "m")) : String.format("%02d", Minutes.minutesBetween(dt1, dt2).getMinutes() % 60) + "m";

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static final int timeDiffMinutes(String dateStart, String dateEnd, double extra){
        int ret = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);

            ret = (int)((Hours.hoursBetween(dt1, dt2).getHours() + extra)*60)
                    + (Minutes.minutesBetween(dt1, dt2).getMinutes() % 60);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("time diff", ret+" menit");
        return ret;

    }

    public static long dateToLong(String date){
        long ret = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = f.parse(date);
            ret = d.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static int dateDiff(String dateStart, String dateEnd){
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(Integer.parseInt(dateStart.substring(0, 4)), Integer.parseInt(dateStart.substring(5, 7)) - 1, Integer.parseInt(dateStart.substring(8, 10)));
        end.set(Integer.parseInt(dateEnd.substring(0, 4)), Integer.parseInt(dateEnd.substring(5, 7)) - 1, Integer.parseInt(dateEnd.substring(8, 10)));
        return (int) ((end.getTimeInMillis() - start.getTimeInMillis()) / (1000 * 60 * 60 * 24));
    }

    public static String numberFormat(int number){
        return NumberFormat.getInstance(new Locale("in", "ID")).format(number);
    }

    public static String numberFormatCurrency(int number){
        return "Rp " + NumberFormat.getInstance(new Locale("in", "ID")).format(number);
    }



    public static ArrayList<CalendarDay> getDates(Date date1, Date date2) {
        ArrayList<CalendarDay> dates = new ArrayList<>();

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        cal2.add(Calendar.DATE, -1);

        while (!cal1.after(cal2) && !cal1.equals(cal2)) {
            cal1.add(Calendar.DATE, 1);
            CalendarDay calendarDay = CalendarDay.from(cal1);
            dates.add(calendarDay);
        }
        return dates;
    }

    public static String getMoneyFormat(Number number, boolean withCurrency) {
        if (number == null) {
            number = 0;
        }

        Utils.logd("number " + number.toString());

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);
//        if (hasil.indexOf(",") == -1) {
//            hasil = hasil + ",-";
//        }
        if (withCurrency) {
            hasil = "Rp " + hasil;
        }
        return hasil;
    }

    public static String getFormattedNumber(Number number) {
        if (number == null) {
            number = 0;
        }

        Utils.logd("number " + number.toString());

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);
//        if (hasil.indexOf(",") == -1) {
//            hasil = hasil + ",-";
//        }
        return hasil;
    }

    public static int getDateDifference(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        startDate = cal.getTime();

        cal.setTime(endDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        endDate = cal.getTime();

        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;

        return (int) elapsedDays;

    }

    public static String upperCaseAllFirst(String value) {
        if(!value.equals("")) {

            char[] array = value.toCharArray();
            // Uppercase first letter.
            array[0] = Character.toUpperCase(array[0]);

            // Uppercase all letters that follow a whitespace character.
            for (int i = 1; i < array.length; i++) {
                if (Character.isWhitespace(array[i - 1])) {
                    array[i] = Character.toUpperCase(array[i]);
                }
            }

            // Result.
            return new String(array);
        }
        else{
            return "";
        }
    }

    private static double distance(double x1, double y1, double x2, double y2){
        return Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2));

    }

    public static String getNearestAirport(double currentLatitude, double currentLongitude, ArrayList<Airport> airports){
        double minDistance =Double.MAX_VALUE;
        String iataCode="JOG";
        for(int i=0; i<airports.size(); i++){
            double distance = distance(currentLatitude, currentLongitude, airports.get(i).getLatitude(), airports.get(i).getLongitude());

//            Log.e(i+" "+airports.get(i).getIataCode()+" "+airports.get(i).getLatitude()+", "+airports.get(i).getLongitude(), distance+" "+airports.get(i).getAirport()+" "+airports.get(i).getCity());
            if(distance < minDistance){
                minDistance = distance;
                iataCode = airports.get(i).getIataCode();
            }
        }
        return iataCode;
    }

    public static String getNearestStation(double currentLatitude, double currentLongitude, ArrayList<Station> stations){
        double minDistance =Double.MAX_VALUE;
        String iataCode="YK";
        for(int i=0; i<stations.size(); i++){
            double distance = distance(currentLatitude, currentLongitude, stations.get(i).getLatitude(), stations.get(i).getLongitude());

            if(distance < minDistance){
                minDistance = distance;
                iataCode = stations.get(i).getIataCode();
            }
        }
        return iataCode;
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}
