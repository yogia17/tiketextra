package com.tiketextra.tiketextra.widget;

import android.graphics.drawable.Drawable;

public class MainMenu {

    public Drawable imageDrw;
    public String title;
    public int id;

    public MainMenu(int id, Drawable imageDrw, String title) {
        this.id = id;
        this.imageDrw = imageDrw;
        this.title = title;
    }

}
