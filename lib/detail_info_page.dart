import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:tiketextra/ui/loaders.dart';

class Global {
  static dynamic jsonFeatures;
}

class DetailInfoPage extends StatelessWidget {
  final dynamic jsonFeatures;
  DetailInfoPage({@required this.jsonFeatures});
  @override
  Widget build(BuildContext context) {
    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 70.0),
        Text(
          jsonFeatures['name'],
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          padding: EdgeInsets.all(30.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: topContentText,
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    );
    // final bottomContentText = Text(
    //   lesson.content,
    //   style: TextStyle(fontSize: 16.0),
    // );

    return FutureBuilder<dynamic>(
        future: _getAllAPI(), //_getAPI(),
        builder: (context, snapshot) {
          return (snapshot.hasData)
              ? buildBody(topContent, snapshot.data, context)
              : new Container(child: _buildSplashScreen());
        });
  }

  Future<List<dynamic>> _getAllAPI() async {
    List<dynamic> result = List();

    // home landing
    await http.get(
      'https://kasamedia.com/apps/public/api/info/' +
          jsonFeatures['id'].toString(),
      //'http://tiketextra.com/hta/promo/' + jsonFeatures['id'].toString(),
      headers: {},
    ).then((response) {
      //print(response.body);
      result.add(json.decode(response.body));
    });
    return result;
  }

_buildSplashScreen() {
  return Stack(
    children: <Widget>[
      Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
      ),
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 100.0,
                width: 100.0,
                child: Image.asset('assets/images/logo-hta.png')),
            ColorLoader4()
          ],
        ),
      ),
      Container(
        height: double.infinity,
        width: double.infinity,
      ),
    ],
  );
}

  Scaffold buildBody(
      Stack topContent, List<dynamic> data, BuildContext context) {
    double fourwidth = MediaQuery.of(context).size.width / 4;
    data = data[0]['info'];
    //print(data[0].toString());
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: Color(0xFFeaeef1),
              leading: IconButton(
                icon: Icon(
                  Icons.chevron_left,
                  size: 40.0,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                  //_onWillPop();
                },
              ),
        title: Text('Info',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.only(left: 10.0),
                    height: fourwidth * 2,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        //image: NetworkImage('https://tiketextra.com/' + jsonFeatures['image_url']),
                        image: NetworkImage('' + jsonFeatures['image_url']),
                        fit: BoxFit.fill,
                      ),
                    )),

                //topContent,

                Container(
                  //width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(15.0),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          data[0]['name'].toString(),
                          style: TextStyle(color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.bold ),
                        ),
                        SizedBox(height: 15.0),
                        new Center(
                          child: Html(
                            data: data[0]['desc'].toString(),
                            padding: EdgeInsets.all(8.0),
                            onLinkTap: (url) {
                              print("Opening $url...");
                            },
                            customRender: (node, children) {
                              if (node is dom.Element) {
                                switch (node.localName) {
                                  case "custom_tag": // using this, you can handle custom tags in your HTML
                                    return Column(children: children);
                                }
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
