import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;
//mendukug pemrograman asinkron
import 'dart:io';
//bekerja pada file dan directory
import 'package:path_provider/path_provider.dart';
import 'package:tiketextra/model/airport.dart';
//pubspec.yml

//kelass Dbhelper
class DbHelper {
  static DbHelper _dbHelper;
  static Database _database;
  static const String ID = 'id';
  static const String NAME = 'name';
  static const String TABLE = 'port';

  DbHelper._createObject();

  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper;
  }

  Future<Database> initDb() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "asset_tiketextra4.db");

// Check if the database exists
    var exists = await databaseExists(path);

    if (!exists) {
      // Should happen only the first time you launch your application
      print("Creating new copy from asset");

      // Make sure the parent directory exists
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}

      // Copy from asset

      ByteData data = await rootBundle.load(join("assets", "tiketextra.db"));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await File(path).writeAsBytes(bytes, flush: true);
    } else {
      print("Opening existing database");
    }
// open the database
    var db = await openDatabase(path, readOnly: true);
    return db;
  }


  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database;
  }

  Future<List<Map<String, dynamic>>> select() async {
    Database db = await this.database;
    var mapList = await db.query('port', orderBy: 'iata_code');
    //var mapList = await db.rawQuery("SELECT * FROM port");
    return mapList;
  }

  Future<List<Airport>> getAirportList() async {
    var airportMapList = await select();
    int count = airportMapList.length;
    List<Airport> airportList = List<Airport>();
    for (int i = 0; i < count; i++) {
      airportList.add(Airport.fromMap(airportMapList[i]));
    }
    return airportList;
  }
  //   Future<List<Airport>> getAirports() async {
  //   var dbClient = await db;
  //   List<Map> maps = await dbClient.query(TABLE, columns: [ID, NAME]);
  //   //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
  //   List<Airport> airport = [];
  //   if (maps.length > 0) {
  //     for (int i = 0; i < maps.length; i++) {
  //       airport.add(Airport.fromMap(maps[i]));
  //     }
  //   }
  //   return airport;
  // }
}
