import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tiketextra/util/routes.dart';
import 'util/globals.dart';


void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(
      MaterialApp(
        initialRoute: Constants.ROUTE_HOME_PAGE,
        routes: Routes.routes,
        //theme: ThemeData(fontFamily: 'cac'),
      ),
    );
  });
}
