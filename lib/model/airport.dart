class Airport {
  int _id;
  String _name;
  String _iata_code;

  // konstruktor versi 1
  Airport(this._name, this._iata_code);

  // konstruktor versi 2: konversi dari Map ke Airport
  Airport.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._name = map['name'];
    this._iata_code = map['iata_code'];
  }
  //getter dan setter (mengambil dan mengisi data kedalam object)
  // getter
  int get id => _id;
  String get name => _name;
  String get iata_code => _iata_code;

  // setter  
  set name(String value) {
    _name = value;
  }

  set iata_code(String value) {
    _iata_code = value;
  }

  // konversi dari Airport ke Map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['name'] = name;
    map['iata_code'] = iata_code;
    return map;
  }  

}