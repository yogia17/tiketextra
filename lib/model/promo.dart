class Promo {
  String title;
  String image_url;
  String desc;

  Promo(
      {this.title, this.image_url, this.desc});
}