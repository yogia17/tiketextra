import 'package:tiketextra/model/promo.dart';
import 'package:flutter/material.dart';
import 'package:tiketextra/detail_info_page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:progress_indicators/progress_indicators.dart';

class FlightResult extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new ListPage(title: 'Flight Result'),
      // home: DetailPage(),
    );
  }
}

class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List lessons;

  @override
  void initState() {
    lessons = getPromos();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: Color(0xFF115680),
      leading: IconButton(
        icon: Icon(
          Icons.chevron_left,
          size: 40.0,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pop(context);
          //_onWillPop();
        },
      ),
      title: Text(widget.title,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal)),
    );

    return FutureBuilder<dynamic>(
        future: _getAllAPI(), //_getAPI(),
        builder: (context, snapshot) {
          return (snapshot.hasData)
              ? buildBody(topAppBar, snapshot.data)
              : new Container(child: _buildSplashScreen());
        });
  }

  Container buildBody(AppBar topAppBar, List<dynamic> data) {
    data = data[0]['info'];
    return new Container(
        child: Scaffold(
      backgroundColor: Color(0xFFeaeef1),
      appBar: topAppBar,
      body: Container(
        // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              color: Colors.white,
              margin: new EdgeInsets.symmetric(horizontal: 0.0, vertical: 6.0),
              child: InkWell(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "Lion Air",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                    Icon(Icons.flight, color: Colors.red)
                                  ],
                                ),
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Stack(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 8,
                                        ),
                                        child: Container(
                                          height: 70,
                                          width: 1.0,
                                          color: Colors.grey[100],
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 8, right: 8, top:1),
                                            child: Container(
                                              height: 15.0,
                                              width: 15.0,
                                              decoration: new BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.blue,
                                              ),
                                              child: new Container(
                                                margin: new EdgeInsets.all(3.0),
                                                height: 15.0,
                                                width: 15.0,
                                                decoration: new BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 10),
                                            child: Text(
                                              "",
                                            ),
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 0),
                                            child: Container(
                                              height: 15.0,
                                              width: 15.0,
                                              decoration: new BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.blue,
                                              ),
                                              child: new Container(
                                                margin: new EdgeInsets.all(3.0),
                                                height: 15.0,
                                                width: 15.0,
                                                decoration: new BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 8),
                                          child: Text(
                                            "08:55 JOG",
                                            style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 13,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 8),
                                          child: Text(
                                            "1 jam 5 menit (Langsung)",
                                            style: TextStyle(
                                              color: Colors.grey[600],
                                              fontSize: 13,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            "10:00 CGK",
                                            style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 13,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Text(
                                "Tidak Tersedia Bagasi",
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400),
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 3),
                                child: Text(
                                  "Harga per orang",
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8),
                                child: Text(
                                  "Rp567.600",
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.black,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 20),
                                child: Container(
                                  // width: 200,
                                  height: 30.0,
                                  decoration: BoxDecoration(
                                    gradient: new LinearGradient(
                                        colors: [
                                          Color(0xFFffca00),
                                          Color(0xFFffca00),
                                        ],
                                        begin: Alignment(0.5, -1.0),
                                        end: Alignment(0.5, 1.0)),
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                  child: new Material(
                                    child: MaterialButton(
                                      child: Text(
                                        'Rp567.600',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 13,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute<Null>(builder:
                                                (BuildContext context) {
                                          return new FlightResult();
                                        }));
                                      },
                                      highlightColor:
                                          Colors.yellow.withOpacity(0.5),
                                      splashColor:
                                          Colors.yellow.withOpacity(0.5),
                                    ),
                                    color: Colors.transparent,
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                ),
                              ),
                              Text(
                                "LIHAT DETAIL",
                                style: TextStyle(
                                    color: Color(0xFF13b0f3),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailInfoPage(
                                jsonFeatures: data[index],
                              )));
                },
              ),
            );
          },
        ),
      ),
    ));
  }
}

_buildSplashScreen() {
  return Stack(
    children: <Widget>[
      Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
/*
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                begin: Alignment.centerLeft,
                end: new Alignment(10.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [backgroundColor1, backgroundColor2], // whitish to gray
                tileMode: TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
*/
      ),
      Center(
        child: GlowingProgressIndicator(
            duration: Duration(milliseconds: 400),
            child: Container(
                height: 100.0,
                width: 100.0,
                child: Image.asset('assets/images/logo-hta.png'))),
      ),
      Container(
        height: double.infinity,
        width: double.infinity,
      ),
    ],
  );
}

Future<List<dynamic>> _getAllAPI() async {
  List<dynamic> result = List();

  // home landing
  await http.get(
    'https://kasamedia.com/apps/public/api/info',
    headers: {},
  ).then((response) {
    //print(response.body);
    result.add(json.decode(response.body));
  });
  return result;
}

List getPromos() {
  return [
    Promo(
        title: "Tour Bersama Tiket Extra",
        image_url:
            "https://tiketextra.com/admin/filebox/tour/kotak_foto_tour1.jpg",
        desc:
            "Nikmati layanan check in secara online yang dilakukan oleh admin kami 1 hari sebelum jadwal penerbangan Anda sehingga Anda tidak perlu repot untuk antri check in pada bandara keberangkatan."),
  ];
}
