import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tiketextra/pages/promo.dart';
import 'package:tiketextra/pages/info.dart';
import 'package:tiketextra/pages/testimoni.dart';
import 'package:tiketextra/pages/flight/flight_search.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'dart:async';

import 'package:tiketextra/pages/fab_bottom_app_bar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';

import 'package:tiketextra/ui/loaders.dart';


class Global {
  static dynamic jsonFeatures;
}

class HomeViewPage extends StatefulWidget {
  final dynamic jsonFeatures;
  const HomeViewPage({@required this.jsonFeatures});
  @override
  State createState() => HomeViewPageState(jsonFeatures);
}

class HomeViewPageState extends State<HomeViewPage>
    with TickerProviderStateMixin {
  static const MethodChannel methodChannel2 =
      MethodChannel('com.example.platformchannel');

  Future<void> _openNative() async {
    await methodChannel2.invokeMethod('showNativeView');
  }
  Future<void> _openKereta() async {
    await methodChannel2.invokeMethod('showKeretaView');
  }
  Future<void> _openPesanan() async {
    await methodChannel2.invokeMethod('showPesananView');
  }
  Future<dynamic> _handleMethod(MethodCall call) async {
    switch(call.method) {
      case "message":
        debugPrint(call.arguments);
        return new Future.value("");
        print('tes');
    }
  }
  bool _visible;
  PageController _pageController;

  final dynamic jsonFeatures;
  final keyIsFirstLoaded = 'is_first_loaded';
  @override
  void initState() {
    _visible = false;
    _pageController = new PageController();
  }

  HomeViewPageState(this.jsonFeatures);
  @override
  Widget build(BuildContext context) {
    double fourwidth = MediaQuery.of(context).size.width / 4;
    //print(jsonFeatures);
    return Scaffold(
      body: Stack(
        children: <Widget>[
          new PageView(
            children: [buildBackground(context, fourwidth)],
            onPageChanged: onPageChanged,
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
          ),
          _buildPopUp(fourwidth),
        ],
      ),
      bottomNavigationBar: FABBottomAppBar(
        centerItemText: '',
        color: Colors.grey,
        selectedColor: Colors.red,
        notchedShape: CircularNotchedRectangle(),
        onTabSelected: _onTap,
        items: [
          FABBottomAppBarItem(iconData: Icons.home, text: 'Home'),
          FABBottomAppBarItem(iconData: Icons.card_giftcard, text: 'Promo'),
          FABBottomAppBarItem(iconData: Icons.info, text: 'Info'),
          FABBottomAppBarItem(iconData: Icons.chat, text: 'Testi'),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _buildFab(context),
    );
  }

  _onTap(int index) {
    if (index > 0) {
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        if (index == 1) {
          return new PromoPage();
        } else if (index == 2) {
          return new InfoPage();
        } else if (index == 3) {
          return new TestimoniPage();
        }
      }));
    }
  }

  void onPageChanged(int page) {
    setState(() {});
  }

  _buildPopUp(double fourwidth) {

    return Container(
      padding: const EdgeInsets.only(bottom: 30),
      child: Center(
        child: AnimatedOpacity(
          opacity: _visible ? 0.99 : 0.0,
          duration: Duration(milliseconds: 500),
          child: Container(
            width: fourwidth * 3.5,
            height: fourwidth * 1.8,
            child: Card(
                elevation: 4.0,
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(height: 15.0),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        InkWell(
                          child: Column(
                            children: <Widget>[
                              new Container(
                                height: fourwidth * 0.5,
                                width: fourwidth * 0.65,
                                decoration: new BoxDecoration(
                                  //color: Colors.grey[200],
                                  shape: BoxShape.rectangle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/pesawat.png')),
                                ),
                              ),
                              SizedBox(height: 5.0),
                              new Text('Pesawat',
                                  style: TextStyle(fontSize: 12.0)),
                            ],
                          ),
                          onTap: _openNative,
                        ),
                        InkWell(
                          child: Column(
                            children: <Widget>[
                              new Container(
                                height: fourwidth * 0.5,
                                width: fourwidth * 0.65,
                                decoration: new BoxDecoration(
                                  //color: Colors.grey[200],
                                  shape: BoxShape.rectangle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/kereta.png')),
                                ),
                              ),
                              SizedBox(height: 5.0),
                              new Text('Kereta',
                                  style: TextStyle(fontSize: 12.0)),
                            ],
                          ),
                          onTap: _openKereta,
                          //onTap: () {
                          //   _launchURL('https://www.tiketextra.com/train');
                          // },
                        ),
                        InkWell(
                          child: Column(
                            children: <Widget>[
                              new Container(
                                height: fourwidth * 0.5,
                                width: fourwidth * 0.65,
                                decoration: new BoxDecoration(
                                  //color: Colors.grey[200],
                                  shape: BoxShape.rectangle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/hotel.png')),
                                ),
                              ),
                              SizedBox(height: 5.0),
                              new Text('Hotel',
                                  style: TextStyle(fontSize: 12.0)),
                            ],
                          ),
                          onTap: () {
                            _launchURL('https://www.tiketextra.com/hotel');
                          },
                        ),
                       InkWell(
                          child: Column(
                            children: <Widget>[
                              new Container(
                                height: fourwidth * 0.5,
                                width: fourwidth * 0.65,
                                decoration: new BoxDecoration(
                                  //color: Colors.grey[200],
                                  shape: BoxShape.rectangle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/pesanan.png')),
                                ),
                              ),
                              SizedBox(height: 5.0),
                              new Text('Pesanan',
                                  style: TextStyle(fontSize: 12.0)),
                            ],
                          ),
                          onTap: _openPesanan,
                          //onTap: () {
                          //   _launchURL('https://www.tiketextra.com/train');
                          // },
                        ),
                        ],
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  Widget _buildFab(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
_buildSplashScreen();
        Future<void> _openNative() async {
          await methodChannel2.invokeMethod('showNativeView');
        }
       //_openNative();
        setState(() {
          _visible = !_visible;
        });
      },
      tooltip: 'Increment',
      child: Container(
        height: MediaQuery.of(context).size.width * 0.25 * 0.6,
        width: MediaQuery.of(context).size.width * 0.25 * 0.6,
        decoration: new BoxDecoration(
          //color: Colors.grey[200],
          shape: BoxShape.rectangle,
          image: new DecorationImage(
              fit: BoxFit.cover, image: AssetImage('assets/images/book.png')),
        ),
      ),
      elevation: 2.0,
    );
  }

  Stack buildBackground(BuildContext context, double fourwidth) {
    return Stack(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.green,
              child: _buildSliderWidgets(jsonFeatures[0]),
            ),
          ],
        ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            _socialRow(fourwidth),
            SizedBox(height: 50),
          ],
        ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 50.0, right: 50.0, top: 25.0),
              alignment: Alignment.center,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new FlatButton(
                      padding: const EdgeInsets.symmetric(
                          vertical: 12.0, horizontal: 15.0),
                      onPressed: () => {},
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Container(
                            height: 120.0,
                            width: 120.0,
                            child: Image.asset(
                              'assets/images/logo-hta.png',
                              //color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ],
    );
  }

  showDialogIfFirstLoaded(BuildContext context, dynamic json) async {
    json = json['popup'];
    //print(json);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstLoaded = prefs.getBool(keyIsFirstLoaded);
    //print(isFirstLoaded);
    if (isFirstLoaded == null) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          String backgroundColor = "0xFF" +
              json[0]['image_url'].toString().replaceAll('#', '').split("|")[0];
          String textColor = "0xFF" +
              json[0]['image_url'].toString().replaceAll('#', '').split("|")[1];
          return Theme(
            data: Theme.of(context).copyWith(
              dialogBackgroundColor: Color(int.parse(backgroundColor)),
            ),
            child: AlertDialog(
              title: Html(
                defaultTextStyle: TextStyle(color: Color(int.parse(textColor))),
                data: json[0]['name'].toString(),
                padding: EdgeInsets.all(8.0),
                onLinkTap: (url) {
                  print("Opening $url...");
                },
                customRender: (node, children) {
                  if (node is dom.Element) {
                    switch (node.localName) {
                      case "custom_tag": // using this, you can handle custom tags in your HTML
                        return Column(children: children);
                    }
                  }
                },
              ),

              //new Text(json[0]['name'].toString(), style: TextStyle(color: Colors.white),),
              content: SingleChildScrollView(
                  child: Html(
                defaultTextStyle: TextStyle(color: Color(int.parse(textColor))),
                data: json[0]['desc'].toString(),
                padding: EdgeInsets.all(8.0),
                onLinkTap: (url) {
                  print("Opening $url...");
                },
                customRender: (node, children) {
                  if (node is dom.Element) {
                    switch (node.localName) {
                      case "custom_tag": // using this, you can handle custom tags in your HTML
                        return Column(children: children);
                    }
                  }
                },
              )),

              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("OK",
                      style: TextStyle(color: Color(int.parse(textColor)))),
                  onPressed: () {
                    // Close the dialog
                    Navigator.of(context).pop();
                    prefs.setBool(keyIsFirstLoaded, false);
                  },
                ),
              ],
            ),
          );
        },
      );
    }
  }

  _launchURL(String type) async {
    if (type == 'whatsapp') {
      var listWA = ['6282244649999', '6282245829999'];
      var randomWA = (listWA.toList()..shuffle()).first;
      var url = "http://wa.me/" + randomWA;
      await canLaunch(url)
          ? launch(url)
          : print(
              "open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");
    } else if (type == 'telegram') {
      var url = "http://t.me/Tiketextra2";
      await canLaunch(url)
          ? launch(url)
          : print(
              "open app link or do a snackbar with notification that there is no installed");
    } else if (type == 'line') {
      var url = "http://line.me/ti/p/~tiketextra";
      await canLaunch(url)
          ? launch(url)
          : print(
              "open app link or do a snackbar with notification that there is no installed");
    } else if (type == 'facebook') {
      var url = "https://m.me/tiketextra";
      await canLaunch(url)
          ? launch(url)
          : print(
              "open app link or do a snackbar with notification that there is no installed");
    } else {
      await canLaunch(type)
          ? launch(type)
          : print(
              "open app link or do a snackbar with notification that there is no installed");
    }
  }

  Row _socialRow(double fourwidth) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        InkWell(
          child: new Container(
            height: fourwidth * 0.6,
            width: fourwidth * 0.6,
            decoration: new BoxDecoration(
              //color: Colors.grey[200],
              shape: BoxShape.rectangle,
              image: new DecorationImage(
                  fit: BoxFit.cover, image: AssetImage('assets/images/wa.png')),
            ),
          ),
          onTap: () {
            print("tapped on container");
            _launchURL('whatsapp');
          },
        ),
        InkWell(
            child: new Container(
              height: fourwidth * 0.6,
              width: fourwidth * 0.6,
              decoration: new BoxDecoration(
                //color: Colors.grey[200],
                shape: BoxShape.rectangle,
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/telegram.png')),
              ),
            ),
            onTap: () {
              _launchURL('telegram');
            }),
        InkWell(
            child: new Container(
              height: fourwidth * 0.6,
              width: fourwidth * 0.6,
              decoration: new BoxDecoration(
                //color: Colors.grey[200],
                shape: BoxShape.rectangle,
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/line.png')),
              ),
            ),
            onTap: () {
              _launchURL('line');
            }),
        InkWell(
            child: new Container(
              height: fourwidth * 0.6,
              width: fourwidth * 0.6,
              decoration: new BoxDecoration(
                //color: Colors.grey[200],
                shape: BoxShape.rectangle,
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/fb-msg.png')),
              ),
            ),
            onTap: () {
              _launchURL('facebook');
            }),
      ],
    );
  }

_buildSplashScreen() {
  return Stack(
    children: <Widget>[
      Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
      ),
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 100.0,
                width: 100.0,
                child: Image.asset('assets/images/logo-hta.png')),
            ColorLoader4()
          ],
        ),
      ),
      Container(
        height: double.infinity,
        width: double.infinity,
      ),
    ],
  );
}

  _buildSliderWidgets(dynamic json) {
    json = json['slides'];
    List<String> imageSlider = List();
    for (int i = 0; i < json.length; i++) {
      imageSlider.add(
          //'https://tiketextra.com/' + json[i]["image_url"].toString() + "|||");
          '' + json[i]["image_url"].toString() + "|||");
    }
    // List<String> imageSlider = List();
    // imageSlider.add("https://kasamedia.com/tiketextra/plane.jpg" +
    //     "|" +
    //     "" +
    //     "|" +
    //     "" +
    //     "|" +
    //     "");
    // imageSlider.add("https://kasamedia.com/tiketextra/train.jpg" +
    //     "|" +
    //     "" +
    //     "|" +
    //     "" +
    //     "|" +
    //     "");
    // imageSlider.add("https://kasamedia.com/tiketextra/pillows.jpg" +
    //     "|" +
    //     "" +
    //     "|" +
    //     "" +
    //     "|" +
    //     "");

    return Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: <Widget>[
            Container(
              child: Image.network(
                imageSlider[0].split('|')[0],
                fit: BoxFit.cover,
              ),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
            ),
          ],
        ));
  }
}
