import 'package:tiketextra/model/promo.dart';
import 'package:flutter/material.dart';
import 'package:tiketextra/detail_info_page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:tiketextra/ui/loaders.dart';

class InfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListPage(title: 'Info');
  }
}

class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List lessons;

  @override
  void initState() {
    lessons = getPromos();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final topAppBar = AppBar(
      leading: IconButton(
        icon: Icon(
          Icons.chevron_left,
          size: 40.0,
          color: Colors.black,
        ),
        onPressed: () {
          Navigator.pop(context);
          //_onWillPop();
        },
      ),
      elevation: 0.1,
      backgroundColor: Color(0xFFeaeef1),
      title: Text(widget.title,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
    );

    return FutureBuilder<dynamic>(
        future: _getAllAPI(), //_getAPI(),
        builder: (context, snapshot) {
          return (snapshot.hasData)
              ? buildBody(topAppBar, snapshot.data)
              : new Container(child: _buildSplashScreen());
        });
  }

  Container buildBody(AppBar topAppBar, List<dynamic> data) {
    data = data[0]['info'];
    return new Container(
        child: Scaffold(
      backgroundColor: Color(0xFFeaeef1),
      appBar: topAppBar,
      body: Container(
        // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) {
            String imageUrl = data[index]["image_url"].toString();
            String title = data[index]["name"].toString();
            double fourwidth = MediaQuery.of(context).size.width / 4;
            return Card(
              elevation: 8.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: InkWell(
                child: Column(
                  children: <Widget>[
                    new Container(
                      height: fourwidth * 2,
                      width: fourwidth * 4,
                      decoration: new BoxDecoration(
                        //color: Colors.grey[200],
                        shape: BoxShape.rectangle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage('' + imageUrl)),
                        //fit: BoxFit.fill, image: NetworkImage('https://tiketextra.com/'+image_url)),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(color: Colors.white),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        title: Text(
                          title,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        trailing: Icon(Icons.keyboard_arrow_right,
                            color: Colors.black, size: 30.0),
                      ),
                    ),
                  ],
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailInfoPage(
                                jsonFeatures: data[index],
                              )));
                },
              ),
            );
          },
        ),
      ),
    ));
  }
}


_buildSplashScreen() {
  return Stack(
    children: <Widget>[
      Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
      ),
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 100.0,
                width: 100.0,
                child: Image.asset('assets/images/logo-hta.png')),
            ColorLoader4()
          ],
        ),
      ),
      Container(
        height: double.infinity,
        width: double.infinity,
      ),
    ],
  );
}

Future<List<dynamic>> _getAllAPI() async {
  List<dynamic> result = List();

  // home landing
  await http.get(
    'https://kasamedia.com/apps/public/api/info',
    headers: {},
  ).then((response) {
    //print(response.body);
    result.add(json.decode(response.body));
  });
  return result;
}

List getPromos() {
  return [
    Promo(
        title: "Tour Bersama Tiket Extra",
        image_url:
            "https://tiketextra.com/admin/filebox/tour/kotak_foto_tour1.jpg",
        desc:
            "Nikmati layanan check in secara online yang dilakukan oleh admin kami 1 hari sebelum jadwal penerbangan Anda sehingga Anda tidak perlu repot untuk antri check in pada bandara keberangkatan."),
  ];
}
