import 'package:flutter/material.dart';
import 'package:tiketextra/pages/home.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:tiketextra/ui/loaders.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(home: buildNavbarAnimated(context));
  }
}

buildNavbarAnimated(BuildContext context) {
  double fourwidth = MediaQuery.of(context).size.width / 4;
  return new Scaffold(
    body: FutureBuilder<dynamic>(
        future: _getAllAPI(), //_getAPI(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Container(child: _buildSplashScreen());
            default:
              if (snapshot.hasError)
                return new Container(child: _buildSplashScreen());
              //return Center(child: new Text('Error: ${snapshot.error}', style: TextStyle(fontSize: 15),));
              else
                return HomeViewPage(
                  jsonFeatures: snapshot.data,
                );
            //return buildNavbarAnimated(context, snapshot.data);
          }
        }),
  );
}

Future<List<dynamic>> _getAllAPI() async {
  List<dynamic> result = List();
  final response = await http.get('https://kasamedia.com/apps/public/api/home');

  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    result.add(json.decode(response.body));
    final responsepopup =
        await http.get('https://kasamedia.com/apps/public/api/popup');
    if (responsepopup.statusCode == 200) {
      result.add(json.decode(responsepopup.body));
    }
    return result;
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
  // home landing
  // await http.get(
  //   'http://tiketextra.com/hta/slide',
  //   headers: {},
  // ).then((response) {
  //   //print(response.body);
  //   result.add(json.decode(response.body));
  // });
  // return result;
}

_buildSplashScreen() {
  return Stack(
    children: <Widget>[
      Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
      ),
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 100.0,
                width: 100.0,
                child: Image.asset('assets/images/logo-hta.png')),
            ColorLoader4()
          ],
        ),
      ),
      Container(
        height: double.infinity,
        width: double.infinity,
      ),
    ],
  );
}
