import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:tiketextra/ui/loaders.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;

class TestimoniPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListPage(title: 'Testimoni');
  }
}

class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List lessons;

  @override
  void initState() {
    print(lessons);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double fourwidth = MediaQuery.of(context).size.width / 4;

    _launchURL(String url) async {
      await canLaunch(url) ? launch(url) : print("");
    }

    final topAppBar = AppBar(
      leading: IconButton(
        icon: Icon(
          Icons.chevron_left,
          size: 40.0,
          color: Colors.black,
        ),
        onPressed: () {
          Navigator.pop(context);
          //_onWillPop();
        },
      ),
      elevation: 0.1,
      backgroundColor: Color(0xFFeaeef1),
      title: Text(widget.title,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
    );

    return FutureBuilder<dynamic>(
        future: _getAllAPI(), //_getAPI(),
        builder: (context, snapshot) {
          return (snapshot.hasData)
              ? new Container(
                  child: Scaffold(
                  backgroundColor: Color(0xFFeaeef1),
                  appBar: topAppBar,
                  body:
                      buildBody(fourwidth, _launchURL, context, snapshot.data),
                ))
              : new Container(child: _buildSplashScreen());
        });
  }

  Container buildBody(double fourwidth, Future<dynamic> _launchURL(String url),
      BuildContext context, List<dynamic> data) {
    List<dynamic> youtube = data;
    //print(data);
    data = data[0]['testimonial'];
    //print(data);
    youtube = youtube[1]['youtube'];
    print(youtube[0]);
    return Container(
        // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: data.length,
            itemBuilder: (BuildContext context, int index) {
              String image_url = data[index]["image_url"].toString();
              String name = data[index]["name"].toString();
              String body = data[index]["desc"]
                  .toString()
                  .replaceAll('<p>', '')
                  .replaceAll('</p>', '');
              double fourwidth = MediaQuery.of(context).size.width / 4;
              //print(image_url);
              if (index == 0) {
                return Column(
                  children: <Widget>[
                    Card(
                      elevation: 8.0,
                      margin: new EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 6.0),
                      child: InkWell(
                          child: Column(
                            children: <Widget>[
                              new Container(
                                height: fourwidth * 1.5,
                                width: fourwidth * 4,
                                decoration: new BoxDecoration(
                                  //color: Colors.grey[200],
                                  shape: BoxShape.rectangle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      //image: NetworkImage('https://i.ytimg.com/vi/-BluzlCoFHQ/hqdefault.jpg')),
                                      image: NetworkImage(
                                          youtube[0]['image_url'])),
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(color: Colors.white),
                                child: ListTile(
                                  contentPadding:
                                      EdgeInsets.only(left: 20.0, right: 0),
                                  leading: Container(
                                    child: Icon(Icons.play_circle_filled,
                                        color: Colors.red, size: 40.0),
                                  ),
                                  title: Text(
                                    youtube[0]['name'],
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              )
                            ],
                          ),
                          onTap: () {
                            _launchURL(youtube[0]['desc']);
                          }),
                    ),
                    buildListView(context, name, image_url, body),
                  ],
                );
              }
              return buildListView(context, name, image_url, body);
            }));
  }

  Container buildListView(
      BuildContext context, String name, String image_url, String body) {
    //print(data.toString());
    return Container(
      height: 130.0,
      margin: const EdgeInsets.only(top: 16.0, bottom: 8.0),
      child: new FlatButton(
        onPressed: () => {},
        child: new Stack(
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.only(left: 0.0, right: 0.0),
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: new BorderRadius.circular(8.0),
                boxShadow: <BoxShadow>[
                  new BoxShadow(
                      color: Colors.black,
                      blurRadius: 3.0,
                      offset: new Offset(0.0, 2.0))
                ],
              ),
              child: new Container(
                margin: const EdgeInsets.only(top: 0.0, left: 10.0),
                constraints: new BoxConstraints.expand(),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      width: 90,
                      child: Column(
                        children: <Widget>[
                          new Container(
                            alignment: new FractionalOffset(0.0, 0.0),
                            margin: const EdgeInsets.only(top: 20.0),
                            child: new Hero(
                              tag: 'planet-icon-1',
                              child: Container(
                                width: 80.0,
                                height: 80.0,
                                decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                        fit: BoxFit.fill,
                                        image: new NetworkImage(image_url))),
                                // image: new NetworkImage('https://tiketextra.com/' +image_url))),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(left: 0, top: 10),
                      //padding: const EdgeInsets.all(16.0),
                      //height: MediaQuery.of(context).size.width * 0.5,
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: new Column(
                        children: <Widget>[
                          new Text(
                            name,
                            //overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Html(
                                  data: body,
                                  padding: EdgeInsets.all(0.0),
                                  onLinkTap: (url) {
                                    print("Opening $url...");
                                  },
                                  customRender: (node, children) {
                                    if (node is dom.Element) {
                                      switch (node.localName) {
                                        case "custom_tag": // using this, you can handle custom tags in your HTML
                                          return Column(children: children);
                                      }
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

_buildSplashScreen() {
  return Stack(
    children: <Widget>[
      Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
      ),
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 100.0,
                width: 100.0,
                child: Image.asset('assets/images/logo-hta.png')),
            ColorLoader4()
          ],
        ),
      ),
      Container(
        height: double.infinity,
        width: double.infinity,
      ),
    ],
  );
}

Future<List<dynamic>> _getAllAPI() async {
  List<dynamic> result = List();

  // testimoni
  await http.get(
    'https://kasamedia.com/apps/public/api/testi',
    headers: {},
  ).then((response) {
    //print(response.body);
    result.add(json.decode(response.body));
  });
  // youtube
  await http.get(
    'https://kasamedia.com/apps/public/api/youtube',
    headers: {},
  ).then((response) {
    //print(response.body);
    result.add(json.decode(response.body));
  });
  return result;
}
