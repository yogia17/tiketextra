import 'package:flutter/material.dart';
import 'package:tiketextra/model/airport.dart';

class EntryForm extends StatefulWidget {
  final Airport airport;

  EntryForm(this.airport);

  @override
  EntryFormState createState() => EntryFormState(this.airport);
}
//class controller
class EntryFormState extends State<EntryForm> {
  Airport airport;

  EntryFormState(this.airport);

  TextEditingController nameController = TextEditingController();
  TextEditingController iata_codeController = TextEditingController();  

  @override
  Widget build(BuildContext context) {
    //kondisi
    if (airport != null) {
      nameController.text = airport.name;
      iata_codeController.text = airport.iata_code;
    }
    //rubah
    return Scaffold(
      appBar: AppBar(
        title: airport == null ? Text('Tambah') : Text('Rubah'),
        leading: Icon(Icons.keyboard_arrow_left),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 15.0, left:10.0, right:10.0),
        child: ListView(
          children: <Widget> [
            // nama
            Padding (
              padding: EdgeInsets.only(top:20.0, bottom:20.0),
              child: TextField(
                controller: nameController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Nama Lengkap',             
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                onChanged: (value) {                  
                  //                                                    
                },
              ),
            ),

            // telepon
            Padding (
              padding: EdgeInsets.only(top:20.0, bottom:20.0),
              child: TextField(
                controller: iata_codeController,
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                  labelText: 'Telepon',                
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                onChanged: (value) {                  
                  //
                },
              ),
            ),

            // tombol button
            Padding (
              padding: EdgeInsets.only(top:20.0, bottom:20.0),
              child: Row(
                children: <Widget> [
                  // tombol simpan
                  Expanded(
                    child: RaisedButton(
                      color: Theme.of(context).primaryColorDark,
                      textColor: Theme.of(context).primaryColorLight,
                      child: Text(
                        'Save',
                        textScaleFactor: 1.5,
                      ),
                      onPressed: () {
                        if (airport == null) {
                          // tambah data
                          airport = Airport(nameController.text, iata_codeController.text);
                        } else {
                          // ubah data
                          airport.name = nameController.text;
                          airport.iata_code = iata_codeController.text;
                        }
                        // kembali ke layar sebelumnya dengan membawa objek airport
                        Navigator.pop(context, airport);
                      },
                    ),
                  ),
                  Container(width: 5.0,),
                  // tombol batal
                  Expanded(
                    child: RaisedButton(
                      color: Theme.of(context).primaryColorDark,
                      textColor: Theme.of(context).primaryColorLight,
                      child: Text(
                        'Cancel',
                        textScaleFactor: 1.5,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}