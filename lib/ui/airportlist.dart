import 'package:flutter/material.dart';
//letak package folder flutter
import 'package:tiketextra/ui/airport_add.dart';
import 'package:tiketextra/pages/flight/flight_search.dart';
import 'package:tiketextra/model/airport.dart';
import 'package:tiketextra/helpers/dbhelper.dart';
import 'package:sqflite/sqflite.dart';
//untuk memanggil fungsi yg terdapat di daftar pustaka sqflite
import 'dart:async';
//pendukung program asinkron

class AirportList extends StatefulWidget {
  @override
  AirportListState createState() => AirportListState();
}

class AirportListState extends State<AirportList> {
  DbHelper dbHelper = DbHelper();
  int count = 0;
  List<Airport> airportList;
@override
  void initState() {
    super.initState();
    refreshList();
  }
 
  refreshList() {
        final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<Airport>> airportListFuture = dbHelper.getAirportList();
      airportListFuture.then((airportList) {
        setState(() {
          this.airportList = airportList;
          this.count = airportList.length;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (airportList == null) {
      airportList = List<Airport>();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Pilih Bandara'),
      ),
      body: createListView(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: 'Tambah Data',
        onPressed: () async {
          var airport = await navigateToEntryForm(context, null);
          if (airport != null) addAirport(airport);
        },
      ),
    );
  }

  Future<Airport> navigateToEntryForm(
      BuildContext context, Airport airport) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return EntryForm(airport);
    }));
    return result;
  }
  Future<Airport> navigateToFlightSearch(
      BuildContext context, Airport airport) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return FlightSearch(airport);
    }));
    return result;
  }

  ListView createListView() {
    TextStyle textStyle = Theme.of(context).textTheme.subhead;
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          color: Colors.white,
          elevation: 2.0,
          child: ListTile(
            title: Text(
              this.airportList[index].iata_code,
              style: textStyle,
            ),
            subtitle: Text(this.airportList[index].name),
            onTap: () async {
              updateListView();
              Navigator.pop(context,'OK');
              //await navigateToFlightSearch(context, this.airportList[index]);
            },
          ),
        );
      },
    );
  }

  //buat airport
  void addAirport(Airport object) async {
    updateListView();
  }

  //edit airport
  void editAirport(Airport object) async {
    updateListView();
  }

  //delete airport
  void deleteAirport(Airport object) async {
    updateListView();
  }

  //update airport
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<Airport>> airportListFuture = dbHelper.getAirportList();
      airportListFuture.then((airportList) {
        setState(() {
          this.airportList = airportList;
          this.count = airportList.length;
        });
      });
    });
  }
}
