class Constants {

  // API
  static const String BASE_URL = "http://beta.devsandbox.me/";
  static const String ROUTE_HOME_PAGE = "/";
  static const String ROUTE_LOGIN_PAGE = "/login";
  static const String ROUTE_PROMO_PAGE = "/promo";

  static const String ROUTE_FORM_FLIGHT = "/flight";
}

class Globals {

  static int brandID;
  static bool alreadyLogin;

  static String provinceID = "";
  static String cityID = "";
  static String subdistrictID = "";

  static String countryName = "";
  static String provinceName = "";
  static String cityName = "";
  static String subdistrictName = "";
  static String postalCode = "";
  static String village = "";

  static int langSelected = 1;
  static int imageIndex;

  static var timeAddCart;
  static var timeOut;
  static var offset = 60;
  static bool timerIsInactive;
  static bool timerPopupIsAlreadyShowing;
}