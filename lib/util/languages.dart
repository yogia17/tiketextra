// Contoh penggunaan:
// AppText.PAYMENT_METHOD[Lang.INDONESIAN] = "METODE PEMBAYARAN"

class Lang {

  // assoc index
  static const int ENGLISH = 0;
  static const int INDONESIAN = 1;

}

class AppText {

  // general
  static const List<String> POPUP_EXIT = ["Exit from ZB Store","Keluar dari ZB Store"];
  static const List<String> POPUP_TIMEOUT_TITLE = ["Your shopping time is running out","Waktu belanja Anda hampir habis"];
  static const List<String> POPUP_TIMEOUT_MESSAGE = ["Please complete payment or your cart will be cleared","Silahkan selesaikan pembayaran atau keranjang Anda akan dikosongkan"];
/*
  static const List<String> POPUP_TIMEOUT_MESSAGE = ["Your shopping cart will be cleared now. "
                                                        "Do you need an aditional minutes to finish your payment?",
                                                    "Keranjang belanja Anda akan dikosongkan sekarang. "
                                                        "Apakah Anda butuh waktu tambahan untuk menyelesaikan pembayaran?"];
*/
  static const List<String> POPUP_YES = ["Yes","Ya"];
  static const List<String> POPUP_NO = ["No","Tidak"];


  // side menu
  static const List<String> HOME_ORDER = ["My Orders","Pesanan Saya"];
  static const List<String> HOME_FAVE = ["My Favorites","Favorit Saya"];
  static const List<String> HOME_ADDRESS = ["My Address","Alamat Saya"];
  static const List<String> HOME_LANG = ["My Language","Bahasa Saya"];
  static const List<String> HOME_LOGIN = ["Login","Masuk"];
  static const List<String> HOME_LOGOUT = ["Logout","Keluar"];

  static const List<String> HOME_LANG_CHANGE = ["CHANGE LANGUAGE","UBAH BAHASA"];

  // my order
  static const List<String> HOME_MY_ORDER_HEADER = ["My Orders","Pesanan Saya"];
  static const List<String> HOME_MY_ORDER_EMPTY = ["Data unavailable","Tidak ada data"];
  static const List<String> HOME_MY_ORDER_UNPAID = ["Upaid","Belum Bayar"];
  static const List<String> HOME_MY_ORDER_PACKING = ["Packing","Dikemas"];
  static const List<String> HOME_MY_ORDER_SENT = ["Sent","Dikirim"];
  static const List<String> HOME_MY_ORDER_COMPLETED = ["Completed","Selesai"];
  static const List<String> HOME_MY_ORDER_ITEM = ["Item","Barang"];
  static const List<String> HOME_MY_ORDER_DETAILS = ["Order Details","Detail Order"];

  // login page
  static const List<String> SIGN_IN = ["Sign In","Masuk"];
  static const List<String> SIGN_IN_MSG = ["Sign in using your social account","Masuk dengan akun media sosial Anda"];

  // product by kategori
  static const List<String> CATEGORY = ["Category","Kategori"];

  // list product
  static const List<String> ADD_TO_CART = ["ADD TO CART","TAMBAH KE KERANJANG"];
  static const List<String> FAVE_BUTTON = ["Favorite","Favorit"];

  // product detail
  static const List<String> PRODUCT_HEADER = ["PRODUCT DETAILS","DETIL PRODUK"];
  //static const List<String> PRODUCT_CHOSE_SIZE = ["Chose Size","Pilih Ukuran"];
  //static const List<String> PRODUCT_CHOSE_COLOR = ["Chose Color","Pilih Warna"];
  static const List<String> PRODUCT_CHOSE_QTY = ["Quantity","Jumlah"];
  static const List<String> PRODUCT_SIZE = ["Size","Ukuran"];
  static const List<String> PRODUCT_COLOR = ["Color","Warna"];
  static const List<String> PRODUCT_ITEM_QUANTITY = ["Item Quantity","Jumlah Barang"];
  static const List<String> PRODUCT_CUT_QUANTITY =  ["Cut  Quantity","Jumlah Potong"];
  static const List<String> PRODUCT_LOGIN_WARNING = ["Please login first","Silakan login dulu"];
  static const List<String> PRODUCT_CUT_NOTE = ["Cut Note","Catatan Potong"];
  static const List<String> PRODUCT_CUT_FEE = ["Cut Fee","Biaya Potong"];
  static const List<String> PRODUCT_CUT_ITEM = ["Item No.","Barang ke-"];
  static const List<String> PRODUCT_CUT_TO = [" is cut to "," dipotong jadi "];
  static const List<String> PRODUCT_SELECT_SIZE = ["Chose Size","Pilih Ukuran"];
  static const List<String> PRODUCT_SELECT_COLOR = ["Chose Color","Pilih Warna"];
  static const List<String> PRODUCT_SELECT_ITEM_QTY = ["Chose Item Qty","Pilih Jumlah Barang"];
  static const List<String> PRODUCT_SELECT_CUT_QTY = ["Chose Cut Qty","Pilih Jumlah Potong"];
  static const List<String> PRODUCT_DESCRIPTION = ["PRODUCT DESCRIPTION","DESKRIPSI PRODUK"];
  static const List<String> PRODUCT_SIZE_DETAILS = ["SIZE DETAILS","DETAIL UKURAN"];
  static const List<String> PRODUCT_SOLD_OUT = [" sold out"," habis"];
  static const List<String> PRODUCT_COLOR_AVAILABLE = ["Available in ","Tersedia dalam "];
  static const List<String> PRODUCT_COLOR_LABEL = [" color(s)"," warna"];
  static const List<String> PRODUCT_PRE_ORDER = ["Ready on ","Dikirim "];

  // general
  static const List<String> CHANGE = ["CHANGE","UBAH"];
  static const List<String> SAVE = ["SAVE","SIMPAN"];
  static const List<String> SEARCH_RESULT = ["Search result: ","Hasil pencarian: "];

  // cart
  static const List<String> CART_HEADER = ["CART","KERANJANG BELANJA"];
  static const List<String> CART_UPDATE = ["UPDATE CART","UPDATE PESANAN"];
  static const List<String> CART_EMPTY = ["Your cart is empty","Keranjang kosong, silahkan belanja dulu"];
  static const List<String> CART_INVOICE = ["Total Invoice","Total Tagihan"];
  static const List<String> CART_CUT_OPTION = ["Cut options","Pilih potong atau tidak?"];
  static const List<String> CART_CUT_NO = ["Item is not cut","Barang tidak dipotong"];
  static const List<String> CART_CUT_YES = ["Item is cut","Barang dipotong"];

  //checkout
  static const List<String> CHECKOUT_HEADER = ["PAYMENT","PEMBAYARAN"];
  static const List<String> CHECKOUT_ADDRESS = ["SHIPPING ADDRESS","ALAMAT PENGIRIMAN"];
  static const List<String> ADDRESS_LIST = ["MY ADDRESS","ALAMAT SAYA"];
  static const List<String> CHECKOUT_ADDRESS_ADD = ["NEW ADDRESS","ALAMAT BARU"];
  static const List<String> CHECKOUT_ADDRESS_EMPTY = ["No address found","Belum ada alamat"];

  // new/update address


  //checkout
  static const List<String> CHECKOUT_DROPSHIPPER = ["Sent as dropshipper","Kirim sebagai dropshipper"];
  static const List<String> CHECKOUT_SHIPPING = ["Select shipping","Pilih jenis pengiriman"];
  static const List<String> CHECKOUT_VOUCHER = ["Use coupon code","Gunakan voucher/kupon"];

  static const List<String> CHECKOUT_SUMMARY = ["ORDER SUMMARY","RINGKASAN BELANJA"];
  static const List<String> CHECKOUT_SHIPPING_SERVICE = ["Shipping Service","Jasa Pengiriman"];
  static const List<String> CHECKOUT_SHIPPING_FEE = ["Shipping Fee","Biaya Pengiriman"];
  static const List<String> CHECKOUT_CUT_FEE = ["Cut Fee","Biaya Potong"];
  static const List<String> CHECKOUT_ETA = ["ETD","Perkiraan sampai"];
  static const List<String> CHECKOUT_ITEM_QUANTITY = ["Item Qty","Jumlah Barang"];
  static const List<String> CHECKOUT_PRICE = ["Total Price","Total Harga"];
  static const List<String> CHECKOUT_DISCOUNT = ["Discount","Diskon"];
  static const List<String> CHECKOUT_TOTAL = ["Total Invoice","Total Tagihan"];

  static const List<String> CHECKOUT_PAYMENT = ["SELECT PAYMENT METHOD","PILIH METODE PEMBAYARAN"];
  static const List<String> CHECKOUT_PAY_BUTTON = ["PAY NOW","BAYAR"];
  // address

  // payment va
  static const List<String> VA_TITLE = ["Select Virtual Account","Pilih Virtual Account"];
  static const List<String> VA_SUBTITLE = ["Available Virtual Accounts","Virtual Account yang tersedia"];

  // payment direct debit
  static const List<String> DD_TITLE = ["Select Direct Debit","Pilih Direct Debit"];
  static const List<String> DD_SUBTITLE = ["Available Direct Debits"," Direct Debit yang tersedia"];

  // payment Counter
  static const List<String> COUNTER_TITLE = ["Select Counter","Pilih Counter"];
  static const List<String> COUNTER_SUBTITLE = ["Available Counters"," Counter yang tersedia"];

  // payment info
  static const List<String> PAYMENT_INFO_HEADER = ["Payment Info","Info Pembayaran"];
  static const List<String> PAYMENT_INFO_TOTAL = ["TOTAL PAYMENT","TOTAL PEMBAYARAN"];
  static const List<String> PAYMENT_INFO_HOW_TO= ["HOW TO PAY","CARA PEMBAYARAN"];
}