class RemoteConfig {
  static final Map<dynamic, String> config = {
    //"AuthorizationToken":
    //"",
    "BASE_URL": "http://shop.devsandbox.me/",
    "BASE_PRODUCTS_URL": "api/products",
    "BASE_CART_URL": "api/get-cart",
    "REMOVE_CART_URL": "api/remove-cart/",
    "ADD_CART_URL": "api/add-cart/",
    "GOOGLE_LOGIN_URL": "api/social/gg/login",
    "FACEBOOK_LOGIN_URL": "api/social/fb/login",
  };
}