import 'package:flutter/material.dart';
import 'package:tiketextra/util/globals.dart';
import 'package:tiketextra/pages/promo.dart';
import 'package:tiketextra/pages/landing_page.dart';
class Routes {
  static final routes = <String, WidgetBuilder>{
    // core
    Constants.ROUTE_HOME_PAGE: (BuildContext context) => LandingPage(),
    Constants.ROUTE_PROMO_PAGE: (BuildContext context) => PromoPage(),
  };
}
